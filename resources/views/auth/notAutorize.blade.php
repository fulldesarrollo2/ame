<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title> | SSF Login</title>

        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="../css/animate.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">

        <link rel="shortcut icon" href="../imagenes/favicon.ico" />

    </head>

    <body class="gray-bg">


        <div class="middle-box text-center animated fadeInDown">
            <h1>404</h1>
            <h3 class="font-bold">Página no Encontrada</h3>

            <div class="error-desc">
                El recurso solicitado no se encuentra disponible o el usuario con el que intenta acceder no cuenta con los permisos necesarios.<form class="form-inline m-t" role="form">
                <a class="btn btn-primary" href="{{ url('/welcome') }}">
                    <i class="fa fa-sign-out"></i> Salir
                </a>
                </form>
            </div>
        </div>

        <!-- Mainly scripts -->
        <script src="js/jquery-2.1.1.js"></script>
        <script src="js/bootstrap.min.js"></script>

    </body>

</html>






