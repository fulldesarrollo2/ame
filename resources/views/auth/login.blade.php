<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8>
<meta http-equiv=X-UA-Compatible content="IE=edge">
<title>SSF | Login</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name=viewport>
<title> | SFF</title>
<link rel="shortcut icon" href=../img/favicon.ico />
<link rel=stylesheet href=../css/bootstrap.min.css>
<link rel=stylesheet href=../css/font-awesome.min.css>
<link rel=stylesheet href=../css/ionicons.min.css>
<link rel=stylesheet href=../css/styleAdmin.min.css>
</head>
<body class="hold-transition login-page">
<div class=login-box>
<div class=login-logo>
</div>
<div class=login-box-body>
<h3 class=login-box-msg>Bienvenido</h3>
<p class=login-box-msg>Sistema de Simulación Financiera</p>
<p style=font-weight:bold>Iniciar Sesión</p>
<form method=POST action="{{ url('/login') }}">
{{ csrf_field() }}
<div class="form-group{{ $errors->has('usuario_nick') ? ' has-error' : '' }} has-feedback">
<input id=usuario_nick type=text class=form-control name=usuario_nick value="{{old('usuario_nick')}}" placeholder=Usuario>
<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
@if ($errors->has('usuario_nick'))
<span class=help-block>
<strong>{{ $errors->first('usuario_nick') }}</strong>
</span>
@endif
</div>
<div class="form-group{{ $errors->has('usuario_password') ? ' has-error' : '' }} has-feedback">
<input id=usuario_password type=password class=form-control name=usuario_password placeholder=Password>
<span class="glyphicon glyphicon-lock form-control-feedback"></span>
@if ($errors->has('usuario_password'))
<span class=help-block>
<strong>{{ $errors->first('usuario_password') }}</strong>
</span>
@endif
</div>
<div class="row text-right">
<div class=col-xs-12>
<button type=submit class="btn btn-primary btn-block btn-flat">Iniciar Sesión</button>
</div>
</div>
</form>
<br/>
<a href="{{ url('/password/reset') }}">Restaurar Contraseña</a><br>
<br/>
<div class="social-auth-links text-center">
<img style=width:100px;height:auto class=img-preview alt=image src=../img/logos/logoAme.png> &nbsp;&nbsp;
<img style=width:100px;height:auto class=img-preview alt=image src=../img/logos/logoUe.png> &nbsp;&nbsp;
<img style=width:100px;height:auto class=img-preview alt=image src=../img/logos/uim.jpg>
</div>
</div>
</div>
<script src=../plugins/jQuery/jquery-2.2.3.min.js></script>
<script src=../js/bootstrap.min.js></script>
</body>
</html>