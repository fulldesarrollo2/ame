<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8>
<meta http-equiv=X-UA-Compatible content="IE=edge">
<meta name=viewport content="width=device-width, initial-scale=1.0">
<meta name=csrf-token content="{{ csrf_token() }}" />
<title> | SFF</title>
<link rel="shortcut icon" href=../imagenes/favicon.ico />
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name=viewport>
<link rel=stylesheet href=../css/bootstrap.min.css>
<link rel=stylesheet href=../css/font-awesome.min.css>
<link rel=stylesheet href=../css/ionicons.min.css>
<link rel=stylesheet href=../css/styleAdmin.min.css>
<link rel=stylesheet href=../css/skins/_all-skins.min.css>
</head>
<body class="hold-transition skin-blue layout-top-nav">
<div class=wrapper>
<header class=main-header>
<nav class="navbar navbar-static-top">
<div>
<div class=navbar-header>
<a href=/welcome class=navbar-brand><b>SISTEMA SIMULACIÓN FINANCIERA</b></a>
<button type=button class="navbar-toggle collapsed" data-toggle=collapse data-target=#navbar-collapse>
<i class="glyphicon glyphicon-align-justify"></i>
</button>
</div>
<div class=navbar-custom-menu>
<ul class="nav navbar-nav">
<li class="dropdown user user-menu">
<a style=text-align:right>
<img src=../img/icon/user.png class=user-image alt="User Image">
<span>{{ Auth::user()->entidad->entidad_nombre}} - {{ Auth::user()->usuario_nombres }} {{ Auth::user()->usuario_apellidos }}</span>
<br>
{{ Auth::user()->perfilIngreso->perfil_ingreso_nombre }}<br>
<?php
$dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
echo $dias[date('w')] . " " . date('d') . " de " . $meses[date('n') - 1] . " del " . date('Y');
?>
<br>
</a>
</li>
</ul>
</div>
</div>
</nav>
<nav class="navbar navbar-static-top" style=background-color:#005AA7>
<div>
<div class="collapse navbar-collapse pull-left" id=navbar-collapse>
<ul class="nav navbar-nav">
@for ($i = 0; $i < count($lstMenuInterfaz); $i++)
<li class=dropdown>
<a aria-expanded=false role=button href=# class=dropdown-toggle data-toggle=dropdown> {{$lstMenuInterfaz[$i]['interfaz_descripcion']}} <span class=caret></span></a>
<ul role=menu class=dropdown-menu>
@for ($j = 0; $j < count($lstMenuInterfaz[$i]['0']); $j++)
<li>
<a href="{{$lstMenuInterfaz[$i]['0'][$j]['interfaz_ruta']}}"><i class="glyphicon glyphicon-record"></i>&nbsp;&nbsp;{{$lstMenuInterfaz[$i]['0'][$j]['interfaz_descripcion']}} </a>
</li>
@endfor

</ul>
</li>
@endfor
<li class="dropdown">
	<a aria-expanded=false role=button href=# class=dropdown-toggle data-toggle=dropdown>Autoevaluacion <span class="caret"></span></a>
	<ul role="menu" class="dropdown-menu">
		<li>
			<a href="{{ route('adminInforServMunicipales.index') }}">Inventario y Costeo</a>
			<a href="{{ route('serviciosBasicosInversiones.index') }}">Inversiones</a>
			<a href="{{ route('variableServAdministrativa.index') }}">Estructura Administrativa</a>
		</li>
	</ul>
</li>

<li class="dropdown user user-menu hidden-md hidden-sm hidden-lg">
<a href="{{ url('/logout')}}" class=dropdown-toggle>
<i class="glyphicon glyphicon-off"></i>
<span>Cerrar Sesión </span>
</a>
</li>
</ul>
</div>
<div class="navbar-custom-menu hidden-xs">
<ul class="nav navbar-nav">
<li class="dropdown user user-menu">
<a href="{{ url('/logout')}}" class=dropdown-toggle>
<i class="glyphicon glyphicon-off"></i>
<span>Cerrar Sesión </span>
</a>
</li>
</ul>
</div>
</div>
</nav>
</header>
<div class=content-wrapper>
<div class=container>
<section class=content-header>
@yield('header')
@if(Session::has('message'))
<div class="alert alert-success">
<button type=button class=close data-dismiss=alert>&times;</button>
<label>{{Session::get('message')}}</label>
</div>
@endif
@if(Session::has('messageError'))
<div class="alert alert-danger">
<button type=button class=close data-dismiss=alert>&times;</button>
<label>{{Session::get('messageError')}}</label>
</div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger">
<button type="button" class="close" data-dismiss="alert">&times;</button>
@foreach ($errors->all() as $error)
<label>{{ $error }}</label>
@endforeach
</div>
@endif
<div id=mensajes name=mensajes class=hide>
<label id=textoMensaje name=textoMensaje></label>
</div>
</section>
<section class="content">
@yield('content')
</section>
</div>
</div>
<div id="gnrError" class="modal fade">
<div class="modal-dialog" role=document>
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Mensaje del Sistema</h4>
</div>
<div class=modal-body>
<p id=gnrMensaje></p>
</div>
<div class=modal-footer>
<a class="btn btn-primary" href="{{ url('/logout') }}">
<i class="fa fa-sign-out"></i> Salir
</a>
</div>
</div>
</div>
</div>
<footer class=main-footer>
<div class=container>
</div>
</footer>
</div>
<script src="../js/ValidadorDatos.js"></script>
<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/app.min.js"></script>
<script src="../js/jquery.blockUI.js"></script>
@yield('script')
<script>function toTop(){window.scrollTo(0,0)}$("#cancelar").click(function(){location.reload()});</script>
</body>
</html>       
