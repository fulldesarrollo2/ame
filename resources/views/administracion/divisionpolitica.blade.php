@extends('layouts.app')
@section('css')
@endsection
@section('header')
<h1>
DIVISIÓN POLÍTICA
</h1>
<ol class="breadcrumb">
<li><a><i class="glyphicon glyphicon-th-large"></i>Administración</a></li>
<li class="active">División Política</li>
</ol>
<br/>
@endsection
@section('content')
<div class="box box-default">
<form class="form-horizontal" role="form" method="POST" id="guardaDivisionPolitica" action="/guardaDivisionPolitica">
<div class="box-body">
<input type="hidden" name="division_politica_id" id="division_politica_id">
<input type="hidden" name="division_politica_tipo" id="division_politica_tipo">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="col-md-6">
<div id="div_dpSeleccionada" name="lbl_dpSeleccionada" class="form-group">
<label>Seleccione</label>
<div style="height:200px;overflow-x:auto;border:1px solid #d2d6de">
<div id="arbolDivisionPolitica" class="treeview"></div>
</div>
<br/>
<div class="form-group">
<label class="col-md-12">División Política Seleccionada</label>
<div class="col-md-10">
<input type="text" disabled="true" class="form-control" class="division_politica_seleccionada" id="division_politica_seleccionada">
<label class="hide" id="lbl_perfil_ingreso_nombre" name="error"></label>
</div>
<div class="col-md-2">
<button class="btn btn-primary" id="editar_division_politica" name="editar_division_politica" type="button" onclick="llenaractualizacion(this)"><i class="glyphicon glyphicon-edit"></i></button>
</div>
<label class="hide col-md-12" id="lbl_dpSeleccionada" name="lbl_dpSeleccionada"></label>
</div>
</div>
</div>
<div class="col-md-6">
<div class="box box-default">
<div class="box-header with-border">
<h3 class="box-title">Ingreso / Actualización de Datos</h3>
</div>
@if($permiso) 
<div class="box-body">
<div class="form-group" id="div_division_politica_capital">
<label class="col-sm-2 control-label">Capital de provincia</label>
<div class="col-sm-10 radio">
<label>
<input type="radio" name="division_politica_capital" class="radio-division" value="S" />Si</label>
<label>
<input type="radio" name="division_politica_capital" class="radio-division" value="N" />No</label>
<label class="hide" id="lbl_division_politica_capital" name="error"></label>
</div>
</div>
<div class="form-group" id="div_division_politica_codigo">
<label class="col-sm-2 control-label">Código</label>
<div class="col-sm-10">
<input type="text" name="division_politica_codigo" id="division_politica_codigo" class="form-control">
<label class="hide" id="lbl_division_politica_codigo" name="error"></label>
</div>
</div>
<div class="form-group" id="div_division_politica_nombre">
<label class="col-sm-2 control-label">Nombre</label>
<div class="col-sm-10">
<input type="text" id="division_politica_nombre" name="division_politica_nombre" class="form-control" style="text-transform:uppercase">
<label class="hide" id="lbl_division_politica_nombre" name="error"></label>
</div>
</div>
<div class="form-group hide" id="div_division_politica_estado_id">
<label class="col-sm-2 control-label">Estado</label>
<div class="col-sm-10 radio">
<label>
<input type="radio" name="estado_id" class="radio-division" value="1" />Activo</label>
<label>
<input type="radio" name="estado_id" class="radio-division" value="2" />Inactivo</label>
<label class="hide" id="lbl_division_politica_estado_id" name="error"></label>
</div>
</div>
<div class="box-footer text-right">
<button type="button" id="cancelar" class="btn btn-white">CANCELAR</button>
<button id="btn_accion" name="btn_accion" type="button" onclick="valguardadivisionpolitica()" class="btn btn-primary">GUARDAR</button>
</div>
</div>
@endif   
@if(!$permiso)    
<div class="box-body">
<div class="form-group" id="div_division_politica_capital">
<label class="col-sm-2 control-label">Capital de provincia</label>
<div class="col-sm-10 radio">
<label>
<input type="radio" name="division_politica_capital" disabled="true" class="radio-division disabled" value="S" />Si</label>
<label>
<input type="radio" name="division_politica_capital" disabled="true" class="radio-division disabled" value="N" />No</label>
</div>
</div>
<div class="form-group" id="div_division_politica_codigo">
<label class="col-sm-2 control-label">Código</label>
<div class="col-sm-10">
<input type="text" name="division_politica_codigo" disabled="true" id="division_politica_codigo" class="form-control disabled">
</div>
</div>
<div class="form-group" id="div_division_politica_nombre">
<label class="col-sm-2 control-label">Nombre</label>
<div class="col-sm-10">
<input type="text" id="division_politica_nombre"  disabled="true" name="division_politica_nombre" class="form-control disabled" style="text-transform:uppercase">
</div>
</div>
<div class="form-group">
<label class="col-sm-2 control-label">Estado</label>
<div class="col-sm-10 radio">
<label>
<input type="radio" name="estado_id" disabled="true" class="radio-division disabled" value="1" />Activo</label>
<label>
<input type="radio" name="estado_id" disabled="true" class="radio-division disabled" value="2" />Inactivo</label>
<label class="hide" id="lbl_division_politica_estado_id" name="error"></label>
</div>
</div>
</div> 
@endif
</div>
</div>
</div>
</form>
</div>
@endsection
@section('script')
<script src="../js/bootstrap-treeview.min.js"></script>
<script src="../js/DatosArbol.js"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var lstDivPoljs = <?php echo json_encode($objDivisionPolitica); ?>;
    var arbolDivisionPolitica = dataArbolDinamico(lstDivPoljs, "division_politica_nombre", "division_politica_id", "division_politica", "true", "division_politica_codigo", "division_politica_nombre");
    var accion = 'G';
    
    $('#arbolDivisionPolitica').treeview({
        levels: 1,
        color: "#555",
        showBorder: false,
        data: arbolDivisionPolitica,
        multiSelect: $('#chk-select-multi').is(':checked'),
        onNodeSelected: function (event, node) {
            $('#division_politica_seleccionada').val(node.text);
            $('#division_politica_id').val(node.tags);
        },
        onNodeUnselected: function (event, node) {
            $('#division_politica_seleccionada').val("");
            $('#division_politica_id').val("");
        }
    });

    $('#division_politica_codigo').on("keyup",function() {
         validarCampoRegExp('division_politica_codigo', $(this).val(),/^(\d+)$/,'El formato del código no es correcto. Ejemplo: 100');
    });
    
    function valguardadivisionpolitica() {
        var capital = $('input[name=division_politica_capital]:checked').val();
        var divisionPolitica = $("#division_politica_seleccionada").val();
        var divisionPoliticaId = $("#division_politica_id").val();
        var codigo = $("#division_politica_codigo").val();
        var nombre = $("#division_politica_nombre").val();
        if(accion == 'A')
           var estado = $('input[name=estado_id]:checked').val();
        else
           var estado = '1';
        
        $('#mensajes').removeClass('alert alert-danger');
        $('#mensajes').addClass('hide');
        $('#textoMensaje').text("");
        $('div').removeClass('has-error');
        $('label[name=error]').addClass('hide');
        var bValid = false;
        var bValid1 = validarCampoLleno(divisionPolitica, 'campo división política','dpSeleccionada');
        var bValid2 = validarCampoLleno(capital, 'campo capital','division_politica_capital');
        var bValid3 = validarCampoLleno(codigo, 'campo código','division_politica_codigo');
        var bValid31 = validarCampoRegExp('division_politica_codigo', codigo,/^(\d+)$/,'El formato del código no es correcto. Ejemplo: 100');
        var bValid4 = validarCampoLleno(nombre, 'campo nombre','division_politica_nombre');
        var bValid5 = validarCampoLleno(estado, 'campo estado','division_politica_estado_id');
        
        bValid = bValid1 && bValid2 && bValid3 && bValid31 && bValid4 && bValid5;
        
        if (bValid) {
           $.ajax({
                url: '/validarDivisionPolitica',
                type: 'post',
                "_token": "{{ csrf_token() }}",
                async: true,
                data: {
                    'division_politica_id': divisionPoliticaId,
                    'division_politica_capital': capital,
                    'division_politica_codigo': codigo,
                    'division_politica_nombre': nombre,
                    'estado_id': estado,
                    'accion': accion
                },
                beforeSend: function() {
                    $.blockUI({
                        message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var gnrMensaje = $("#gnrMensaje");
                    toTop();
                    if (jqXHR.status == '401') {
                        gnrMensaje.text(textStatus + " - " + errorThrown + " ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                        $('#gnrError').modal('show');
                    } else {
                        $('#mensajes').addClass('alert alert-danger');
                        $('#mensajes').removeClass('hide');
                        var textoMensaje = $("#textoMensaje");
                        textoMensaje.text("ERROR: " + jqXHR.status + " - " + textStatus + " - " + errorThrown);
                    }
                },
                success: function(data) {
                    if (data !== '') {
                        $('#mensajes').addClass('alert alert-danger');
                        $('#mensajes').removeClass('hide');
                        $('#textoMensaje').text('');
                        for (var i in data) {
                            $('#textoMensaje').append(data[i] + '<br/>');
                        }
                    } else {
                        $("#guardaDivisionPolitica").submit();
                    }
                },
                complete: function() {
                    setTimeout($.unblockUI, 1000);
                }
            });
        }
    }

    function llenaractualizacion(identifier) {
        var cadena = $('#division_politica_id').val();
        $('#mensajes').removeClass('alert alert-danger');
        $('#mensajes').addClass('hide');
        $('#textoMensaje').text("");
        $('div').removeClass('has-error');
        $('label[name=error]').addClass('hide');
        var bValid = false;
        bValid = validarCampoLleno(cadena, "campo división política", "dpSeleccionada");
        $('#guardaDivisionPolitica').attr('action', '/actualizaDivisionPolitica');
        if (bValid) {
            $.ajax({
                url: '/recuperarDivisionPolitica',
                type: 'post',
                "_token": "{{ csrf_token() }}",
                async: true,
                data: {'id': cadena},
                beforeSend: function () {
                    $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var gnrMensaje = $("#gnrMensaje");
                    toTop();
                    if (jqXHR.status == '401') {
                        gnrMensaje.text(textStatus + " - " + errorThrown + " ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                        $('#gnrError').modal('show');
                    } else {
                        $('#mensajes').addClass('alert alert-danger');
                        $('#mensajes').removeClass('hide');
                        var textoMensaje = $("#textoMensaje");
                        textoMensaje.text("ERROR: " + jqXHR.status + " - " + textStatus + " - " + errorThrown);
                    }
                },
                success: function (data) {
                    var cadenas = JSON.stringify(data);
                    var divisionPolitica = eval('(' + cadenas + ')');
                    accion = 'A';
                    $("#division_politica_codigo").val(divisionPolitica.division_politica_codigo);
                    $("#division_politica_nombre").val(divisionPolitica.division_politica_nombre);
                    $("input[name='division_politica_capital'][value='" + divisionPolitica.division_politica_capital + "']").prop("checked", true);
                    $("input[name='estado_id'][value='" + divisionPolitica.estado_id + "']").prop("checked", true);
                    $("#division_politica_id").val(divisionPolitica.division_politica_id);
                    $("#division_politica_tipo").val(divisionPolitica.division_politica_tipo);
                    $("#btn_accion").html('ACTUALIZAR');
                    $("#div_division_politica_estado_id").removeClass('hide');
                },
                complete: function () {
                    setTimeout($.unblockUI, 1000);
                }
            });
        }

    }

</script>
@endsection

