@extends('layouts.app')
@section('content')
<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2><span class="text-navy"><i class="fa fa-chevron-circle-right"></i> Administración de catálogo de categorías</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                <div class="col-lg-12">
                                    @if(Session::has('message'))
                                    <div class="alert alert-success" >
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <p> {{Session::get('message')}} </p>
                                    </div>
                                    @endif
                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    <div id="mensajes" data-dismiss="alert" role="alert" class="text-left" >                            
                                        <p id="textoMensaje"></p>
                                    </div>
                                    
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <form class="form-horizontal" id="target" method="POST" name="formValiInconInformacion" action="{{url('/guardarCatalogoCategoria')}}" role="form">
                                             <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                         <label class="col-sm-2 control-label">División Politica</label>
                                                            <div class="col-sm-8">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-body">
                                                                        <ul id="treeview-checkable" style="text-align:left;">
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label class="col-sm-2 control-label">Rango Poblacional</label>
                                                        <div class="col-sm-4">
                                                              <select class="form-combo col-sm-10" id="categoria_id" name="categoria_id">
                                                                <option>Seleccionar...</option>
                                                                @foreach($objCategoria as $select) 
                                                                <option  value="{{$select->categoria_id}}">{{$select->categoria_nombre}} - {{$select->categoria_minimo}} - {{$select->categoria_maximo}}</option> 
                                                                @endforeach
                                                            </select>
                                                             <div class="col-sm-2">
                                                                        <button class="btn btn-primary" id="btn_editar_categoria"  name="btn_editar_categoria" type="button"><i class="fa fa-edit"></i></button>
                                                                    </div>
                                                        </div>
                                                        <div class="col-sm-2 text-center">
                                                            <button id="pAdd" class="btn btn-outline btn-primary" type="button">
                                                                <i class="fa fa-plus" aria-hidden="true"></i> Agregar</button>
                                                            <button id="pRemove" class="btn btn-outline btn-primary" type="button">
                                                                <i class="fa fa-times" aria-hidden="true"></i> Quitar</button>
                                                        </div>
                                                        <div class="col-sm-4">
                                                             <div class="form-group">
                                                                <label class="col-sm-12 control-label">Cantones Seleccionados</label>
                                                                <select class='form-control pickListSelect' id='pickData' multiple>
                                                                </select>
                                                            </div>
                                                            <input type="hidden" name="division_politica_id" id="division_politica_id">
                                                        </div>
                                                        <input type="hidden" name="lsta_remove_categoria" id="lsta_remove_categoria">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12 text-right">
                                                        <button id="almacenar" class="btn btn-primary" type="button">Almacenar</button>
                                                       
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Editar Categoría</h4>
      </div>
      <div class="modal-body">
        <div class="row">

             <form class="form-horizontal" id="formCategoria" method="POST" name="formCategoria" action="{{url('/actualizarCategoria')}}" role="form">
             <input type="hidden" name="_token" value="{{ csrf_token() }}">
             <div class="form-group">
                <label class="col-sm-4 control-label">Nombre de la categoría</label>
                <div class="col-sm-7">
                    <input type="text" id="categoria_nombre" name="categoria_nombre"  value="{{ old('categoria_nombre') }}" class="form-control">
                </div>
            </div> </br>
            <div class="form-group">
                <label class="col-sm-4 control-label">Mínimo</label>
                <div class="col-sm-7">
                    <input type="text" id="categoria_minimo" name="categoria_minimo"  value="{{ old('categoria_minimo') }}" class="form-control">
                </div>
            </div> </br>
              <div class="form-group">
                <label class="col-sm-4 control-label">Máximo</label>
                <div class="col-sm-7">
                    <input type="text" id="categoria_maximo" name="categoria_maximo"  value="{{ old('categoria_maximo') }}" class="form-control">
                </div>
            </div>
            <input type="hidden" name="idCategoria" id="idCategoria">
            </form>
          </div>
        </div>
      <div class="modal-footer">
       <button id="btn_actualizar_categoria" class="btn btn-primary" type="button">Actualizar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
  
  var lsySelect2 = [] ; 
    
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


$( "#categoria_id" ).change(function() {
  CrearArbol();
});

var lstDivPoljs = <?php echo json_encode($objDivisionPolitica); ?>;
var canton = <?php echo json_encode($canton); ?>;

  var menu=[];
        
        var a = 1;
        var menu = dataArbol(lstDivPoljs,a);


        var lsySelect= Array();
        

        var $checkableTree = $('#treeview-checkable').treeview({
          data: menu,
          showIcon: false,
          showCheckbox: true,
          levels: 1,
          color: "#555",
          showBorder: false,
          onNodeChecked: function(event, node) {
            $('#checkable-output').prepend('<p>' + node.text + ' was checked</p>');
           
             if(node.nivel<3){
                 
                 
                  $('#mensajes').addClass('alert alert-danger');
                   var tips =$("#textoMensaje");
                   tips.text("Error !  solo puede seleccionar Cantones ");
                   $checkableTree.treeview('uncheckNode', [ node.nodeId , { silent: true } ])
             }else{
                 lsySelect.push(node);
             }
          },

                  
          onNodeUnchecked: function (event, node) {
            $('#checkable-output').prepend('<p>' + node.text + ' was unchecked</p>');
            
            for(var i = 0; i < lsySelect.length; i++) { 
                if(lsySelect[i].href == node.href) {
                    lsySelect.splice(i,1); break; } 
            }
           
             
          }
        });

       lsySelect2 = lsySelect; 

         


          function CrearArbol (){

            var categoria_id = $("#categoria_id").val();
            $.ajax({
                        url: '{{url('/consultaCantonesCategorias')}}',
                        type: 'post',
                        "_token": "{{ csrf_token() }}",
                        async: true,
                        data: {'categoria_id':categoria_id},
                        beforeSend: function(data){   
                            $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                        },
                        success:  function(data) { 
                            
                            console.log(lsySelect2);
                            lsySelect=data;

                              $('#pickData').empty();
                              for(var i = 0; i < lsySelect.length; i++) { 

                                if(lsySelect[i].division_politica_tipo == 'C')
                                {
                                     $('#pickData').append($('<option>', {
                                        value: lsySelect[i].division_politica_id,
                                        id: lsySelect[i].division_politica_id,
                                        text:lsySelect[i].division_politica_nombre
                                    }));
                                }  
                            }

                         var lstInterfazjs = <?php echo json_encode($objDivisionPolitica); ?>;
                          var menu=[];
                         
                           for(var i=0;i<lstInterfazjs.length;i++){

                                var lstregion=lstInterfazjs[i];

                                    var region={
                                    selectable: false,
                                    icon: "glyphicon glyphicon-stop",
                                     selectedIcon: "glyphicon glyphicon-stop",
                                    text: lstregion.division_politica_nombre,
                                    href: lstregion.division_politica_id,
                                    tags: [(lstregion.division_politica && lstregion.division_politica.length>0)?lstregion.division_politica.length>0:0],
                                    nivel: 1,
                                    nodes: [],
                                    
                                    };
                                     for(var x=0;x<lstregion.division_politica.length;x++){
                                      var lstProvincia=lstregion.division_politica[x];
                                       var provincia={
                                            text: lstProvincia.division_politica_nombre,
                                            href: lstProvincia.division_politica_id,
                                            tags: [(lstregion.division_politica && lstProvincia.division_politica.length>0)?lstProvincia.division_politica.length>0:0],
                                            nivel: 2,
                                            nodes: [],
                                            
                                           
                                            };
                                            for(var y=0;y<lstProvincia.division_politica.length;y++){
                                                 var checkedVal=false;
                                                 var lstCanton=lstProvincia.division_politica[y];
                                                 
  
                                                  for(var a in lsySelect2){
                                                  
                                                   if(lstCanton.division_politica_id==lsySelect2[a].href){
                                                      checkedVal=true; 
                                                      break;
                                                    }
                                               }
                                           
                                              for(var e in data){
                                               
                                                   if(lstCanton.division_politica_id==data[e].division_politica_id){
                                                      checkedVal=true; 
                                                      break;
                                                    }
                                               }

                                             
                                                 var canton={
                                                      text: lstCanton.division_politica_nombre,
                                                      href: lstCanton.division_politica_id,
                                                      tags: [0],
                                                      nivel: 3,
                                                      
                                                        state: {
                                                        checked: checkedVal,
                                                        disabled: false,
                                                        expanded: true,
                                                        selected: false
                                                         }
                                                      };
                                                provincia.nodes.push(canton);
                                            }
                                             region.nodes.push(provincia);
                                     }
                                      menu.push(region);
                                  }
                            
                            var $checkableTree = $('#treeview-checkable').treeview({
                                data: menu,
                                showIcon: false,
                                showCheckbox: true,
                                onNodeChecked: function(event, node) {
                                      
                                $('#checkable-output').prepend('<p>' + node + ' was checked</p>');
                                   if(node.nodes && node.nodes.length>0){
                                
                                     $('#mensajes').addClass('alert alert-danger');
                                     var tips =$("#textoMensaje");
                                     tips.text("Error !  solo puede seleccionar Cantones ");
                                     $checkableTree.treeview('uncheckNode', [ node.nodeId , { silent: true } ])
                                 
                                   }else{
                                        var adDiv=false;
                                        for(var i = 0; i < lsySelect.length; i++) { 
                                          if(lsySelect[i].division_politica_id==node.href) {
                                            lsySelect[i].estado_id=activo;
                                            adDiv=true;
                                            break; 
                                          }
                                       }
                                       for(var i = 0; i < lsySelect2.length; i++) { 
                                          if(lsySelect2[i].href==node.href) {
                                            adDiv=true;
                                            break; 
                                          }
                                       }
                                       if(!adDiv){ 
                                        lsySelect.push(node);
                                        console.log(lsySelect);
                                    }
                                    lsySelect2 = lsySelect;
                                     console.log(lsySelect2);
                                   }
                            }, 
                                onNodeUnchecked: function (event, node) {
                                  $('#checkable-output').prepend('<p>' + node + ' was unchecked</p>');
                                    for(var i = 0; i < lsySelect.length; i++) { 
                                       if(lsySelect[i].division_politica_id==node.href) {
                                         lsySelect[i].estado_id=inactivo; break; 
                                       } 
                                       if(lsySelect[i].href==node.href) {
                                            lsySelect.splice(i,1); break; }
                                       }

                                     }
                                 
                              });
                           setTimeout($.unblockUI, 1000);  
                        }   
                    });
        
          }

         $("#pAdd").on('click', function() {

              $('#pickData').empty();
              for(var i = 0; i < lsySelect.length; i++) { 

                if(lsySelect[i].division_politica_id && lsySelect[i].division_politica_tipo == 'C' )
                {
                     $('#pickData').append($('<option>', {
                        value: lsySelect[i].division_politica_id,
                        id: lsySelect[i].division_politica_id,
                        text:lsySelect[i].division_politica_nombre
                    }));
                } if(lsySelect[i].href) {
                     $('#pickData').append($('<option>', {
                        value: lsySelect[i].href,
                        id: lsySelect[i].href,
                        text:lsySelect[i].text
                    }));
                } 
            }

            for(var t = 0; t < lsySelect2.length; t++) { 
               $('#pickData option[value='+lsySelect2[t].href+']').remove();
               if(lsySelect2[t].href){
                  $('#pickData').append($('<option>', {
                            value: lsySelect2[t].href,
                            id: lsySelect2[t].href,
                            text:lsySelect2[t].text
                        }));
                }
           }
           for(var r = 0; r < lstRemove.length; r++) { 

                 $('#pickData option[value='+lstRemove[r]+']').remove();
              }

          });

     var lstRemove = [];
     $("#pRemove").on('click', function() {
          var categoria_remove = $("#pickData option:selected");
               categoria_remove.remove();
                lstRemove.push(categoria_remove.val());
                console.log(lstRemove);
                $("#lsta_remove_categoria").val(lstRemove)
        });

     $("#btn_editar_categoria").on('click', function() {
        var categoria_id = $("#categoria_id option:selected").val();
        bValid = true;
        bValid = bValid && validarCampoLleno(categoria_id, "categoria id");
            if(bValid){

            $.ajax({
               url  :'{{ url('/buscarDatosCategoria') }}',
               type: 'post',
               "_token": "{{ csrf_token() }}",
               async: true,
               data :{'categoria_id' : categoria_id},

               error : function(jqXHR, textStatus, errorThrown) {
                    $('#mensajes').addClass('alert alert-danger');
                    var textoMensaje =$("#textoMensaje");
                    textoMensaje.text("No se han encontrados datos. ");             

               },

               success : function(data) {
                    console.log(data);
                    $("#idCategoria").val(data.categoria_id);
                    $("#categoria_nombre").val(data.categoria_nombre);
                    $("#categoria_minimo").val(data.categoria_minimo);
                    $("#categoria_maximo").val(data.categoria_maximo);
                    $('#myModal').modal('show'); 
               }
           });  
           
        }
    });

      $("#btn_actualizar_categoria").on('click', function() {
         var categoria_id = $("#categoria_id").val(),
            categoria_nombre = $("#categoria_nombre").val(),
            categoria_maximo = $("#categoria_maximo").val(),
            categoria_minimo = $("#categoria_minimo").val();

            bValid = true;
            bValid = bValid && validarCampoLleno(categoria_id, "categoria id");
            bValid = bValid && validarCampoLleno(categoria_nombre, "campo nombre categoria");
            bValid = bValid && validarCampoLleno(categoria_maximo, "campo categoria máximo");
            bValid = bValid && validarCampoLleno(categoria_minimo, "campo categoria mínimo");
        
            if(bValid){

               $( "#formCategoria").submit();

            }
    });



    function dataArbol(lstArbol,a){
        
         var lst = [];
           for(var i=0;i<lstArbol.length;i++){
               var raiz=lstArbol[i];
               var arbol={text:"",href:"",tags:0};
               //Longitud los hijos del arbol
               var tamanioHijos = raiz.division_politica?raiz.division_politica.length:0;
               arbol.text= raiz.division_politica_nombre;
               arbol.href= raiz.division_politica_id;
               arbol.tags= [tamanioHijos];
               arbol.nivel= a;
               //Si el arbol tiene raices, se le agregan los hijos
               if (tamanioHijos>0){
                 a++;  
                 arbol.nodes= dataArbol(raiz.division_politica,a);
                 a--;
               }
               lst.push(arbol);
           }
           return lst;
   }

    $("#almacenar").click(function() {
        var lstaCantonesGuardar = new Array(),
        categoria_id = $("#categoria_id").val();

        $("#pickData option").each(function(){
             interfaz_id =  $(this).attr('id');
             
             lstaCantonesGuardar.push(interfaz_id);
        });

        bValid = true;
        bValid = bValid && validarCampoLleno(lstaCantonesGuardar, "campo cantones");
        bValid = bValid && validarCampoLleno(categoria_id, "campo rango poblacional");
        
        if(bValid){
               $("#division_politica_id").val(lstaCantonesGuardar);
                 $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
               $( "#target").submit();
               setTimeout($.unblockUI, 3000);
            }
}); 


</script>
@endsection