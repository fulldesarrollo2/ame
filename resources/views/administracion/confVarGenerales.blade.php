@extends('layouts.app')
@section('content')
<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2>
                            <span class="text-navy"><i class="fa fa-chevron-circle-right"></i>
                                Administración de variables de información general </span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                @if(Session::has('message'))
                                <div class="alert alert-success" >
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <p> {{Session::get('message')}} </p>
                                </div>
                                @endif
                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <div id="mensajes" data-dismiss="alert" role="alert" class="text-left" >                            
                                    <p id="textoMensaje"></p>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="col-lg-6" for="sel1">Aplicación</label>
                                        <div class="col-lg-6">
                                            <select class="form-control" id="sel1" onchange="tipoaplicacion();">
                                                <option value="0">Tipo Aplicación</option>
                                                <option value="1">General</option>
                                                <option value="2">Especifico</option>
                                            </select>
                                        </div>
                                        <div id="periodo" style="display: none;">
                                            <label class="col-lg-6" for="sel2">Periodos</label>
                                            <br><br>
                                            <div class="col-lg-6" id="container-periodo">
                                                <select multiple size="2" class="form-control" id="periodos">
                                                    <option selected="selected">Seleccione un periodo</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <div id="division" style="display: none;">
                                            <label class="col-lg-4 control-label">División Politica</label>
                                            <div class="col-lg-8">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div id="arbolDivisionPolitica" class="step-content"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <input type="hidden" name="division_politica_id" class="division_politica_id" id="division_politica_id">
                                <div class="hr-line-dashed"></div>
                                <div id="var-generales"></div>
                                <div id="frm-edicion">
                                    <div id="Edicion-General" class="col-lg-12" style="display: none;">
                                        <form  method="POST" id="frm-Edicion-General" name="formActualizarVarGeneral" enctype="multipart/form-data"  action="{{url('/editarVariableGeneral')}}" role="form">
                                            <input type="hidden" name="variable_general_id" class="division_polvariable_general_iditica_id" id="variable_general_id">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Nombre</label>
                                                <div class="col-lg-4">
                                                    <input id="variable_general_nombre_programatico" onkeypress="return sololetras(event)" type="text"
                                                           maxlength="254" name="variable_general_nombre_programatico"
                                                           class="form-control"></input>
                                                </div>
                                                <label class="col-lg-1 control-label ">Descripción</label>
                                                <div class="col-lg-5">
                                                    <input type="text" onkeypress="return sololetras(event)" class="form-control" maxlength="254"
                                                           name="variable_general_descripcion"
                                                           id="variable_general_descripcion" />
                                                </div>
                                            </div>
                                            <br><br>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Fuente</label>

                                                <div class="col-lg-4">
                                                    <input onkeypress="return sololetras(event)" id="variable_general_fuente" type="text"
                                                           maxlength="254" name="variable_general_fuente"
                                                           class="form-control"></input>
                                                </div>
                                                <label class="col-lg-1 control-label ">Unidades</label>
                                                <div class="col-lg-5">
                                                    <input type="text" onkeypress="return sololetras(event)" class="form-control" maxlength="254"
                                                           name="variable_general_unidad" id="variable_general_unidad"
                                                           />
                                                </div>
                                            </div>
                                            <br><br>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Estado</label>
                                                <div class="col-lg-4">
                                                    <label> <input type="radio" name="estado_id" class="radio-estadio" value="1" />Activo </label> 
                                                    <label> <input type="radio" name="estado_id" class="radio-estadio" value="2" />Inactivo </label>
                                                </div>
                                                <div class="col-lg-6"></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-6 col-lg-offset-4">
                                                    <button type="button"
                                                            onclick="valEdicionVarGeneral();"
                                                            class="btn btn-primary">Almacenar</button>
                                                    <button type="button" onclick="cancelardivisionpolitica();"
                                                            class="btn btn-primary">cancelar</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <br>
                                    <div id="Edicion-Especifica" style="display: none;">
                                        <form class="form-horizontal" method="POST" id="actualizarValorVarGeneral" enctype="multipart/form-data"  action="{{url('editarValorVariableGeneral')}}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" id="valor_variable_general_id" name="valor_variable_general_id">
                                            <div class="col-lg-3"></div>
                                            <div class="col-lg-6">
                                                <div class="col-lg-8 ">
                                                    <input id="txt_valvargeneral_valor" type="text" onKeyPress="return soloNumeros(event)"
                                                           name="valor_variable_general_valor"
                                                           class="form-control"></input>
                                                </div>
                                                <label id="valvargeneral-unidad"
                                                       class="col-lg-4 text-left control-label">Unidades</label>
                                            </div>
                                            <div class="col-lg-3"></div>
                                            <div class="form-group">
                                                <div class="col-lg-12" style="margin-top: 1%">
                                                    <div class="col-lg-6 col-lg-offset-4">
                                                        <button type="button"
                                                                onclick="valEdicionVarGeneralEspecifica();"
                                                                class="btn btn-primary">Almacenar</button>
                                                        <button type="button" onclick="cancelardivisionpolitica();"
                                                                class="btn btn-primary">cancelar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var lstDivPoljs = <?php echo json_encode($objDivisionPolitica); ?>;
    var arbolDivisionPolitica = dataArbolDinamico(lstDivPoljs, "division_politica_nombre", "division_politica_id", "division_politica", "true", "division_politica_codigo", "division_politica_nombre");

    $('#arbolDivisionPolitica').treeview({
        levels: 1,
        color: "#555",
        showBorder: false,
        data: arbolDivisionPolitica,
        multiSelect: $('#chk-select-multi').is(':checked'),
        onNodeSelected: function (event, node) {
            $('#division_politica_nom').val(node.text);
            $('#division_politica_id').val(node.tags);
            validarcanton();
        },
        onNodeUnselected: function (event, node) {
            $('#nomCtaSeleccionada').val("");
        }
    });

    function tipoaplicacion() {
        if ($("#sel1").val() == 1) {
            cancelar();
            variablesgenerales();
        }
        if ($("#sel1").val() == 2) {
            cancelar();
            document.getElementById("var-generales").innerHTML = "";
            $("#division").show();
        }
    }

    function cancelar() {
        $("#Edicion-General").hide();
        $("#periodo").hide();
        $("#Edicion-Especifica").hide();
        $("#division").hide();
        $("#variable_general_nombre_programatico").val("");
        $("#variable_general_fuente").val("");
        $("#variable_general_descripcion").val("");
        $("#variable_general_id").val("");
        $("#valor_variable_general_id").val("");
        $("#txt_valvargeneral_valor").val("");
    }

    function validarcanton() {
        var id = $('#division_politica_id').val();
        var canton = false;
        $.ajax({
            url: '/divisionPoliticaCanton',
            type: 'post',
            "_token": "{{ csrf_token() }}",
            async: true,
            data: {'id': cadena},
            beforeSend: function () {
                $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var gnrMensaje = $("#gnrMensaje");
                toTop();
                if (jqXHR.status == '401') {
                    gnrMensaje.text(textStatus + " - " + errorThrown + " ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                    $('#gnrError').modal('show');
                } else {
                    $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var textoMensaje = $("#textoMensaje");
                    textoMensaje.text("ERROR: " + jqXHR.status + " - " + textStatus + " - " + errorThrown);
                }
            },
            success: function (data) {
                var cadena = data;
                if (data.division_politica_tipo == "C") {
                    $.ajax({
                        url: '/consultaPeriodo',
                        type: 'post',
                        async: 'true',
                        beforeSend: function (data) {
                            $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var gnrMensaje = $("#gnrMensaje");
                            toTop();
                            if (jqXHR.status == '401') {
                                gnrMensaje.text(textStatus + " - " + errorThrown + " ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                                $('#gnrError').modal('show');
                            } else {
                                $('#mensajes').addClass('alert alert-danger');
                                $('#mensajes').removeClass('hide');
                                var textoMensaje = $("#textoMensaje");
                                textoMensaje.text("ERROR: " + jqXHR.status + " - " + textStatus + " - " + errorThrown);
                            }
                        },
                        success: function (data) {
                            variables = verperiodos(data);
                            var combo = '<select onchange="aplicacionespecifica();" multiple size="2" class="form-control" id="periodos"> ' +
                                    variables +
                                    '</select>';

                            document.getElementById("container-periodo").innerHTML = combo;
                            $("#periodo").show();
                        },
                        complete: function () {
                            setTimeout($.unblockUI, 1000);
                        }

                    });
                } else {
                    document.getElementById("container-periodo").innerHTML = "";
                    $("#periodo").hide();
                }

            },
            complete: function () {
                setTimeout($.unblockUI, 1000);
            }

        });
    }

    function vervariables(data) {
        var tregistros = '';

        for (var i in data) {
            tregistros = tregistros + '<tr >' +
                    '  <td>' + data[i].variable_general_nombre_programatico + ' </td>';
            if (data[i].variable_general_descripcion !== null) {
                tregistros = tregistros + '  <td>' + data[i].variable_general_descripcion + '</td>';
            } else {
                tregistros = tregistros + '  <td> </td>';
            }
            if (data[i].variable_general_fuente !== null) {
                tregistros = tregistros + '  <td>' + data[i].variable_general_fuente + '</td>';
            } else {
                tregistros = tregistros + '  <td> </td>';
            }
            if (data[i].variable_general_unidad !== null) {
                tregistros = tregistros + '  <td>' + data[i].variable_general_unidad + '</td>';
            } else {
                tregistros = tregistros + '  <td> </td>';
            }
            if (data[i].estado_id == 1) {
                tregistros = tregistros + '<td> Activo </td>';
            }
            if (data[i].estado_id == 2) {
                tregistros = tregistros + '<td> Inactivo </td>';
            }
            tregistros = tregistros + '<td>&nbsp;&nbsp; <a data-id=' + data[i].variable_general_id + ' class="fa fa-pencil" onclick="edita_VariableGeneral(this)" type="button" title="seleccione" >  </a> </td>'

            tregistros = tregistros + '</tr>';
        }
        return tregistros;
    }

    function verperiodos(data) {
        var tperiodos = '<option value="0">Seleccione un periodo</option>';
        for (var i in data) {
            tperiodos = tperiodos + '<option value=' + data[i].periodo_anio + '>' + data[i].periodo_anio;
            tperiodos = tperiodos + '</option>';

        }
        return tperiodos;
    }

    function vervariablesespecifica(data) {
        var tregistros = '';
        for (var i in data) {
            tregistros = tregistros + '<tr data-id="' + JSON.stringify(data[i]) + '">' +
                    '  <td>' + data[i].variablegeneral.variable_general_nombre_programatico + ' </td>';
            if (data[i].valor_variable_general_valor !== null) {
                tregistros = tregistros + '  <td>' + data[i].valor_variable_general_valor + '</td>';
            } else {
                tregistros = tregistros + '  <td> </td>';
            }
            if (data[i].variablegeneral.variable_general_unidad !== null) {
                tregistros = tregistros + '  <td>' + data[i].variablegeneral.variable_general_unidad + '</td>';
            } else {
                tregistros = tregistros + '  <td> </td>';
            }
            if (data[i].variable_general_fuente !== null) {
                tregistros = tregistros + '  <td>' + data[i].variablegeneral.variable_general_fuente + '</td>';
            } else {
                tregistros = tregistros + '  <td> </td>';
            }
            tregistros = tregistros + '<td>&nbsp;&nbsp; <a data-id=' + data[i].valor_variable_general_id + ' class="fa fa-pencil" onclick="edita_VariableEspecifica(this)" type="button" title="seleccione" >  </a> </td>'
            tregistros = tregistros + '</tr>';
        }
        return tregistros;
    }

    function edita_VariableEspecifica(identifier) {
        var fila = $(identifier).data('id');
        $.ajax({
            url: '/consultaEspecifica',
            type: 'post',
            data: {'id': fila},
            beforeSend: function () {
                $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var gnrMensaje = $("#gnrMensaje");
                toTop();
                if (jqXHR.status == '401') {
                    gnrMensaje.text(textStatus + " - " + errorThrown + " ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                    $('#gnrError').modal('show');
                } else {
                    $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var textoMensaje = $("#textoMensaje");
                    textoMensaje.text("ERROR: " + jqXHR.status + " - " + textStatus + " - " + errorThrown);
                }
            },
            success: function (data) {
                $("#Edicion-General").hide();
                $("#Edicion-Especifica").show();
                $('#valor_variable_general_id').val(data[0].valor_variable_general_id);
                $('#valvargeneral-unidad').text(data[0].variablegeneral.variable_general_unidad);
                $('#txt_valvargeneral_valor').val(data[0].valor_variable_general_valor);
            },
            complete: function () {
                setTimeout($.unblockUI, 1000);
            }
        });

    }

    function edita_VariableGeneral(identifier) {
        var fila = $(identifier).data('id');
        $.ajax({
            url: '/consultaVariableGeneral',
            type: 'post',
            data: {'id': fila},
            beforeSend: function () {
                $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var gnrMensaje = $("#gnrMensaje");
                toTop();
                if (jqXHR.status == '401') {
                    gnrMensaje.text(textStatus + " - " + errorThrown + " ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                    $('#gnrError').modal('show');
                } else {
                    $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var textoMensaje = $("#textoMensaje");
                    textoMensaje.text("ERROR: " + jqXHR.status + " - " + textStatus + " - " + errorThrown);
                }
            },
            success: function (data) {
                $('#Edicion-Especifica').hide();
                $('#Edicion-General').show();
                $('#variable_general_id').val(data.variable_general_id);
                $('#variable_general_nombre_programatico').val(data.variable_general_nombre_programatico);
                $('#variable_general_descripcion').val(data.variable_general_descripcion);
                $('#variable_general_fuente').val(data.variable_general_fuente);
                $('#variable_general_unidad').val(data.variable_general_unidad);
                $("input[class='radio-estadio'][value='" + data.estado_id + "']").prop("checked", true);
                setTimeout($.unblockUI, 1000);
            },
            complete: function () {
                setTimeout($.unblockUI, 1000);
            }

        });

    }

    function variablesgenerales() {
        $.ajax({
            url: '/buscarLstVariableGeneral',
            type: 'post',
            async: 'true',
            beforeSend: function () {
                $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var gnrMensaje = $("#gnrMensaje");
                toTop();
                if (jqXHR.status == '401') {
                    gnrMensaje.text(textStatus + " - " + errorThrown + " ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                    $('#gnrError').modal('show');
                } else {
                    $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var textoMensaje = $("#textoMensaje");
                    textoMensaje.text("ERROR: " + jqXHR.status + " - " + textStatus + " - " + errorThrown);
                }
            },
            success: function (data) {
                var cadena = JSON.stringify(data);
                variables = vervariables(data);
                var tabla = '<table id="lstEntidad" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">' +
                        ' <thead>' +
                        '<tr>' +
                        '      <th>Nombre </th>' +
                        '      <th>Descripción</th>' +
                        '      <th>Fuente </th>' +
                        '      <th>Unidad </th>' +
                        '      <th>Estado</th>' +
                        '      <th></th>' +
                        '</tr>' +
                        ' </thead>' +
                        '    <tbody> ' +
                        variables +
                        '    </tbody> ' +
                        '</table>';
                document.getElementById("var-generales").innerHTML = tabla;
                $('#lstEntidad').DataTable({
                    responsive: true
                });
                setTimeout($.unblockUI, 1000);
            },
            complete: function () {
                setTimeout($.unblockUI, 1000);
            }
        });
    }

    function aplicacionespecifica() {
        var selector = $("#periodos").val();
        var id = $('#division_politica_id').val();
        var periodo = selector;
        if (id != "" && periodo != "") {
            $.ajax({
                url: '/buscarLstVariableEspecifica',
                type: 'post',
                async: 'true',
                data: {'id': id,
                    'periodo': periodo},
                beforeSend: function (data) {
                    $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var gnrMensaje = $("#gnrMensaje");
                    toTop();
                    if (jqXHR.status == '401') {
                        gnrMensaje.text(textStatus + " - " + errorThrown + " ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                        $('#gnrError').modal('show');
                    } else {
                        $('#mensajes').addClass('alert alert-danger');
                        $('#mensajes').removeClass('hide');
                        var textoMensaje = $("#textoMensaje");
                        textoMensaje.text("ERROR: " + jqXHR.status + " - " + textStatus + " - " + errorThrown);
                    }
                },
                success: function (data) {
                    variables = vervariablesespecifica(data);
                    var tabla = '<table id="lstEntidad" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">' +
                            ' <thead>' +
                            '<tr>' +
                            '      <th>Seleccione </th>' +
                            '      <th>Valor</th>' +
                            '      <th>U.Medida </th>' +
                            '      <th>Fuente </th>' +
                            '      <th></th>' +
                            '</tr>' +
                            ' </thead>' +
                            '    <tbody> ' +
                            variables +
                            '    </tbody> ' +
                            '</table>';
                    document.getElementById("var-generales").innerHTML = tabla;
                    $('#lstEntidad').DataTable({
                        responsive: true
                    });
                },
                complete: function () {
                    setTimeout($.unblockUI, 1000);
                }
            });
        }
    }

    function valEdicionVarGeneral() {
        var nombre = document.getElementById("variable_general_nombre_programatico").value;
        var fuente = document.getElementById("variable_general_fuente").value;
        var unidad = document.getElementById("variable_general_unidad").value;
        var descripcion = document.getElementById("variable_general_nombre_programatico").value;

        var bValid = true;
        bValid = bValid && validarCampoLleno(nombre, 'nombre');
        bValid = bValid && validarCampoLleno(fuente, 'fuente');
        bValid = bValid && validarCampoLleno(unidad, 'unidad');
        bValid = bValid && validarCampoLleno(descripcion, 'descripcion');

        if (bValid) {
            $.blockUI({message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
            $("#frm-Edicion-General").submit();
        }
    }

    function valEdicionVarGeneralEspecifica() {
        var valor = document.getElementById("txt_valvargeneral_valor").value;

        var bValid = true;
        bValid = bValid && validarCampoLleno(valor, 'valor');

        if (bValid) {
            $.blockUI({message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
            $("#actualizarValorVarGeneral").submit();
        }
    }

    function soloNumeros(e) {
        var key = window.Event ? e.which : e.keyCode
        return (key >= 48 && key <= 57)
    }

    function verperiodo(data) {
        var tperiodo = '';
        for (var i in data) {
            tperiodo = tperiodo + '<tr data-id="' + data[i].variable_general_id + '">' +
                    '  <td>' + data[i].variable_general_nombre_programatico + ' </td>' +
                    '  <td>' + data[i].variable_general_descripcion + '</td>' +
                    '  <td>' + data[i].variable_general_fuente + '</td>' +
                    '  <td>' + data[i].variable_general_unidad + '</td>';
            if (data[i].estado_id == 1) {
                tregistros = tregistros + '<td> Activo </td>';
            }
            if (data[i].estado_id == 2) {
                tregistros = tregistros + '<td> Inactivo </td>';
            }

            tregistros = tregistros + '</tr>';
        }
        return tperiodo;
    }

    function periodos() {
        $.ajax({
            url: '/buscarLstVariableGeneral',
            type: 'post',
            async: 'true',
            beforeSend: function () {
                $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var gnrMensaje = $("#gnrMensaje");
                toTop();
                if (jqXHR.status == '401') {
                    gnrMensaje.text(textStatus + " - " + errorThrown + " ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                    $('#gnrError').modal('show');
                } else {
                    $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var textoMensaje = $("#textoMensaje");
                    textoMensaje.text("ERROR: " + jqXHR.status + " - " + textStatus + " - " + errorThrown);
                }
            },
            success: function (data) {
                variables = vervariables(data);
                var tabla = '<table id="lstEntidad" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">' +
                        ' <thead>' +
                        '<tr>' +
                        '      <th>Nombre </th>' +
                        '      <th>Descripción</th>' +
                        '      <th>Fuente </th>' +
                        '      <th>Unidad </th>' +
                        '      <th>Estado</th>' +
                        '</tr>' +
                        ' </thead>' +
                        '    <tbody> ' +
                        variables +
                        '    </tbody> ' +
                        '</table>';

                document.getElementById("var-generales").innerHTML = tabla;
                $('#lstEntidad').DataTable({
                    responsive: true
                });
            },
            complete: function () {
                setTimeout($.unblockUI, 1000);
            }

        });
    }

    function sololetras(event) {
        var inputValue = event.which;
        if (!(inputValue >= 97 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    }


</script>
@endsection