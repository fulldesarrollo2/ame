@extends('layouts.app')
@section('css')
@endsection
@section('header')
<h1>
PERFILES
</h1>
<ol class="breadcrumb">
<li><a><i class="glyphicon glyphicon-th-large"></i>Administración</a></li>
<li class="active">Perfiles</li>
</ol>
<br/>
@endsection
@section('content')
<div class="box box-default">
<div class="box-body">
<div class="row">
<form role="form" id="target" method="POST" action="{{ url('/guardarPerfil') }}">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" id="perfil_ingreso_id" name="perfil_ingreso_id">
<input type="hidden" name="perfil_ingreso_interfaz" id="perfil_ingreso_interfaz">
<div class="col-md-5">
<div id="div_perfil_ingreso_nombre" class="form-group">
<label>Nombre</label>
<input type="text" name="perfil_ingreso_nombre" id="perfil_ingreso_nombre" class="form-control" maxlength="100" style="text-transform:uppercase">
<label class="hide" id="lbl_perfil_ingreso_nombre" name="error"></label>
</div>
</div>
<div class="col-md-1">
<div class="form-group">
<label style="color:#FFF">...............</label>
<button class="btn btn-primary" id="buscar" name="buscar" type="button"><i class="glyphicon glyphicon-search"></i></button>
</div>
</div>
<div class="col-md-6">
<div id="div_perfil_ingreso_descripcion" class="form-group" name="message">
<label>Descripción</label>
<input type="text" name="perfil_ingreso_descripcion" id="perfil_ingreso_descripcion" maxlength="100" class="form-control" style="text-transform:uppercase">
<label class="hide" id="lbl_perfil_ingreso_descripcion" name="error"></label>
</div>
</div>
<div class="col-md-12">
<div id="div_pickList" class="form-group">
<label>Interfaces</label>
<div id="pickList"></div>
</div>
</div>
<div class="col-md-12">
<div class="form-group">
<div class="text-right">
<button id="cancelar" class="btn btn-white" type="button">CANCELAR</button>
<button id="actualizar" class="btn btn-primary hide" type="button">ACTUALIZAR</button>
<button id="almacenar" class="btn btn-primary" type="button">GUARDAR</button>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">/*<![CDATA[*/$.ajaxSetup({headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')}});var lstInterfazjs=<?php echo json_encode($lstInterfaz); ?>;var val=lstInterfazjs;$.fn.pickList=function(options){var opts=$.extend({},$.fn.pickList.defaults,options);this.fill=function(){var option='';$.each(opts.data,function(key,val){option+='<option id='+val.id+'>'+val.text+'</option>';});this.find('#pickData').append(option);};this.controll=function(){var pickThis=this;$("#pAdd").on('click',function(){var p=pickThis.find("#pickData option:selected");p.clone().appendTo("#pickListResult");p.remove();});$("#pAddAll").on('click',function(){var p=pickThis.find("#pickData option");p.clone().appendTo("#pickListResult");p.remove();});$("#pRemove").on('click',function(){var p=pickThis.find("#pickListResult option:selected");p.clone().appendTo("#pickData");p.remove();});$("#pRemoveAll").on('click',function(){var p=pickThis.find("#pickListResult option");p.clone().appendTo("#pickData");p.remove();});};this.getValues=function(){var objResult=[];this.find("#pickListResult option").each(function(){objResult.push({id:this.id,text:this.text});});return objResult;};this.init=function(){var pickListHtml="<div class='row'>"+"  <div class='col-sm-5'>"+"    <select class='form-control pickListSelect' id='pickData' multiple></select>"+" </div>"+" <div class='col-sm-2 pickListButtons'>"+"   <button id='pAdd' class='btn btn-primary btn-sm' type='button'>"+opts.add+"</button>"+"      <button id='pAddAll' class='btn btn-primary btn-sm' type='button'>"+opts.addAll+"</button>"+"   <button id='pRemove' class='btn btn-primary btn-sm' type='button'>"+opts.remove+"</button>"+"   <button id='pRemoveAll' class='btn btn-primary btn-sm' type='button'>"+opts.removeAll+"</button>"+" </div>"+" <div class='col-sm-5'>"+"    <select class='form-control pickListSelect' id='pickListResult' multiple></select>"+" </div>"+"</div>";this.append(pickListHtml);this.fill();this.controll();};this.init();return this;};$.fn.pickList.defaults={add:'Añadir',addAll:'Añadir todos',remove:'Remover',removeAll:'Remover todos'};var pick=$("#pickList").pickList({data:val});$("#almacenar").click(function(){var lstInterfazGuardar=new Array(),perfil_ingreso_nombre=$("#perfil_ingreso_nombre").val(),perfil_ingreso_descripcion=$("#perfil_ingreso_descripcion").val();$("#pickListResult option").each(function(){interfaz_id=$(this).attr('id');lstInterfazGuardar.push(interfaz_id);});$('#mensajes').removeClass('alert alert-danger');$('#mensajes').addClass('hide');$('#textoMensaje').text("");var bValid=false;var bValid1=validarCampoLleno(perfil_ingreso_nombre,"campo nombre","perfil_ingreso_nombre");var bValid2=validarCampoLleno(perfil_ingreso_descripcion,"campo descripción","perfil_ingreso_descripcion");var bValid3=validarCampoLleno(lstInterfazGuardar,"campo interfaz","pickList");bValid=bValid1&&bValid2&&bValid3;$("#perfil_ingreso_interfaz").val(lstInterfazGuardar);if(bValid){$.ajax({url:'/validarPerfil',type:'post',"_token":"{{ csrf_token() }}",async:true,data:{'perfil_ingreso_nombre':perfil_ingreso_nombre,'perfil_ingreso_descripcion':perfil_ingreso_descripcion,'perfil_ingreso_interfaz':lstInterfazGuardar},beforeSend:function(){$.blockUI({message:'<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});},error:function(jqXHR,textStatus,errorThrown){var gnrMensaje=$("#gnrMensaje");toTop();if(jqXHR.status=='401'){gnrMensaje.text(textStatus+" - "+errorThrown+" ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");$('#gnrError').modal('show');}else{$('#mensajes').addClass('alert alert-danger');$('#mensajes').removeClass('hide');var textoMensaje=$("#textoMensaje");textoMensaje.text("ERROR: "+jqXHR.status+" - "+textStatus+" - "+errorThrown);}},success:function(data){if(data!==''){$('#mensajes').addClass('alert alert-danger');$('#mensajes').removeClass('hide');$('#textoMensaje').text('');for(var i in data){$('#textoMensaje').append(data[i]+'<br/>');}}else{$("#target").submit();}},complete:function(){setTimeout($.unblockUI,1000);}});}});$("#buscar").click(function(){$("#perfil_ingreso_descripcion").val("");$("#perfil_ingreso_id").val("");$('#pickListResult').find('option').remove();var perfil_ingreso_nombre=$("#perfil_ingreso_nombre").val();$('#mensajes').removeClass('alert alert-danger');$('#mensajes').addClass('hide');$("#almacenar").removeClass("hide");$("#actualizar").addClass("hide");$('#textoMensaje').text("");$('div').removeClass('has-error');$('label[name=error]').addClass('hide');var bValid=false;bValid=validarCampoLleno(perfil_ingreso_nombre,"Campo nombre","perfil_ingreso_nombre");if(bValid){$.ajax({url:'/buscarPerfil',type:'post',"_token":"{{ csrf_token() }}",async:true,data:{'perfil_ingreso_nombre':perfil_ingreso_nombre},beforeSend:function(){$.blockUI({message:'<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});},error:function(jqXHR,textStatus,errorThrown){var gnrMensaje=$("#gnrMensaje");toTop();if(jqXHR.status=='401'){gnrMensaje.text(textStatus+" - "+errorThrown+" ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");$('#gnrError').modal('show');}else{$('#mensajes').addClass('alert alert-danger');$('#mensajes').removeClass('hide');var textoMensaje=$("#textoMensaje");textoMensaje.text("ERROR: "+jqXHR.status+" - "+textStatus+" - "+errorThrown);}},success:function(data){if(data!=""){var datos=eval(data.lstInterfazRelacionaPerfil);$("#perfil_ingreso_descripcion").val(data.perfil_ingreso_descripcion);$("#perfil_ingreso_id").val(data.perfil_ingreso_id);if(datos==null||datos.length==0){$('#mensajes').addClass('alert alert-danger');$('#mensajes').removeClass('hide');var textoMensaje=$("#textoMensaje");textoMensaje.text("No se han encontrado Interfaces para este Perfil. ");}else{for(var i in datos){$('#pickListResult').append('<option id='+datos[i].id+'>'+datos[i].text+' </option>');$("#pickData option[id="+datos[i].id+"]").remove();}
$("#actualizar").removeClass("hide");$("#almacenar").addClass("hide");}}else{$('#mensajes').addClass('alert alert-danger');$('#mensajes').removeClass('hide');var textoMensaje=$("#textoMensaje");textoMensaje.text("No se han encontrados datos. ");}},complete:function(){setTimeout($.unblockUI,1000);}});}});$("#actualizar").click(function(){var lstInterfazGuardar=new Array(),perfil_ingreso_id=$("#perfil_ingreso_id").val(),perfil_ingreso_nombre=$("#perfil_ingreso_nombre").val(),perfil_ingreso_descripcion=$("#perfil_ingreso_descripcion").val();$("#pickListResult option").each(function(){interfaz_id=$(this).attr('id');lstInterfazGuardar.push(interfaz_id);});$('#mensajes').removeClass('alert alert-danger');$('#mensajes').addClass('hide');$('#textoMensaje').text("");$('div').removeClass('has-error');$('label[name=error]').addClass('hide');var bValid=false;var bValid0=validarCampoLleno(perfil_ingreso_id,"campo perfil","perfil_ingreso_nombre");var bValid1=validarCampoLleno(perfil_ingreso_nombre,"campo nombre","perfil_ingreso_nombre");var bValid2=validarCampoLleno(perfil_ingreso_descripcion,"campo descripción","perfil_ingreso_descripcion");var bValid3=validarCampoLleno(lstInterfazGuardar,"campo interfaz","pickList");bValid=bValid0&&bValid1&&bValid2&&bValid3;if(bValid){$("#perfil_ingreso_interfaz").val(lstInterfazGuardar);$.ajax({url:'/validarPerfil',type:'post',"_token":"{{ csrf_token() }}",async:true,data:{'perfil_ingreso_nombre':perfil_ingreso_nombre,'perfil_ingreso_descripcion':perfil_ingreso_descripcion,'perfil_ingreso_interfaz':lstInterfazGuardar,'perfil_ingreso_id':perfil_ingreso_id},beforeSend:function(){$.blockUI({message:'<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});},error:function(jqXHR,textStatus,errorThrown){var gnrMensaje=$("#gnrMensaje");toTop();if(jqXHR.status=='401'){gnrMensaje.text(textStatus+" - "+errorThrown+" ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");$('#gnrError').modal('show');}else{$('#mensajes').addClass('alert alert-danger');$('#mensajes').removeClass('hide');var textoMensaje=$("#textoMensaje");textoMensaje.text("ERROR: "+jqXHR.status+" - "+textStatus+" - "+errorThrown);}},success:function(data){if(data!==''){$('#mensajes').addClass('alert alert-danger');$('#mensajes').removeClass('hide');$('#textoMensaje').text('');for(var i in data){$('#textoMensaje').append(data[i]+'<br/>');}}else{$('#target').attr('action','{{ url('/actualizarPerfil') }}');$("#target").submit();}},complete:function(){setTimeout($.unblockUI,1000);}});}});/*]]>*/</script>
@endsection