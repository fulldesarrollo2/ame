@extends('layouts.app')

@section('content')

<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2><span class="text-navy"><i class="fa fa-chevron-circle-right"></i> Administración de Usuarios</span></h2>
                    </div>
                </div>
            </div>
        </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox float-e-margins">
          <div class="ibox-content text-left p-md">
            <div class="wrapper wrapper-content animated fadeInRight">
              <div class="row">
                  <div class="col-lg-12">
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                   @if(Session::has('message'))
                    <div class="alert alert-success" >
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <p> {{Session::get('message')}} </p>
                    </div>
                    @endif
                    @if (notify()->ready())
                    <div class="alert alert-{{ notify()->type() }}">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <label>{{ notify()->message() }}</label>
                    </div>
                    @endif
                      <div id="mensajes" name="mensajes" class="hide">
                          <label id="textoMensaje" name="textoMensaje"></label>
                      </div>
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                              <form class="form-horizontal" method="POST" id="target" name="formCuenta" enctype="multipart/form-data"  action="{{url('/guardarUsuario')}}" role="form">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  <input type="hidden" id="cuenta_id_editar" name="cuenta_id_editar"/>

                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-2">Tipo</label>
                                        <div class="col-sm-10">
                                            @foreach($objTipoEntidad as $objTipoEntidad)
                                            <div class="radio"><label> <input type="radio" class="check_entidad" value="{{ $objTipoEntidad->tipo_entidad_id}}" id="tipo_entidad_id" required name="tipo_entidad_id">{{ $objTipoEntidad->nombre}}</label></div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-12">División Política</label>
                                        <div class="col-sm-12">
                                            <div class="panel panel-default">
                                                <div class="panel-body" id="divCuentas">
                                                    <div id="arbolDivisionPolitica" class="step-content" >
                                                    </div>
                                                </div>
                                            </div>
                                           <input type="hidden" name="division_politica_id" class="division_politica_id" id="division_politica_id">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="col-sm-4 control-label">Entidades</label>
                                        <div class="col-sm-8">
                                            <select class="form-control m-b" name="entidad_id" id="entidad_id" >
                                                <option value="">Seleccionar...</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-12">
                                      <div class="panel panel-default">
                                          <div class="panel-heading">
                                                Formulario
                                          </div>
                                          <div class="panel-body col-md-7">
                                            <div class="form-group"><label class="col-sm-3 control-label">Nombre de usuario</label>
                                              <div class="col-sm-7">
                                                <input type="text" id="usuario_nick" name="usuario_nick"  value="{{ old('usuario_nick') }}" class="form-control">
                                                </div>
                                                <div class="col-sm-1">
                                                        <button class="btn btn-primary" id="buscar" name="buscar" type="button"><i class="fa fa-search"></i>&nbsp;</button>
                                                    </div>
                                                    
                                              </div>
                                              <div class="form-group"><label class="col-lg-3 control-label">Apellidos</label>

                                              <div class="col-lg-9"><input type="text" value="{{ old('usuario_apellidos') }}" id="usuario_apellidos" name="usuario_apellidos" class="form-control"></div>
                                              </div>
                                              <div class="form-group">
                                                <label class="col-lg-3 control-label">Nombres</label>

                                                <div class="col-lg-9"><input type="text" value="{{ old('usuario_nombres') }}" id="usuario_nombres" name="usuario_nombres" class="form-control"></div>
                                              </div>
                                              <div class="form-group">
                                                <label class="col-lg-3 control-label">Identificacón</label>

                                                <div class="col-lg-9"><input type="text" value="{{ old('usuario_identificacion') }}" id="usuario_identificacion" name="usuario_identificacion" class="form-control"></div>
                                              </div>
                                              <div class="form-group">
                                                <label class="col-lg-3 control-label">Teléfono</label>

                                                <div class="col-lg-9"><input type="text" value="{{ old('usuario_telefono') }}" id="usuario_telefono" name="usuario_telefono" class="form-control"></div>
                                              </div>
                                              <div class="form-group">
                                                <label class="col-lg-3 control-label">Email</label>

                                                <div class="col-lg-9"><input type="email" value="{{ old('usuario_email') }}" id="usuario_email" name="usuario_email" class="form-control"></div>
                                              </div>
                                            </div>
                                            <div class="panel-body col-md-5">
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">Perfil</label>

                                                    <div class="col-lg-9">
                                                        <select class="form-control m-b" name="perfil_id" id="perfil_id" onchange="validarPerfil();" >
                                                            <option value="">Seleccionar...</option>
                                                            @foreach($objPerfil as $objPerfil)
                                                                <option value="{{$objPerfil->perfil_ingreso_id}}">{{$objPerfil->perfil_ingreso_nombre}}</option>
                                                             @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                    <label class="col-lg-3 control-label">Estado</label>

                                                    <div class="col-lg-9">
                                                        <div class="checkbox">
                                                          <label><input id="estado_id" name="estado_id" type="checkbox" class="check_estado" value="2">Inactivo</label>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="usuario_id" id="usuario_id">
                                                  </div>
                                              </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 text-right">
                                        <button class="btn btn-white" type="button" id="btn_limpiar">Cancelar</button>
                                        <button class="btn btn-primary" type="button" id="btn_guardar">Almacenar</button>
                                        <button class="btn btn-primary hide" type="button" id="btn_editar">Actualizar</button>
                                    </div>
                                </div>
                          </form>
                        </div>
                     </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script>

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        
var lstDivPoljs = <?php echo json_encode($objDivisionPolitica); ?>;
var arbolDivisionPolitica = dataArbolDinamico(lstDivPoljs,"division_politica_nombre","division_politica_id","division_politica","true","division_politica_codigo","division_politica_nombre");
    $('#arbolDivisionPolitica').treeview({
        levels: 1,
        color: "#555",
        showBorder: false,
        data: arbolDivisionPolitica,
        multiSelect: $('#chk-select-multi').is(':checked'),
        onNodeSelected: function (event, node) {
            $('#checkable-output').prepend('<p>' + node.text + ' was checked</p>');
                 if(node.nodes && node.nodes.length>0){
                     $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var textoMensaje =$("#textoMensaje");
                    textoMensaje.text("Error! Solo se puede seleccionar cantones. "); 
                 }else{
                      $('#mensajes').addClass('hide');
                      $('#division_politica_nom').val(node.text);
                      $('#division_politica_id').val(node.tags);
                      cargarEntidades();
                 }
        },
        onNodeUnselected: function (event, node) {
            $('#nomCtaSeleccionada').val("");
        }
    });

    $("input[name=tipo_entidad_id]").click(function () {
        cargarEntidades();
    });


function cargarEntidades(){
    toTop();
      var division_politica_id  =  $('#division_politica_id').val(),
            tipoEntidad               = $('input[name=tipo_entidad_id]:checked').val();
              $('#entidad_id').empty();

            $.ajax({
                url: '{{ url('/consultaEntRelacionaDivPoTipo') }}',
                        type: 'post',
                        async: true,
                        data: {'division_politica_id':division_politica_id,'tipoEntidad':tipoEntidad},
                         beforeSend: function() {
                       $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                             },
                        success:  function(data) {
                        console.log(data);
                        if (data == ''){
                           $('#mensajes').addClass('alert alert-danger');
                            $('#mensajes').removeClass('hide');
                            var textoMensaje =$("#textoMensaje");
                            textoMensaje.text("No se han encontrados datos. ");  
                        } else
                            {
                            $('#entidad_id').append('<option>Seleccione...</option>');
                              for(var i in data){
                                    $('#entidad_id').append('<option id='+data[i].entidad_id+' value='+data[i].entidad_id+'>'+ data[i].entidad_nombre+' </option>');
                                }
                            
                               setTimeout($.unblockUI, 1000);
                            }
                        },
                        complete: function() {
                        setTimeout($.unblockUI, 1000);
                    },
            });

}

function validarPerfil(){
    toTop();
    var ameAdministrador = <?php echo json_encode($ameAdministrador);?>,
    administrador = <?php echo json_encode($administrador);?>,
    perfilIngreso = <?php echo json_encode($perfil);?>,
    usuario = <?php echo json_encode($usuario);?>,
    perfil_id        = $( "#perfil_id option:selected" ).val();

    if( perfilIngreso == administrador & perfil_id != usuario){

        $('#mensajes').addClass('alert alert-danger');
        $('#mensajes').removeClass('hide');
        var textoMensaje =$("#textoMensaje");
        textoMensaje.text("No puede asignar este perfil. "); 
        return false;       
        
    }
    else{
        
        return true;
    }
  }



$('#btn_guardar').click(function (event) {
        toTop();
        var bValid = true,
         entidad_id       = $( "#entidad_id option:selected" ).val(),
         perfil_id        = $( "#perfil_id option:selected" ).val(),
         usuario_nick     = $("#usuario_nick").val(),
         usuario_nombres  = $("#usuario_nombres").val(),
         usuario_apellidos= $("#usuario_apellidos").val(),
         usuario_identificacion = $("#usuario_identificacion").val(),
         usuario_telefono = $('#usuario_telefono').val(),
         usuario_email    = $('#usuario_email').val(),
         estado_id        = $('#estado_id:checked').val();

    
        
        bValid = bValid && validarCampoLleno(entidad_id, 'Entidad');
        bValid = bValid && validarCampoLleno(usuario_nick, 'Usuario Nombre');
        bValid = bValid && validarCampoLleno(usuario_nombres, 'Nombres');
        bValid = bValid && validarCampoLleno(usuario_apellidos, 'Apellidos');
        bValid = bValid && validarCampoLleno(usuario_identificacion, 'Identificación');
        bValid = bValid && validarCampoLleno(usuario_telefono, 'Teléfono');
        bValid = bValid && validarCampoLleno(usuario_email, 'Email');
        bValid = bValid && validarCampoLleno(perfil_id, 'Perfil');
        bvalid = bValid && validarCedula();
        bValid = bValid && validarPerfil();
        
        
        if (bValid == true){
            $('#mensajes').removeClass('alert alert-danger');
            $('#mensajes').addClass('hide');
            $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
            $( "#target" ).submit();
            setTimeout($.unblockUI, 3000);
        }
       
    });

$( "#usuario_identificacion" ).change(function() {
toTop();
  bvalid = validarCedula();

});

 $("#buscar").click(function() {
        toTop();
        $("#usuario_apellidos").val("");
        $("#usuario_nombres").val("");
        $("#usuario_identificacion").val("");
        $("#usuario_telefono").val("");
        $("#usuario_email").val("");
        $('#entidad_id').val("");
        $('#perfil_id').val("");
        $('#estado_id').val("");


        $( "#btn_editar" ).removeClass("hide");
        $( "#btn_guardar" ).addClass("hide");

        var usuario_nick = $("#usuario_nick").val();

        var bValid = true;
        bValid = bValid && validarCampoLleno(usuario_nick, "Campo nombre de usuario")

        if(bValid){
            $('#mensajes').removeClass('alert alert-danger');
            $('#mensajes').addClass('hide');
            
               $.ajax({
                url: '{{ url('/buscarUsuario') }}',
                        type: 'post',
                        async: true,
                        data: {
                            'usuario_nick':usuario_nick
                              },
                        beforeSend: function() {
                            $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                             },
                        success:  function(data) {
                            console.log(data);
                        if (data == ''){
                           $('#mensajes').addClass('alert alert-danger');
                            $('#mensajes').removeClass('hide');
                            var textoMensaje =$("#textoMensaje");
                            textoMensaje.text("No se han encontrados datos. ");  
                        } else {
                            for(var i in data){
                                $("#usuario_id").val(data[i].usuario_id)
                                $("#usuario_apellidos").val(data[i].usuario_apellidos);
                                $("#usuario_nombres").val(data[i].usuario_nombres);
                                $("#usuario_identificacion").val(data[i].usuario_identificacion);
                                $("#usuario_telefono").val(data[i].usuario_telefono);
                                $("#usuario_email").val(data[i].usuario_email);
                                var value = data[i].tipo_entidad_id;

                                $("input[class='check_entidad'][value='"+value+"']").prop("checked", true);
                                /*Combo Entidad*/
                                $('#entidad_id').append($('<option>', {
                                    value: data[i].entidad_id,
                                    text: data[i].entidad_nombre
                                }));
                                $('#entidad_id').val(data[i].entidad_id);
                                $('#perfil_id').val(data[i].perfil_ingreso_id);
                                /*Muestra el estado del usuario*/
                                $inactivo = 2;
                                (data[i].estado_id== $inactivo)?($("input[class='check_estado']").prop("checked", true)):($("input[class='check_estado']").prop("checked", false));
                              }
                               setTimeout($.unblockUI, 1000);
                            }
                        },
                        complete: function() {
                        setTimeout($.unblockUI, 1000);
                    },
            });

        }
    });

    $('#btn_editar').click(function (event) {
        toTop();
       var bValid = true,
         entidad_id       = $( "#entidad_id option:selected" ).val(),
         perfil_id        = $( "#perfil_id option:selected" ).val(),
         usuario_nick     = $("#usuario_nick").val(),
         usuario_nombres  = $("#usuario_nombres").val(),
         usuario_apellidos= $("#usuario_apellidos").val(),
         usuario_identificacion = $("#usuario_identificacion").val(),
         usuario_telefono = $('#usuario_telefono').val(),
         usuario_email    = $('#usuario_email').val(),
         estado_id        = $('#estado_id:checked').val();
         
         if( estado_id == "" || estado_id == undefined ){
            estado_id ='1';
         }
        
        bValid = bValid && validarCampoLleno(entidad_id, 'Entidad');
        bValid = bValid && validarCampoLleno(usuario_nick, 'Usuario Nombre');
        bValid = bValid && validarCampoLleno(usuario_nombres, 'Nombres');
        bValid = bValid && validarCampoLleno(usuario_apellidos, 'Apellidos');
        bValid = bValid && validarCampoLleno(usuario_identificacion, 'Identificación');
        bValid = bValid && validarCampoLleno(usuario_telefono, 'Teléfono');
        bValid = bValid && validarCampoLleno(usuario_email, 'Email');
        bValid = bValid && validarCampoLleno(perfil_id, 'Perfil');
        bvalid = validarCedula();
        
        
        if (bValid == true){
            $('#mensajes').removeClass('alert alert-danger');
            $('#mensajes').addClass('hide');
            $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
            $('#target').attr('action', '{{ url('/actualizarUsuario') }}');
            $( "#target" ).submit();
            setTimeout($.unblockUI, 3000);
        }
       
    });

  $('#btn_limpiar').click(function (event) {
        toTop();
        $('#mensajes').removeClass('alert alert-danger alert alert-warning');
        $('#mensajes').addClass('hide');
        $('#btn_guardar').removeClass('hide');
        $('#btn_editar').addClass('hide');

        $("#usuario_nick").val("");
        $("#usuario_apellidos").val("");
        $("#usuario_nombres").val("");
        $("#usuario_identificacion").val("");
        $("#usuario_telefono").val("");
        $("#usuario_email").val("");
        $('#entidad_id').val("");
        $('#perfil_id').val("");
        $('#estado_id').val("");

        $("#tipo_entidad_id").attr('disabled',false);
        
        document.formCuenta.action= " {{url('/guardarUsuario')}} ";
        var radios = document.getElementsByName('tipo_fuente_id');
        for (var i = 0; i< radios.length;  i++){
            radios[i].disabled = false;
            radios[i].checked = false;
        }

        $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
        $('#arbolCuentas').treeview({
            data: null,
        });
        setTimeout($.unblockUI, 1000);
        
    });
 


function validarCedula(){
    var cedula = $("#usuario_identificacion").val();
         if(cedula.length == 10){
            var digito_region = cedula.substring(0,2);
                if( digito_region >= 1 && digito_region <=24 ){
    var ultimo_digito   = cedula.substring(9,10);
    var pares = parseInt(cedula.substring(1,2)) + parseInt(cedula.substring(3,4)) + parseInt(cedula.substring(5,6)) + parseInt(cedula.substring(7,8));
    var numero1 = cedula.substring(0,1);
    var numero1 = (numero1 * 2);
    if( numero1 > 9 ){ var numero1 = (numero1 - 9); }
        var numero3 = cedula.substring(2,3);
        var numero3 = (numero3 * 2);
        if( numero3 > 9 ){ var numero3 = (numero3 - 9); }
            var numero5 = cedula.substring(4,5);
            var numero5 = (numero5 * 2);
            if( numero5 > 9 ){ var numero5 = (numero5 - 9); }
                var numero7 = cedula.substring(6,7);
                var numero7 = (numero7 * 2);
                if( numero7 > 9 ){ var numero7 = (numero7 - 9); }
                    var numero9 = cedula.substring(8,9);
                    var numero9 = (numero9 * 2);
                    if( numero9 > 9 ){ var numero9 = (numero9 - 9); }
                        var impares = numero1 + numero3 + numero5 + numero7 + numero9;
                        var suma_total = (pares + impares);
                        var primer_digito_suma = String(suma_total).substring(0,1);
                        var decena = (parseInt(primer_digito_suma) + 1)  * 10;
                        var digito_validador = decena - suma_total;
                        if(digito_validador == 10)
                          var digito_validador = 0;
                        if(digito_validador == ultimo_digito){
                            $('#mensajes').removeClass('alert alert-danger'); 
                            
                            return true;
                        }else{

                            $('#mensajes').addClass('alert alert-danger');
                            $('#mensajes').removeClass('hide');
                            var textoMensaje =$("#textoMensaje");
                            textoMensaje.text("La cédula es Incorrecta. ");  
                            return false;
                        }

                      }else{
                         $('#mensajes').addClass('alert alert-danger');
                            $('#mensajes').removeClass('hide');
                            var textoMensaje =$("#textoMensaje");
                            textoMensaje.text("La cédula no pertenece a ninguna ciudad. ");   
                        return false;
                      }
                   }else{
                        $('#mensajes').addClass('alert alert-danger');
                            $('#mensajes').removeClass('hide');
                            var textoMensaje =$("#textoMensaje");
                            textoMensaje.text("La cédula tiene 10 dígitos. ");  
                        return false;
                   }    
    }
    
  
    
    
    
    
 
 
</script>
@endsection