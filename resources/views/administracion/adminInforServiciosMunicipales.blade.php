@extends('layouts.app')

@section('header')
  <h1>
    Administración de Informaci&oacute;n de Servicios B&aacute;sicos Municipales
  </h1>
  <ol class="breadcrumb">
    <li><a><i class="glyphicon glyphicon-th-large"></i>Administración</a></li>
    <li class="active">Servicios Basicos Municipales</li>
  </ol>
<br/>
@stop

@section('content')

<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Registro Inventario y Costeo</h3>
  </div>
  <div class="box-body">
    <div class="row">
      
      {!! Form::open(['url'=>'infServBasicosMuninc', 'method'=>'POST','id'=>'servBasicoForm','class'=>'form-horizontal']) !!} 
        <div class="col-md-6">
          <div class="box-body">
            <div class="form-group">  
              {!! Form::label('proyectos','Proyectos Registrados',['class'=>'col-sm-4 control-label']) !!}  
              
              <div class="col-sm-8">
                <select name="proyectos" id="proyectos" class="form-control">
                  <option value="">Seleccione</option>
                  @foreach ($lstProyectoServicio as $lstProSer)
                    <option value="{{ $lstProSer->proyecto_servicio_id }}">{{ $lstProSer->proyecto_servicio_nombre }}</option>
                  @endforeach
                </select>
              </div>
            </div>      
        
            <div class="form-group">            
              {!! Form::label('subsistema','Subsistema del Proyecto',['class'=>'col-sm-4 control-label']) !!}
              <div class="col-sm-8">
                {!! Form::select('subsistema',[null=>'Seleccione'],null,['class'=>'form-control']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('periodo','Periodo Vigente',['class'=>'col-sm-4 control-label']) !!}
              <div class="col-sm-8">
                {!! Form::text('periodo',$objVariableSeguridad->variable_seguridad_valor,['class'=>'form-control','readonly'=>'readonly']) !!}
                {!! Form::hidden('seguridadPeriodo', $objVariableSeguridad->variable_seguridad_id, []) !!}
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box-body">
            
            <div class="form-group">
              {!! Form::label('unidadesDeServicios','Unidades de Servicios',['class'=>'col-sm-4 control-label']) !!}
              <div class="col-sm-8">
                <select name="unidad_servicio" id="unidad_servicio" class="form-control">
                  <option value="">Seleccione</option>
                  @foreach ($lstUnidadServicio as $lstUniSer)
                    <option value="{{ $lstUniSer->unidad_servicio_id }}">{{ $lstUniSer->unidad_servicio_nombre }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group">  
              {!! Form::label('tipo','Tipos',['class'=>'col-sm-4 control-label']) !!}
              <div class="col-sm-8">
                {!! Form::select('tipos',[null=>'Seleccione'],null,['class'=>'form-control','id'=>'tipos']) !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('subTipo','Subtipo',['class'=>'col-sm-4 control-label']) !!}
              <div class="col-sm-8">
                {!! Form::select('subTipo',[""=>'Seleccione'],null,['class'=>'form-control']) !!}
              </div>
            </div>
          </div>
        </div>
      <div class="col-md-12">
        <div class="box box-default">
          <div class="col-md-6">
            
              <div class="box-body">
                <div class="form-group">
                  {!! Form::label('caudalSalidaDia','Caudal Salida (L/Dìa)',['class'=>'control-label']) !!}
                  {!! Form::text('caudalSalidaDia',NULL,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                  {!! Form::label('caudalSalidaAnio','Caudal Salida (M3/Año)',['class'=>'control-label']) !!}
                  {!! Form::text('caudalSalidaAnio',NULL,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                  {!! Form::label('TiempoExistenciaAnio','Tiempo de Existencia (Años)',['class'=>'control-label']) !!}
                  {!! Form::text('TiempoExistenciaAnio',NULL,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                  {!! Form::label('vidaUtil','Vida Util',['class'=>'control-label']) !!}
                  {!! Form::text('vidaUtil',NULL,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                  {!! Form::label('ubicacionUTM','Ubicacion (UTM)',['class'=>'control-label']) !!}
                  {!! Form::text('ubicacionUTM',NULL,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                  {!! Form::label('numUnidades','# de Unidades',['class'=>'control-label']) !!}
                  {!! Form::text('numUnidades',NULL,['class'=>'form-control']) !!}
                </div>
              </div>
          </div>
        
        <div class="col-md-6">
          
            <div class="box-body">
              <div class="form-group">
                {!! Form::label('capacidad','Capacidad',['class'=>'control-label']) !!}
                
                  {!! Form::text('capacidad',NULL,['class'=>'form-control']) !!}
                
              </div>

              <div class="form-group">
                {!! Form::label('caudalEntrada','Caudal Entrada (L/S)',['class'=>'control-label']) !!}
                
                  {!! Form::text('caudalEntrada',NULL,['class'=>'form-control']) !!}
                
              </div>

              <div class="form-group">
                {!! Form::label('caudalSalidaLS','Caudal Salida (L/S)',['class'=>'control-label']) !!}
                
                  {!! Form::text('caudalSalidaLS',NULL,['class'=>'form-control']) !!}
                
              </div>

              <div class="form-group">
                {!! Form::label('perdidas','Perdidas %',['class'=>'control-label']) !!}
                
                  {!! Form::text('perdidas',NULL,['class'=>'form-control']) !!}
                
              </div>

              <div class="form-group">
                {!! Form::label('estadoFuncionamiento','Estado de Funcionamiento',['class'=>'control-label']) !!}
                
                  {!! Form::select('estadoFuncionamiento',['Bueno','Regular','Malo'],NULL,['class'=>'form-control','placeholder'=>'Seleccione']) !!}
              </div>
              <div class="form-group box-footer text-right">
                {!! Form::button('CANCELAR',['class'=>'btn btn-white','id'=>'cancelar']) !!}
                {!! Form::submit('GUARDAR',['class'=>'btn btn-primary']) !!}
                
              </div>

            </div>
          </div>
        </div>
      </div>
      {!! Form::close() !!}
     </div>
  </div>
</div>
@endsection


@section('script')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\adminServMuninRequest', '#servBasicoForm') !!}

<script type="text/javascript">
  $(document).ready(function(){
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    $("#proyectos").change(function(){
        var proyectoServicioId = $('#proyectos').val();
        
        $.ajax({
          url: '{{ url('/buscarSubsistemaXproyecto') }}',
          type: 'POST',
          data: {'proyecto_servicio_id': proyectoServicioId},
          success: function(data){
            $('#subsistema').empty();
            $('#subsistema').append('<option value="">Seleccione</option>');
            $.each(data,function(index, objSubSistema){
              $('#subsistema').append('<option value="'+objSubSistema.sub_sistema_id+'">'+objSubSistema.sub_sistema_nombre+'</option>');
            });
          },
        });
    });
});
</script>

<script type="text/javascript">
  $(document).ready(function(){
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    $('#unidad_servicio').change(function(){
      var unidadServicio = $('#unidad_servicio').val();
      $.ajax({
        url: '{{ url('/buscarVariableCosteoXunidadServicio') }}',
        type: 'POST',
        data: {'unidad_servicio_id': unidadServicio},
        success: function(data){
          $('#tipos').empty();
          $('#tipos').append('<option value="">Seleccione</option>');
          $.each(data, function(index,objVarCosto){
            $('#tipos').append('<option value="'+objVarCosto.unidad_servicio_id+'">'+objVarCosto.unidad_servicio_nombre+'</option>');
          });
        }
      });
    });
  });
</script>
<script type="text/javascript">
  $.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $('#tipos').change(function(){
    var tipoServicioID = $('#tipos').val();
    
    $.ajax({
      url: "{{ url('/buscarSubTipoXUnidadServicio') }}",
      type: 'POST',
      data: {'tipoServ_id': tipoServicioID},
      success: function(data){
        $('#subTipo').empty();
        $('#subTipo').append('<option value="">Seleccione</option>');
        $.each(data, function(index,objSubTipo){
          $('#subTipo').append('<option value ="'+objSubTipo.unidad_servicio_id+'">'+objSubTipo.unidad_servicio_nombre+'</option>');
        })
      }
    });
  });

    
</script>

<script type="text/javascript">
      /* 
       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
         
         
           $("#proyecto_servicio_id").change(function (event){
               var proyectoServicioId=$("#proyecto_servicio_id").val();
               
                    $.ajax({
                         url: '{{ url('/buscarSubsistemaXproyecto') }}',
                         type: 'POST',
                         data: {'proyecto_servicio_id': proyectoServicioId},
                         beforeSend: function() {
                            
                          },
                         error : function(jqXHR, textStatus, errorThrown) {
                            var gnrMensaje =$("#gnrMensaje");
                            if(jqXHR.status == '401'){
                                gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar."); 
                                $('#gnrError').modal('show');
                            }     
                        },
                         success: function(data) {
                            
                         var cadena = JSON.stringify(data);
                         var materias= verTipoArchivo(data);
                                var message='<select id="subsistema_id" name="subsistema_id"  class="form-control">'+materias+
                                            '</select>';
                               $("#cmbSubsistema").empty();
                               $("#cmbSubsistema").append(message);  
                     },
                     complete: function() {
                        
                    }
                 });
                    function verTipoArchivo(data){
                         var tregistros = '<option value="">Seleccione..</option>';
                             for(var i in data){
                                 tregistros =tregistros + '<option value='+data[i].sub_sistema_id+'>'+ data[i].sub_sistema_nombre +' </option>';

                          }
                            return tregistros;
                    }
            }); 
    **/
    
</script>
@endsection
    