@extends('layouts.app')

@section('content')

<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2><span class="text-navy"><i class="fa fa-chevron-circle-right"></i> Administración de periodos</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-center p-md">
                        <div class="row">
                                <div class="form-group col-md-12" >
                                     @if(Session::has('message'))
                                    <div class="alert alert-success" >
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <p class="text-left"> {{Session::get('message')}} </p>
                                    </div>
                                    @endif
                                    @if (session()->has('mensajeerror')) 
                                    <div class="alert alert-danger">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <ul>
                                            <li>{{ session()->pull('mensajeerror') }}</li>			            
                                        </ul>
                                    </div>
                                    @endif
                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif

                                </div>
                        </div>
                                <div id="mensajes" data-dismiss="alert" role="alert" class="text-left" >                            
                                    <p class="text-left" id="textoMensaje"></p>
                                </div>
                        
                        @if(Auth::user()->perfil_ingreso_id==$ameTecnico)
                               <div class="form-group">
                                    <form class="form-horizontal" method="POST" name="formCerrarPeriodo" enctype="multipart/form-data" action="{{url('/cierrePeriodo')}}"   role="form">     
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">   
                                      <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="panel panel-default">
                                                <div class="panel-heading text-left">
                                                    Cierre de periodo
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <label class="col-lg-2 control-label" >Periodo Actual :</label>
                                                    <div class="col-lg-5">
                                                        <input id="periodo_anio" type="text" readonly="true" name="periodo_anio" class="form-control" value="{{$objPeriodo->periodo_anio}}" ></input>
                                                        <input id="periodo_id" type="hidden" readonly="true" name="periodo_id" class="form-control" value="{{$objPeriodo->periodo_id}}" ></input>
                                                     </div>
                                                    <div class="col-lg-4">
                                                        <button type="button"  class="btn btn-primary" data-target="#modal_confirmacion" data-toggle="modal" data-toggle="tooltip" data-placement="left" title="Cerrar Periodo" >Cerrar Periodo</button>
                                                    </div>
                                                </div>
                                                <br>
                                          </div>
                                        </div>
                                    </div>
                                   </form>
                               </div>
                         @endif
                               <div id="modal_confirmacion" class="modal fade" data-toggle="modal">
                                    <div class="modal-dialog" role="document">
                                         <div class="modal-content">
                                           <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                             <h4 class="modal-title">Cierre de periodo</h4>
                                           </div>
                                                    <div class="modal-body">
                                                      <p>Confirmación de cierre de periodo</p>
                                                    </div>
                                                   <div class="modal-footer">
                                                       <button type="submit" class="btn btn-default" id="btn_aceptar" >Aceptar</button>
                                                       <button type="button" class="btn btn-primary" id="btn_cancelar" >Cancelar</button>
                                                   </div>
                                                </form>
                                         </div><!-- /.modal-content -->
                                     </div><!-- /.modal-dialog -->
                                </div>
                             
                         
                        
                         
                        @if(Auth::user()->perfil_ingreso_id==$entidad)
                                <form class="form-horizontal" method="POST" name="formInformacionFinanciera" enctype="multipart/form-data"  action="{{url('/cambioPeriodoFiscal')}}" role="form">
                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                       <div id="errorGuardar" data-dismiss="alert" role="alert" class="text-left" >                            
                                              <p class="validaAgregar"></p>
                                       </div>
                                       <div class="form-group">

                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading text-left">
                                                              Cambio de Periodo Fiscal
                                                        </div>
                                                        <br>
                                                        <div class="form-group">   
                                                           <div class="row">
                                                               <label class="col-lg-2 control-label" >Periodo Actual :</label>
                                                               <div class="col-lg-5">
                                                                    <input id="periodo_anio" type="text" readonly="true"  maxlength="13" name="periodo_anio" class="form-control" value="{{$objPeriodo->periodo_anio}}" ></input>
                                                                    <input id="periodo_id" type="hidden" readonly="true" name="periodo_id" class="form-control" value="{{$objPeriodo->periodo_id}}" ></input>
                                                               </div>
                                                               <div class="col-lg-4">
                                                                   
                                                                   @if($subirArchivo) 
                                                                   <button    title="Procesar periodo" id="btn_procesar_id"  class="btn btn-primary"   data-placement="left" title="Cerrar Periodo">Procesar</button>
                                                                    @else 
                                                                     
                                                                     @endif
                                                                     <button type="button" class="btn btn-primary" id="btn_cancelarPeriodoFiscal" >Cancelar</button>
                                                               </div>
                                                           </div>   
                                                        </div> 
                                                        <div class="form-group">
                                                            <div class="row">
                                                             <label class="col-lg-4 control-label" >Información financiera de Cierre de Ejercicio :</label>
                                                             </div>
                                                        </div>
                                                        <div class="form-group"> 
                                                            
                                                           <div class="row"> 
                                                                   <label class="col-lg-3 " for="name" style=""><i class="fa fa-folder-open" aria-hidden="true"></i>
                                                                   Adjuntar Archivo </label>
                                                                   <div class="col-lg-3">
                                                                       <div class="btn btn-default btn-file">
                                                                         <i class="fa fa-paperclip"></i> Adjuntar archivo
                                                                         <input onchange="checkfile(this);" type="file" accept=".csv" required pattern=".+\.(csv)"  id="archivo_carga_archivo_byte" name="archivo_carga_archivo_byte" class="" >
                                                                       </div>
                                                                       <p class="help-block"  >Max. 20MB</p>
                                                                       <div id="texto_notificacion">
                                                                      </div>
                                                                   </div>

                                                           </div>
                                                            
                                                            
                                                        </div>
                                                        <div class="row">

                                                        </div>
                                                  </div>
                                                </div>
                                            </div>


                                       </div>
                                </form>
                               
                         @endif
                                
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
<script>
         function checkfile(sender) {
                 var validExts = new Array(".csv");
                 var fileExt =$("#archivo_carga_archivo_byte").val();
                 //var fileExt = sender.value;
                 fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
                 if (validExts.indexOf(fileExt) < 0) {
                   
                    $('#mensajes').removeClass();
                    $('#mensajes').addClass('alert alert-danger');
                    var tips = $("#textoMensaje");
                    tips.text("Archivo no válido seleccionado, los archivos válidos son de tipo " +
                            validExts.toString() + " .");
                   $("#archivo_carga_archivo_byte").val("");     
                   return false;
                 }
                 else return true;
             }

</script>
@endsection
@section('script')
<script>
    
     $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("#btn_cierrePeriodo").click(function(){
            $("#modal_confirmacion").show();

         });

         $("#btn_cancelar").click(function(){
              $("#modal_confirmacion").hide();
         });
         
        
         $("#btn_cancelaInfFinanciera").click(function(){
              $("#modal_InformaFinanciera").hide();
               
         });
        
         
         $("#btn_cancelarPeriodoFiscal").click(function(){
              $('#mensajes').removeClass();
              var tips = $("#textoMensaje");
                  tips.text("");
              $("#archivo_carga_archivo_byte").val(""); 
         });
         
         
        $("#btn_procesar_id").click(function(){ 
              var bValid=true;
              var archivo=$("#archivo_carga_archivo_byte").val(); 
              bValid = bValid && validarCampoLleno(archivo, "Adjuntar archivo para cierre del ejercicio");
               $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
               setTimeout($.unblockUI, 2000);
              if(bValid){
                  document.formInformacionFinanciera.submit();
                  
              }
         });
         
          $("#btn_aceptar").click(function(){
             $("#modal_confirmacion").hide();
             $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
             setTimeout($.unblockUI, 2000);
             document.formCerrarPeriodo.submit();
         });
         
        
         
    });
</script>
@endsection
