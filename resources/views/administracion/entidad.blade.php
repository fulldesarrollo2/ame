@extends('layouts.app')
@section('content')
<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2><span class="text-navy"><i class="fa fa-chevron-circle-right"></i> Entidades</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                <div class="col-lg-12">
                                    @if(Session::has('message'))
                                    <div class="alert alert-success" >
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <p> {{Session::get('message')}} </p>
                                    </div>
                                    @endif
                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    <div id="mensajes" data-dismiss="alert" role="alert" class="text-left" >                            
                                        <p id="textoMensaje"></p>
                                    </div>
                                    <form class="form-horizontal" id="target" method="POST" name="formGuardarPac" enctype="multipart/form-data"  action="{{url('/guardarEntidad')}}" role="form">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                        <input type="hidden" name="entidad_listNuev" id="entidad_listNuev">
                                        <input type="hidden" name="entidad_listAnt" id="entidad_listAnt">
                                        <input id="entidad_id" type="hidden" name="entidad_id" class="form-control" ></input>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" >Ruc</label>
                                            <div class="col-lg-4">
                                                <input id="entidad_ruc" type="text"  maxlength="13" name="entidad_ruc" class="form-control"  ></input>
                                            </div>
                                            <label class="col-lg-1 control-label " >Nombre</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control" maxlength="254" name="entidad_nombre" id="entidad_nombre" style="text-transform:uppercase;"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" >Codigo Ente Contable</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" maxlength="254" name="entidad_codigo_ente" id="entidad_codigo_ente" />
                                            </div>
                                            <label class="col-lg-1 control-label" >Dirección</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control" maxlength="254" name="entidad_direccion" id="entidad_direccion" style="text-transform:uppercase;"/>
                                            </div>

                                        </div>


                                        <div class="form-group " >
                                            <label class="col-lg-2 control-label" >Nombre máxima autoridad </label>
                                            <div class="col-lg-4">
                                                <input type="email" class="form-control" maxlength="254" name="entidad_autoridad" id="entidad_autoridad" style="text-transform:uppercase;"/>
                                            </div>
                                            <label class="col-lg-1 control-label" >Teléfonos</label>
                                            <div class="col-lg-5">
                                                <input type="number" maxlength="20" class="form-control" name="entidad_telefono" id="entidad_telefono" min="0"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">División Politica</label>
                                            <div class="col-sm-4">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <ul id="treeview-checkable" style="text-align:left;">
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <label class="col-sm-1 control-label" >Tipo</label>
                                            <div class="col-sm-5">
                                                <select class="form-control" id="tipo_entidad_id" name="tipo_entidad_id">
                                                    <option  value="">Seleccione...</option> 
                                                    @foreach($lstTipoEntidad as $select) 
                                                    <option  value="{{$select->tipo_entidad_id}}">{{$select->nombre}}</option> 
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>



                                        <div class="form-group text-right">
                                            <button type="button" class="btn btn-white check-node" id="btn_editar">ACTUALIZAR</button>
                                            <button type="button" class="btn btn-primary" id="btn_guardar" >GUARDAR</button>

                                        </div>




                                    </form>

                                    <div class="hr-line-dashed"></div>

                                    <table id="lstEntidad" class="table disp"  cellspacing="0" width="100%">
                                        <thead>
                                            <tr>  
                                                <th>&nbsp;&nbsp;COD.</th>
                                                <th>&nbsp;&nbsp;Nº RUC</th>
                                                <th>&nbsp;&nbsp;Nombre</th>
                                                <th>&nbsp;&nbsp;Tipo</th>
                                                <th></th>
                                            </tr>
                                        </thead>  
                                        <tbody>    
                                            @foreach($lstEntidad as $selectEntidad)
                                            <tr data-id="{{$selectEntidad}}">
                                                <td>&nbsp;&nbsp;{{$selectEntidad->entidad_id}} </td>
                                                <td>&nbsp;&nbsp;{{$selectEntidad->entidad_ruc}} </td>
                                                <td>&nbsp;&nbsp;{{$selectEntidad->entidad_nombre}}</td>
                                                <td>&nbsp;&nbsp;{{$selectEntidad->tipoEntidad->nombre}}</td>
                                                <td>&nbsp;&nbsp;
                                                    <a   class="fa fa-check btn_selectEntidades" type="button" title="seleccione" >  </a> 
                                                </td>     
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function validarRuc() {
            var number = document.getElementById('entidad_ruc').value;
            var dto = number.length;
            var valor;
            var acu = 0;
            if (number == "") {
                $('#mensajes').removeClass();
                $('#mensajes').addClass('alert alert-danger');
                var tips = $("#textoMensaje");
                tips.text("No has ingresado ruc, porfavor ingresar el datos correspondiente.");
                return false;
            } else {
                for (var i = 0; i < dto; i++) {
                    valor = number.substring(i, i + 1);
                    if (valor == 0 || valor == 1 || valor == 2 || valor == 3 || valor == 4 || valor == 5 || valor == 6 || valor == 7 || valor == 8 || valor == 9) {
                        acu = acu + 1;
                    }
                }
                if (acu == dto) {
                    while (number.substring(10, 13) != 001) {
                        $('#mensajes').removeClass();
                        $('#mensajes').addClass('alert alert-danger');
                        var tips = $("#textoMensaje");
                        tips.text("Los tres últimos dígitos no tienen el código del RUC 001.");
                        return false;


                    }
                    while (number.substring(0, 2) > 24) {
                        $('#mensajes').removeClass();
                        $('#mensajes').addClass('alert alert-danger');
                        var tips = $("#textoMensaje");
                        tips.text("Los dos primeros dígitos no pueden ser mayores a 24.");
                        return false;

                    }
                    // alert('Se procederá a analizar el respectivo RUC.');
                    var porcion1 = number.substring(2, 3);
                    if (porcion1 < 6) {
                        $('#mensajes').removeClass();
                        $('#mensajes').addClass('alert alert-success');
                        var tips = $("#textoMensaje");
                        tips.text("El RUC está escrito correctamente. El tercer dígito es menor a 6, por lo \ntanto el usuario es una persona natural.");
                        return true;
                    } else {
                        if (porcion1 == 6) {
                            $('#mensajes').removeClass();
                            $('#mensajes').addClass('alert alert-success');
                            var tips = $("#textoMensaje");
                            tips.text("El RUC está escrito correctamente. El tercer dígito es igual a 6, por lo \ntanto el usuario es una entidad pública.");
                            return true;
                        } else {
                            if (porcion1 == 9) {
                                $('#mensajes').removeClass();
                                $('#mensajes').addClass('alert alert-success');
                                var tips = $("#textoMensaje");
                                tips.text("El RUC está escrito correctamente. El tercer dígito es igual a 9, por lo \ntanto el usuario es una sociedad privada.");
                                return true;
                            } else {
                                $('#mensajes').removeClass();
                                $('#mensajes').addClass('alert alert-success');
                                var tips = $("#textoMensaje");
                                tips.text("El RUC es incorrecto.");
                                return false;
                            }
                        }
                    }
                } else {
                    $('#mensajes').removeClass();
                    $('#mensajes').addClass('alert alert-danger');
                    var tips = $("#textoMensaje");
                    tips.text("Campo Ruc no ingresar texto.");
                    return false;

                }
            }
        }

        $('#lstEntidad').DataTable({
            responsive: true
        });

        $("#btn_editar").prop('disabled', true)

        function dataArbol(lstArbol, a) {

            var lst = [];
            for (var i = 0; i < lstArbol.length; i++) {
                var raiz = lstArbol[i];
                var arbol = {text: "", href: "", tags: 0};
                //Longitud los hijos del arbol
                var tamanioHijos = raiz.division_politica ? raiz.division_politica.length : 0;
                arbol.text = raiz.division_politica_nombre;
                arbol.href = raiz.division_politica_id;
                arbol.tags = [tamanioHijos];
                arbol.nivel = a;
                //Si el arbol tiene raices, se le agregan los hijos
                if (tamanioHijos > 0) {
                    a++;
                    arbol.nodes = dataArbol(raiz.division_politica, a);
                    a--;
                }
                lst.push(arbol);
            }
            return lst;
        }

        var lstInterfazjs = <?php echo json_encode($lstDivision); ?>;
        var activo =<?php echo json_encode($estadoActivo); ?>;
        var inactivo =<?php echo json_encode($estadoInactivo); ?>;
        var mancomunidad =<?php echo json_encode($mancomunidad); ?>;

        var menu = [];
        var a = 1;
        var menu = dataArbol(lstInterfazjs, a);

        var lsySelect = Array();
        var $checkableTree = $('#treeview-checkable').treeview({
            data: menu,
            showIcon: false,
            showCheckbox: true,
            levels: 1,
            color: "#555",
            showBorder: false,
            onNodeChecked: function (event, node) {
                $('#checkable-output').prepend('<p>' + node.text + ' was checked</p>');

                if (node.nivel < 3) {
                    $('#mensajes').addClass('alert alert-danger');
                    var tips = $("#textoMensaje");
                    tips.text("Error !  solo puede seleccionar Cantones ");
                    $checkableTree.treeview('uncheckNode', [node.nodeId, {silent: true}])
                } else {
                    lsySelect.push(node);
                }

            },
            onNodeUnchecked: function (event, node) {
                $('#checkable-output').prepend('<p>' + node.text + ' was unchecked</p>');
                for (var i = 0; i < lsySelect.length; i++) {
                    if (lsySelect[i].href == node.href) {
                        lsySelect.splice(i, 1);
                        break;
                    }
                }


            }
        });
        var findCheckableNodess = function () {


        };
        var checkableNodes = findCheckableNodess();

        $("#btn_editar").click(function () {
            var bValid = true;
            var entidad_ruc = $("#entidad_ruc").val();
            var entidad_nombre = $("#entidad_nombre").val();
            var entidad_codigo_ente = $("#entidad_codigo_ente").val().toUpperCase();
            var entidad_direccion = $("#entidad_direccion").val().toUpperCase();
            var entidad_autoridad = $("#entidad_autoridad").val().toUpperCase();
            var entidad_telefono = $("#entidad_telefono").val().toUpperCase();
            var tipo_entidad_id = $("#tipo_entidad_id").val();
            var entidad_id = $("#entidad_id").val();



            bValid = bValid && validarCampoLleno(entidad_ruc, "Nombre   Ruc");
            bValid = bValid && validarCampoLleno(entidad_nombre, "Nombre de Entidad");
            bValid = bValid && validarCampoLleno(entidad_codigo_ente, "Nombre Codigo Ente Contable");
            bValid = bValid && validarCampoLleno(entidad_direccion, "Nombre Dirección");
            bValid = bValid && validarCampoLleno(entidad_autoridad, "Nombre Nombre máxima autoridad");
            bValid = bValid && validarCampoLleno(entidad_telefono, "Nombre Teléfonos");
            bValid = bValid && validarCampoLleno(tipo_entidad_id, "Nombre Tipo");

            var lstarrayanq = new Array();
            var lstarraynuev = new Array();
            var lstActivasMancomunadas = new Array();
            for (var i = 0; i < lsySelect.length; i++) {
                if (lsySelect[i].estado_id == inactivo) {

                    lstarrayanq.push(lsySelect[i].ent_relaciona_div_po_id);

                }
                if (lsySelect[i].estado_id == activo) {
                    lstActivasMancomunadas.push(lsySelect[i].ent_relaciona_div_po_id);
                }

                if (lsySelect[i].href != null) {
                    lstarraynuev.push(lsySelect[i].href);
                }
            }

            if (tipo_entidad_id != mancomunidad)
            {
                if (lstarraynuev.length > 1)
                {
                    $('#mensajes').removeClass();
                    $('#mensajes').addClass('alert alert-danger');
                    var tips = $("#textoMensaje");
                    tips.text("Solo el tipo entidad  mancomunidad permite más cantones .");

                    bValid = false;
                }

                if (lstActivasMancomunadas.length > 1) {
                    $('#mensajes').removeClass();
                    $('#mensajes').addClass('alert alert-danger');
                    var tips = $("#textoMensaje");
                    tips.text("Solo el tipo entidad  mancomunidad permite más cantones .");

                    bValid = false;
                }
            }

            $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
            setTimeout($.unblockUI, 1000);
            if (bValid) {
                $("#entidad_listNuev").val(lstarraynuev);
                $("#entidad_listAnt").val(lstarrayanq);
                $('#target').attr('action', '{{ url(' / editarEntidad') }}');
                $("#target").submit();
            }

        });

        $('#btn-toggle-checked.check-node').on('click', function (e) {
            $($checkableTree).each(function () {});
        });

        var lsySelect;
        $(".btn_selectEntidades").click(function () {
            var fila = $(this).parents('tr');
            var cadena = JSON.stringify(fila.data('id'));
            var objEntidad = eval('(' + cadena + ')');
            $("#btn_editar").prop('disabled', false);
            $("#btn_guardar").prop('disabled', true);
            $("#entidad_ruc").val(objEntidad.entidad_ruc);
            $("#entidad_nombre").val(objEntidad.entidad_nombre);
            $("#entidad_codigo_ente").val(objEntidad.entidad_codigo_ente);
            $("#entidad_direccion").val(objEntidad.entidad_direccion);
            $("#entidad_autoridad").val(objEntidad.entidad_autoridad);
            $("#entidad_telefono").val(objEntidad.entidad_telefono);
            $("#entidad_id").val(objEntidad.entidad_id);
            $("#tipo_entidad_id").val("");
            $("#tipo_entidad_id").val(objEntidad.tipo_entidad.tipo_entidad_id);
            $.ajax({
                url: '/consultaEntRelacionaDivPo',
                type: 'post',
                "_token": "{{ csrf_token() }}",
                async: true,
                data: {'entidad_id': objEntidad.entidad_id},
                beforeSend: function () {
                    $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var gnrMensaje = $("#gnrMensaje");
                    toTop();
                    if (jqXHR.status == '401') {
                        gnrMensaje.text(textStatus + " - " + errorThrown + " ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                        $('#gnrError').modal('show');
                    } else {
                        $('#mensajes').addClass('alert alert-danger');
                        $('#mensajes').removeClass('hide');
                        var textoMensaje = $("#textoMensaje");
                        textoMensaje.text("ERROR: " + jqXHR.status + " - " + textStatus + " - " + errorThrown);
                    }
                },
                success: function (data) {
                    lsySelect = data;
                    var lstInterfazjs = <?php echo json_encode($lstDivision); ?>;
                    var menu = [];
                    for (var i = 0; i < lstInterfazjs.length; i++) {
                        var lstregion = lstInterfazjs[i];
                        var region = {
                            selectable: false,
                            icon: "glyphicon glyphicon-stop",
                            selectedIcon: "glyphicon glyphicon-stop",
                            text: lstregion.division_politica_nombre,
                            href: lstregion.division_politica_id,
                            tags: [(lstregion.division_politica && lstregion.division_politica.length > 0) ? lstregion.division_politica.length > 0 : 0],
                            nivel: 1,
                            nodes: [],
                        };
                        for (var x = 0; x < lstregion.division_politica.length; x++) {
                            var lstProvincia = lstregion.division_politica[x];
                            var provincia = {
                                text: lstProvincia.division_politica_nombre,
                                href: lstProvincia.division_politica_id,
                                tags: [(lstregion.division_politica && lstProvincia.division_politica.length > 0) ? lstProvincia.division_politica.length > 0 : 0],
                                nivel: 2,
                                nodes: [],
                            };
                            for (var y = 0; y < lstProvincia.division_politica.length; y++) {
                                var checkedVal;
                                var lstCanton = lstProvincia.division_politica[y];
                                for (var e in data) {
                                    if (lstCanton.division_politica_id == data[e].division_politica_id) {
                                        checkedVal = true;
                                        break;
                                    } else {
                                        checkedVal = false;
                                    }
                                }
                                var canton = {
                                    text: lstCanton.division_politica_nombre,
                                    href: lstCanton.division_politica_id,
                                    tags: [0],
                                    nivel: 3,
                                    state: {
                                        checked: checkedVal,
                                        disabled: false,
                                        expanded: true,
                                        selected: false
                                    }
                                };
                                provincia.nodes.push(canton);
                            }
                            region.nodes.push(provincia);
                        }
                        menu.push(region);
                    }
                    var $checkableTree = $('#treeview-checkable').treeview({
                        data: menu,
                        showIcon: false,
                        showCheckbox: true,
                        onNodeChecked: function (event, node) {
                            $('#checkable-output').prepend('<p>' + node + ' was checked</p>');
                            if (node.nivel < 3) {
                                $('#mensajes').addClass('alert alert-danger');
                                var tips = $("#textoMensaje");
                                tips.text("Error !  solo puede seleccionar Cantones ");
                                $checkableTree.treeview('uncheckNode', [node.nodeId, {silent: true}])
                            } else {
                                var adDiv = false;
                                for (var i = 0; i < lsySelect.length; i++) {
                                    if (lsySelect[i].division_politica_id == node.href) {
                                        lsySelect[i].estado_id = activo;
                                        adDiv = true;
                                        break;
                                    }
                                }
                                if (!adDiv) {
                                    lsySelect.push(node);
                                }
                            }
                        },
                        onNodeUnchecked: function (event, node) {
                            $('#checkable-output').prepend('<p>' + node + ' was unchecked</p>');
                            for (var i = 0; i < lsySelect.length; i++) {
                                if (lsySelect[i].division_politica_id == node.href) {
                                    lsySelect[i].estado_id = inactivo;
                                    break;
                                }
                                if (lsySelect[i].href == node.href) {
                                    lsySelect.splice(i, 1);
                                    break;
                                }
                            }
                        }
                    });
                },
                complete: function () {
                    setTimeout($.unblockUI, 1000);
                }
            });
        });

        $("#btn_guardar").click(function () {
            var bValid = true;
            var entidad_ruc = $("#entidad_ruc").val();
            var entidad_nombre = $("#entidad_nombre").val();
            var entidad_codigo_ente = $("#entidad_codigo_ente").val().toUpperCase();
            var entidad_direccion = $("#entidad_direccion").val().toUpperCase();
            var entidad_autoridad = $("#entidad_autoridad").val().toUpperCase();
            var entidad_telefono = $("#entidad_telefono").val().toUpperCase();
            var tipo_entidad_id = $("#tipo_entidad_id").val();
            bValid = validarRuc();
            bValid = bValid && validarCampoLleno(entidad_ruc, "Nombre  Ruc");
            bValid = bValid && validarCampoLleno(entidad_nombre, "Nombre de entidad");
            bValid = bValid && validarCampoLleno(entidad_codigo_ente, "Nombre Codigo Ente Contable");
            bValid = bValid && validarCampoLleno(entidad_direccion, "Nombre dirección");
            bValid = bValid && validarCampoLleno(entidad_autoridad, "Nombre máxima autoridad");
            bValid = bValid && validarCampoLleno(entidad_telefono, "Nombre teléfono");
            bValid = bValid && validarCampoLleno(tipo_entidad_id, "Nombre tipo entidad");
            var lstarraynuevo = new Array();
            for (var i = 0; i < lsySelect.length; i++) {
                if (lsySelect[i].href != null) {
                    lstarraynuevo.push(lsySelect[i].href);
                }
            }
            if (tipo_entidad_id != mancomunidad)
            {
                if (lstarraynuevo.length > 1)
                {
                    $('#mensajes').removeClass();
                    $('#mensajes').addClass('alert alert-danger');
                    var tips = $("#textoMensaje");
                    tips.text("Solo el tipo entidad  mancomunidad permite más cantones .");

                    bValid = false;
                }
            }

            bValid = bValid && validarCampoLleno(lstarraynuevo, "Debe seleccionar división política");

            $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
            setTimeout($.unblockUI, 1000);
            if (bValid) {
                $("#entidad_listNuev").val(lstarraynuevo);
                $("#target").submit();
            }



        });
    });
</script>       
@endsection