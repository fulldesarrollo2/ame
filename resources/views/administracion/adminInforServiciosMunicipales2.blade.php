@extends('layouts.app')

@section('content')

<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2><span class="text-navy"><i class="fa fa-chevron-circle-right"></i> Administración de Informaci&oacute;n de Servicios B&aacute;sicos Municipales</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                <div class="col-lg-12">
                                     @if(Session::has('message'))
                                    <div class="alert alert-success" >
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <p> {{Session::get('message')}} </p>
                                    </div>
                                    @endif

                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    
                                    @if (notify()->ready())
                                    <div class="alert alert-{{ notify()->type() }}">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <label>{{ notify()->message() }}</label>
                                    </div>
                                    @endif
                                    
                                    <div id="mensajes" name="mensajes" class="hide">
                                        <label id="textoMensaje" name="textoMensaje"></label>
                                    </div>
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <form class="form-horizontal" method="POST" id="formValiInconInformacion" enctype="multipart/form-data"  action="{{url('/guardarValiInconInformacion')}}" role="form">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                Proyecto
                                                            </div>
                                                            <div class="panel-body">
                                                                 <div class="form-group">
                                                                        <label class="col-sm-2 control-label">Cargo</label>
                                                                        <div class="col-sm-4">
                                                                            <select class="form-control m-b" name="proyecto_servicio_id" id="proyecto_servicio_id" >
                                                                                <option value="">Seleccionar...</option>
                                                                                @foreach($lstProyectoServicio as $selectProyecServicio)
                                                                                <option value="{{$selectProyecServicio->proyecto_servicio_id}}">{{$selectProyecServicio->proyecto_servicio_nombre}}</option>
                                                                                @endforeach
                                                                            </select> 
                                                                        </div>
                                                                   </div>
                                                                
                                                                <div class="form-group">
                                                                        <label class="col-sm-2 control-label">Indicador</label>
                                                                        <div class="col-sm-4">
                                                                            <div id="cmbSubsistema"></div>
                                                                        </div>
                                                                   </div>
                                                                
                                                                <div class="row">
                                                                     @if($objVariableSeguridad!=null)
                                                                    <label class="col-lg-2 control-label" >Periodo Actual :</label>

                                                                        <div class="col-lg-4">
                                                                                <input id="variable_seguridad_id" type="text" readonly="true" name="variable_seguridad_id" class="form-control" value="{{$objVariableSeguridad->variable_seguridad_valor}}" ></input>
                                                                        </div>
                                                                        <div class="col-lg-4">
                                                                            <button type="button"  class="btn btn-primary" data-target="#modal_confirmacion" data-toggle="modal" data-toggle="tooltip" data-placement="left" title="Cerrar Periodo" >Cerrar Periodo</button>
                                                                        </div>
                                                                     @endif
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>



@endsection

@section('script')
<script type="text/javascript">
       
       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
         
         
           $("#proyecto_servicio_id").change(function (event){
               var proyectoServicioId=$("#proyecto_servicio_id").val();
               
                    $.ajax({
                         url: '{{ url('/buscarSubsistemaXproyecto') }}',
                         type: 'post',
                         data: {'proyecto_servicio_id': proyectoServicioId},
                         beforeSend: function() {
                            
                          },
                         error : function(jqXHR, textStatus, errorThrown) {
                            var gnrMensaje =$("#gnrMensaje");
                            if(jqXHR.status == '401'){
                                gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar."); 
                                $('#gnrError').modal('show');
                            }     
                        },
                         success: function(data) {
                            
                         var cadena = JSON.stringify(data);
                         var materias= verTipoArchivo(data);
                                var message='<select id="subsistema_id" name="subsistema_id"  class="form-control">'+materias+
                                            '</select>';
                               $("#cmbSubsistema").empty();
                               $("#cmbSubsistema").append(message);  
                     },
                     complete: function() {
                        
                    }
                 });
                    function verTipoArchivo(data){
                         var tregistros = '<option value="">Seleccione..</option>';
                             for(var i in data){
                                 tregistros =tregistros + '<option value='+data[i].sub_sistema_id+'>'+ data[i].sub_sistema_nombre +' </option>';

                          }
                            return tregistros;
                    }
            }); 
    
</script>
@endsection
    