@extends('layouts.app')
@section('content')
<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2><span class="text-navy"><i class="fa fa-chevron-circle-right"></i> Catálogo de Cuentas Financieras Personalizadas</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                <div class="col-lg-12">
                                    @if(Session::has('message'))
                                    <div class="alert alert-success" >
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <p> {{Session::get('message')}} </p>
                                    </div>
                                    @endif
                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    <div id="mensajes" name="mensajes" class="hide">
                                        <label id="textoMensaje" name="textoMensaje"></label>
                                    </div>
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <form class="form-horizontal" method="POST" name="formCuenta" enctype="multipart/form-data"  action="{{url('/guardarCtaPersonalizada')}}" role="form">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" id="cuenta_id_editar" name="cuenta_id_editar"/>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Tipo Cuenta</label>
                                                    <div class="col-sm-4">
                                                        <select class="form-control m-b" name="tipo_cuenta_id" id="tipo_cuenta_id" >
                                                            <option value="">Seleccionar...</option>
                                                            @foreach($lstTipoCuenta as $tipoCuenta)
                                                            <option value="{{$tipoCuenta->tipo_cuenta_id}}">{{$tipoCuenta->tipo_cuenta_descripcion}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Cuentas Oficiales Cargadas</label>
                                                    <div class="col-sm-10">
                                                        <div class="panel panel-default">
                                                            <div class="panel-body" id="divCuentas">
                                                                <div id="arbolCuentas" class="step-content" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group hide" id="divCtaSeleccionada">
                                                    <label class="col-sm-2 control-label">Cuenta Seleccionada</label>
                                                    <div class="col-sm-9">
                                                        <input id="nomCtaSeleccionada" type="text" disabled="true"  class="form-control">
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <button class="btn btn-primary" id="btn_editarCuenta"  name="btn_editarCuenta" type="button"><i class="fa fa-edit"></i></button>
                                                    </div>
                                                </div>
                                                <div class="hr-line-dashed"></div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                Ingreso/Actualización de Nuevas Cuentas
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">Seleccionar la clase</label>
                                                                    <div class="col-sm-10">
                                                                        @foreach($lstTipoFuente as $tipoFuente)
                                                                        <div class="radio"><label> 
                                                                                <input type="radio" value="1" id="clase_id" name="clase_id" required>Grupo</label>
                                                                        </div>
                                                                        <div class="radio"><label> 
                                                                                <input type="radio" value="2" id="clase_id" name="clase_id" required>Auxiliar</label>
                                                                        </div>
                                                                        @endforeach 
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">Código</label>
                                                                    <div class="col-sm-4">
                                                                        <input type="text" id="cuenta_codigo" name="cuenta_codigo"  value="{{ old('cuenta_codigo') }}" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group"><label class="col-lg-2 control-label">Nombre</label>
                                                                    <div class="col-lg-10"><input type="text" value="{{ old('cuenta_nombre') }}" id="cuenta_nombre" name="cuenta_nombre" class="form-control"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12 text-right">
                                                        <button class="btn btn-white" type="button" id="btn_limpiar">CANCELAR</button>
                                                        <button class="btn btn-primary" type="button" id="btn_guardar">GUARDAR</button>
                                                        <button class="btn btn-primary hide" type="button" id="btn_editar">ACTUALIZAR</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function buscarCuenta() {
            var bValid = true;
            $("#cuenta_nombre").val("");
            $("#cuenta_codigo").val("");
            $("#cuenta_id_editar").val("");
            $('#divCtaSeleccionada').addClass('hide');
            $('#nomCtaSeleccionada').val("");
            var tipoCuenta = $("#tipo_cuenta_id option:selected").val();
            bValid = bValid && validarCampoLleno(tipoCuenta, 'Tipo Cuenta');
            if (bValid) {
                $('#mensajes').removeClass('alert alert-danger');
                $('#mensajes').addClass('hide');
                $.ajax({
                    url: '/buscarCuentaxTipo',
                    type: 'post',
                    "_token": "{{ csrf_token() }}",
                    async: true,
                    data: {'tipoCuenta': tipoCuenta},
                    beforeSend: function () {
                        $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        var gnrMensaje = $("#gnrMensaje");
                        toTop();
                        if (jqXHR.status == '401') {
                            gnrMensaje.text(textStatus + " - " + errorThrown + " ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                            $('#gnrError').modal('show');
                        } else {
                            $('#mensajes').addClass('alert alert-danger');
                            $('#mensajes').removeClass('hide');
                            var textoMensaje = $("#textoMensaje");
                            textoMensaje.text("ERROR: " + jqXHR.status + " - " + textStatus + " - " + errorThrown);
                        }
                    },
                    success: function (data) {
                        if (data == '') {
                            $('#mensajes').removeClass('alert alert-danger hide');
                            $('#mensajes').addClass('alert alert-warning');
                            var tips = $("#textoMensaje");
                            tips.text("Advertencia !  No se han encontrado Cuentas Oficiales. ");
                            $('#arbolCuentas').treeview({
                                data: null,
                            });
                        } else
                        {
                            var arbol = dataArbolDinamico(data, "cuenta_nombre", "cuenta_id", "cuenta_hijo", "true", "cuenta_codigo", "cuenta_nombre");
                            $('#arbolCuentas').treeview({
                                levels: 1,
                                color: "#555",
                                showBorder: false,
                                data: arbol,
                                multiSelect: $('#chk-select-multi').is(':checked'),
                                onNodeSelected: function (event, node) {
                                    $("#cuenta_id_editar").val();
                                    $("#cuenta_codigo").val();
                                    $("#cuenta_nombre").val();

                                    $('#divCtaSeleccionada').removeClass('hide');
                                    $("#cuenta_id_editar").val(node.tags);
                                    $('#nomCtaSeleccionada').val(node.text);
                                },
                                onNodeUnselected: function (event, node) {
                                    $('#divCtaSeleccionada').addClass('hide');
                                    $('#nomCtaSeleccionada').val("");
                                    $("#tipo_cuenta_id").attr('disabled', false);
                                    $("#cuenta_nombre").val("");
                                    $("#cuenta_codigo").val("");
                                }
                            });
                        }
                    },
                    complete: function () {
                        setTimeout($.unblockUI, 1000);
                    },
                });
            }
        }

        $("#tipo_cuenta_id").change(function () {
            buscarCuenta();
        });

        $('#btn_editarCuenta').click(function (event) {
            $('#mensajes').removeClass('alert alert-danger');
            $('#mensajes').addClass('hide');
            $('#btn_guardar').addClass('hide');
            $('#btn_editar').removeClass('hide');
            document.formCuenta.action = " {{url('/actualizarCtaPersonalizada')}} ";
            var cuenta_id = $("#cuenta_id_editar").val();
            $.ajax({
                url: '/buscarCuentaSeleccionada',
                type: 'post',
                "_token": "{{ csrf_token() }}",
                async: true,
                data: {'cuenta_id': cuenta_id},
                beforeSend: function () {
                    $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var gnrMensaje = $("#gnrMensaje");
                    toTop();
                    if (jqXHR.status == '401') {
                        gnrMensaje.text(textStatus + " - " + errorThrown + " ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                        $('#gnrError').modal('show');
                    } else {
                        $('#mensajes').addClass('alert alert-danger');
                        $('#mensajes').removeClass('hide');
                        var textoMensaje = $("#textoMensaje");
                        textoMensaje.text("ERROR: " + jqXHR.status + " - " + textStatus + " - " + errorThrown);
                    }
                },
                success: function (data) {
                    if (data == '') {
                        $('#mensajes').removeClass('alert alert-warning');
                        $('#mensajes').addClass('alert alert-danger');
                        var tips = $("#textoMensaje");
                        tips.text("Error ! " + "No se han encontrado resultados.");
                    } else
                    {
                        $("#cuenta_codigo").val(data.cuenta_codigo);
                        $("#cuenta_nombre").val(data.cuenta_nombre);
                        $("#tipo_cuenta_id").attr('disabled', true);

                        var radios = document.getElementsByName('tipo_fuente_id');
                        for (var i = 0; i < radios.length; i++) {
                            radios[i].disabled = true;
                        }
                    }
                },
                complete: function () {
                    setTimeout($.unblockUI, 1000);
                }
            });
        });

        $('#btn_limpiar').click(function (event) {
            $('#mensajes').removeClass('alert alert-danger alert alert-warning');
            $('#mensajes').addClass('hide');
            $('#btn_guardar').removeClass('hide');
            $('#btn_editar').addClass('hide');
            $('#divCtaSeleccionada').addClass('hide');
            $('#nomCtaSeleccionada').val("");
            $("#tipo_cuenta_id").attr('disabled', false);
            $("#cuenta_nombre").val("");
            $("#cuenta_codigo").val("");
            $("#tipo_cuenta_id").val("");
            $("#cuenta_id_editar").val("");
            document.formCuenta.action = " {{url('/guardarCuenta')}} ";
            var radios = document.getElementsByName('tipo_fuente_id');
            for (var i = 0; i < radios.length; i++) {
                radios[i].disabled = false;
                radios[i].checked = false;
            }
            $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
            $('#arbolCuentas').treeview({
                data: null,
            });
            setTimeout($.unblockUI, 1000);
        });

        $('#btn_guardar').click(function (event) {
            var bValid = true;
            var cuentaCodigo = $("#cuenta_codigo").val();
            var pattern = new RegExp(/^((\d+).{1})+(\d+)$/);
            var ctaCodigo = pattern.test(cuentaCodigo);
            var nombreCuenta = $("#cuenta_nombre").val();
            var tipoCuenta = $("#tipo_cuenta_id").val();
            var cuentaOficial = $('#nomCtaSeleccionada').val();
            $('#mensajes').removeClass('alert alert-danger');
            $('#mensajes').addClass('hide');
            bValid = bValid && validarCampoLleno(tipoCuenta, 'Tipo Cuenta');
            bValid = bValid && validarCampoLleno(cuentaOficial, 'Cuenta Oficial');
            bValid = bValid && validarCampoLleno(cuentaCodigo, 'Código Cuenta');
            bValid = bValid && validarBoolean(ctaCodigo, 'El formato del código cuenta no es correcto. Ejemplo: 1.1.1');
            bValid = bValid && validarCampoLleno(nombreCuenta, 'Nombre Cuenta');
            if (bValid == true) {
                $('#mensajes').removeClass('alert alert-danger');
                $('#mensajes').addClass('hide');
                $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                document.formCuenta.submit();
                setTimeout($.unblockUI, 3000);
            }
        });

        $('#btn_editar').click(function (event) {
            var bValid = true;
            var cuentaCodigo = $("#cuenta_codigo").val();
            var nombreCuenta = $("#cuenta_nombre").val();
            var tipoCuenta = $("#tipo_cuenta_id").val();
            var cuentaOficial = $('#nomCtaSeleccionada').val();
            bValid = bValid && validarCampoLleno(tipoCuenta, 'Tipo Cuenta');
            bValid = bValid && validarCampoLleno(cuentaOficial, 'Cuenta Oficial');
            bValid = bValid && validarCampoLleno(cuentaCodigo, 'Código Cuenta');
            bValid = bValid && validarCampoLleno(nombreCuenta, 'Nombre Cuenta');
            if (bValid == true) {
                $('#mensajes').removeClass('alert alert-danger');
                $('#mensajes').addClass('hide');
                $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                document.formCuenta.submit();
                setTimeout($.unblockUI, 3000);
            }
        });
    });

</script>
@endsection