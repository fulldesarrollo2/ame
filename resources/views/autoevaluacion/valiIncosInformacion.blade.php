@extends('layouts.app')

@section('content')
<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2><span class="text-navy"><i class="fa fa-chevron-circle-right"></i> Validación de Inconsistencias de la Información</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                <div class="col-lg-12">
                                     @if(Session::has('message'))
                                    <div class="alert alert-success" >
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <p> {{Session::get('message')}} </p>
                                    </div>
                                    @endif

                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    
                                    @if (notify()->ready())
                                    <div class="alert alert-{{ notify()->type() }}">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <label>{{ notify()->message() }}</label>
                                    </div>
                                    @endif
                                    
                                    <div id="mensajes" name="mensajes" class="hide">
                                        <label id="textoMensaje" name="textoMensaje"></label>
                                    </div>
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <form class="form-horizontal" method="POST" id="formValiInconInformacion" enctype="multipart/form-data"  action="{{url('/guardarValiInconInformacion')}}" role="form">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                Seleccionar la Entidad (Usuario AME)
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">Tipo</label>
                                                                     <div class="col-sm-4">
                                                                        @foreach($lstTipoEntidad as $tipoEntidad)
                                                                        <div class="radio"><label> <input type="radio" value="{{$tipoEntidad->tipo_entidad_id}}" id="tipo_entidad_id" name="tipo_entidad_id" required>{{$tipoEntidad->nombre}}</label></div>
                                                                        @endforeach
                                                                    </div>                                                                 
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">División Política</label>
                                                                    <div class="col-sm-4">
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-body" >
                                                                                <div id="arbolDivisionPolitica" class="step-content" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" name="division_politica_id" class="division_politica_id" id="division_politica_id">
                                                                     </div>
                                                                  
                                                                    <label class="col-sm-2 control-label">Entidades</label>
                                                                    <div class="col-sm-4">
                                                                        <select class="form-control m-b" name="entidad_id" id="entidad_id" >
                                                                            <option value="">Seleccionar...</option>
                                                                        </select>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="hr-line-dashed"></div>
                                                        <label class="col-sm-2 control-label">Selección de Periodos</label>
                                                        <div class="col-sm-4">
                                                            <select class="form-control m-b" name="periodo_id" id="periodo_id" >
                                                                <option value="">Seleccionar...</option>
                                                                @foreach($lstPeriodo as $periodo)
                                                                <option value="{{$periodo->periodo_id}}">{{$periodo->periodo_anio}}-{{$periodo->periodo_id}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <label class="col-sm-2 control-label">Tipo de Información</label>
                                                        <div class="col-sm-4">
                                                            <select class="form-control m-b" name="tipo_cuenta_id" id="tipo_cuenta_id" >
                                                                <option value="">Seleccionar...</option>
                                                                @foreach($lstTipoCuenta as $tipoCuenta)
                                                                <option value="{{$tipoCuenta->tipo_cuenta_id}}">{{$tipoCuenta->tipo_cuenta_descripcion}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-10" id="lstuentas">
                                                            <br>
                                                             <label class="col-sm-4 control-label">Cuentas Oficiales Cargadas</label>
                                                            <div class="form-group">
                                                                <div class="col-sm-10">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-body" id="divCuentas">
                                                                            <div id="arbolCuentas" class="step-content" >
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="modal-footer col-sm-10">
                                                            <a  id="btn_consultar" class="btn btn-primary" type="button">
                                                                <i class="fa fa-search"></i> Consultar
                                                            </a>
                                                            <a  id="btn_imprimir" class="btn btn-primary" type="button" href="javascript:imprSelec('lstuentas')" >
                                                                <i class="fa fa-print"></i> Imprimir
                                                            </a>
                                                            <button type="button" class="btn btn-primary" id="btn_exportarExcel" >
                                                               <i class="fa fa-file-excel-o"></i> Exportar a Excel

                                                             </button>
                                                            <button type="button" class="btn btn-primary" id="btn_exportarPdf" >
                                                              <i class="fa fa-file-pdf-o"></i> Exportar a pdf
                                                             </button>  
                                                         </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>
@endsection

@section('script')
<script type="text/javascript">
        function imprSelec(nombre)
        {
            window.print( );
            window.close( );
            /*var ficha = document.getElementById(nombre);
            var ventimp = window.open(' ', 'lstuentas');
            ventana.document.write('<html><head><title>Imprimiendo...</title></head><body onprint="self.close()">');  
            ventimp.document.write( ficha.innerHTML );
            ventimp.document.close();
            ventimp.print( );
            ventimp.close();*/
           
        } 

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
var lstDivPoljs = <?php echo json_encode($objDivisionPolitica); ?>;
var arbolDivisionPolitica = dataArbolDinamico(lstDivPoljs,"division_politica_nombre","division_politica_id","division_politica","true","division_politica_codigo","division_politica_nombre");
$('#arbolDivisionPolitica').treeview({
    levels: 1,
    color: "#555",
    showBorder: false,
    data: arbolDivisionPolitica,
    multiSelect: $('#chk-select-multi').is(':checked'),
    onNodeSelected: function (event, node) {
        $('#division_politica_nom').val(node.text);
        $('#division_politica_id').val(node.tags);
        console.log(node.tags);
        cargarEntidades();
    },
    onNodeUnselected: function (event, node) {
        $('#nomCtaSeleccionada').val("");
    }
});
    //Metodo utilizado para cargar Entidades por tipo y división politica
    function cargarEntidades(){
      var division_politica_id  =  $('#division_politica_id').val(),
            tipoEntidad               = $('input[name=tipo_entidad_id]:checked').val();
              $('#entidad_id').empty();
              var textoMensaje =$("#textoMensaje");
              $('#mensajes').removeClass();
              textoMensaje.text("");  
            $.ajax({
                url: '{{ url('/consultaEntidadPorDivTipo') }}',
                        type: 'post',
                        async: true,
                        data: {'division_politica_id':division_politica_id,'tipoEntidad':tipoEntidad},
                         beforeSend: function() {
                       $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                             },
                        success:  function(data) {
                        console.log(data);
                        if (data == ''){
                             $('#mensajes').addClass('alert alert-danger');
                            $('#mensajes').removeClass('hide');
                            var textoMensaje =$("#textoMensaje");
                            textoMensaje.text("No se han encontrados datos. ");  
                        } else
                            {
                            $('#entidad_id').append('<option>Seleccione...</option>');
                              for(var i in data){
                                    $('#entidad_id').append('<option id='+data[i].entidad_id+' value='+data[i].entidad_id+'>'+ data[i].entidad_nombre+' </option>');
                                }
                            
                               setTimeout($.unblockUI, 1000);
                            }
                        },
                        complete: function() {
                        setTimeout($.unblockUI, 1000);
                    },
            });

    }
    
    
    $("#btn_exportarPdf").click(function(){
        bValid = true; 
        var entidad_id=$('#entidad_id').val();
        var periodo_id=$('#periodo_id').val();
        var tipo_cuenta_id=$('#tipo_cuenta_id').val();
        bValid = bValid && validarCampoLleno(entidad_id, " entidad");
        bValid = bValid && validarCampoLleno(periodo_id, " periodo");
        bValid = bValid && validarCampoLleno(tipo_cuenta_id, " tipo Información"); 
        if(bValid){
                    $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                    setTimeout($.unblockUI, 1000);                      
                    $('#formValiInconInformacion').attr('action', '{{ url('/imprimirPdfValiIncosIn') }}');
                    $("#formValiInconInformacion").submit();    
             }
    });
    
    $("#btn_exportarExcel").click(function(){
        var entidad_id=$('#entidad_id').val();
        var periodo_id=$('#periodo_id').val();
        var tipo_cuenta_id=$('#tipo_cuenta_id').val();

        bValid = true; 
        bValid = bValid && validarCampoLleno(entidad_id, " entidad");
        bValid = bValid && validarCampoLleno(periodo_id, " periodo");
        bValid = bValid && validarCampoLleno(tipo_cuenta_id, " tipo Información"); 
        if(bValid) {
             $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
             setTimeout($.unblockUI, 1000);
             $('#formValiInconInformacion').attr('action', '{{ url('/imprimirExcelValiIncosIn') }}');
                  $("#formValiInconInformacion").submit();
            }
    });
    
    
    
    $("#btn_consultar").click(function(){
     var textoMensaje =$("#textoMensaje");
     $('#mensajes').removeClass();
     textoMensaje.text("");  
     var entidad_id=$('#entidad_id').val();
     var periodo_id=$('#periodo_id').val();
     var tipo_cuenta_id=$('#tipo_cuenta_id').val();
     
    bValid=true; 
    bValid = bValid && validarCampoLleno(entidad_id, " entidad");
    bValid = bValid && validarCampoLleno(periodo_id, " periodo");
    bValid = bValid && validarCampoLleno(tipo_cuenta_id, " tipo Información");
    if(bValid){ 
            $.ajax({
                         url: '{{ url('/consultaValCuentaEntidad') }}',
                         type: 'post',
                         data: {'entidad_id': entidad_id,'periodo_id': periodo_id,'tipo_cuenta_id': tipo_cuenta_id  },
                         beforeSend: function() {
                              $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                             },
                         error : function(jqXHR, textStatus, errorThrown) {
                            var gnrMensaje =$("#gnrMensaje");
                            if(jqXHR.status == '401'){
                                gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar."); 
                                $('#gnrError').modal('show');
                            }     
                        },
                         success: function(data) {
                              console.log(data);
                              setTimeout($.unblockUI, 1000);
                         if (data.length==0){
                             $('#mensajes').addClass('alert alert-danger');
                            $('#mensajes').removeClass('hide');
                            var textoMensaje =$("#textoMensaje");
                            textoMensaje.text("No se han encontrados datos. "); 
                            $('#arbolCuentas').empty();
                        }else{   
                            
                             var arbol = dataArbolCuentas(data,"cuenta_nombre","cuenta_id","cuenta_hijo","false","cuenta_codigo","cuenta_nombre","valor","inconsistente");
                             console.log(arbol);
                             $('#arbolCuentas').treeview({
                               color: "#ffffff",
                                showTags: true,
                                data: arbol,
                                multiSelect: $('#chk-select-multi').is(':checked'),
                                onNodeSelected: function (event, node) {
                                    $("#cuenta_id_editar").val(); 
                                    $("#cuenta_codigo").val();
                                    $("#cuenta_nombre").val(); 

                                    $('#divCtaSeleccionada').removeClass('hide');
                                    $("#cuenta_id_editar").val(node.tags);
                                    $('#nomCtaSeleccionada').val(node.text);
                                },
                                onNodeUnselected: function (event, node) {
                                    $('#divCtaSeleccionada').addClass('hide');
                                    $('#nomCtaSeleccionada').val("");
                                    $("#tipo_cuenta_id").attr('disabled',false);
                                    $("#cuenta_nombre").val("");
                                    $("#cuenta_codigo").val("");
                                }
                            });  
                       }          
                     },
                      complete: function() {
                     
                    }       
                 });
        }
    });
        

</script>
@endsection