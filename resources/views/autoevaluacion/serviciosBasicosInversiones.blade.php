@extends('layouts.app')

@section('header')
  <h1>
    Administración de Informaci&oacute;n de Servicios B&aacute;sicos Municipales
  </h1>
  <ol class="breadcrumb">
    <li><a><i class="glyphicon glyphicon-th-large"></i>Administración</a></li>
    <li class="active">Servicios Basicos Municipales</li>
  </ol>
<br/>
@stop

@section('content')

<div class="box box-default">
	<div class="box-header with-border">
    	<h3 class="box-title">Inversiones (Infraestructuras, Bienes Muebles)</h3>
  	</div>
  	<div class="box-body">
    	<div class="row">
    		{!! Form::open(['url'=>'guardarInversiones','method'=>'POST','id'=>'formInversion']) !!}
          <div class="col-sm-12">
            {{--
      			{!! Form::label('servicios','Servicios Relacionados') !!}
      			&nbsp; &nbsp; &nbsp; &nbsp;  
      			{!! Form::checkbox('aguaPotable') !!}&nbsp;
      			{!! Form::label('aguaPotable','Agua Potable') !!}
            &nbsp; &nbsp; &nbsp;
      			{!! Form::checkbox('alcantarillado') !!}&nbsp;
      			{!! Form::label('alcantarillado','Alcantarillado') !!}
            &nbsp; &nbsp; &nbsp;
      			{!! Form::checkbox('residuosSolidos') !!}&nbsp;
      			{!! Form::label('residuosSolidos','Residuos Solidos') !!}
            --}}
          <!--</div>-->
          <br><br>
          <div class="col-sm-6">
            {!! Form::label('tipoInversion','Tipo Inversion',['class'=>'col-sm-12 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::select('tipoInversion',['bienesMuebles'=>'Bienes Muebles','infraestructura'=>'Infraestructura'],NULL,['placeholder'=>'seleccione','class'=>'form-control']) !!}
            </div>
          </div>
          <div class="form-group">
            {!! Form::label('fechaInversion','Fecha Inversion',['class'=>'col-sm-6 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::date('fechaInversion',NULL,['class'=>'form-control','placeholder'=>'dd/mm/aaaa']) !!}
            </div>
          </div>
          <div class="form-group">
            {!! Form::label('descripcion','Descriptcion',['class'=>'col-sm-12 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::textarea('descripcion',null,['class'=>'form-control']) !!}
            </div>
          </div>
          <div class="form-group">
            {!! Form::label('valorAdquisicion','Valor Adquisicion',['class'=>'col-sm-6 control-label']) !!}
            <div class="col-sm-6">
              <div class="input-group">
                <div class="input-group-addon">$</div>
                {!! Form::text('valorAdquisicion',NULL,['class'=>'form-control']) !!}
              </div>
            </div>
          </div>
          <div class="form-group">
            {!! Form::label('vidaUtil','Vida Util',['class'=>'col-sm-6 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::text('vidaUtil',Null,['class'=>'form-control']) !!}
            </div>
          </div>
          <div class="form-group">
            {!! Form::label('estado','Estado',['class'=>'col-sm-6 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::select('estado',[''=>'Seleccione'],NULL,['class'=>'form-control']) !!}
            </div>
          </div>
          <div class="form-group" id="divCantidad">
            {!! Form::label('cantidad','Cantidad',['class'=>'col-sm-6 control-label']) !!}
            <div class="col-sm-6">
              {!! Form::text('cantidad',NULL,['class'=>'form-control']) !!}
            </div>
          </div>
          
          </div>
          <div class="col-sm-12">
            <div class="box-footer text-right">
              {!! Form::button('Cancelar',['class'=>'btn btn-white']) !!}
              {!! Form::submit('Almacenar',['class'=>'btn btn-primary']) !!}
            </div>
          </div>
    		{!! Form::close()!!}

    	</div>
    </div>
</div>


@endsection

@section('script')

  <script>
    $(document).ready(function(){
      $('#divCantidad').hide();
      $("#tipoInversion").change(function(){
        var tipo = $("#tipoInversion").val();
        if(tipo == 'bienesMuebles'){
          $('#estado').empty();
          $('#estado').append('<option value="">Seleccione</option>');
          $('#estado').append('<option value="bueno">Bueno</option>');
          $('#estado').append('<option value="malo">Malo</option>');
          $('#estado').append('<option value="reparacion">Reparacion</option>');
          $('#estado').append('<option value="baja">Baja</option>');
          $('#estado').append('<option value="planificado">Planificado</option>');
          $('#formInversion').attr('action','guardarInversionesBM');
          $('#divCantidad').show();
        }else{
          if(tipo == 'infraestructura'){
            $('#estado').empty();
            $('#estado').append('<option value="">Seleccione</option>');
            $('#estado').append('<option value="funcionamiento">Funcionamiento</option>');
            $('#estado').append('<option value="planificado">Planificado</option>');
            $('#formInversion').attr('action','guardarInversiones');
            $('#divCantidad').hide();
          }else{
            if(tipo == ''){
              $('#estado').empty();
              $('#estado').append('<option value="">Seleccione</option>');
            }
          }
        }
        
      });
    });
  </script>

  <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
  {!! JsValidator::formRequest('App\Http\Requests\inversionesRequest','#formInversion') !!}
@endsection