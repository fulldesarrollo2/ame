@extends('layouts\app')
@section('content')

<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2><span class="text-navy"><i class="fa fa-chevron-circle-right"></i> Analisis Vertical</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                <div class="col-lg-12">
                                    @if (session()->has('mensajeexito')) 
                                    <div class="alert alert-success">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <ul>
                                            <li>{{ session()->pull('mensajeexito') }}</li>			            
                                        </ul>
                                    </div>
                                    @endif

                                    @if (session()->has('mensajeerror')) 
                                    <div class="alert alert-danger">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <ul>
                                            <li>{{ session()->pull('mensajeerror') }}</li>			            
                                        </ul>
                                    </div>
                                    @endif


                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif


                                    <div id="mensajes" name="mensajes" class="hide">
                                        <label id="textoMensaje" name="textoMensaje"></label>
                                    </div>

                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <form class="form-horizontal">
                                                <input type="hidden" name="division_politica_id"
                                                       class="division_politica_id" id="division_politica_id"> <input
                                                       type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                
												<div class="form-group">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-2">Tipo</label>
                                                            <div class="col-sm-10">
                                                                @foreach($lstTipoEntidad as $lstTipoEntidad)
                                                                <div class="radio">
                                                                    <label> <input type="radio" class="check_entidad"
                                                                                   value="{{ $lstTipoEntidad->tipo_entidad_id}}"
                                                                                   id="tipo_entidad_id" required name="tipo_entidad_id">{{
																		$lstTipoEntidad->nombre}}
                                                                    </label>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-12">División Política</label>
                                                            <div class="col-sm-12">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-body" id="divCuentas">
                                                                        <div id="arbolDivisionPolitica" class="step-content">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" name="division_politica_id"
                                                                       class="division_politica_id" id="division_politica_id">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Entidades</label>
                                                            <div class="col-sm-8">
                                                                <select class="form-control m-b" name="entidad_id"
                                                                        id="entidad_id">
                                                                    <option value="">Seleccionar...</option>
                                                                </select>
                                                            </div>
                                                            
                                                            <label class="col-sm-4 control-label">Fuente de
                                                                información</label>
                                                            <div class="col-sm-8">
                                                                <select class="form-control m-b col-md-6"
                                                                        name="tipo_fuente_id" id="tipo_fuente_id">
                                                                    <option value="">Seleccionar...</option>
                                                                    @foreach($lstTipoFuente as $lstTipoFuente)
                                                                    <option value="{{ $lstTipoFuente->tipo_fuente_id}}"
                                                                            name="tipo_fuente_id">{{
																		$lstTipoFuente->tipo_fuente_descripcion_alias}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            
                                                            <label class="col-sm-4 control-label">Periodo desde</label>
                                                            <div class="col-sm-8">
                                                                <select class="form-control m-b col-md-6" name="fuente_info" id="periodo_id_desde">
                                                                </select>				
                                                            </div>
                                                            
                                                            <label class="col-sm-4 control-label">Periodo hasta</label>
                                                            <div class="col-sm-8">
                                                                <select class="form-control m-b col-md-6" name="fuente_info" id="periodo_id_hasta">
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="col-sm-12 text-right">
                                                                <button id="btn_buscar" type="button"  class="btn btn-primary">Consultar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                  
                                                <div  class="col-sm-12">
                                                        <div id="divGraficos" class="row">
                                                        </div>
                                                        
                                                        <div id='oculto'>
                                                        <div id="js-legend" class="chart-legend"></div>
                                                         </div>
                                                    </div>
                                                
                                                <div class="col-sm-12">
                                                        <table id="tblCuentas" class="table table-striped table-bordered" style="text-align: center;" cellspacing="0" width="100%">
                                                            <thead>
                                                                <td>ÁNALISIS VERTICAL</td>
                                                            </thead>
                                                            
                                                        </table>
                                                </div>
                                                
                                                <div id="tabla-proyeccion"></div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
var tipoFuenteBdE = <?php echo json_encode($fuenteBdE); ?>;
$(document).ready(function (){
    
    $('#tblCuentas').dataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
    });
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    
    var lstDivPoljs = <?php echo json_encode($lstDivisionPolitica); ?>;
    var arbolDivisionPolitica = dataArbolDinamico(lstDivPoljs, "division_politica_nombre", "division_politica_id", "division_politica", "true", "division_politica_codigo", "division_politica_nombre");
    $('#arbolDivisionPolitica').treeview({
        levels: 1,
        color: "#555",
        showBorder: false,
        data: arbolDivisionPolitica,
        multiSelect: $('#chk-select-multi').is(':checked'),
        onNodeSelected: function (event, node) {
            $('#checkable-output').prepend('<p>' + node.text + ' was checked</p>');
            if (node.nodes && node.nodes.length > 0) {
                $('#mensajes').addClass('alert alert-danger');
                $('#mensajes').removeClass('hide');
                var textoMensaje = $("#textoMensaje");
                textoMensaje.text("Error! Solo se puede seleccionar cantones. ");
            } else {
                $('#mensajes').addClass('hide');
                $('#division_politica_nom').val(node.text);
                $('#division_politica_id').val(node.tags);
                cargarEntidades();
            }
        },
        onNodeUnselected: function (event, node) {
            $('#nomCtaSeleccionada').val("");
        }
    });

    $("input[name=tipo_entidad_id]").click(function () {
        cargarEntidades();
    });

    $("#tipo_fuente_id").change(function () {
        periodos(2);
    });

    $("#entidad_id").change(function () {
        periodos(2);
    });
	
    $("#example").click(function(){
        $(this).addClass('selected').siblings().removeClass('selected');    
        var row = $(this).html();   
        var value=$(this).find('td:first').html();
        alert(row);    
    });
    
    
    var lgGrafico = [];
    var totCeldas = 0;
    var totCeldasGraf = 0;
    
    $('#btn_buscar').click(function (event) {
        
        var entidad_id = $("#entidad_id option:selected").val();
        var tipo_fuente_id = $("#tipo_fuente_id option:selected").val();
        var periodo_id_desde = $("#periodo_id_desde option:selected").val();
        var periodo_id_desde_text = $("#periodo_id_desde option:selected").text();
        var periodo_id_hasta = $("#periodo_id_hasta option:selected").val();
        var periodo_id_hasta_text = $("#periodo_id_hasta option:selected").text();
        var bValid = true;
        tipo_fuente_id = parseInt(tipo_fuente_id);
        var totAnios= 0;
        var totMeses= 0;
        
        var table = $('#tblCuentas').DataTable();
        table.clear();
        $('#tblCuentas').dataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "destroy": true
        });
        $('#tblCuentas thead').html('<tr><td>ÁNALISIS VERTICAL</td></tr>');
        
        $('#tblCuentas').dataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "destroy": true
        });
        
        $('#mensajes').removeClass('alert alert-danger');
        $('#mensajes').addClass('hide');
        var textoMensaje =$("#textoMensaje");
        textoMensaje.text("");  
        var thead= "<tr><td rowspan='2'>CUENTAS</td>";
        var cabecera;
        var thead2= "<tr><td>Recaudado/Devengado</td>";
        var tamanio;
        var inicioPeriodo;
        var finPeriodo;
                       
        if(tipo_fuente_id === tipoFuenteBdE){
           if(parseInt(periodo_id_hasta_text) <= parseInt(periodo_id_desde_text)){
                bValid = false;
                toTop();
                $('#mensajes').addClass('alert alert-danger');
                $('#mensajes').removeClass('hide');
                var textoMensaje =$("#textoMensaje");
                textoMensaje.text("El período hasta no puede ser menor ni igual al periodo desde.");
           }else{
                totAnios = parseInt(periodo_id_hasta_text) - parseInt(periodo_id_desde_text);
                var anios = periodo_id_desde_text;
                var anios2 = periodo_id_desde_text;
                tamanio =  totAnios;
                inicioPeriodo = anios;
                finPeriodo = periodo_id_hasta_text;
                totCeldas = totAnios + 1;
                totCeldasGraf = totCeldas - 1;
                for(var j=0;j<totAnios;j++){
                    thead = thead + "<td rowspan='2'>"+anios+"</td>";
                    anios= parseInt(anios)+1;
                }
                thead = thead + "<td colspan='1'>"+periodo_id_hasta_text+"</td>";
                thead = thead + "<td colspan='"+(totAnios)+"'>PARTICIPACIÓN VERTICAL</td><td rowspan='2'>% PROMEDIO HISTÓRICO</td></tr>";

                for(var j=0;j<(totAnios);j++){
                    anios2= parseInt(anios2)+1;
                    thead2 = thead2 + "<td>"+anios2+"</td>";
                    lgGrafico.push(anios2);
                }
                thead2 = thead2 +"</tr>";
                cabecera= thead +thead2;
                bValid = true;
           }
        }
        else{
            var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
            
            var str1 = periodo_id_desde_text;
            var res1 = str1.split(" - ");
            var mes1 = res1[0];
            var anio1 = res1[1];
            
            var str2 = periodo_id_hasta_text;
            var res2 = str2.split(" - ");
            var mes2 = res2[0];
            var anio2 = res2[1];
            
            var index1 = meses.indexOf(mes1);
            var index2 = meses.indexOf(mes2);
            
            index1= parseInt(index1)+1;
            index2= parseInt(index2)+1;

            if(parseInt(anio1) === parseInt(anio2)){
                if(index1 >= index2){
                    bValid = false;
                    toTop();
                    $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var textoMensaje =$("#textoMensaje");
                    textoMensaje.text("El período hasta no puede ser menor ni igual al periodo Desde.");
                }else{
                    totAnios = anio1 - anio2;
                    totMeses = index2 - index1;
                    var meseIn= parseInt(index1);
                    var meseIn2= parseInt(index1);
                    var meseFn= parseInt(index2);
                    
                    inicioPeriodo = meseIn;
                    finPeriodo = meseFn;
                    totCeldas = totMeses;
                    totCeldasGraf = totMeses-1;
                    
                    for(var j=0;j<totMeses;j++){
                        var nombreMes= meses[meseIn - 1];
                        thead = thead + "<td rowspan='2'>"+nombreMes+"</td>";
                        meseIn= parseInt(meseIn)+1;
                    }
                    var nombreMesFn= meses[meseFn - 1];
                    thead = thead + "<td colspan='1'>"+nombreMesFn+"</td>";
                    thead = thead + "<td colspan='"+(totMeses)+"'>CRECIMIENTO HORIZONTAL</td><td rowspan='2'>% PROMEDIO HISTÓRICO</td></tr>";

                    for(var j=0;j<(totMeses);j++){
                        meseIn2= parseInt(meseIn2)+1;
                        var nombreMes= meses[meseIn2 - 1];
                        thead2 = thead2 + "<td>"+nombreMes+"</td>";
                    }
                    thead2 = thead2 +"</tr>";
                    cabecera= thead +thead2;
                    bValid = true;
                }
            }else{
                
                    bValid = false;
                    toTop();
                    $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var textoMensaje =$("#textoMensaje");
                    textoMensaje.text("El año del período hasta es mayor al año del periodo Desde.");
            }
        }
            
        bValid = bValid && validarCampoLleno(entidad_id, 'Entidad');
        bValid = bValid && validarCampoLleno(tipo_fuente_id, 'Fuente de Información');
        bValid = bValid && validarCampoLleno(periodo_id_desde, 'Período Desde');
        bValid = bValid && validarCampoLleno(periodo_id_hasta, 'Período Hasta');

        if(bValid)
        {
            $.ajax({
                url: '{{ url('/buscarLstValCtaEntidadxFiltro') }}',
                type: 'post',
                "_token": "{{ csrf_token() }}",
                async: true,
                data: {'entidad_id':entidad_id,'tipo_fuente_id':tipo_fuente_id,'periodo_id_desde':periodo_id_desde,'periodo_id_hasta':periodo_id_hasta,'tamanio':tamanio,'inicioPeriodo':inicioPeriodo,'finPeriodo':finPeriodo},
                beforeSend: function() {
                    $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var gnrMensaje = $("#gnrMensaje");
                    toTop();
                    if (jqXHR.status == '401') {
                        gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                        $('#gnrError').modal('show');
                    }else
                        alert(errorThrown);
                },
                success:  function(data) {
                   
                    if (data == ''){
                       toTop();
                       $('#mensajes').addClass('alert alert-danger');
                       $('#mensajes').removeClass('hide');
                       var textoMensaje =$("#textoMensaje");
                       textoMensaje.text("No se han encontrados datos. ");  
                    }else{
                        $('#tblCuentas thead').html(cabecera);
                       
                        $('#tblCuentas').dataTable({
                            data: data,
                            "scrollX": true,
                            "paging":   false,
                            "ordering": false,
                            "info":     false,
                            "destroy": true
                        });
                    }
                },
                complete: function() {
                    setTimeout($.unblockUI, 1000);
                },
            });
        }else{
           toTop();
        }

    });
    

    $('#tblCuentas tbody').on( 'click', 'tr', function () {
        var table = $('#tblCuentas').DataTable();
        var indice= table.row(this).index();

        if ($(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            $("#div"+indice).remove();
            document.getElementById('oculto').style.display = 'none';
            
        }
        else {
            $(this).addClass('selected');
            var array=[];
            var celdaInicial = 1;
            var tot=celdaInicial + totCeldas;
            var totFin=tot+totCeldasGraf;
            var $i=0;
            var nomCuenta;
            var lista = [];
            $(this).children("td").each(function() 
            {
                if($i === 0){
                    nomCuenta=$(this).text();
                }
                if($i >= tot && $i<totFin){
                   var col= $(this).text();
                   array.push(col);
                }
                $i++;
            });            
        
            
            var div = "<div id='div"+indice+"' class='col-lg-6'><div class='ibox float-e-margins'><div class='ibox-title'>";
                div = div + "<h5>"+nomCuenta+"</h5><div ibox-tools></div></div><div class='ibox-content'>";
                div = div + "<div><canvas id='can"+indice+"' height='140'></canvas></div></div></div></div>";
            $("#divGraficos").append(div);
            document.getElementById('oculto').style.display = 'block';
            console.log(indice);
            

            for (x=0;x<array.length;x++){                
                var positivo;
                var numero = array[x];
                if(numero.indexOf("-")){
                    positivo = numero;
                }else{
                    positivo = -(numero);
                }
                    
            
                
                var randomColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
                var datoschart = {value:parseInt(positivo), color:randomColor, highlight:randomColor, label:lgGrafico[x]};
                lista.push(datoschart);
            }
            
            
                                
            
            
            var doughnutData = lista;
            
            var doughnutOptions = {
            segmentShowStroke: true,
                    segmentStrokeColor: "#fff",
                    segmentStrokeWidth: 2,
                    percentageInnerCutout: 45, // This is 0 for Pie charts
                    animationSteps: 100,
                    animationEasing: "easeOutBounce",
                    animateRotate: true,
                    animateScale: false,
                    responsive: true,
                    scaleBeginAtZero: false,
                    scaleStartValue : -50,
            };
            var ctx = document.getElementById('can' + indice).getContext("2d");
            var myNewChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);
            document.getElementById('js-legend').innerHTML = myNewChart.generateLegend();
            }

       
    });


    $('#btn_limpiar').click(function (event) {
            toTop();
            $('#mensajes').removeClass('alert alert-danger alert alert-warning');
            $('#mensajes').addClass('hide');
            $('#btn_guardar').text('Almacenar');
            $('#variable_indicador_id').val("");
            $('#lblProyFinanciera').addClass('hide');
            $('#divProyFinanciera').addClass('hide');
            $("#tipo_indicador_id").val("");
            $("#indicador_nombre").val("");
            $("#indicador_objetivo").val("");
            $("#indicador_alcance").val("");
            $("#lblFormula").text("");
            $("#lblFormula_2").text("");
            $("#indicador_formula").val("");
            $('#nivel_alerta_id').selectpicker('destroy');
            $('#nivel_alerta_id').html('');
            $('#nivel_alerta_id_2').selectpicker('destroy');
            $('#nivel_alerta_id_2').html('');

            $("#indicador_id").val(""); 
            $("#lstRangosAceptacion").val("");
            $("#lstNivelesAlerta").val("");
            $("#lstNivelesAlertaAct").val(""); 
            $("#criterioId").val("");
            $("#lstCambioMejora").val("");

            $("#categoria_id").val(""); 
            $("#valor_minimo").val("");
            $("#valor_maximo").val("");

            $("#rango_aceptacion_id_1").val("");
            $("#tipo_nivel_alerta_id").selectpicker('val',''); 
            $("#valorDesde").val("");
            $("#valorHasta").val("");

            $("#rango_aceptacion_id").val(""); 
            $("#nivel_alerta_id").selectpicker('val',''); 
            $("#nivel_alerta_descripcion").val("");
            $("#nivel_alerta_interpretacion").val("");  

            $("#rango_aceptacion_id_2").val(""); 
            $("#nivel_alerta_id_2").selectpicker('val',''); 
            $("#componentes_formula").val("");
            $("#criterio_cambio_criterio").val("");  
            valorFormula = '';
            formula = '';
            document.formIndicador.action = " {{url('/guardarIndicador')}} ";
            var radios = document.getElementsByName('indicador_proyeccion_financiera');
            for (var i = 0; i < radios.length; i++) {
                radios[i].checked = false;
            }
            $("#variable_indicador_id").val("");
            $("#operador_matematico").val("");
            $("#signo_agrupacion").val("");
            $("#valorConstante").val("");
            $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
            $('#arbolVarIndicador').treeview({
            data: null,
            });
            setTimeout($.unblockUI, 1000);
        });
        
});
var lstGraficos = [];
    var chart;
    var lstDivGraficos = [];
    function botones() {
        var botones = '';
        botones = botones + '<div class="form-group row">' +
                '<div class="col-lg-3"></div>' +
                '<div class="form-group botones text-center col-lg-6 col-xs-12" id="botones_export">' +
                '</div>' +
                '<div class="col-lg-3"></div>	</div>';

        return botones;
    }

    function graficarperiodos(elemento, desde, hasta) {
        var periodo_id = [];
        var guardar = false;
        var fecha = [];
        var retorno = [];
        $(elemento + " option").each(function () {
            if ($(this).attr('value') == desde) {
                guardar = true;
            }
            if (guardar == true) {
                periodo_id.push($(this).attr('value'));
                fecha.push($(this).text());
            }
            if ($(this).attr('value') == hasta) {
                guardar = false;
            }
        });
        retorno.push(fecha);
        retorno.push(periodo_id);
        return retorno;
    }

    function mostrasanalisisver() {

        
        var entidad = document.getElementById("entidad_id").value;
        var valdesde = document.getElementById("periodo_id_desde").value;
        var valhasta = document.getElementById("periodo_id_hasta").value;
        var tipo_fuente = document.getElementById("tipo_fuente_id").value;
        var bValid = true;
        var cuentas = [];
        var array = [];
        var lstcuentas = [];
        var bandera = true;
        if (bValid) {
            $.ajax({
                url: '{{ url('/buscarLstValCtaEntidadxFiltro') }}',
                type: 'post',
                async: 'true',
                "_token": "{{ csrf_token() }}",
                async: true,
                        data: {'entidad': entidad,
                            'tipo_fuente': tipo_fuente,
                            'valdesde': valdesde,
                            'valhasta': valhasta},
                error: function (jqXHR, textStatus, errorThrown) {
                    var gnrMensaje = $("#gnrMensaje");
                    if (jqXHR.status == '401') {
                        gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                        $('#gnrError').modal('show');
                    }
                },
                beforeSend: function (data) {
                    $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                },
                success: function (data) {
                var periodos=graficarperiodos('#periodo_id_desde',valdesde,valhasta);
                console.log(periodos);
                console.log('datos');
                for(var i in data){
                	var idcuenta=data[i].cuenta_id;
                	for(var k in lstcuentas){
            			if(parseInt(idcuenta)===parseInt(lstcuentas[k])){
            				bandera=false;
            				break;
                		}else{ bandera=true;}
                	}
                	if(bandera){
                	lstcuentas.push(data[i].cuenta_id);
                	for(var j in data){
                		
						if(data[i].cuenta_id==data[j].cuenta_id){
							array.push(data[j]);
						}
						if(j==data.length-1){
							cuentas.push(array);
							array= new Array();
						}
                   	}
                	}

                }
                console.log(cuentas);
				var thead='<table id="example" class="table table-striped table-bordered dt-responsive nowrap " cellspacing="0" width="100%"><thead><tr><th></th><th>Datos</th>';
				var headconten='';
                for(var i in periodos[0]){  
                	headconten=headconten+'<th>'+periodos[0][i]+'</th>';
                }
                for(var i in periodos[0]){  
                	headconten=headconten+'<th>'+periodos[0][i]+'</th>';
                }
                headconten=headconten+'<th> % Promedio Historico</th>';
                headconten=thead+''+headconten+'</tr></thead><tbody>'
                //document.getElementById("example").innerHTML = ;

              	
 
               	var tregistros = ''; var promedio=[];var promediohis=[]; var arrayporcentaje=[];
                    for(var j in cuentas){
                    	var tregistros = tregistros+'<tr>';
                            var arrayperiodos=0;k=0;arrayperiodos2=0;indice=0;indice2=0;indice3=0;maximo=cuentas[j].length-1;encontrado=false;
                           porcenthorizon=[];
                          	 var suma=0;
                        	do {
                            	console.log("indice 1 "+indice +'indice 2 '+indice2);
                            	if(arrayperiodos==0 && k==0){
                            		var periodocuenta=parseInt(cuentas[j][indice2].periodo_id);var actual=parseInt(periodos[1][arrayperiodos]);
                            		tregistros=tregistros+'<th>'+cuentas[j][k].cuenta_codigo+'</th>';
                            		tregistros=tregistros+'<th>'+cuentas[j][k].cuenta_nombre+'</th>';
                            		if(parseInt(cuentas[j][indice2].periodo_id)===parseInt(periodos[1][arrayperiodos])){
                        				tregistros=tregistros+'<th>'+cuentas[j][indice2].val_cuenta_entidad_valor+'</th>';
                        				encontrado=true;suma=suma+parseInt(cuentas[j][indice2].val_cuenta_entidad_valor);
                        			}else{tregistros=tregistros+'<th>0</th>';suma=suma+0;}
                                }
                        		else{
                        			var periodocuenta=parseInt(cuentas[j][indice2].periodo_id);var actual=parseInt(periodos[1][arrayperiodos]);
	                            	if(parseInt(cuentas[j][indice2].periodo_id)===parseInt(periodos[1][arrayperiodos])){
	                				tregistros=tregistros+'<th>'+cuentas[j][indice2].val_cuenta_entidad_valor+'</th>';
	                				encontrado=true;suma=suma+parseInt(cuentas[j][indice2].val_cuenta_entidad_valor);
	                				}else{tregistros=tregistros+'<th>0</th>';suma=suma+0;}
                        		}    
                            	arrayperiodos++;					
								k++;
                            	if(k==maximo || encontrado){k=0;
									if(encontrado==true){if(indice2<maximo){indice2++;}encontrado=false;
										
									}
                            	}
                			} while (arrayperiodos!=periodos[1].length);
                        	promedio.push(suma);k=0;
							//Analisis Vertical %
                        	do {
                            	console.log("indice 1 "+indice +'indice 2 '+indice2);
                            	if(arrayperiodos2==0 && k==0){
                            		var periodocuenta=parseInt(cuentas[j][indice3].periodo_id);var actual=parseInt(periodos[1][arrayperiodos2]);
                            		if(parseInt(cuentas[j][indice3].periodo_id)===parseInt(periodos[1][arrayperiodos2])){
                            			calculo=parseInt(cuentas[j][indice3].val_cuenta_entidad_valor)/promedio[j]*100;
                        				tregistros=tregistros+'<th>'+calculo.toFixed(2)+'%</th>';
                        				encontrado=true;porcenthorizon.push(calculo.toFixed(2));
                        			}else{tregistros=tregistros+'<th>0</th>';suma=suma+0;array.push(0);porcenthorizon.push(parseFloat("0"));}
                                }
                        		else{
                        			var periodocuenta=parseInt(cuentas[j][indice3].periodo_id);var actual=parseInt(periodos[1][arrayperiodos2]);
	                            	if(parseInt(cuentas[j][indice3].periodo_id)===parseInt(periodos[1][arrayperiodos2])){
	                            		calculo=parseInt(cuentas[j][indice3].val_cuenta_entidad_valor)/promedio[j]*100;
                        				tregistros=tregistros+'<th>'+calculo.toFixed(2)+'%</th>';
                        				encontrado=true;porcenthorizon.push(calculo.toFixed(2));
	                				}else{tregistros=tregistros+'<th>0</th>';array.push(0);porcenthorizon.push(parseFloat("0"));}
                        		}    
                            	arrayperiodos2++;					
								k++;
                            	if(k==maximo || encontrado){k=0;
									if(encontrado==true){if(indice3<maximo){indice3++;}encontrado=false;
										
									}
                            	}
                			} while (arrayperiodos2!=periodos[1].length);
                        	arrayporcentaje.push(porcenthorizon);
                			indice++;var sumporcen=0;
                			for(n in arrayporcentaje[j]){
								sumporcen=sumporcen+parseFloat(arrayporcentaje[j][n]);
                    		}
                			tregistros=tregistros+'<th>'+sumporcen.toFixed(2)+'%</th>';
                			promediohis.push(sumporcen/(arrayporcentaje[0].length));
                        tregistros=tregistros+'</tr>';
                    }
                tregistro=tregistros+'</tbody></table>';
                console.log(promedio);   console.log(arrayporcentaje);
               	console.log(promediohis);
                document.getElementById("tabla-proyeccion").innerHTML = headconten+''+tregistros;
                $(document).ready(function() {
           		 $('#example').DataTable({
           		    responsive: false,"scrollX": true,
                            "paging":   false,
                            "ordering": false,
                            "info":     false,
                            "destroy": true
           		});          
                        
           		} );
                console.log(cuentas);
                setTimeout($.unblockUI, 1000); 
                }
    	});
        }


    }
    
    
    
    function generarGrafico(data, periodos, container){
       container = eval(container);
    // $("#divGraficos").append("<div class='col-md-6' style='height: 400px;width:450px;' id='"+container+"'></div>");  
    console.log(container);
//     chart = new Highcharts.Chart({
//                chart: {
//                    type: 'pie',
//                    renderTo: container
//                },
//        title: {
//            text: '',
//            x: -20 //center
//        },
//        subtitle: {
//            text: '',
//            x: -20
//        },
//        xAxis: {
//            categories: periodos
//        },
//        yAxis: {
//            title: {
//                text: ''
//            },
//            plotLines: [{
//                value: 0,
//                width: 1,
//                color: '#808080'
//            }]
//        },
//        tooltip: {
//             valuePrefix: '$'
//        },
//        legend: {
//            layout: 'vertical',
//        
//        }, 
//        series:  data
//    });

     Highcharts.chart(container, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Grafico de Composición'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: data
//        series: [{
//            name: 'Brands',
//            colorByPoint: true,
//            data: [{
//                name: 'Microsoft Internet Explorer',
//                y: 56.33
//            }, {
//                name: 'Chrome',
//                y: 24.03,
//                sliced: true,
//                selected: true
//            }, {
//                name: 'Firefox',
//                y: 10.38
//            }, {
//                name: 'Safari',
//                y: 4.77
//            }, {
//                name: 'Opera',
//                y: 0.91
//            }, {
//                name: 'Proprietary or Undetectable',
//                y: 0.2
//            }]
//        }]
    });
    lstDivGraficos.push(container);
    lstGraficos.push(chart);
    
}
    
    
    function proyecto(data, mayor) {
        var tregistros = '';
        for (var i in data) {
            for (var j in data[i].componenteproyecto) {
                for (var k in data[i].componenteproyecto[j].actividadproyecto) {
                    tregistros = tregistros + '<tr>';
                    tregistros = tregistros + '  <td>' + data[i].proyecto_financiero_nombre + ' </td>';
                    tregistros = tregistros + '  <td>' + data[i].componenteproyecto[j].componente_proyecto_descripcion + ' </td>';
                    tregistros = tregistros + '  <td>' + data[i].componenteproyecto[j].actividadproyecto[k].actividad_proyecto_descripcion + ' </td>';
                    tregistros = tregistros + '  <td>' + data[i].componenteproyecto[j].actividadproyecto[k].actividad_proyecto_fuente_financiamiento + ' </td>';
                    for (var l in data[i].componenteproyecto[j].actividadproyecto[k].ejecucion) {
                        tregistros = tregistros + '  <td>' + data[i].componenteproyecto[j].actividadproyecto[k].ejecucion[l].ejecucion_valor + ' </td>';
                    }
                    if (mayor > data[i].componenteproyecto[j].actividadproyecto[k].ejecucion.length) {
                        for (m = 0; m < (mayor - data[i].componenteproyecto[j].actividadproyecto[k].ejecucion.length); m++) {
                            tregistros = tregistros + '  <td> </td>';
                        }
                    }
                    tregistros = tregistros + '</tr>';
                }
            }

        }
        console.log(tregistros);
        return tregistros;
    }

</script>

@endsection
