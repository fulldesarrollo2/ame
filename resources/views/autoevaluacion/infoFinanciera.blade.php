@extends('layouts.app')

@section('content')
<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2><span class="text-navy"><i class="fa fa-chevron-circle-right"></i> Administración de Información Financiera</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                <div class="col-lg-12">
                                     @if(Session::has('message'))
                                    <div class="alert alert-success" >
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <p> {{Session::get('message')}} </p>
                                    </div>
                                    @endif

                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    
                                    <div id="mensajes" name="mensajes" class="hide">
                                        <label id="textoMensaje" name="textoMensaje"></label>
                                    </div>
                                    
                
                                     <hr size="1" />
                                    <form class="form-horizontal" id="guardaInformacionFinanciera" name="formInf" method="POST"  role="form" enctype="multipart/form-data"   action="{{url('/guardarInfoFinan')}}" >
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                                        
                                                <br>
                                                <div class="row">  
                                                    <label class="col-sm-2 control-label" >Fuente </label>
                                                    <div class="col-sm-4">
                                                        <select class="form-control" id="tipo_fuente_id" name="tipo_fuente_id">
                                                            <option  value="">Seleccione...</option> 
                                                            @foreach($lstTipoFuente as $select) 
                                                            <option  value="{{$select->tipo_fuente_id}}">{{$select->tipo_fuente_descripcion}}</option> 
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <label class="col-sm-2 control-label" >Periodo </label>
                                                    <div class="col-sm-4">
                                                        <div id="cmb_periodo_id"></div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <label class="col-lg-2 control-label" >Selección de tipo de archivo </label>
                                                    <div class="col-sm-4">
                                                         <div id="cmb_tipo_archivo_id"></div>
                                                    </div>
                                                    
                                                    <label class="col-lg-2 control-label" >Selecciòn de tipo de cuenta </label>
                                                    <div class="col-sm-4">
                                                         <div id="cmb_tipo_cuenta_id"></div>
                                                    </div>
                                                </div>
                                                <br>
                                                 <div class="row">
                                                    <label class="col-lg-2 control-label" > Cédulas de ingreso</label>
                                                    <div class="col-sm-4">
                                                         <div id="cmbCedulasIngresos"></div>
                                                    </div>
                                                    
                                                    <label class="col-lg-2 control-label" > Cédulas de Gastos</label>
                                                    <div class="col-sm-4">
                                                         <div id="cmbCedulasGastos"></div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row"> 
                                                        <label class="col-lg-2 " for="name" style=""><i class="fa fa-folder-open" aria-hidden="true"></i>
                                                        Adjuntar Archivo </label>
                                                        <div class="col-lg-3">
                                                            <div class="btn btn-default btn-file">
                                                              <i class="fa fa-paperclip"></i> Adjuntar archivo
                                                              <input onchange="checkfile(this);" type="file" accept=".csv" required pattern=".+\.(csv)"  id="archivo_carga_archivo_byte" name="archivo_carga_archivo_byte" class="" >
                                                            </div>
                                                            <p class="help-block"  >.</p>
                                                            <div id="texto_notificacion">
                                                           </div>
                                                        </div>
                                                </div>
                                                <div class="row "> 
                                                    <div class="col-md-25 col-md-offset-9">  
                                                        <button type="button" class="btn btn-primary" id="btn_Procesar" > Procesar</button>
                                                        <button type="button" class="btn btn-default" id="btn_Cancelar" > Cancelar</button>
                                                   </div>
                                                </div>   
                                            </form>
                                    
                              @if($error) 
                                  <form class="form-horizontal" id="formReporte" name="formReporte" method="POST"  role="form" enctype="multipart/form-data"   action="{{url('/imprimirPdf')}}" >
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">   
                                               
                                                
                                                     <div class="modal-footer">
                                                         <a  id="btn_imprimir" class="btn btn-primary" type="button" href="javascript:imprSelec('formReporte')" >
                                                             <i class="fa fa-print"></i> Imprimir
                                                         </a>
                                                        <button type="button" class="btn btn-primary" id="btn_exportarExcel" >
                                                               <i class="fa fa-file-excel-o"></i> Exportar a Excel

                                                        </button>
                                                        <button type="submit" class="btn btn-primary" id="btn_exportarPdf" >
                                                              <i class="fa fa-file-pdf-o"></i> Exportar a pdf
                                                        </button>  
                                                     </div>
                                                
                                                <br>
                                        
                                       
                                        
                                            <div  id="lstEnti"  > 
                                                  @if($lstErrores!=null ) 
                                                  
                                                    <table id="lstError" name="lstEntidad" class="table disp"  cellspacing="0" width="100%">
                                                     <thead>
                                                         <tr>  
                                                             <th>&nbsp;Id columna Inconsistente.</th>
                                                             <th>&nbsp;Número de columnas</th>
                                                             <th>&nbsp;Posición de columnas</th>
                                                             <th>&nbsp;Tipo de dato</th>
                                                             <th>&nbsp;Longitud de dato</th>
                                                             <th>&nbsp;Requerido</th>
                                                         </tr>
                                                     </thead>  
                                                     <tbody name="lstEntidad" >    

                                                         @for ($i = 0; $i < count($lstErrores); $i++)
                                                         <tr>
                                                             <td>{{$lstErrores[$i]['idColumna'] }} </td>
                                                             <td>{{$lstErrores[$i]['numColum']}} </td>
                                                             <td>{{$lstErrores[$i]['posColum']}}</td>
                                                             <td>{{$lstErrores[$i]['tipoDato']}}</td>
                                                             <td>{{$lstErrores[$i]['logDato']}}</td>
                                                             <td>{{$lstErrores[$i]['requerido']}}</td>

                                                         </tr>
                                                         @endfor
                                                     </tbody>
                                                    </table>
                                                  
                                                   @endif
                                          </div>
                                             @if($lstCuentaNoReferencia!=null) 
                                           
                                            <div class="row"> 
                                             <div class="col-md-6">
                                                 
                                                <table id="lstcuentaInexistentes" name="lstEntidad" class="table disp"  cellspacing="0" width="100%">
                                                   
                                                               <thead>
                                                                   <tr>  
                                                                       <th>&nbsp;No existe referencia de cuenta .</th>

                                                                   </tr>
                                                               </thead>  
                                                               <tbody>    

                                                                   @for ($x = 1; $x < count($lstCuentaNoReferencia)+1; $x++)
                                                                   <tr>
                                                                       <td>{{$lstCuentaNoReferencia[$x]['cuenta'] }} </td>
                                                                   </tr>
                                                                   @endfor
                                                               </tbody>
                                                </table >
                                                 
                                              
                                            </div> 
                                                <br>
                                            <div class="col-md-6">
                                                <table    class="table disp">
                                                            <tr>
                                                               <td># de Entidades en el archivo</td><td>contenido1</td>
                                                            </tr>
                                                            <tr>
                                                               <td># de cuentas de ingreso</td><td>contenido3</td>
                                                            </tr>
                                                            <tr>
                                                               <td># de cuentas de gastos</td><td>contenido5</td>
                                                            </tr>
                                                </table>
                                            </div>    
                                                
                                                
                                            </div>
                                             @endif
                                </form> 
                                @endif 
                                @if(!$error) 
                                    @if($lstCuentaRegistradas!=null) 
                                     <form class="form-horizontal" id="formReporte" name="formReporte" method="POST"  role="form" enctype="multipart/form-data"   action="{{url('/imprimirPdf')}}" >
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">   
                                               
                                                
                                                     <div class="modal-footer">
                                                         <a  id="btn_imprimir" class="btn btn-primary" type="button" href="javascript:imprSelec('formReporte')" >
                                                             <i class="fa fa-print"></i> Imprimir
                                                         </a>
                                                        <button type="button" class="btn btn-primary" id="btn_exportarExcel" >
                                                               <i class="fa fa-file-excel-o"></i> Exportar a Excel

                                                        </button>
                                                        <button type="submit" class="btn btn-primary" id="btn_exportarPdf" >
                                                              <i class="fa fa-file-pdf-o"></i> Exportar a pdf
                                                        </button>  
                                                     </div>
                                                
                                                <br>
                                        
                                        <table id="lstError" name="lstEntidad" class="table disp"  cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>  
                                                        <th>&nbsp;Codigo</th>
                                                        <th>&nbsp;Cuentas</th>
                                                        <th>&nbsp;Totales</th>
                                                    </tr>
                                                </thead>  
                                                <tbody name="lstEntidad" > 
                                                    @for ($a = 0; $a < count($lstCuentaRegistradas); $a++)
                                                    <tr>
                                                        <td>{{$lstCuentaRegistradas[$a]['codigo']  }} </td>
                                                        <td>{{$lstCuentaRegistradas[$a]['cuenta']  }} </td>
                                                        <td>{{$lstCuentaRegistradas[$a]['totales'] }}</td>
                                                    </tr>
                                                    @endfor
                                                </tbody>
                                        </table>
                                    @endif  
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')

<script language="JavaScript" type="text/javascript">
 function myFunction() {
            $(document).on('keydown', function(e) {
                    if(e.ctrlKey && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80) ){
              alert("Please use the Print PDF button below for a better rendering on the document");
              e.cancelBubble = true;
              e.preventDefault();

              e.stopImmediatePropagation();
           } 
           });
        }
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }); 
        
        
         $('#lstError').DataTable({
            responsive: true
        });
        
        $('#lstcuentaInexistentes').DataTable({
            responsive: true
        });
       
        function imprSelec(nombre)
        {
            window.print( );
            window.close( );
            /*var ficha = document.getElementById(nombre);
            var ventimp = window.open(' ', 'popimpr');
            ventimp.document.write( ficha.innerHTML );
            ventimp.document.close();
            ventimp.print( );
            ventimp.close();*/
        } 

        
        
         $("#btn_exportarExcel").click(function (event){
              $('#formReporte').attr('action', '{{ url('/imprimirExcel') }}');
              $("#formReporte").submit();
             
          });
          
        $("#btn_exportarPdf").click(function (event){
           $('#formReporte').attr('action', '{{ url('/imprimirPdf') }}');
           $("#formReporte").submit();

        });
        $("#btn_Cancelar").click(function (event){
              $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
             setTimeout($.unblockUI, 4000);
           $('#guardaInformacionFinanciera').attr('action', '{{ url('/infoFinanciera') }}');
           
           $("#guardaInformacionFinanciera").submit();

        });
        
        
        
        function checkfile(sender) {
                 var validExts = new Array(".csv");
                 var fileExt =$("#archivo_carga_archivo_byte").val();
                 //var fileExt = sender.value;
                 fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
                 if (validExts.indexOf(fileExt) < 0) {
                   
                    $('#mensajes').removeClass();
                    $('#mensajes').addClass('alert alert-danger');
                    var tips = $("#textoMensaje");
                    tips.text("Archivo no válido seleccionado, los archivos válidos son de tipo " +
                            validExts.toString() + " .");
                   $("#archivo_carga_archivo_byte").val("");     
                   return false;
                 }
                 else return true;
        }
        
        $("#btn_Procesar").click(function (event){
                var bValid=true;
                var tipo_fuente_id= $("#tipo_fuente_id").val();
                var tipo_archivo_id=$("#tipo_archivo_id").val();
                var periodo_id=$("#periodo_id").val();
                var tipo_cuenta_id=$("#tipo_cuenta_id").val();
                var cedula_ingreso_id=$("#cedula_ingreso_id").val();
                var cedula_gasto_id=$("#cedula_gasto_id").val();
                //var tipo_entidad_id=$("#archivo_carga_archivo_byte").val();
                
                
                bValid = bValid && validarCampoLleno(tipo_fuente_id, "Nombre tipo Fuente");
                bValid = bValid && validarCampoLleno(tipo_archivo_id, "Nombre tipo Archivo");
                bValid = bValid && validarCampoLleno(periodo_id, "Nombre periodo");
                bValid = bValid && validarCampoLleno(tipo_cuenta_id, "Nombre tipo cuenta");
                if(tipo_cuenta_id!="ninguna"){
                     bValid = bValid && validarCampoLleno(cedula_ingreso_id, "Nombre cedulas de ingreso");
                     bValid = bValid && validarCampoLleno(cedula_gasto_id, "Nombre cedulas de gasto");
                }
                bValid = bValid && checkfile();
                
                if(bValid){
                    
                     $('#mensajes').removeClass();
                     $('#mensajes').addClass('hide');
                     $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                     setTimeout($.unblockUI, 7000);
                     $("#guardaInformacionFinanciera").submit(); 
                }
          
          });
        
        function consultarCedulaGastos(){ //$("#tipo_archivo_id").change(function (event){
                var tipo_cuenta_id= $("#tipo_cuenta_id").val();
               
                    $.ajax({
                         url: '{{ url('/buscarCedulasGastos') }}',
                         type: 'post',
                         data: {'tipo_cuenta_id': tipo_cuenta_id},
                         beforeSend: function() {
                            
                          },
                         error : function(jqXHR, textStatus, errorThrown) {
                            var gnrMensaje =$("#gnrMensaje");
                            if(jqXHR.status == '401'){
                                gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar."); 
                                $('#gnrError').modal('show');
                            }     
                        },
                         success: function(data) {
                            
                         var cadena = JSON.stringify(data);
                         var materias= verTipoArchivo(data);
                                var message='<select id="cedula_gasto_id" name="cedula_gasto_id"  class="form-control">'+materias+
                                            '</select>';
                               $("#cmbCedulasGastos").empty();
                               $("#cmbCedulasGastos").append(message);  
                     },
                     complete: function() {
                        
                    }
                 });
                    function verTipoArchivo(data){
                         var tregistros = '<option value="">Seleccione..</option>';
                             for(var i in data){
                                 tregistros =tregistros + '<option value='+data[i].cedula.cedula_id+'>'+ data[i].cedula.cedula_nombre +' </option>';

                          }
                            return tregistros;
                    }
            } 
            function consultarcedulas(){ //$("#tipo_archivo_id").change(function (event){
               var tipo_cuenta_id= $("#tipo_cuenta_id").val();
               
                    $.ajax({
                         url: '{{ url('/buscarCedulasIngresos') }}',
                         type: 'post',
                         data: {'tipo_cuenta_id': tipo_cuenta_id},
                         beforeSend: function() {
                            $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                          },
                         error : function(jqXHR, textStatus, errorThrown) {
                            var gnrMensaje =$("#gnrMensaje");
                            if(jqXHR.status == '401'){
                                gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar."); 
                                $('#gnrError').modal('show');
                            }     
                        },
                         success: function(data) {
                            
                         var cadena = JSON.stringify(data);
                         var materias= verTipoArchivo(data);
                                var message='<select id="cedula_ingreso_id" name="cedula_ingreso_id"  class="form-control">'+materias+
                                            '</select>';
                               $("#cmbCedulasIngresos").empty();
                               $("#cmbCedulasIngresos").append(message);  
                     },
                     complete: function() {
                        setTimeout($.unblockUI, 1000);
                    }
                           
                   });
                    function verTipoArchivo(data){
                         var tregistros = '<option value="">Seleccione..</option>';
                             for(var i in data){
                                 tregistros =tregistros + '<option value='+data[i].cedula.cedula_id+'>'+ data[i].cedula.cedula_nombre +' </option>';

                          }
                            return tregistros;
                    }
                consultarCedulaGastos();    
                
            } 
           
          function consultarCuenta(){ //$("#tipo_archivo_id").change(function (event){
               var tipo_archivo_id= $("#tipo_archivo_id").val();
                $("#cmbCedulasGastos").empty();
                $("#cmbCedulasIngresos").empty();
              
                    $.ajax({
                         url: '{{ url('/buscarTipCuentDeTipArchivo') }}',
                         type: 'post',
                         data: {'tipo_archivo_id': tipo_archivo_id},
                         beforeSend: function() {
                            $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                          },
                         error : function(jqXHR, textStatus, errorThrown) {
                            var gnrMensaje =$("#gnrMensaje");
                            if(jqXHR.status == '401'){
                                gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar."); 
                                $('#gnrError').modal('show');
                            }     
                        },
                         success: function(data) {
                           
                         var cadena = JSON.stringify(data);
                         var materias= verTipoArchivo(data);
                                var message='<select id="tipo_cuenta_id" onchange="consultarcedulas();" name="tipo_cuenta_id"  class="form-control">'+materias+
                                            '</select>';
                               $("#cmb_tipo_cuenta_id").empty();
                               $("#cmb_tipo_cuenta_id").append(message);  
                     },
                      complete: function() {
                      //  setTimeout($.unblockUI, 10000);
                    }       
                 });
                    function verTipoArchivo(data){
                           
                            if(data.length>0){
                                 var tregistros = '<option value="">Seleccione..</option>';
                                for(var i in data){

                                    tregistros =tregistros + '<option  value='+data[i].tipo_cuenta_id+'>'+ data[i].tipo_cuenta_descripcion +' </option>';

                               }
                             }else{ var tregistros = '<option value="ninguna">Ninguna</option>';}
                            return tregistros;
                    }
            
                 // metodo implementado para obtener la configuracion del archivo a subir    
                   $.ajax({
                         url: '{{ url('/buscarParamArchCarga') }}',
                         type: 'post',
                         data: {'tipo_archivo_id': tipo_archivo_id},
                         beforeSend: function() {
                            $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                          },
                         error : function(jqXHR, textStatus, errorThrown) {
                            var gnrMensaje =$("#gnrMensaje");
                            if(jqXHR.status == '401'){
                                gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar."); 
                                $('#gnrError').modal('show');
                            }     
                        },
                         success: function(data) {
                            
                            
                                $('#mensajes').addClass('alert alert-warning');
                                $('#mensajes').removeClass('hide');
                               var tips = $("#textoMensaje");
                               tips.text("" + data.mensaje + ""); 
                               if(data.bar!=0)
                               {
                                   $("#btn_Procesar").attr('disabled',false)
                               }else{
                                   $("#btn_Procesar").attr('disabled',true)
                               }
                     },
                     complete: function() {
                        setTimeout($.unblockUI, 1000);
                    }
                 }); 
            } 
       
             /**
             * Consulta el  combo tipo de archivo
             * Consulta combo de periodo
              */
      $("#tipo_fuente_id").change(function (event){
               var tipo_fuente_id=$("#tipo_fuente_id").val();
                    $.ajax({
                         url: '{{ url('/buscarTipoArchivo') }}',
                         type: 'post',
                         data: {'tipo_fuente_id': tipo_fuente_id},
                         beforeSend: function() {
                            $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                          },
                         error : function(jqXHR, textStatus, errorThrown) {
                            var gnrMensaje =$("#gnrMensaje");
                            if(jqXHR.status == '401'){
                                gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar."); 
                                $('#gnrError').modal('show');
                            }     
                        },
                         success: function(data) {
                            
                         var cadena = JSON.stringify(data);
                         var materias= verTipoArchivo(data);
                                     var message='<select id="tipo_archivo_id" onchange="consultarCuenta();"  name="tipo_archivo_id" class="form-control">'+materias+
                                            '</select>';
                               $("#cmb_tipo_archivo_id").empty();
                               $("#cmb_tipo_archivo_id").append(message);  
                     },
                     complete: function() {
                        setTimeout($.unblockUI, 100);
                    }
                 });
                    function verTipoArchivo(data){
                         var tregistros = '<option value="">Seleccione..</option>';
                             for(var i in data){
                                 tregistros =tregistros + '<option value='+data[i].tipo_archivo.tipo_archivo_id+'>'+ data[i].tipo_archivo.tipo_archivo_nombre +' </option>';

                          }
                            return tregistros;
                    }
            
                // método consulta periodos por tipo fuente
                $.ajax({
                         url: '{{ url('/buscarPeriodoPorFuente') }}',
                         type: 'post',
                         data: {'tipo_fuente_id': tipo_fuente_id},
                         beforeSend: function() {
                            $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                          },
                         error : function(jqXHR, textStatus, errorThrown) {
                            var gnrMensaje =$("#gnrMensaje");
                            if(jqXHR.status == '401'){
                                gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar."); 
                                $('#gnrError').modal('show');
                            }     
                        },
                         success: function(data) {
                            
                         var cadena = JSON.stringify(data);
                         var materias= verPeriodo(data);
                                     var message='<select id="periodo_id"   name="periodo_id" class="form-control">'+materias+
                                            '</select>';
                               $("#cmb_periodo_id").empty();
                               $("#cmb_periodo_id").append(message);  
                     },
                     complete: function() {
                        setTimeout($.unblockUI, 100);
                    }
                 });
                    function verPeriodo(data){
                         var tregistros = '<option value="">Seleccione..</option>';
                             for(var i in data){
                                 tregistros =tregistros + '<option value='+data[i].periodo_id+'>'+ data[i].periodo_anio +' - '+data[i].periodo_mes+ ' </option>';

                          }
                            return tregistros;
                    }  
            });
</script>

@endsection