@extends('layouts.app')

@section('content')
 <div class="row">
                  

<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2><span class="text-navy"><i class="fa fa-chevron-circle-right"></i> Administracón de proyecto Financieros</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                <div class="col-lg-12">
                                    @if(count($errors) > 0)
                                            <div class="alert alert-danger">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    @foreach($errors->all() as $error)
                                                    <label>{{ $error }}</label> 
                                                    </br>
                                                    @endforeach
                                                    
                                            </div>
                                    @endif
                                    
                                    @if (notify()->ready())
                                    <div class="alert alert-{{ notify()->type() }}">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <label>{{ notify()->message() }}</label>
                                    </div>
                                    @endif
                                    

                                    <div id="mensajes" name="mensajes" class="hide">

                                        <label id="textoMensaje" name="textoMensaje"></label>
                                    </div>

                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                         
                                        <div class="panel blank-panel">
                                            <div class="panel-heading">
                                                <div class="panel-options">

                                                    <ul class="nav nav-tabs">
                                                        <li class="active"><a id="enlace" data-toggle="tab" href="#tab-1">Proyecto Financiero</a></li>
                                                        <li class=""><a data-toggle="tab" href="#tab-2">Buscar</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="tab-content">
                                                    <div id="tab-1" class="tab-pane active">
                                     
                                            <form class="form-horizontal" method="POST" name="formValiInconInformacion" enctype="multipart/form-data"  action="{{url('/guardarInfoEspIndicador')}}" role="form">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                 <div class="form-group">
                                                    <label class="col-lg-2 control-label " >Nombre</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="proyecto_financiero_nombre" name="proyecto_financiero_nombre">
                                                    </div>
                                                    <label class="col-lg-2 control-label " >Objetivo</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="proyecto_financiero_descripcion" name="proyecto_financiero_descripcion">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6 ">
                                                        <label class="control-label text-right">
                                                            <input id="proyecto_financiero_tipo" name="proyecto_financiero_tipo" type="checkbox">Proyecto de Servicios básicos municipales
                                                        </label>
                                                    </div>
                                                    <label class="col-lg-2 control-label " >Periodo Actual</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" value="{{$objPeriodoActual->variable_seguridad_valor }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group" >
                                                    <label class="col-lg-2 control-label">Range select</label>
                                                    <div class="col-sm-4 " id="data_5">
                                                        <div class="input-daterange input-group" id="datepicker">
                                                            <input type="date" class="input-sm form-control" id="fcha_inicio" name="start" />
                                                            <span class="input-group-addon">to</span>
                                                            <input type="date" class="input-sm form-control" id="fcha_fin"  name="end"/>
                                                        </div>
                                                    </div>
                                                    <label class="col-lg-2 control-label">Estado</label>
                                                     <div class="col-lg-4">
                                                        <select class="form-control m-b" name="estado_id" id="estado_id" >
                                                            <option value="">Seleccionar...</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            Componentes
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="form-group">
                                                                <label class="col-lg-12" >Descripción Componente</label>
                                                                     <div class="col-sm-5">
                                                               <input type="text" name="descripcion_componente" id="descripcion_componente" class="form-control">
                                                                </div>
                                                               <div class="col-sm-2">
                                                                    <button id="btn_agregar" class="btn btn-outline btn-primary" type="button">
                                                                        <i class="fa fa-plus" aria-hidden="true"></i> Agregar</button>
                                                                    <button id="btn_quitar" class="btn btn-outline btn-primary" type="button">
                                                                        <i class="fa fa-times" aria-hidden="true"></i> Quitar</button>
                                                               </div>
                                                               <div class="col-sm-5">
                                                               <select class="form-control" id="cmb_componentes" name="cmb_componentes" multiple>
                                                                     
                                                                </select>
                                                               </div>
                                                            </div>
                                                               <input type="hidden" name="proyecto_id" id="proyecto_id">
                                                        </div>


                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            Actividades del Componente
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Descripción Actividad</label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" name="descripcion_actividad" id="descripcion_actividad" class="form-control">
                                                                </div>
                                                                <label class="col-sm-2 control-label">Fuente de Financiamiento</label>
                                                                <div class="col-sm-4">
                                                                    <input type="text" name="fuente_financiamiento" id="fuente_financiamiento" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Catálogo de Cuentas Oficiales Cargadas</label>
                                                                <div class="col-sm-10">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-body" id="divCuentas">
                                                                             <div id="arbolCuentas" class="step-content" >
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" name="cuenta_id_editar" id="cuenta_id_editar">
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-12 text-right">
                                                                    <button id="btn_agregar_actividad" class="btn btn-outline btn-primary" type="button">
                                                                        <i class="fa fa-plus" aria-hidden="true"></i> Agregar</button>
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="hr-line-dashed"></div>
                                                                <div class="table-responsive">
                                                                    <table id="tbl_proyecto" class="table disp"  cellspacing="0" width="100%">
                                                                        <thead>

                                                                        </thead>  
                                                                        <tbody>    

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12 text-right">
                                                        <button id="btn_almacenar_proyecto" class="btn btn-primary" type="button">Almacenar</button>
                                                        <button id="btn_cancelar_proyecto" class="btn btn-primary" type="button">Cancelar</button>
                                                    </div>
                                                </div>
                                            </form>
                                            </div>
                                            <div id="tab-2" class="tab-pane">
                                             <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                              <table class=" display table table-bordered" id="table">
                                                <thead>
                                                  <th>Nombre del proyecto</th>
                                                  <th>Descripción</th>
                                                  <th>Tipo</th>
                                                  <th>Fecha inicio</th>
                                                  <th>Fecha fin</th>
                                                  <th>...</th>
                                                </thead>
                                                <tbody>
                                                  @foreach($lstaProyectoFinanciero as $proyectoFinanciero)
                                                  <tr>
                                                    <td>{{ $proyectoFinanciero->proyecto_financiero_nombre}}</td>
                                                    <td>{{ $proyectoFinanciero->proyecto_financiero_descripcion}}</td>
                                                    <td>{{ $proyectoFinanciero->proyecto_financiero_tipo }}</td>
                                                    <td>{{ $proyectoFinanciero->proyecto_financiero_inicio }}</td> 
                                                    <td>{{ $proyectoFinanciero->proyecto_financiero_fin }}</td>
                                                    <td>
                                                      <a href="#" class="iconrs" onclick="editarProyecto('{{ $proyectoFinanciero->proyecto_financiero_id}}')"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></i>Editar</a> </br>
                                                    </td>
                                                  </tr>
                                                  @endforeach
                                                </tbody>
                                              </table>

                                            </div>
                                          </div>
                                         </div>
                                         </div>
                                     </div>
                                </div>
                            </div>
                        </div>
                     </div>
                 </div>
            </div>
          </div>
     </div>
  </div>
</div>
@endsection

@section('script')
<script src="../js/plugins/datapicker/bootstrap-datepicker.js"></script> <!--Añadir en el app.blade-->

<script type="text/javascript">
 
    $(document).ready(function () {   
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        buscarCuenta();
    });



buscarCuenta();

    function buscarCuenta() {
        
        var data =  <?php echo json_encode($lstCuentasPresupuestarias); ?>;              
        var arbol = dataArbolDinamico(data,"cuenta_nombre","cuenta_id","cuenta_hijo","true","cuenta_codigo","cuenta_nombre");
        $('#arbolCuentas').treeview({
            levels: 1,
            color: "#555",
            showBorder: false,
            data: arbol,
            multiSelect: $('#chk-select-multi').is(':checked'),
            onNodeSelected: function (event, node) {
                $("#cuenta_id_editar").val(); 
                $("#cuenta_codigo").val();
                $("#cuenta_nombre").val(); 

                $('#divCtaSeleccionada').removeClass('hide');
                $("#cuenta_id_editar").val(node.tags);
                $('#nomCtaSeleccionada').val(node.text);
            },
            onNodeUnselected: function (event, node) {
                $('#divCtaSeleccionada').addClass('hide');
                $('#nomCtaSeleccionada').val("");
                $("#tipo_cuenta_id").attr('disabled',false);
                $("#cuenta_nombre").val("");
                $("#cuenta_codigo").val("");
            }
        });

        }
 
 $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });    
  
$( "#fcha_inicio" ).blur(function() {
  bValid = true;
        bValid = bValid && checkFechaFin()
      if(bValid){
        buscarPeriodo();
    }
});

$( "#fcha_fin" ).blur(function() {
       bValid = true;
        bValid = bValid && checkFechaFin()
      if(bValid){
        buscarPeriodo();
    }
});

function checkFechaFin() {
    var f = new Date();
    var i;
    var j;
    var pacFecIni = $("#fcha_inicio").val();
    var pacFecNac = $("#fcha_fin").val();
    if (f.getMonth() < 10)
        i = "0" + (f.getMonth() + 1);
    else
        i = (f.getMonth() + 1);
    if (f.getDate() < 10)
        j = "0" + f.getDate();
    else
        j = f.getDate();
    var fActual = f.getFullYear() + "-" + i + "-" + j;
   
        if (pacFecNac == pacFecIni) {
            $('#mensajes').removeClass('hide');
            $('#mensajes').removeClass('alert alert-info');
            $('#mensajes').removeClass('alert alert-warning');
            $('#mensajes').addClass('alert alert-danger');
            var tips = $("#textoMensaje");
            tips.text("Error ! La Fecha Fin no puede ser igual a la Fecha Inicio ");
            return false;
        } 
        else
        {
            if (pacFecNac < pacFecIni) {
              $('#mensajes').removeClass('hide');
                $('#mensajes').removeClass('alert alert-info');
                $('#mensajes').removeClass('alert alert-warning');
                $('#mensajes').addClass('alert alert-danger');
                var tips = $("#textoMensaje");
            tips.text("Error ! La Fecha Fin no puede ser  menor a la Fecha Inicio ");
            return false;
            } 
            else
            {
                return true;
            }
        }

}





 function buscarPeriodo(){
     var fcha_inicio = $("#fcha_inicio").val(),
        fcha_fin = $("#fcha_fin").val(),
         proyecto_id = $("#proyecto_id").val(),
        bValid = true;
        bValid = bValid && validarCampoLleno(fcha_inicio, "Campo feha inicio")
        bValid = bValid && validarCampoLleno(fcha_fin, "Campo fecha fin")

        if(bValid){
    
             
            $.ajax({
               url  :'{{ url('/buscarPeriodo') }}',
               type: 'post',
               "_token": "{{ csrf_token() }}",
               async: true,
               data :{'fcha_inicio' : fcha_inicio, 'fcha_fin' : fcha_fin,'proyecto_id':proyecto_id},

               error : function(jqXHR, textStatus, errorThrown) {
               },
                beforeSend: function(data){   
                            $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                        },

               success : function(data) {
                $('#estado_id').empty();
                objEstado = data.objEstado;

                nperiodos = data.countPeriodos.length;
                ejecucion = data.ejecucion;
                periodo = data.countPeriodos;
                actividad = data.actividad;
                var anio = 0;
                   if(data == ""){
                    }else{
                        for(var t = 0; t < objEstado.length; t++) { 
                            $('#estado_id').append($('<option>', {
                                value: objEstado[t].estado_id,
                                id: objEstado[t].estado_id,
                                text:objEstado[t].estado_descripcion
                           }));
                        }
                        
                        if(actividad == ""){
                            $(".anio").remove();
                            for(var i = 0; i< nperiodos; i++) {
                                anio = i+1;
                                $( ".thinner" ).before( "<th class='anio'>Año "+anio+"</th>" );
                            }
                        } else{
                            $("#tbl_proyecto tbody tr").remove();
                            $("#tbl_proyecto thead tr").remove();

                            var thead = "<tr>"+
                            "<th>&nbsp;&nbsp;CONPONENTES.</th>"+
                                            "<th>&nbsp;&nbsp;ACTIVIDADES</th>"+
                                            "<th>&nbsp;&nbsp;FUENTE FINANCIERA</th>"+
                                            "<th>&nbsp;&nbsp;GRUPO DE GASTOS</th>";

                            for(var a = 0; a< nperiodos; a++) {
                                anio = a+1;
                                var anioperiodo = periodo[a].periodo_id;
                                thead = thead + "<th id='"+anioperiodo+"'>Año "+periodo[a].periodo_anio+"</th>";
                            }

                            thead = thead +"<th  class='thinner'>&nbsp;&nbsp;-</th>"+
                            "</tr>";
                            var numRow= 1;
                            var numCol;
                            $("#tbl_proyecto thead").append(thead);
                            for(var i in actividad){
                                for(var j  in actividad[i]){
                                    var newRowContent = "<tr id='"+actividad[i][j].actividad_proyecto_id+"'>"+
                                                    "<td>"+actividad[i][j].componente_proyecto_descripcion+"</td>"+
                                                    "<td>" +actividad[i][j].actividad_proyecto_descripcion+"</td>"+
                                                    "<td>" +actividad[i][j].actividad_proyecto_fuente_financiamiento+"</td>"+
                                                    "<td>"+actividad[i][j].cuenta_codigo+"</td>" +
                                                     "<td class='inner' >  <a class='eliminarlsta'>"+
                                        "<i class='fa fa-times' aria-hidden='true'></i> Quitar</button></td>"+
                                             "</tr>";

                                    $("#tbl_proyecto tbody").append(newRowContent);
                                  
                                   for (var w = 0; w< nperiodos; w++) {
                                    numCol = w+1;
                                    $("#tbl_proyecto tr:last td.inner").before( "<td><input type='text' /></td>" );
                                }

                                      

                                    for (var d = 0; d < actividad[i][j].ejecucion.length; d++) {
                                        
                                        var anioejecucion = actividad[i][j].ejecucion[d].periodo_id;
                                        var totAnio= anio;
                                        var sumAnio= 0;
                                        
                                        while (totAnio != 0) { 
                                             var td = 4;
                                            var id = 0;
                                            td = td + sumAnio;
                                            tdAfter = td - 1 ;
                                            $('#tbl_proyecto thead tr').each(function () {
                                                id = $(this).find("th:eq("+(td)+")").attr('id');
                                            });
                                            if(id == 0){
                                            }else{
                                                var campoValor="";
                                                campoValor=$('#tbl_proyecto tr').eq(numRow).find("td:eq("+td+") input[type='text']").val();
                                                if(parseInt(id) == anioejecucion){
                                                    $('#tbl_proyecto tr').eq(numRow).find('td').eq(td).html("<input id='"+actividad[i][j].ejecucion[d].ejecucion_id+"' class='"+periodo[i].periodo_id+"' type='text' class='form-control' value='"+actividad[i][j].ejecucion[d].ejecucion_valor+"'/>");
                                                }else{ 
                                                   if(campoValor == null ){
                                                     $('#tbl_proyecto tr').eq(numRow).find('td').eq(td).html("<input class='' type='text' class='' value=''/>");  
                                                   }
                                                }
                                            }
                                            totAnio=totAnio-1;
                                            sumAnio=sumAnio+1;
                                         
                                        }
                                       
                                    }  
                                    numRow = numRow +1;
                                }
                            }
                        }
                    }
               }
           });  
          setTimeout($.unblockUI, 3000);
      }
    }

    function editarProyecto(proyecto_id){

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
     $.ajax({
               url  :'{{ url('/editarProyecto') }}',
               type: 'post',
               "_token": "{{ csrf_token() }}",
               async: true,
               data :{
                     'proyecto_id' : proyecto_id
                    },

               error : function(jqXHR, textStatus, errorThrown) {
                   console.log(textStatus);        
               },
               beforeSend: function(data){   
                            $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                        },

               success : function(data) {
                proyecto = data.proyecto;
                componentes = data.componentes;
                actividad = data.actividad;
                buscarCuenta();
                $('#mensajes').addClass('hide');
                $("#enlace").click();
                $("#proyecto_financiero_nombre").val(proyecto.proyecto_financiero_nombre);
                $("#proyecto_financiero_descripcion").val(proyecto.proyecto_financiero_descripcion);
                $("#proyecto_id").val(proyecto.proyecto_financiero_id);
                $("#proyecto_financiero_tipo").val(proyecto.proyecto_financiero_tipo);
                $("#fcha_inicio").val(proyecto.proyecto_financiero_inicio);
                $("#fcha_fin").val(proyecto.proyecto_financiero_fin);
                 buscarPeriodo();
                $("#estado_id").val(proyecto.estado_id);
                $("#descripcion_actividad").val("");
                $("#fuente_financiamiento").val("");

                 $('#cmb_componentes').empty();
                 $("#tbl_proyecto tbody tr").remove();

                    //Carga componentes con sus respectivos id
                        for(var i in componentes){
                            $("#proyecto_id").val(componentes[i].proyecto_financiero_id)
                            $('#cmb_componentes').append($('<option>', {
                                value: componentes[i].componente_proyecto_id,
                                id: componentes[i].componente_proyecto_id,
                                text:componentes[i].componente_proyecto_descripcion
                            }));
                         }

            setTimeout($.unblockUI, 2000);  
           }
        });

}
$("#btn_cancelar_proyecto").on('click',function(){
     $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
       limpiar();
    setTimeout($.unblockUI, 1000);  
});

function limpiar(){
        $("#proyecto_financiero_nombre").val("");
        $("#proyecto_financiero_descripcion").val("");
        $("#proyecto_id").val("");
        $("#proyecto_financiero_tipo").val("");
        $("#fcha_inicio").val("");
        $("#fcha_fin").val("");
        $("#estado_id").val("");
        $("#descripcion_actividad").val("");
        $("#fuente_financiamiento").val("");
        $('#cmb_componentes').empty();
        $("#tbl_proyecto tbody tr").remove();
        $(".anio").remove();
    }

    $("#btn_agregar").on('click', function() {
        var p =$("#descripcion_componente");
        bValid = true;
        bValid = bValid && validarCampoLleno(p.val(), "Campo componente");
       if(bValid){
            $('#cmb_componentes').append($('<option>', {
                text: p.val()
            }));
            agregarComponente();
            p.val("");
        }
        
      });

    $("#btn_quitar").on('click', function() {
        var p =$("#cmb_componentes option:selected"),
         componente_id =$("#cmb_componentes").val();

       p.remove();

        $.ajax({
               url  :'{{ url('/eliminarComponente') }}',
               type: 'post',
               "_token": "{{ csrf_token() }}",
               async: true,
               data :{
                     'componente_id' : componente_id
                    },

               error : function(jqXHR, textStatus, errorThrown) {
                  console.log(textStatus);            
               },

               success : function(data) {
                  $("#tbl_proyecto tbody tr").remove();

                  var newRowContent = "<tr id='"+data.actividad_proyecto_id+"'>"+
                                "<td>"+data.componente.componente_proyecto_descripcion+"</td>"+
                                "<td>" +data.actividad_proyecto_descripcion+"</td>"+
                                "<td>" +data.actividad_proyecto_fuente_financiamiento+"</td>"+
                                "<td>"+data.cuenta_codigo+"</td>"+
                                "<td><input type='text' class='form-control'/></td>"+
                                "<td><input type='text' class='form-control'/></td>"+
                                "<td><input type='text' class='form-control'/></td>"+
                                "<td>  <a class='eliminarlsta'>"+
                                        "<i class='fa fa-times' aria-hidden='true'></i> Quitar</button></td>"+
                            "</tr>";
                $("#tbl_proyecto tbody").append(newRowContent);

               }
        }); 
        
    });

function agregarComponente(){
    //Datos del proyecto
     var lstaComponentesGuardar = new Array(),
          proyecto_financiero_nombre = $("#proyecto_financiero_nombre").val(),
          proyecto_financiero_descripcion = $("#proyecto_financiero_descripcion").val(),
          //en caso de que ya exista
          proyecto_id = $("#proyecto_id").val(),
          proyecto_financiero_tipo = $("#proyecto_financiero_tipo").val(),
          fcha_inicio = $("#fcha_inicio").val(),
          fcha_fin = $("#fcha_fin").val(),
          estado_id = $("#estado_id").val();

      $("#cmb_componentes option").each(function(){
         var idComponente=$(this).attr('id');
         if (typeof idComponente === "undefined") {
             componente_id =  $(this).text();
             lstaComponentesGuardar.push(componente_id);
          }

        });

      bValid = true;
      bValid = bValid && validarCampoLleno(proyecto_financiero_nombre, "Campo nombre");
      bValid = bValid && validarCampoLleno(proyecto_financiero_descripcion, "Campo objetivo");
      bValid = bValid && validarCampoLleno(proyecto_financiero_tipo, "Campo tipo");
      bValid = bValid && validarCampoLleno(fcha_inicio, "Campo rango fecha");
      bValid = bValid && validarCampoLleno(fcha_fin, "Campo  rango fecha");
      bValid = bValid && validarCampoLleno(estado_id, "Campo  estado");
      bValid = bValid && validarCampoLleno(lstaComponentesGuardar, "Campo componentes seleccionados");

       if(bValid){
            $('#mensajes').removeClass('alert alert-danger');
            $('#mensajes').addClass('hide');
            

            $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
              $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
               url  :'{{ url('/guardarComponente') }}',
               type: 'post',
               "_token": "{{ csrf_token() }}",
               async: true,
               data :{
                     'proyecto_financiero_nombre' : proyecto_financiero_nombre,
                     'proyecto_financiero_descripcion' : proyecto_financiero_nombre,
                     'proyecto_financiero_tipo' : proyecto_financiero_tipo,
                     'fcha_inicio' : fcha_inicio,
                     'fcha_fin' : fcha_fin,
                     'estado_id' : estado_id,
                     'lstaComponentesGuardar' : lstaComponentesGuardar,
                     'proyecto_id':proyecto_id
                    },

               error : function(jqXHR, textStatus, errorThrown) {

                    $('#mensajes').addClass('alert alert-danger');
                    var textoMensaje =$("#textoMensaje");
                    textoMensaje.text("No se han encontrados datos. ");             

               },

               success : function(data) {
              
                var componente_proyecto = data.componente_proyecto;
                if(data.componente == ''){
                    $('#mensajes').removeClass('hide');
                     $('#mensajes').addClass('alert alert-warning');
                    var textoMensaje =$("#textoMensaje");
                     textoMensaje.text(data.mensaje);

                }else{
                    $('#mensajes').removeClass('hide');
                    $('#mensajes').removeClass('alert alert-warning');
                    $('#mensajes').addClass('alert alert-success');
                    var textoMensaje =$("#textoMensaje"),
                    componentes = data.componente;
                    textoMensaje.text(data.mensaje);
                    
                    $('#cmb_componentes').empty();

                    //Carga componentes con sus respectivos id
                        for(var i in componentes){
                            $("#proyecto_id").val(componentes[i].proyecto_financiero_id)
                            $('#cmb_componentes').append($('<option>', {
                                value: componentes[i].componente_proyecto_id,
                                id: componentes[i].componente_proyecto_id,
                                text:componentes[i].componente_proyecto_descripcion
                            }));
                         }

                }
               }
           });  
           setTimeout($.unblockUI, 1000);
        }     
}
   
    
$("#btn_agregar_actividad").on('click',function(){

    var descripcion_actividad = $("#descripcion_actividad").val(),
        fuente_financiamiento = $("#fuente_financiamiento").val();
        cuenta_id = $("#cuenta_id_editar").val();
        componente = $("#cmb_componentes option:selected").text();
        componente_id = $("#cmb_componentes option:selected").val();

      bValid = true;
      bValid = bValid && validarCampoLleno(descripcion_actividad, "Campo descripcion actividad");
      bValid = bValid && validarCampoLleno(fuente_financiamiento, "Campo fuente financiamiento");
      bValid = bValid && validarCampoLleno(cuenta_id, "Campo cuenta");
      bValid = bValid && validarCampoLleno(componente_id, "Campo componente");
 

       if(bValid){
            $.ajax({
               url  :'{{ url('/guardarActividad') }}',
               type: 'post',
               "_token": "{{ csrf_token() }}",
               async: true,
               data :{
                     'descripcion_actividad' : descripcion_actividad,
                     'fuente_financiamiento' : fuente_financiamiento,
                     'cuenta_id' : cuenta_id,
                     'componente_id' : componente_id
                    },

               error : function(jqXHR, textStatus, errorThrown) {

                    $('#mensajes').addClass('alert alert-danger');
                    var textoMensaje =$("#textoMensaje");
                    textoMensaje.text("No se han encontrados datos. ");             

               },
               beforeSend: function(data){   
                            $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                        },

               success : function(data) {
                   cuenta = data.objCuentaActividad;
                   data = data.objActividadProyecto;
                  
                   
                 $("#descripcion_actividad").val("");
                 $("#fuente_financiamiento").val("");

                 var newRowContent = "<tr id='"+data.actividad_proyecto_id+"'>"+
                                "<td>"+componente+"</td>"+
                                "<td>" +data.actividad_proyecto_descripcion+"</td>"+
                                "<td>" +data.actividad_proyecto_fuente_financiamiento+"</td>"+
                                "<td id='"+data.actividad_proyecto_id+"'>"+cuenta.cuenta_codigo+"</td>"+
                                "<td class='inner' >  <a class='eliminarlsta'>"+
                                        "<i class='fa fa-times' aria-hidden='true'></i> Quitar</button></td>"+
                            "</tr>";
                

                $(".anio").remove();
                $("#tbl_proyecto tbody").append(newRowContent);
                for (var i = 0; i< nperiodos; i++) {
                    anio = i+1;
                    $("#tbl_proyecto tr:last td.inner").before( "<td><input class='"+periodo[i].periodo_id+"' type='text' class='form-control'/></td>" );
                }

               }
        });
         setTimeout($.unblockUI, 1000);
           
    }


});




function eliminarActividad(actividad_id){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
       $.ajax({
               url  :'{{ url('/eliminarActividad') }}',
               type: 'post',
               "_token": "{{ csrf_token() }}",
               async: true,
               data :{
                     'actividad_id' : actividad_id
                    },
               error : function(jqXHR, textStatus, errorThrown) {
                  console.log(textStatus);            
               },
               success : function(data) {
               }
        }); 
      }

     $(document).on('click', 'a.eliminarlsta', function () {
         $(this).closest('tr').remove();
         var actividad_id = $(this).closest('tr').attr('id');
         eliminarActividad(actividad_id)
         return false;
     });

     $("#btn_almacenar_proyecto").on('click',function(){
        bValid = true;
        bValid = bValid && validarCampoLleno(proyecto_financiero_nombre, "Campo nombre");
        bValid = bValid && validarCampoLleno(proyecto_financiero_descripcion, "Campo objetivo");
        bValid = bValid && validarCampoLleno(proyecto_financiero_tipo, "Campo tipo");
        bValid = bValid && validarCampoLleno(fcha_inicio, "Campo rango fecha");
        bValid = bValid && validarCampoLleno(fcha_fin, "Campo  rango fecha");
        bValid = bValid && validarCampoLleno(estado_id, "Campo  estado");

       if(bValid){   
        guardarProyecto();   
            agregarEjecucion();
        }
    });
     
     function  agregarEjecucion(){
         var lstVal = new Array();
         var periodo_id;
         var lstEjecucion  = 0;
         var pattern = /^((\d+))([.]{1})?(\d{2})$/;
         bValid = true;
         for (var i = 0; i< nperiodos; i++) {
             $('#tbl_proyecto thead tr').each(function () {
                    periodo_id = $(this).find("th:eq("+(4+i)+")").attr('id');
                });
            
            $('#tbl_proyecto tbody tr').each(function () {
                var actividad_id = $(this).attr('id');
                var ejecucion_id = $(this).find("td:eq("+(4+i)+") input[type='text']").attr('id');
                var ejecucion_valor = $(this).find("td:eq("+(4+i)+") input[type='text']").val();
                var valorEjecucion=  pattern.test(ejecucion_valor);
                
                bValid = bValid && validarCampoLleno(ejecucion_valor, "Valor ejecución");
                bValid = bValid && validarBoolean(valorEjecucion,'El formato de ejecución valor no es correcto.');
                if(bValid){
                    lstEjecucion = lstEjecucion + ";" +periodo_id  +","+  actividad_id +","+ ejecucion_id +","+ ejecucion_valor;
                }
            });
        }
  if(bValid){
        $.ajax({
              url  :'{{ url('/agregarEjecucion') }}',
              type: 'post',
              "_token": "{{ csrf_token() }}",
              async: true,
              data :{
                    'lstEjecucion' : lstEjecucion
                   },
             beforeSend: function(data){   
                 $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
             },

              error : function(jqXHR, textStatus, errorThrown) {
              },

              success : function(data) {
               $('#mensajes').removeClass('hide');
               $('#mensajes').removeClass('alert alert-warning');
               $('#mensajes').addClass('alert alert-success');
               var textoMensaje =$("#textoMensaje");
               textoMensaje.text(data);
               limpiar();
              }
       }); 
     }
    }
     
     function guardarProyecto(){
          var proyecto_financiero_nombre = $("#proyecto_financiero_nombre").val(),
          proyecto_financiero_descripcion = $("#proyecto_financiero_descripcion").val(),
          //en caso de que ya exista
          proyecto_id = $("#proyecto_id").val(),
          proyecto_financiero_tipo = $("#proyecto_financiero_tipo").val(),
          fcha_inicio = $("#fcha_inicio").val(),
          fcha_fin = $("#fcha_fin").val(),
          estado_id = $("#estado_id").val();

      
    
            $('#mensajes').removeClass('alert alert-danger');
            $('#mensajes').addClass('hide');

              $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
               url  :'{{ url('/guardarProyecto') }}',
               type: 'post',
               "_token": "{{ csrf_token() }}",
               async: true,
               data :{
                     'proyecto_financiero_nombre' : proyecto_financiero_nombre,
                     'proyecto_financiero_descripcion' : proyecto_financiero_descripcion,
                     'proyecto_financiero_tipo' : proyecto_financiero_tipo,
                     'fcha_inicio' : fcha_inicio,
                     'fcha_fin' : fcha_fin,
                     'estado_id' : estado_id,
                     'proyecto_id':proyecto_id
                    }, 
                    beforeSend: function(data){   
                            $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                        },

                    error : function(jqXHR, textStatus, errorThrown) {

                    $('#mensajes').addClass('alert alert-danger');
                    var textoMensaje =$("#textoMensaje");
                    textoMensaje.text("No se han encontrados datos. ");             
               },
               success : function(data) {
            }
               
           });
            setTimeout($.unblockUI, 1000);
    }


</script>
@endsection
