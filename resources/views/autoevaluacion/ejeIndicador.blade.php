@extends('layouts.app')

@section('content')
<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2><span class="text-navy"><i class="fa fa-chevron-circle-right"></i> Ejecución de Indicadores</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                
                                    @if(Session::has('message'))
                                    <div class="alert alert-success" >
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <p> {{Session::get('message')}} </p>
                                    </div>
                                    @endif

                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    
                                    @if (notify()->ready())
                                    <div class="alert alert-{{ notify()->type() }}">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <label>{{ notify()->message() }}</label>
                                    </div>
                                    @endif
                                    
                                    <div id="mensajesExito" class="alert alert-success hide">
                                        <label id="textoMensajeExito" name="textoMensajeExito"></label>
                                    </div>
                                    
                                    <div id="mensajes" name="mensajes" class="hide">
                                        <label id="textoMensaje" name="textoMensaje"></label>
                                    </div>
                                    
                                    <div>
                                            <form class="form-horizontal" method="POST" id="formEjeIndicadores" enctype="multipart/form-data" role="form">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                Seleccionar la Entidad (Usuario AME)
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">Tipo</label>
                                                                     <div class="col-sm-6 radio">
                                                                        @foreach($lstTipoEntidad as $tipoEntidad)
                                                                        <label> <input type="radio" class="radio-division" value="{{$tipoEntidad->tipo_entidad_id}}" id="tipo_entidad_id" name="tipo_entidad_id" required>{{$tipoEntidad->nombre}}</label>
                                                                        @endforeach
                                                                    </div>                                                                 
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">División Política</label>
                                                                    <div class="col-sm-4">
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-body" >
                                                                                <div id="arbolDivisionPolitica" class="step-content" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" name="division_politica_id" class="division_politica_id" id="division_politica_id">
                                                                     </div>
                                                                  
                                                                    <label class="col-sm-2 control-label">Entidades</label>
                                                                    <div class="col-sm-4">
                                                                        <select class="form-control m-b" name="entidad_id" id="entidad_id" >
                                                                            <option value="">Seleccionar...</option>
                                                                        </select>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="hr-line-dashed"></div>
                                                        
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Fuente de Información</label>
                                                            <div class="col-sm-4">
                                                                <select class="form-control m-b" name="tipo_fuente_id" id="tipo_fuente_id" disabled="true">
                                                                    <option value="">Seleccionar...</option>
                                                                    @foreach($lstTipoFuente as $tipoFuente)
                                                                    <option value="{{$tipoFuente->tipo_fuente_id}}">{{$tipoFuente->tipo_fuente_descripcion}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <label class="col-sm-2 control-label">Selección de Periodos</label>
                                                            <div class="col-sm-4">
                                                                <select class="form-control m-b" name="periodo_id" id="periodo_id">
                                                                    <option value="">Seleccionar...</option>  
                                                                </select>
                                                            </div>
                                                        </div>   
                                                        
                                                        
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Valores de las partidas presupuestarias</label>
                                                            <div class="col-sm-10">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-body" >
                                                                        <div id="arbolCuentas" class="step-content" >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                             </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Catálogo de Indicadores</label>
                                                            <div class="col-sm-4">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-body">
                                                                        <div id="arbolIndicador" class="step-content hide" >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" name="indicador_id" class="indicador_id" id="indicador_id">
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#indicadorTemporalModal" id="btn_agregarIndicadorTemporal"  name="btn_agregarIndicadorTemporal" type="button"><i class="fa fa-plus"></i></button>
                                                            </div>
                                                            <label class="col-sm-1 control-label">Indicadores Temporales</label>
                                                            <div class="col-sm-4">
                                                                <select class="form-control m-b" name="indicador_temporal_creado" id="indicador_temporal_creado" multiple="true">
                                                                    
                                                                </select>
                                                            </div>
                                                        </div>
                                                                   
                                                        <div class="hr-line-dashed"></div>
                                                        
                                                        <div class="form-group">
                                                            <label class="control-label">Resultado de los indicadores</label>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <table id="tablaIndicadoresEvaluados" class="table table-bordered">
                                                                <thead class="thead-default">
                                                                    <tr>  
                                                                        <th>Indicadores</th>
                                                                        <th>Periodo</th>
                                                                        <th>Rango</th>
                                                                        <th>Calificación</th>
                                                                    </tr>
                                                                </thead>  
                                                            </table>
                                                        </div>
                                                        
                                                        <div class="modal-footer col-sm-12">
                                                            <a  id="btn_imprimir" class="btn btn-primary" type="button" href="javascript:imprimir()" >
                                                                <i class="fa fa-print"></i> Imprimir
                                                            </a>
                                                            <button id="btn_exportarExcel" type="button" class="btn btn-primary"  >
                                                               <i class="fa fa-file-excel-o"></i> Exportar a Excel
                                                             </button>
                                                            <button id="btn_exportarPdf" type="button" class="btn btn-primary"  >
                                                              <i class="fa fa-file-pdf-o"></i> Exportar a pdf
                                                             </button>  
                                                         </div>
                                                        
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    
                                        <!----------------------------------------------------- Pop up para crear indicador temporal --------------------------------------------------->
                                            <div class="modal fade" id="indicadorTemporalModal" role="dialog"  aria-hidden="true" data-backdrop="static" data-keyboard="false" tabindex="-1" >
                                                <div class="modal-dialog modal-lg" >
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Diseñador de Indicador Temporal</h4>
                                                        </div>
                                                        <div class="modal-body" id="modalBody">
                                                            <div id="mensajesError" class="alert alert-danger hide"></div>
                                                            
                                                            <div class="form-group">
                                                                <div id="mensajesModal" name="mensajesModal" class="hide">
                                                                    <label id="textoMensajeModal" name="textoMensajeModal"></label>
                                                                </div>
                                                            </div>
                                                            <div >
                                                                <form class="form-horizontal bootstrap-modal-form" method="post" id="formIndicadorTemporal" name="formIndicadorTemporal" action="{{url('/guardarIndicadorTemporal')}}"  />
                                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                    <input id="indicador_temporal_formula" type="hidden" name="indicador_temporal_formula" value=""/>

                                                                    <div class="form-group">
                                                                        <label class="control-label " >Nombre:</label>
                                                                        <input type="text" class="form-control" maxlength="254" name="indicador_temporal_nombre" id="indicador_temporal_nombre" style="text-transform:uppercase;"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label " >Descripción:</label>
                                                                        <input type="text" class="form-control" maxlength="254" name="indicador_temporal_descripcion" id="indicador_temporal_descripcion" style="text-transform:uppercase;"/>
                                                                    </div>
                                                                    <div class="form-group">

                                                                            <div class="panel panel-default">
                                                                                <div class="panel-heading"><h3 class="panel-title">Fórmula:</h3>
                                                                                </div>

                                                                                <div class="panel-body">
                                                                                    <div class="row">
                                                                                        <div class="col-md-6">
                                                                                            <div class="row">
                                                                                                <label class="col-lg-4 control-label " >Variable del Indicador </label>
                                                                                                <div class="col-lg-8">
                                                                                                    <select class="form-control m-b" name="variable_indicador_id" id="variable_indicador_id" >
                                                                                                        <option value="">Seleccionar...</option>
                                                                                                        @foreach($lstVariableIndicador as $variableIndicador)
                                                                                                        <option value="{{$variableIndicador->variable_indicador_id}}">{{$variableIndicador->variable_indicador_nombre}}</option>
                                                                                                        @endforeach
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <label class="col-lg-4 control-label " >Operadores matemáticos</label>
                                                                                                <div class="col-lg-8">
                                                                                                    <select class="form-control m-b" name="operador_matematico" id="operador_matematico">
                                                                                                        <option value="">Seleccionar...</option>
                                                                                                        @foreach($lstOperadoresMatematicos as $operadoresMatematicos)
                                                                                                        <option value="{{$operadoresMatematicos->operadores_matematicos_simbolo}}">{{$operadoresMatematicos->operadores_matematicos_nombre}} {{$operadoresMatematicos->operadores_matematicos_simbolo}}</option>
                                                                                                        @endforeach
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <label class="col-lg-4 control-label " >Signos de agrupación</label>
                                                                                                <div class="col-lg-8">
                                                                                                    <select class="form-control m-b" name="signo_agrupacion" id="signo_agrupacion">
                                                                                                        <option value="">Seleccionar...</option>
                                                                                                        @foreach($lstSignosAgrupacion as $signosAgrupacion)
                                                                                                        <option value="{{$signosAgrupacion->signos_agrupacion_simbolo}}">{{$signosAgrupacion->signos_agrupacion_simbolo}}</option>
                                                                                                        @endforeach
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <label class="col-lg-4 control-label ">Constantes de valor</label>
                                                                                                <div class="col-lg-8">
                                                                                                    <input type="number" min="0" class="form-control" id="valorConstante" name="valorConstante">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6">
                                                                                            <label class="col-lg-12 control-label " id="lblArbol"></label>
                                                                                            <div class="col-lg-12">
                                                                                                <div class="panel panel-default">
                                                                                                    <div class="panel-body" id="divValorVarIndicador">
                                                                                                        <div id="arbolVarIndicador" class="step-content" >
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row text-center">
                                                                                        <button id="btn_agregar" class="btn btn-outline btn-primary" type="button">
                                                                                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar</button>
                                                                                        <button id="btn_quitar" class="btn btn-outline btn-primary" type="button">
                                                                                            <i class="fa fa-times" aria-hidden="true"></i> Quitar</button>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <label class="col-lg-4 form-control-label ">Fórmula Planteada</label>

                                                                                        <div class="col-lg-12">
                                                                                            <div class="panel panel-default">
                                                                                                <div class="panel-body" >
                                                                                                    <label class="col-lg-12 text-center" id="lblFormula" name="lblFormula"></label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                            </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" id="btn_guardar"  class="btn btn-primary">Aceptar</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!----------------------------------------------------- Fin de pop up para crear indicador temporal --------------------------------------------------->
                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>
@endsection

@section('script')
<script type="text/javascript">
 

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
    /*************************************************************************************************************************************************************/
    
    var lstDivPoljs = <?php echo json_encode($objDivisionPolitica); ?>;
    var arbolDivisionPolitica = dataArbolDinamico(lstDivPoljs,"division_politica_nombre","division_politica_id","division_politica","true","division_politica_codigo","division_politica_nombre");
    $('#arbolDivisionPolitica').treeview({
        levels: 1,
        color: "#555",
        showBorder: false,
        data: arbolDivisionPolitica, 
        multiSelect: $('#chk-select-multi').is(':checked'),
        onNodeSelected: function (event, node) {
            $('#division_politica_nom').val(node.text);
            $('#division_politica_id').val(node.tags);
            console.log(node.tags);
            cargarEntidades();  
        },
        onNodeUnselected: function (event, node) {
        }
    });

    /*************************************************************************************************************************************************************/
    var lstIndicadores = <?php echo json_encode($lstIndicador); ?>;
    var cantNodes = lstIndicadores.length;
    var arbolIndicadores = dataArbolDinamicoSinHijo(lstIndicadores,"indicador_nombre","indicador_id");

    //Árbol que muestra la lista de indicadores
    $('#arbolIndicador').treeview({
        levels: 1,
        color: "#555",
        showBorder: false,
        showCheckbox: true,
        selectable:false,
        data: arbolIndicadores,
        onNodeChecked: function (event, node) {
            $('#indicador_nombre').val(node.text);
            $('#indicador_id').val(node.tags);
            console.log(node.tags);
            ejecutarIndicador();
        },
        onNodeUnchecked: function (event, node) {
            $('#indicador_nombre').val(node.text);
            $('#indicador_id').val(node.tags);
            console.log(node.tags);
            ejecutarIndicador();
        }
    });
    
    //Método para actualizar la tabla de los Resultados Indicadores Ejecutados
    function ejecutarIndicador(){
        var entidad_id=$('#entidad_id').val();
        var tipo_fuente_id=$('#tipo_fuente_id').val();
        var periodo_id=$('#periodo_id').val();
        
        var bValid = true; 
        bValid = bValid && validarCampoLleno(entidad_id, " Entidad");
        bValid = bValid && validarCampoLleno(tipo_fuente_id, " Fuente de Información");
        bValid = bValid && validarCampoLleno(periodo_id, " Periodo");

        if(bValid){
            var node,
            nodeId = 0,
            checkedNodes = [];

            do {
                node = $('#arbolIndicador').treeview('getNode', nodeId);
                if (node && node.state.checked) {
                    checkedNodes.push(node.tags);
                }
                nodeId++;
            }while (nodeId < cantNodes); 
            
            $.ajax({
                url: '{{ url('/ejecutarIndicadores') }}',
                type: 'post',
                async: true,
                data: {'indicadores_id':checkedNodes,'entidad_id':entidad_id,'tipo_fuente_id':tipo_fuente_id,'periodo_id':periodo_id},
                beforeSend: function() {
                    $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var gnrMensaje = $("#gnrMensaje");
                    if (jqXHR.status == '401') {
                        gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                        $('#gnrError').modal('show');
                    }
                    else{
                        $('#mensajes').addClass('alert alert-danger');
                        $('#mensajes').removeClass('hide');
                        var textoMensaje =$("#textoMensaje");
                        textoMensaje.text(jqXHR.status); 
                    }
                    toTop();
                },
                success: function(data) {
                    console.log(data);
                    if (data == ''){
                        toTop();
                        $('#mensajes').addClass('alert alert-danger');
                        $('#mensajes').removeClass('hide');
                        var textoMensaje =$("#textoMensaje");
                        textoMensaje.text("No se han encontrados datos. ");  
                    }else{
                        borrarTablaIndicadoresEvaluados();
                        actualizarTablaIndicadoresEvaluados(data);
                    }


                },
                complete: function() {
                    setTimeout($.unblockUI, 1000);
                },
            });
        }else{
            toTop();
        }
    }    
    
    function borrarTablaIndicadoresEvaluados(){
        var tabla = document.getElementById('tablaIndicadoresEvaluados');
        var tablaFilas = tabla.getElementsByTagName('tr');
        var cantFilas = tablaFilas.length;

        for (var x=cantFilas-1; x>0; x--) {
           tabla.removeChild(tablaFilas[x]);
        }
    }
    
    //Método para actualizar la talla de Indicadores Evaluados
    function actualizarTablaIndicadoresEvaluados(data){
        for(var i=0; i < data.length ; i++){ 
            var tr = document.createElement('tr');   

            var td1 = document.createElement('td');
            var td2 = document.createElement('td');
            var td3 = document.createElement('td');
            var td4 = document.createElement('td');

            //var text1 = document.createTextNode(data[i].indicador_nombre);
            var text1 = document.createTextNode(data[i].indicador_formula);
            var text2 = document.createTextNode(data[i].valorFormula);
            var text3 = document.createTextNode('Text3');
            var text4 = document.createTextNode('Text4');

            td1.appendChild(text1);
            td2.appendChild(text2);
            td3.appendChild(text3);
            td4.appendChild(text4);
            
            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);
            
            document.getElementById("tablaIndicadoresEvaluados").appendChild(tr);
            
        }
    }
 
    /*************************************************************************************************************************************************************/
    
    $("#periodo_id").change(function(){
      cargarListaCuentas();   
      $('#arbolIndicador').removeClass('hide');
      $('#arbolIndicador').treeview('uncheckAll', { silent: true });
      borrarTablaIndicadoresEvaluados();
    });
    
    //Metodo para cargar la lista de las cuentas con los valores
    function cargarListaCuentas(){
        var textoMensaje =$("#textoMensaje");
        $('#mensajes').addClass('hide');
        textoMensaje.text("");  
        var entidad_id=$('#entidad_id').val();
        var tipo_fuente_id=$('#tipo_fuente_id').val();
        var periodo_id=$('#periodo_id').val();


        var bValid = true; 
        bValid = bValid && validarCampoLleno(entidad_id, " Entidad");
        bValid = bValid && validarCampoLleno(tipo_fuente_id, " Fuente de Información");
        bValid = bValid && validarCampoLleno(periodo_id, " Periodo");

        if(bValid){ 
            $.ajax({
                         url: '{{ url('/consultaValoresCuentas') }}',
                         type: 'post',
                         data: {'entidad_id': entidad_id,'periodo_id': periodo_id,'tipo_fuente_id': tipo_fuente_id  },
                         beforeSend: function() {
                              $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento por favor </h4>' });
                         },
                         
                         error : function(jqXHR, textStatus, errorThrown) {
                            var gnrMensaje =$("#gnrMensaje");
                            if(jqXHR.status == '401'){
                                gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar."); 
                                $('#gnrError').modal('show');
                            }     
                        },
                        success: function(data) {
                            console.log(data);
                            setTimeout($.unblockUI, 1000);
                            
                            if (data.length==0){
                                $('#mensajes').addClass('alert alert-danger');
                                $('#mensajes').removeClass('hide');
                                var textoMensaje =$("#textoMensaje");
                                textoMensaje.text("No se han encontrados datos. "); 
                                $('#arbolCuentas').empty();
                                toTop();
                            }
                            else{   
                                var arbolCuentas = dataArbolDinamicoValorTag(data,"cuenta","cuenta_codigo","cuenta_nombre");
                                console.log(arbolCuentas);
                                $('#arbolCuentas').treeview({
                                   levels: 1,
                                   showTags: true,
                                   color: "#ffffff",
                                   data: arbolCuentas,
                                   multiSelect: $('#chk-select-multi').is(':checked'),
                                });  
                            }          
                        },
                        complete: function() {
                     
                        }       
                    });
        }
    }
    
    /*************************************************************************************************************************************************************/
    
    var tipoFuenteBdE = <?php echo json_encode($fuenteBdE); ?>;
    $("#tipo_fuente_id").change(function(){
      periodos(1);       
    });
    
    $("#entidad_id").change(function(){
      document.getElementById("tipo_fuente_id").disabled = false;     
    });
    
    /*************************************************************************************************************************************************************/
    
    function imprimir()
    {
        window.print( );
        window.close( );
    }
    
    /*************************************************************************************************************************************************************/
    
    $("#btn_exportarPdf").click(function(){
        var bValid = true; 
        var entidad_id=$('#entidad_id').val();
        var tipo_fuente_id=$('#tipo_fuente_id').val();
        var periodo_id=$('#periodo_id').val();
        bValid = bValid && validarCampoLleno(entidad_id, " entidad");
        bValid = bValid && validarCampoLleno(periodo_id, " periodo");
        bValid = bValid && validarCampoLleno(tipo_fuente_id, " Fuente de Información"); 
        if(bValid){
            var node,
            nodeId = 0,
            checkedNodes = [];

            do {
                node = $('#arbolIndicador').treeview('getNode', nodeId);
                if (node && node.state.checked) {
                    checkedNodes.push(node.tags);
                }
                nodeId++;
            }while (nodeId < cantNodes); 
            
            $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
            setTimeout($.unblockUI, 1000);                      
            $('#formEjeIndicadores').attr('action', '{{ url('/imprimirPdfEjeIndicador') }}');
            $("#formEjeIndicadores").submit();    
        }
        else{
            toTop();
        }   
    });
    
    /*************************************************************************************************************************************************************/
    
    $("#btn_exportarExcel").click(function(){
        var entidad_id=$('#entidad_id').val();
        var tipo_fuente_id=$('#tipo_fuente_id').val();
        var periodo_id=$('#periodo_id').val();

        var bValid = true; 
        bValid = bValid && validarCampoLleno(entidad_id, " entidad");
        bValid = bValid && validarCampoLleno(periodo_id, " periodo");
        bValid = bValid && validarCampoLleno(tipo_fuente_id, " Fuente de Información"); 
        if(bValid) {
            var node,
            nodeId = 0,
            checkedNodes = [];

            do {
                node = $('#arbolIndicador').treeview('getNode', nodeId);
                if (node && node.state.checked) {
                    checkedNodes.push(node.tags);
                }
                nodeId++;
            }while (nodeId < cantNodes); 
            
            $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
            setTimeout($.unblockUI, 1000);
            $('#formEjeIndicadores').attr('action', '{{ url('/imprimirExcelEjeIndicador') }}');
            $("#formEjeIndicadores").submit();
        }
        else{
            toTop();
        }  
    });
    
    /*************************************************************************************************************************************************************/
    //ventana pop up para crear indicador temporal
    $(document).ready(function () {
    
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
        var valorFormula = '';
        var formula = $("#lblFormula").text();
    
        $("#variable_indicador_id").change(function () {
            var varIndicador = $("#variable_indicador_id option:selected").val();
            
            limpiarMensajesModal();
            limpiarMensajesErrorModal();
            
            $("#lblArbol").text("");
            $.ajax({
                    url: '{{ url('/buscarLstVariableIndicador') }}',
                    type: 'post',
                    "_token": "{{ csrf_token() }}",
                    async: true,
                    data: {'tipoVarIndicador': varIndicador},
                    beforeSend: function () {
                        $.blockUI({baseZ: 2000,message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        var gnrMensaje = $("#gnrMensaje");
                        if (jqXHR.status == '401') {
                            gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                            $('#gnrError').modal('show');
                        }
                    },
                    success: function (data) {
                        if (data == '') {
                            $('#mensajesModal').addClass('alert alert-warning');
                            var tips = $("#textoMensajeModal");
                            tips.text("Advertencia !  No se han encontrado datos de la Variable Seleccionada. ");
                            $('#arbolVarIndicador').treeview({
                                data: null,
                            });
                        } 
                        else
                        {
                            var valor1 = <?php echo json_encode($varIndicadorFinanciera); ?>;
                            var valor2 = <?php echo json_encode($varIndicadorServicios); ?>;
                            var arbol;
                            if (parseInt(varIndicador) == valor1) {
                                $("#lblArbol").text("Catálogo de Cuentas Oficiales");
                                arbol = dataArbolDinamico(data, "cuenta_nombre", "cuenta_id", "cuenta_hijo");
                            } 
                            else {
                                if (parseInt(varIndicador) == valor2) {
                                    $("#lblArbol").text("Variables de Servicio");
                                    arbol = dataArbolDinamicoSinHijo(data, "variable_servicio_criterio", "variable_servicio_id");
                                } 
                                else {
                                    $("#lblArbol").text("Variables Generales");
                                    arbol = dataArbolDinamicoSinHijo(data, "variable_general_nombre_programatico", "variable_general_id");
                                }
                            }

                            $('#arbolVarIndicador').treeview({
                            levels: 1,
                                    color: "#555",
                                    showBorder: false,
                                    data: arbol,
                                    multiSelect: $('#chk-select-multi').is(':checked'),
                                    onNodeSelected: function (event, node) {
                                        valorFormula = node.text;
                                    },
                                    onNodeUnselected: function (event, node) {
                                        valorFormula = '';
                                    }
                            });

                        }
                    },
                    complete: function () {
                        setTimeout($.unblockUI, 1000);
                    },
            });
        });

        $('#btn_agregar').click(function (event) {

            var operador_matematico = $("#operador_matematico").val();
            var signo_agrupacion = $("#signo_agrupacion").val();
            var valorConstante = $("#valorConstante").val();
            if (valorFormula != '') {
                $("#lblFormula").text(formula + '  ' + valorFormula);
            } 
            else {
                if (operador_matematico != '') {
                    $("#lblFormula").text(formula + '  ' + operador_matematico);
                } 
                else if (signo_agrupacion != '') {
                    $("#lblFormula").text(formula + '  ' + signo_agrupacion);
                } else if (valorConstante != '') {
                    $("#lblFormula").text(formula + '  ' + valorConstante);
                }
            }
            var operador_matematico = $("#operador_matematico").val("");
            var signo_agrupacion = $("#signo_agrupacion").val("");
            var valorConstante = $("#valorConstante").val("");
            valorFormula = '';
            formula = $("#lblFormula").text();
        });

        $('#btn_quitar').click(function (event) {
            var str = $("#lblFormula").text();
            var res = str.split("  ");
            res.pop();
            var valor = res.toString();
            var res2 = valor.replace(/,/gi, "  ");
            var operador_matematico = $("#operador_matematico").val("");
            var signo_agrupacion = $("#signo_agrupacion").val("");
            var valorConstante = $("#valorConstante").val("");
            valorFormula = '';
            $("#lblFormula").text(res2);
            formula = $("#lblFormula").text();
        });
        
        $('#btn_guardar').click(function (event) {
            toTop();
            var bValid = true;
            var indicador_temporal_nombre = $("#indicador_temporal_nombre").val();
            var indicador_temporal_descripcion = $("#indicador_temporal_descripcion").val();
            var indicador_temporal_formula = $("#lblFormula").text();
            var periodo_id = $("#periodo_id").val();
            var entidad_id = $("#entidad_id").val();
            var tipo_fuente_id=$('#tipo_fuente_id').val();
            
            limpiarMensajesErrorModal();
            limpiarMensajesExitoModal();
            
            bValid = bValid && validarCampoLlenoModal(indicador_temporal_nombre, 'Nombre del Indicador');
            bValid = bValid && validarCampoLlenoModal(indicador_temporal_descripcion, 'Descripción del Indicador');
            bValid = bValid && validarCampoLlenoModal(indicador_temporal_formula, 'Fórmula del Indicador');
            bValid = bValid && validarCampoLlenoModal(periodo_id, 'Periodo');
            bValid = bValid && validarCampoLlenoModal(periodo_id, 'Entidad');
            bValid = bValid && validarCampoLlenoModal(tipo_fuente_id, " Fuente de Información");

            $("#indicador_temporal_formula").val(indicador_temporal_formula);
            if (bValid) {
                limpiarMensajesModal();
                
                $.blockUI({baseZ: 2000,message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                $('#formIndicadorTemporal').submit();
                setTimeout($.unblockUI, 3000);
            }
        });
        
        $('#formIndicadorTemporal').submit(function(event) {
            event.preventDefault();
            
            var formURL = $(this).attr("action");
            var indicador_temporal_nombre = $("#indicador_temporal_nombre").val();
            var indicador_temporal_descripcion = $("#indicador_temporal_descripcion").val();
            var indicador_temporal_formula = $("#indicador_temporal_formula").val();
            var periodo_id = $("#periodo_id").val();
            var entidad_id = $("#entidad_id").val();
            var tipo_fuente_id=$('#tipo_fuente_id').val();
            
            $.ajax({
                url: formURL,
                type: 'post',
                //data: postData,  
                data: {'indicador_temporal_nombre':indicador_temporal_nombre,'indicador_temporal_descripcion':indicador_temporal_descripcion,'indicador_temporal_formula':indicador_temporal_formula,'periodo_id':periodo_id,'entidad_id':entidad_id,'tipo_fuente_id':tipo_fuente_id},
                dataType: 'json',
                success: function(response)
                {
                    if(response.success)
                    {
                        var x = document.getElementById("indicador_temporal_creado");
                        var option = document.createElement("option");
                        option.text = response.indicadorTemporal;
                        x.add(option);
                        
                        $('#mensajesExito').removeClass('hide');
                        var textoMensajeExito = $("#textoMensajeExito");
                        textoMensajeExito.text(response.success); 
                         
                        $('#indicadorTemporalModal').modal('hide');
                        toTop();
                        
                        //$("#btn_guardar").remove();
                    }
                    else if(response.errors)
                    {
                        $.each(response.errors, function( index, value ) {
                            $('#mensajesError' ).removeClass('hide');
                            var node = document.createElement("LI");                 // Create a <li> node
                            var textnode = document.createTextNode(value[0]);         // Create a text node
                            node.appendChild(textnode);                              // Append the text to <li>
                            document.getElementById("mensajesError").appendChild(node);
                        });
                        $('#indicadorTemporalModal').scrollTop(0);
                    }
                },
                error: function(jqXHR, textStatus, thrownError)
                {
                    alert("error");
                    console.log(status + ": " + error);
                    var gnrMensaje = $("#gnrMensaje");
                    if (jqXHR.status == '401') {
                        gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                        $('#gnrError').modal('show');
                    }
                    else{
                        $('#indicadorTemporalModal').scrollTop(0);
                        $('#mensajesModal').addClass('alert alert-danger');
                        $('#mensajesModal').removeClass('hide');
                        var textoMensaje =$("#textoMensajeModal");
                        textoMensaje.text(jqXHR.status); 
                    }
                    toTop();
                }
            });     
        }); 
        
        
        
        function limpiarMensajesErrorModal(){
            $('#mensajesError').addClass('hide');
            $('#mensajesError').empty();
        }
        
        function limpiarMensajesModal(){
            $('#mensajesModal').removeClass('alert alert-danger');
            $('#mensajesModal').removeClass('alert alert-warning');
            $('#mensajesModal').addClass('hide');
            var textoMensajeModal =$("#textoMensajeModal");
            textoMensajeModal.text(""); 
        }
        
        function limpiarMensajesExitoModal(){
            $('#mensajesExito').addClass('hide');
            var textoMensajeExito =$("#textoMensajeExito");
            textoMensajeExito.text(""); 
        }
            
        function limpiarIndicadorTemporalModal(){
            limpiarMensajesExitoModal();
            limpiarMensajesErrorModal();
            limpiarMensajesModal();
            
            $('#variable_indicador_id').val("");
            $("#indicador_temporal_nombre").val("");
            $("#indicador_temporal_descripcion").val("");
            $("#lblFormula").text("");
            $("#indicador_temporal_formula").val("");
            $("#operador_matematico").val("");
            $("#signo_agrupacion").val("");
            $("#valorConstante").val("");
            $("#lblArbol").text("");
            
            valorFormula = '';
            formula = '';
            
            $('#arbolVarIndicador').treeview({
            data: null,
            });
        }
        
        //función a ejecutar cuando se cierra la ventana modal
        /*$('#indicadorTemporalModal').on('hidden.bs.modal', function (e) {
            limpiarIndicadorTemporalModal();
        });*/

        //función a ejecutar cuando se cierra la ventana modal
        $('#indicadorTemporalModal').on('shown.bs.modal', function() { 
            limpiarIndicadorTemporalModal();
        });
        
    });

</script>
@endsection