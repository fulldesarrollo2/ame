@extends('layouts.app')

@section('header')
  <h1>
    Administración de Informaci&oacute;n de Servicios B&aacute;sicos Municipales
  </h1>
  <ol class="breadcrumb">
    <li><a><i class="glyphicon glyphicon-th-large"></i>Administración</a></li>
    <li class="active">Servicios Basicos Municipales</li>
  </ol>
<br/>
@stop

@section('content')

<div class="box box-default">
	<div class="box-header with-border">
    	<h3 class="box-title">Estructura Administrativa</h3>
  	</div>
  	<div class="box-body">
    	<div class="row">
			{!! Form::open(['route'=>'variableServAdministrativa.store', 'method'=>'POST','id'=>'servBasicoForm','class'=>'form-horizontal']) !!}
				<div class="col-sm-12">
          			
          				<div class="col-sm-3">
	          				<div class="form-group">
								{!! Form::label('servicios','Servicios Relacionados',['class'=>'col-sm-12 control-label']) !!}
							</div>
							<div class="form-group">
							{!! Form::checkbox('aguaPotable') !!}
							{!! Form::label('aguaPotable','Agua Potable',['control-label']) !!}
							</div>
							<div class="form-group">
							{!! Form::checkbox('alcantarillado') !!}
							{!! Form::label('alcantarillado','Alcantarillado',['control-label']) !!}
							</div>
							<div class="form-group">
							{!! Form::checkbox('residuosSolidos') !!}
							{!! Form::label('residuosSolidos','Residuos Solidos',['control-label']) !!}
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								{!! Form::label('cargo','Cargo',['class'=>'col-sm-12 control-label']) !!}
								<div class="col-sm-12">
									{{--!! Form::select('cargo',['presidente','Vicepresidente'],null,['class'=>'form-control']) !!--}}
									<select name="cargo" id="cargo" class="form-control">
										<option value="">Seleccione</option>
										@foreach($listCargo as $cargos)	
											<option value="{{ $cargos->cargo_id}}">{{ $cargos->cargo_nombre }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group">
								{!! Form::label('remuneracion','Remuneracion',['class'=>'control-label']) !!}
								<div class="col-sm-12">
									<div class="input-group">
										<div class="input-group-addon">$</div>
										{!! Form::text('remuneracion',null,['class'=>'form-control']) !!}
									</div>
								</div>
							</div>
							<div class="form-group">
								{!! Form::label('participacion','Participacion',['class'=>'control-label']) !!}
								<div class="col-sm-12">
									<div class="input-group">
										<div class="input-group-addon">%</div>
										{!! Form::text('participacion',null,['class'=>'form-control']) !!}
									</div>
								</div>
							</div>
							<div class="form-group">
								{!! Form::label('tipoCosto','Tipo Costo',['class'=>'control-label']) !!}
								<div class="col-sm-12">
									{!! Form::radio('tipoCosto','Fijo',null) !!}
									{!! Form::label('tipoCosto','Fijo') !!}
									<br>
									{!! Form::radio('tipoCosto','Variable',null) !!}
									{!! Form::label('tipoCosto','Variable') !!}
								</div>
							</div>
							<div class="form-group">
								{!! Form::label('cantidadPersonas','Cantidad de Personas',['class'=>'control-label']) !!}
								<div class="col-sm-12">
									{!! Form::text('cantidadPersonas',null,['class'=>'form-control']) !!}
								</div>
							</div>
							<br>
							<div class="form-group box-footer text-right">
								{!! Form::button('CANCELAR',['id'=>'cancelar','class'=>'btn btn-white']) !!}
								{!! Form::submit('ALMACENAR',['class'=>'btn btn-primary']) !!}
							</div>
						</div>
										
				</div>

			{!! Form::close() !!}
		</div>
  	</div>
</div>

@endsection

@section('script')

	<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
	{!! JsValidator::formRequest('App\Http\Requests\variableServAdminRequest', '#servBasicoForm') !!}

@endsection