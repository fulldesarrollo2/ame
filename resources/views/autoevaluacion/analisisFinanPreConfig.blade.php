@extends('layouts\app')

@section('content')

<div id="page-wrapper" class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">

		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content text-left p-md">
					@if (session()->has('mensajeexito')) 
                                    <div class="alert alert-success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <ul>
                                            <li>{{ session()->pull('mensajeexito') }}</li>			            
                                        </ul>
                                    </div>
                                    @endif

                                    @if (session()->has('mensajeerror')) 
                                    <div class="alert alert-danger">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <ul>
                                            <li>{{ session()->pull('mensajeerror') }}</li>			            
                                        </ul>
                                    </div>
                                    @endif


                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif


                                    <div id="mensajes" name="mensajes" class="hide">
                                        <label id="textoMensaje" name="textoMensaje"></label>
                                    </div>
						<h2>
							<span class="text-navy"><i class="fa fa-chevron-circle-right"></i>
								Catálogo de análisis financiero pre configurado </span>
						</h2>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content text-left p-md">
						<div class="wrapper wrapper-content animated fadeInRight">
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg-10">
							
								
									<div id="analisis-financiero" class="row">
										<label class="col-lg-12 text-left">Análisis Comparativos Ingresados</label> <br>
										<div class="col-lg-3"></div>
										<div class="col-lg-6" id="container-periodo">
											<select multiple size="3" onchange="consultarAnalisis();" class="form-control" id="analisis">
												<option value="0" selected="selected">Seleccionar Analisis</option>
												@foreach ($objAnalisisPreConfigurado as $analisis)  
												<option value="{{$analisis->analisis_pre_configurado_id}}">{{$analisis->analisis_pre_configurado_nombre}}</option>
												@endforeach
											</select>
										</div>
										<div class="col-lg-3"></div>
									</div>
								
							</div>
							<div class="col-lg-1"></div>
					</div>
							<div class="row" style="margin-top: 1%">
								<form method="POST" id="guardaAnalisisPreConfig" action="/guardaAnalisisPreConfig">
									<input type="hidden" name="_token" value="{{ csrf_token() }}" />
									<input type="hidden" name="analisis_pre_configurado_id" class="analisis_pre_configurado_id" id="analisis_pre_configurado_id">
									<div class="row">
										<div class="form-group row">
											<label class="col-lg-2 text-right">Nombre</label>
											<div class="col-lg-8">
												<input class="form-control" type="text"
													id="analisis_pre_configurado_nombre"
													name="analisis_pre_configurado_nombre">
											</div>
											<div class="col-lg-2"></div>
										</div>

										<div class="form-group">

											<label class="col-lg-2 text-right control-label">Cuentas
												Oficiales Cargadas</label>
											<div class="col-lg-10">
												<div class="panel panel-default">
													<div class="panel-body" id="divCuentas">
														<div id="arbolCuentas" class="step-content"></div>
													</div>
												</div>
											</div>

										</div>

										<div class="form-group">
											<input class="form-control" type="hidden" id="cuenta_id_editar" name="cuenta_id_editar" />
											<input type="hidden" id="nomCtaSeleccionada" name="nomCtaSeleccionada">
											<div class="col-lg-4 text-center">

												<button class="btn btn-primary" type="button"
													onclick="agregarcuenta();">
													<i class="fa fa-plus" aria-hidden="true"></i> Agregar
												</button>
												<button class="btn btn-danger" type="button"onclick="eliminarcuenta();">
													<i class="fa fa-times" aria-hidden="true"></i> Eliminar
												</button>
											</div>
											<div class="col-lg-8">
												<label class="col-lg-2">Cuentas</label>
												<div class="col-lg-10">
													<select class="form-control" id="cuentasComparativas"
														name="cuentasComparativas[]" multiple>

													</select>
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="col-lg-1"></div>
											<div class="col-lg-10">
												<label class="col-lg-12 text-left">Criterio de análisis</label>
												<br>
												<div class="col-lg-1"></div>
												<div class="col-lg-10">
													<textarea class=" form-control"
														name="analisis_preconfigurado_criterio"
														id="analisis_preconfigurado_criterio"></textarea>
												</div>
												<div class="col-lg-1"></div>
											</div>
											<div class="col-lg-1"></div>
										</div>

										<div id="estadoActualizacion" style="display: none;">
										
											<label class="col-lg-2 form-group  text-right control-label">Estado</label>
											<div class="col-lg-10">
												<label> <input type="radio" name="estado_id"
													class="radio-estado" value="1" />Activo
												</label> <label> <input type="radio" name="estado_id"
													class="radio-estado" value="2" />Inactivo
												</label>
											</div>
										
										</div>

										<div class="form-group ">
											<div class="row">
												<div class="col-lg-12 text-center" style="margin-top: 1%;">
													<button type="button" id="almacenar" style="margin: 0px auto;"
														onclick="valAnalisisPreConfig();" class="btn btn-primary">Almacenar</button>
													<button type="button" onclick="cancelardivisionpolitica();" style="margin: 0px auto;"
														class="btn btn-white">cancelar</button>
												</div>
											</div>
										</div>

									</div>

								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
var lstCuentasjs = <?php echo json_encode($objCuenta); ?>;
var arbol = dataArbolDinamico(lstCuentasjs,"cuenta_nombre","cuenta_id","cuenta_hijo","true","cuenta_codigo","cuenta_nombre");
$('#arbolCuentas').treeview({
    levels: 1,
    color: "#555",
    showBorder: false,
    data: arbol,
    multiSelect: $('#chk-select-multi').is(':checked'),
    onNodeSelected: function (event, node) {
        $("#cuenta_id_editar").val(); 
        $("#cuenta_id_editar").val(node.tags);
        var str=node.text;var nomcuenta="";
        var res = str.split(" ");
        for(var i in res){
            if(i!=0){
                if(i==1){
            	nomcuenta=nomcuenta+res[i];}
            	if(i>=2){
            	nomcuenta=nomcuenta+" "+res[i];
                }
           	}
        }
       	$('#nomCtaSeleccionada').val(nomcuenta);
    },
    onNodeUnselected: function (event, node) {
 
        $("#cuenta_id_editar").val("");
       
    }
});

function agregarcuenta(){
	var existe=false;
	var cuenta = document.getElementById("cuenta_id_editar").value;
	var cuentanombre = document.getElementById("nomCtaSeleccionada").value;
	if(cuenta!="" && cuentanombre!=""){
        $("#cuentasComparativas option").each(function(){
        	   if($(this).attr('value')==cuenta){
        		   existe=true;
               }
        });
        alert(existe);
        if(existe==false){
		$("#cuentasComparativas").append('<option value="'+cuenta+'">'+cuentanombre+'</option>');}
    }
}

function consultarAnalisis(){
	
	var analisis = $("#analisis").val();
	
	
	if(analisis!=0){
	$.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
	$.ajax({
        url: '{{ url('/consultarAnalisisPreConfig') }}',
        type: 'POST',
        data: {'analisis_pre_configurado_id': analisis},
        success: function(data){
          	$('#guardaAnalisisPreConfig').attr('action', '/actualizarAnalisisPreConfig');
          	$("#estadoActualizacion").show();
          	$("#analisis_pre_configurado_id").val(data[0].analisis_pre_configurado.analisis_pre_configurado_id);
          	$("#analisis_pre_configurado_nombre").val(data[0].analisis_pre_configurado.analisis_pre_configurado_nombre);
          	$("#analisis_preconfigurado_criterio").val(data[0].analisis_pre_configurado.analisis_pre_configurado_criterio);
          	$("input[class='radio-estado'][value='"+data[0].analisis_pre_configurado.estado_id+"']").prop("checked",true);
          	var option = '';
          	for(var i in data){
          		option=option+"<option value="+data[i].cuenta.cuenta_id+">"+data[i].cuenta.cuenta_nombre+"</option>";
                }

          	document.getElementById("cuentasComparativas").innerHTML = option;
            setTimeout($.unblockUI, 1000);            
        }     
        }); 
    }
}

function valAnalisisPreConfig(){
	
	var nombre=  document.getElementById("analisis_pre_configurado_nombre").value;
	var codigo= document.getElementById("analisis_preconfigurado_criterio").value;
	var cuentas=$("#cuentasComparativas").find('option').length;
	var bValid=true;
	bValid=bValid&&validarCampoLleno(nombre,'nombre');
	bValid=bValid&&validarCampoLleno(nombre,'criterio');

	if(bValid && cuentas>=2){
	$('#cuentasComparativas option').prop('selected', true);
	$( "#guardaAnalisisPreConfig" ).submit(); 
	}
}

function eliminarcuenta(){
var selector = $("#cuentasComparativas").val();
$("#cuentasComparativas option[value='"+selector+"']").remove();

}
 function cancelardivisionpolitica(){
$('#guardaAnalisisPreConfig').attr('action', '/guardaAnalisisPreConfig');
$("#analisis_pre_configurado_id").val(""); $("#analisis_preconfigurado_criterio").val(""); 
$("#analisis_pre_configurado_nombre").val("");document.getElementById("cuentasComparativas").innerHTML = "";
$("#cuentasComparativas").val("");$("#estadoActualizacion").hide();$("input[class='radio-estado']").prop("checked",false);
}

</script>
@endsection