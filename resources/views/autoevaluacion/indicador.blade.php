@extends('layouts.app')

@section('content')
<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2><span class="text-navy"><i class="fa fa-chevron-circle-right"></i> Información básica del Indicador</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                <div class="col-lg-12">
                                    @if(count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        @foreach($errors->all() as $error)
                                        <label>{{ $error }}</label> 
                                        </br>
                                        @endforeach
                                    </div>
                                    @endif

                                    @if (notify()->ready())
                                    <div class="alert alert-{{ notify()->type() }}">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <label>{{ notify()->message() }}</label>
                                    </div>
                                    @endif

                                    <div id="mensajes" name="mensajes" class="hide">
                                        <label id="textoMensaje" name="textoMensaje"></label>
                                    </div>

                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <form class="form-horizontal" method="POST" name="formIndicador" enctype="multipart/form-data"  action="{{url('/guardarIndicador')}}" role="form">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input id="indicador_formula" type="hidden" name="indicador_formula" value="">
                                                <input id="indicador_id" type="hidden" name="indicador_id" value="">
                                                <input id="lstRangosAceptacion" type="hidden" name="lstRangosAceptacion" value="" multiple="yes">
                                                <input id="lstNivelesAlerta" type="hidden" name="lstNivelesAlerta" value="" multiple="yes">
                                                <input id="lstNivelesAlertaAct" type="hidden" name="lstNivelesAlertaAct" value="" multiple="yes">
                                                <input id="criterioId" type="hidden" name="criterioId" value="" multiple="yes">
                                                <input id="lstCambioMejora" type="hidden" name="lstCambioMejora" value="" multiple="yes">

                                                <div class="panel-heading">
                                                    <div class="panel-options">
                                                        <ul class="nav nav-tabs" role="tablist">
                                                            <li class="active"><a data-toggle="tab" href="#tab-1">Información Básica</a></li>
                                                            <li id="tabInfoEsp" class="hide"><a data-toggle="tab" href="#tab-2">Información Específica</a></li>
                                                            <li id="tabInfoPro" class="hide"><a data-toggle="tab" href="#tab-3">Información Prospectiva</a></li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="panel-body">
                                                    <div class="tab-content">
                                                        <div id="tab-1" class="tab-pane active">
                                                                <div class="form-group">
                                                                    <label class="col-sm-1 control-label">Tipo</label>
                                                                    <div class="col-sm-5">
                                                                        @foreach($lstTipoIndicador as $tipoIndicador)
                                                                        <div class="radio"><label> <input type="radio" value="{{$tipoIndicador->tipo_indicador_id}}" id="tipo_indicador_id" name="tipo_indicador_id" required>{{$tipoIndicador->tipo_indicador_nombre}}</label></div>
                                                                        @endforeach
                                                                    </div>
                                                                    <label id="lblProyFinanciera"  class="hide col-sm-3 control-label">Relacionado con Proyección Financiera</label>
                                                                    <div  id="divProyFinanciera" class="hide col-sm-3">
                                                                        <div class="radio-inline"><label> <input type="radio" value="S" name="indicador_proyeccion_financiera">SI</label></div>
                                                                        <div class="radio-inline"><label> <input type="radio" value="N" name="indicador_proyeccion_financiera">NO</label></div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-lg-1 control-label " >Nombre</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="form-control" maxlength="254" name="indicador_nombre" id="indicador_nombre" style="text-transform:uppercase;"/>
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <button class="btn btn-primary" id="btn_editar"  name="btn_editarCuenta" type="button"><i class="fa fa-edit"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-lg-1 control-label " >Objetivo</label>
                                                                    <div class="col-lg-5">
                                                                        <textarea class="form-control" maxlength="254" rows="4" cols="50" name="indicador_objetivo" id="indicador_objetivo" style="text-transform:uppercase;resize: none;"></textarea>                                                        
                                                                    </div>
                                                                    <label class="col-lg-1 control-label " >Alcance</label>
                                                                    <div class="col-lg-5">
                                                                        <textarea class="form-control" maxlength="254" rows="4" cols="50" name="indicador_alcance" id="indicador_alcance" style="text-transform:uppercase;resize: none;"></textarea>                                                        
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-heading">
                                                                                Fórmula
                                                                            </div>
                                                                            <div class="panel-body col-lg-6">
                                                                                <div class="form-group">
                                                                                    <label class="col-lg-4 control-label " >Variable</label>
                                                                                    <div class="col-lg-8">
                                                                                        <select class="form-control m-b" name="variable_indicador_id" id="variable_indicador_id" >
                                                                                            <option value="">Seleccionar...</option>
                                                                                            @foreach($lstVariableIndicador as $variableIndicador)
                                                                                            <option value="{{$variableIndicador->variable_indicador_id}}">{{$variableIndicador->variable_indicador_nombre}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="col-lg-4 control-label " >Operadores matemáticos</label>
                                                                                    <div class="col-lg-8">
                                                                                        <select class="form-control m-b" name="operador_matematico" id="operador_matematico">
                                                                                            <option value="">Seleccionar...</option>
                                                                                            @foreach($lstOperadoresMatematicos as $operadoresMatematicos)
                                                                                            <option value="{{$operadoresMatematicos->operadores_matematicos_simbolo}}">{{$operadoresMatematicos->operadores_matematicos_nombre}} {{$operadoresMatematicos->operadores_matematicos_simbolo}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="col-lg-4 control-label " >Signos de agrupación</label>
                                                                                    <div class="col-lg-8">
                                                                                        <select class="form-control m-b" name="signo_agrupacion" id="signo_agrupacion">
                                                                                            <option value="">Seleccionar...</option>
                                                                                            @foreach($lstSignosAgrupacion as $signosAgrupacion)
                                                                                            <option value="{{$signosAgrupacion->signos_agrupacion_simbolo}}">{{$signosAgrupacion->signos_agrupacion_simbolo}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="col-lg-4 control-label " >Constantes de valor</label>
                                                                                    <div class="col-sm-8">
                                                                                        <input type="number" min="0" class="form-control" id="valorConstante" name="valorConstante">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="panel-body col-lg-6">
                                                                                <label class="col-lg-12 control-label " id="lblArbol"></label>
                                                                                <div class="col-lg-12">
                                                                                    <div class="panel panel-default">
                                                                                        <div class="panel-body" id="divValorVarIndicador">
                                                                                            <div id="arbolVarIndicador" class="step-content" >
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="panel-body">
                                                                                <div class="form-group">
                                                                                    <div class="col-sm-12 text-center">
                                                                                        <button id="btn_agregar" class="btn btn-outline btn-primary" type="button">
                                                                                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar</button>
                                                                                        <button id="btn_quitar" class="btn btn-outline btn-primary" type="button">
                                                                                            <i class="fa fa-times" aria-hidden="true"></i> Quitar</button>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <div class="col-sm-12">
                                                                                        <div class="panel panel-default">
                                                                                            <div class="panel-heading">
                                                                                                Fórmula Planteada
                                                                                            </div>
                                                                                            <div class="panel-body">
                                                                                                <div class="form-group">
                                                                                                    <label class="col-lg-12 text-center" id="lblFormula" name="lblFormula"></label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <div class="col-sm-12 text-right">
                                                                                        <button id="btn_limpiar" class="btn btn-default" type="button">Limpiar</button>
                                                                                        <button id="btn_guardar" class="btn btn-primary" type="button">Almacenar</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        </div>     
                                                        
                                                        <div id="tab-2" class="tab-pane">
                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            Rangos de Aceptación
                                                                        </div>
                                                                        <div class="panel-body">
                                                                            <div class="col-lg-6">
                                                                                <div class="form-group">
                                                                                    <label class="col-lg-4 control-label " >Categoría Poblacional</label>
                                                                                    <div class="col-lg-8">
                                                                                        <select class="form-control m-b" name="categoria_id" id="categoria_id">
                                                                                            <option value="">Seleccionar...</option>
                                                                                            @foreach($lstCategoria as $Categoria)
                                                                                            <option value="{{$Categoria->categoria_id}}">{{$Categoria->categoria_nombre}}&nbsp;&nbsp; ~ &nbsp;&nbsp;{{$Categoria->categoria_minimo}} - {{$Categoria->categoria_maximo}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="col-lg-4 control-label " >Valor Mínimo</label>
                                                                                    <div class="col-sm-8">
                                                                                        <input type="number" class="form-control" id="valor_minimo" name="valor_minimo" min="0">
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="col-lg-4 control-label " >Valor Máximo</label>
                                                                                    <div class="col-sm-8">
                                                                                        <input type="number" class="form-control" id="valor_maximo" name="valor_maximo" min="0">
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <div class="col-lg-12 text-right">
                                                                                        <button id="btn_agregar" class="btn btn-outline btn-primary" type="button" onclick="addTabla(1,'tblRangos')">
                                                                                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar</button>
                                                                                        <button id="btn_quitar" class="btn btn-outline btn-primary" type="button" onclick="removeTabla(1,'tblRangos')">
                                                                                            <i class="fa fa-times" aria-hidden="true"></i> Quitar</button>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-heading">
                                                                                        Rangos Ingresados
                                                                                    </div>
                                                                                    <div class="panel-body">
                                                                                        <div class="form-group">
                                                                                            <table id="tblRangos" class="table table-bordered">
                                                                                                <tbody>           
                                                                                                </tbody>
                                                                                            </table> 
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12 text-right">
                                                                                <button id="btn_guardar_rangos" class="btn btn-primary" type="button">Almacenar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        
                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            Niveles de Alerta
                                                                        </div>
                                                                        <div class="panel-body">
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <label class="col-lg-4 control-label " >Rangos de Aceptación</label>
                                                                                <div class="col-lg-8">
                                                                                    <select class="form-control m-b" name="rango_aceptacion_id_1" id="rango_aceptacion_id_1">
                                                                                    </select>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label class="col-lg-4 control-label " >Niveles Definidos</label>
                                                                                <div class="col-lg-8">
                                                                                    <select class="form-control m-b selectpicker" name="tipo_nivel_alerta_id" id="tipo_nivel_alerta_id" title="Seleccione...">
                                                                                        @foreach($lstTipoNivelAlerta as $tipoNivelAlerta)
                                                                                            <option value="{{$tipoNivelAlerta->tipo_nivel_alerta_id}}" data-content="<span class='label label-success' style=background-color:{{$tipoNivelAlerta->tipo_nivel_alerta_color}};color:{{$tipoNivelAlerta->tipo_nivel_alerta_color}}; >...</span>&nbsp;&nbsp;{{$tipoNivelAlerta->tipo_nivel_alerta_nombre}}">{{$tipoNivelAlerta->tipo_nivel_alerta_color}}-{{$tipoNivelAlerta->tipo_nivel_alerta_nombre}}</option>
                                                                                        @endforeach
                                                                                    </select>

                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label class="col-lg-4 control-label " >Valor Desde</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="number" class="form-control" id="valorDesde" name="valorDesde" min="0">
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label class="col-lg-4 control-label " >Valor Hasta</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="number" class="form-control" id="valorHasta" name="valorHasta" min="0">
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="form-group">
                                                                                <div class="col-lg-12 text-right">
                                                                                    <button id="btn_agregar" class="btn btn-outline btn-primary" type="button" onclick="addTabla(2,'tblNivel')">
                                                                                        <i class="fa fa-plus" aria-hidden="true"></i> Agregar</button>
                                                                                    <button id="btn_quitar" class="btn btn-outline btn-primary" type="button" onclick="removeTabla(2,'tblNivel')">
                                                                                        <i class="fa fa-times" aria-hidden="true"></i> Quitar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <table id="tblNivel" class="table table-bordered" style="text-align: center;">
                                                                                     <tr>
                                                                                         <th style="text-align: center;font-size:small;background-color:#FFFFFF"></th>
                                                                                         <th style="text-align: center;font-size:small;background-color:#FFFFFF">Categoría</th>
                                                                                         <th style="text-align: center;font-size:small;background-color:#FFFFFF">Nivel</th>
                                                                                         <th style="text-align: center;font-size:small;background-color:#FFFFFF">Desde - Hasta</th>
                                                                                     </tr>
                                                                                <tbody>           
                                                                                </tbody>
                                                                            </table> 
                                                                        </div>
                                                                        <div class="col-sm-12 text-right">
                                                                            <button id="btn_guardar_nivelAlerta" class="btn btn-primary" type="button">Almacenar</button>
                                                                        </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="tab-3" class="tab-pane">
                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            Guía del Indicador
                                                                        </div>
                                                                        <div class="panel-body">
                                                                            <div class="col-lg-6">
                                                                                <div class="form-group">
                                                                                    <label class="col-lg-4 control-label " >Rangos de Aceptación</label>
                                                                                    <div class="col-lg-8">
                                                                                        <select class="form-control m-b" name="rango_aceptacion_id" id="rango_aceptacion_id">
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">                                                                            
                                                                                    <label class="control-label col-lg-4 " >Niveles de Alerta</label>
                                                                                    <div class="col-lg-8">
                                                                                         <select class="form-control m-b selectpicker" name="nivel_alerta_id" id="nivel_alerta_id" title="Seleccione..."></select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <div class="col-sm-12 text-right">
                                                                                        <button id="btn_agregar" class="btn btn-outline btn-primary" type="button" onclick="addTabla(3,'tblGuia')">
                                                                                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar</button>
                                                                                        <button id="btn_quitar" class="btn btn-outline btn-primary" type="button" onclick="removeTabla(3,'tblGuia')">
                                                                                            <i class="fa fa-times" aria-hidden="true"></i> Quitar</button>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-heading">
                                                                                        Guía Ingresada
                                                                                    </div>
                                                                                    <div class="panel-body">
                                                                                        <div class="form-group">
                                                                                            <table id="tblGuia" class="table table-bordered" style="text-align: center;">
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12">
                                                                                <div class="form-group">
                                                                                    <label class="col-lg-2 control-label" >Descripción de Resultados</label>
                                                                                    <div class="col-lg-10">
                                                                                        <textarea class="form-control" id="nivel_alerta_descripcion" name="nivel_alerta_descripcion" maxlength="254" rows="4" cols="50" style="text-transform:uppercase;resize: none;"></textarea>                                                        
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="col-lg-2 control-label" >Interpretación de Resultados</label>
                                                                                    <div class="col-lg-10">
                                                                                        <textarea class="form-control" id="nivel_alerta_interpretacion" name="nivel_alerta_interpretacion" maxlength="254" rows="4" cols="50" style="text-transform:uppercase;resize: none;"></textarea>                                                        
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12 text-right">
                                                                                <button id="btn_actualizar_nivel_alerta" class="btn btn-primary" type="button">Almacenar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            Acciones de Cambio y Mejora
                                                                        </div>
                                                                        <div class="panel-body">
                                                                            <div class="form-group">
                                                                                <label class="col-lg-2 control-label " >Rangos de Aceptación</label>
                                                                                <div class="col-lg-4">
                                                                                    <select class="form-control m-b" name="rango_aceptacion_id_2" id="rango_aceptacion_id_2"></select>
                                                                                </div>
                                                                                <label class="col-lg-2 control-label " >Niveles de Alerta</label>
                                                                                <div class="col-lg-4">
                                                                                    <select class="form-control m-b selectpicker" name="nivel_alerta_id_2" id="nivel_alerta_id_2" title="Seleccione..."></select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                    <div class="col-sm-12">
                                                                                        <div class="panel panel-default">
                                                                                            <div class="panel-heading">
                                                                                                Fórmula Planteada
                                                                                            </div>
                                                                                            <div class="panel-body">
                                                                                                <div class="form-group">
                                                                                                    <label class="col-lg-12 text-center" id="lblFormula_2" name="lblFormula_2"></label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-lg-2" >Componentes de la Fórmula</label>
                                                                                <div class="col-lg-4">
                                                                                    <select class="form-control m-b" id="componentes_formula" name="componentes_formula" >
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-sm-6 ">
                                                                                    <button id="agregar" class="btn btn-outline btn-primary" type="button" onclick="addTabla(4,'tblCambio')">
                                                                                        <i class="fa fa-plus" aria-hidden="true"></i> Agregar</button>
                                                                                    <button id="quitar" class="btn btn-outline btn-primary" type="button" onclick="addTabla(4,'tblCambio')">
                                                                                        <i class="fa fa-times" aria-hidden="true"></i> Quitar</button>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="col-lg-12">
                                                                                    <div class="panel panel-default">
                                                                                        <div class="panel-heading">
                                                                                            Criterio Cambio y Mejora Ingresado
                                                                                        </div>
                                                                                        <div class="panel-body">
                                                                                        <div class="form-group">
                                                                                            <table id="tblCambio" class="table table-bordered" style="text-align: center;">
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-lg-2 control-label" >Criterio Cambio y Mejora</label>
                                                                                <div class="col-lg-10">
                                                                                    <textarea id="criterio_cambio_criterio" name="criterio_cambio_criterio" class="form-control" maxlength="254" rows="4" cols="50" style="text-transform:uppercase;resize: none;"></textarea>                                                        
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12 text-right">
                                                                                <button id="btn_guardar_cambio" class="btn btn-primary" type="button">Almacenar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">

var totR_Rangos = 0;
var totR_Nivel = 0;
var totR_Guia = 0;
var totR_Cambio = 0;

function addTabla(opcion,id) {
    
    var bValid=true;
    $('#mensajes').removeClass('alert alert-danger alert alert-warning');
    $('#mensajes').addClass('hide');

    switch(opcion) {
        case 1:
            var categoria_id=$("#categoria_id option:selected").val();
            var categoria_text=$("#categoria_id option:selected").text();
            var valor_minimo=$("#valor_minimo").val();
            var valor_maximo= $("#valor_maximo").val();
            var tableReg = document.getElementById(id);
            var found = false;
            
            if(parseFloat(valor_minimo) < parseFloat(valor_maximo))
               bValid=true;
            else{
               bValid=false;
               toTop();
               $('#mensajes').addClass('alert alert-danger');
               $('#mensajes').removeClass('hide');
               var tips = $("#textoMensaje");
               tips.text("Error ! " + "Valor mínimo no es menor que el valor Máximo.");
            }
            
            bValid = bValid && validarCampoLleno(categoria_id, "Categoría");
            bValid = bValid && validarCampoLleno(valor_minimo, "Valor Mínimo");
            bValid = bValid && validarCampoLleno(valor_maximo, "Valor Máximo");
            
            if(bValid){
                var str = categoria_text;
                var res = str.split("~");
                res.pop();
                var valor = res[0]
                var valorfinal = valor.trim();

                if(tableReg.rows.length > 0){
                    for (var i = 0 ; i < tableReg.rows.length; i++)
                    {
                        cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
                        for (var j = 0; j < cellsOfRow.length && !found; j++)
                        {
                            compareWith = cellsOfRow[j].innerText;
                            if (valorfinal.length == 0 || (compareWith.indexOf(valorfinal) > -1))
                            {
                                found = true; 
                                continue;
                            }
                        }   
                        if(found){
                           continue; 
                        }
                    }

                    if(found){
                        toTop();
                        $('#mensajes').addClass('alert alert-danger');
                        $('#mensajes').removeClass('hide');
                        var tips = $("#textoMensaje");
                        tips.text("Error ! " + "Categoría ya se encuentra ingresada.");
                    }
                 }
            }
            else{
             toTop();   
            }
            
            if(bValid && !found){
                var objTable = document.getElementById(id);
                var objRow = objTable.insertRow(objTable.rows.length);
                var objCell1 = objRow.insertCell(0);
                var objCell2 = objRow.insertCell(1);
                var objCell3 = objRow.insertCell(1);
                var objInputCheckBox = document.createElement("input");
                objInputCheckBox.type = "checkbox";
                var objInputLabel1Box = document.createElement("label");
                var objInputLabel2Box = document.createElement("label");
                objCell1.appendChild(objInputCheckBox);
                objCell2.appendChild(objInputLabel1Box);
                objCell3.appendChild(objInputLabel2Box);
                objInputCheckBox.innerHTML = categoria_id;
                objInputLabel2Box.innerHTML = valor_minimo + ' - ' + valor_maximo;
                objInputLabel1Box.innerHTML =valorfinal;
                $("#categoria_id").val(""); 
                $("#valor_minimo").val("");
                $("#valor_maximo").val("");
                totR_Rangos = totR_Rangos + 1;    
            }
            break;
        case 2:
            var rango_aceptacion_id=$("#rango_aceptacion_id_1 option:selected").val();
            var rango_aceptacion_text=$("#rango_aceptacion_id_1 option:selected").text();
            var nivel_alerta_id=$("#tipo_nivel_alerta_id option:selected").val();
            var nivel_alerta_text=$("#tipo_nivel_alerta_id option:selected").text();
            var valor_desde=$("#valorDesde").val();
            var valor_hasta= $("#valorHasta").val();
            var tableReg = document.getElementById(id);
            var found = false;
            
            if(parseFloat(valor_desde) < parseFloat(valor_hasta))
               bValid=true;
            else{
               bValid=false;
               toTop();
               $('#mensajes').addClass('alert alert-danger');
               $('#mensajes').removeClass('hide');
               var tips = $("#textoMensaje");
               tips.text("Error ! " + "Valor desde no es menor que el valor Hasta.");
            }
            
            bValid = bValid && validarCampoLleno(rango_aceptacion_id, "Rango de Aceptación");
            bValid = bValid && validarCampoLleno(nivel_alerta_id, "Nivel Definido");
            bValid = bValid && validarCampoLleno(valor_desde, "Valor Desde");
            bValid = bValid && validarCampoLleno(valor_hasta, "Valor Hasta");
            
            if(bValid){
                var str = rango_aceptacion_text;
                var res = str.split("~");
                res.pop();
                var valor = res[0];
                var valorfinal = valor.trim();
                
                var str1 = nivel_alerta_text;
                var res1 = str1.split("-");
                var valor1 = res1[0];
                var valor2 = res1[1];
                var valorFinal1 = valor1.trim();
                var valorFinal2 = valor2.trim();

                var valorComparar = valorfinal+' ...   '+valorFinal2;
                if(tableReg.rows.length > 0){
                    for(var i = 0 ; i < tableReg.rows.length; i++)
                    {
                        cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
                        for(var j = 0; j < cellsOfRow.length && !found; j++)
                        {
                            compareWith = cellsOfRow[j].innerText;
                            if(valorfinal.length == 0 || (compareWith.indexOf(valorfinal) > -1))
                            {
                                compareWith2 = cellsOfRow[j].innerText +' '+cellsOfRow[j+1].innerText;
                                if(valorComparar.length == 0 || (compareWith2.indexOf(valorComparar) > -1))
                                { 
                                  found = true; 
                                  continue;
                                }
                            }
                        }   
                        if(found){
                           continue; 
                        }
                    }

                    if(found){
                        toTop();
                        $('#mensajes').addClass('alert alert-danger');
                        $('#mensajes').removeClass('hide');
                        var tips = $("#textoMensaje");
                        tips.text("Error ! " + "Categoría y Nivel ya se encuentran ingresados.");
                    }
                 }
            }
            else{ 
                toTop();
            }

            if(bValid && !found){
                var objTable = document.getElementById(id);
                var objRow = objTable.insertRow(objTable.rows.length);
                var objCell1 = objRow.insertCell(0);
                var objCell2 = objRow.insertCell(1);
                var objCell3 = objRow.insertCell(1);
                var objCell4 = objRow.insertCell(1);
                var objInputCheckBox = document.createElement("input");
                objInputCheckBox.type = "checkbox";
                var objInputLabel1Box = document.createElement("label");
                var objInputLabel2Box = document.createElement("label");
                var objInputLabel3Box = document.createElement("label");
                objCell1.appendChild(objInputCheckBox);
                objCell2.appendChild(objInputLabel1Box);
                objCell3.appendChild(objInputLabel2Box);
                objCell4.appendChild(objInputLabel3Box);
                objInputCheckBox.innerHTML = rango_aceptacion_id +" - "+nivel_alerta_id;
                objInputLabel3Box.innerHTML = valorfinal;
                objInputLabel2Box.innerHTML = "<span style='background-color:"+valorFinal1+";color:"+valorFinal1+"' >...</span> &nbsp;&nbsp;"+valorFinal2; 
                objInputLabel1Box.innerHTML = valor_desde + ' - ' + valor_hasta;
                
                $("#rango_aceptacion_id_1").val("");
                $("#tipo_nivel_alerta_id").selectpicker('val',''); 
                $("#valorDesde").val("");
                $("#valorHasta").val("");
                
                totR_Nivel = totR_Nivel + 1;
                   
            }
            break;
        case 3:
            var rango_aceptacion_id=$("#rango_aceptacion_id option:selected").val();
            var rango_aceptacion_text=$("#rango_aceptacion_id option:selected").text();
            var nivel_alerta_id=$("#nivel_alerta_id option:selected").val();
            var nivel_alerta_text=$("#nivel_alerta_id option:selected").text();
            var nivel_alerta_descripcion=$("#nivel_alerta_descripcion").val();
            var nivel_alerta_interpretacion= $("#nivel_alerta_interpretacion").val();
            var tableReg = document.getElementById(id);
            var found = false;

            bValid = bValid && validarCampoLleno(rango_aceptacion_id, "Rango de Aceptación");
            bValid = bValid && validarCampoLleno(nivel_alerta_id, "Nivel Alerta");
            bValid = bValid && validarCampoLleno(nivel_alerta_descripcion, "Descripcion");
            bValid = bValid && validarCampoLleno(nivel_alerta_interpretacion, "Interpretacion");

            if(bValid){
                var str = rango_aceptacion_text;
                var res = str.split("~");
                res.pop();
                var valor = res[0];
                var valorfinal = valor.trim();

                var str1 = nivel_alerta_text;
                var res1 = str1.split("-");
                var valor1 = res1[0];
                var valor2 = res1[1];
                var valorFinal1 = valor1.trim();
                var valorFinal2 = valor2.trim();
                
                var valorComparar = valorfinal+' ...   '+valorFinal2;

                if(tableReg.rows.length > 0){
                    for (var i = 0 ; i < tableReg.rows.length; i++)
                    {
                        cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
                        for(var j = 0; j < cellsOfRow.length && !found; j++)
                        {
                            compareWith = cellsOfRow[j].innerText;
                            if(valorfinal.length == 0 || (compareWith.indexOf(valorfinal) > -1))
                            {
                                compareWith2 = cellsOfRow[j].innerText +' '+cellsOfRow[j+1].innerText;
                                if(valorComparar.length == 0 || (compareWith2.indexOf(valorComparar) > -1))
                                { 
                                  found = true; 
                                  continue;
                                }
                            }
                        }      
                        if(found){
                           continue; 
                        }
                    }

                    if(found){
                        toTop();
                        $('#mensajes').addClass('alert alert-danger');
                        $('#mensajes').removeClass('hide');
                        var tips = $("#textoMensaje");
                        tips.text("Error ! " + "Categoría y Nivel ya se encuentran ingresados.");
                    }
                 }
            }
            else{
                toTop();
            }
            if(bValid && !found){
                totR_Guia = totR_Guia + 1;
                
                var objTable = document.getElementById(id);
                var objRow = objTable.insertRow(objTable.rows.length);
                var objCell1 = objRow.insertCell(0);
                var objCell2 = objRow.insertCell(1);
                var objCell3 = objRow.insertCell(1);
                var objCell4 = objRow.insertCell(1);
                var objCell5 = objRow.insertCell(1);
                var objCell6 = objRow.insertCell(1);
                var objInputCheckBox = document.createElement("input");
                objInputCheckBox.type = "checkbox";
                var objInputLabel1Box = document.createElement("label");
                var objInputLabel2Box = document.createElement("label");
                var objInputLabel3Box = document.createElement("label");
                var objInputLabel4Box = document.createElement("label");
                var objInputLabel5Box = document.createElement("label");
                
                objCell1.appendChild(objInputCheckBox);
                objCell2.appendChild(objInputLabel1Box);
                objCell2.setAttribute("style", "display:none");
                objCell3.appendChild(objInputLabel2Box);                
                objCell3.setAttribute("style", "display:none");
                
                objCell4.appendChild(objInputLabel3Box);
                objCell5.appendChild(objInputLabel4Box);
                objCell6.appendChild(objInputLabel5Box);
                objInputCheckBox.innerHTML  = nivel_alerta_id;
                objInputLabel5Box.innerHTML = 'Criterio '+totR_Guia;
                objInputLabel4Box.innerHTML = valorfinal;
                objInputLabel3Box.innerHTML = "<span style='background-color:"+valorFinal1+";color:"+valorFinal1+"' >...</span> &nbsp;&nbsp;"+valorFinal2;
                objInputLabel2Box.innerHTML = nivel_alerta_descripcion; 
                objInputLabel1Box.innerHTML = nivel_alerta_interpretacion;

                $("#rango_aceptacion_id").val(""); 
                $("#nivel_alerta_id").selectpicker('val',''); 
                $("#nivel_alerta_descripcion").val("");
                $("#nivel_alerta_interpretacion").val("");  
            }
            break;
            
        case 4:
            var rango_aceptacion_id=$("#rango_aceptacion_id_2 option:selected").val();
            var rango_aceptacion_text=$("#rango_aceptacion_id_2 option:selected").text();
            var nivel_alerta_id=$("#nivel_alerta_id_2 option:selected").val();
            var nivel_alerta_text=$("#nivel_alerta_id_2 option:selected").text();
            var componentes_formula_id=$("#componentes_formula option:selected").val();
            var componentes_formula_text=$("#componentes_formula option:selected").text();
            var criterio_cambio_criterio=$("#criterio_cambio_criterio").val();
            var tableReg = document.getElementById(id);
            var found = false;

            bValid = bValid && validarCampoLleno(rango_aceptacion_id, "Rango de Aceptación");
            bValid = bValid && validarCampoLleno(nivel_alerta_id, "Nivel Alerta");
            bValid = bValid && validarCampoLleno(componentes_formula_id, "Componente Fórmula");
            bValid = bValid && validarCampoLleno(criterio_cambio_criterio, "Criterio Cambio y Mejora");

            if(bValid){
                var str = rango_aceptacion_text;
                var res = str.split("~");
                res.pop();
                var valor = res[0];
                var valorfinal = valor.trim();

                var str1 = nivel_alerta_text;
                var res1 = str1.split("-");
                var valor1 = res1[0];
                var valor2 = res1[1];
                var valorFinal1 = valor1.trim();
                var valorFinal2 = valor2.trim();
                
                var valorComparar = valorfinal+' ...   '+valorFinal2;

                if(tableReg.rows.length > 0){
                    for (var i = 0 ; i < tableReg.rows.length; i++)
                    {
                        cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
                        for(var j = 0; j < cellsOfRow.length && !found; j++)
                        {
                            compareWith = cellsOfRow[j].innerText;
                            if(valorfinal.length == 0 || (compareWith.indexOf(valorfinal) > -1))
                            {
                                compareWith2 = cellsOfRow[j].innerText +' '+cellsOfRow[j+1].innerText;
                                if(valorComparar.length == 0 || (compareWith2.indexOf(valorComparar) > -1))
                                { 
                                  found = true; 
                                  continue;
                                }
                            }
                        }      
                        if(found){
                           continue; 
                        }
                    }

                    if(found){
                        toTop();
                        $('#mensajes').addClass('alert alert-danger');
                        $('#mensajes').removeClass('hide');
                        var tips = $("#textoMensaje");
                        tips.text("Error ! " + "Categoría y Nivel ya se encuentran ingresados.");
                    }
                 }
            }
            else{
                toTop();
            }
            if(bValid && !found){
                totR_Cambio = totR_Cambio + 1;
                
                var objTable = document.getElementById(id);
                var objRow = objTable.insertRow(objTable.rows.length);
                var objCell1 = objRow.insertCell(0);
                var objCell2 = objRow.insertCell(1);
                var objCell3 = objRow.insertCell(1);
                var objCell4 = objRow.insertCell(1);
                var objCell5 = objRow.insertCell(1);
                var objCell6 = objRow.insertCell(1);
                var objInputCheckBox = document.createElement("input");
                objInputCheckBox.type = "checkbox";
                var objInputLabel1Box = document.createElement("label");
                var objInputLabel2Box = document.createElement("label");
                var objInputLabel3Box = document.createElement("label");
                var objInputLabel4Box = document.createElement("label");
                var objInputLabel5Box = document.createElement("label");
                
                objCell1.appendChild(objInputCheckBox);
                objCell2.appendChild(objInputLabel1Box);
                objCell2.setAttribute("style", "display:none");
                objCell3.appendChild(objInputLabel2Box);       
                objCell4.appendChild(objInputLabel3Box);
                objCell5.appendChild(objInputLabel4Box);
                objCell6.appendChild(objInputLabel5Box);
                objInputCheckBox.innerHTML  = nivel_alerta_id;
                objInputLabel5Box.innerHTML = 'Criterio '+totR_Cambio;
                objInputLabel4Box.innerHTML = valorfinal;
                objInputLabel3Box.innerHTML = "<span style='background-color:"+valorFinal1+";color:"+valorFinal1+"' >...</span> &nbsp;&nbsp;"+valorFinal2;
                objInputLabel2Box.innerHTML = componentes_formula_text; 
                objInputLabel1Box.innerHTML = criterio_cambio_criterio;

                $("#rango_aceptacion_id_2").val(""); 
                $("#nivel_alerta_id_2").selectpicker('val',''); 
                $("#componentes_formula").val("");
                $("#criterio_cambio_criterio").val("");  
            }
            break;
    }
}    

function removeTabla(opcion,id) {
   
    var objTable = document.getElementById(id); 
    var iRow = objTable.rows.length; 
    var counter = 0;
    $('#mensajes').removeClass('alert alert-danger');
    $('#mensajes').addClass('hide');
    $("#textoMensaje").text("");
    
    switch(opcion) {
        case 1:
            if (objTable.rows.length > 0) { 
                for (var i = 0; i < objTable.rows.length; i++) { 
                     var chk = objTable.rows[i].cells[0].childNodes[0]; 
                     if (chk.checked) { 
                            objTable.deleteRow(i); 
                            iRow--; 
                            i--; 
                            counter = counter + 1;
                            totR_Rangos = totR_Rangos - 1;
                     } 
                } 
                if (counter == 0) {
                    toTop();
                    $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var tips = $("#textoMensaje");
                    tips.text("Error ! " + "No se han seleccionado rangos a eliminar.");
                } 
            }else{
                toTop();
                $('#mensajes').addClass('alert alert-danger');
                $('#mensajes').removeClass('hide');
                var tips = $("#textoMensaje");
                tips.text("Error ! " + "No se ha añadido productos."); 
            }  
        break;
        
        case 2:
            if (objTable.rows.length > 1) { 
                for (var i = 1; i < objTable.rows.length; i++) { 
                     var chk = objTable.rows[i].cells[0].childNodes[0]; 
                     if (chk.checked) { 
                            objTable.deleteRow(i); 
                            iRow--; 
                            i--; 
                            counter = counter + 1;
                            totR_Nivel = totR_Nivel - 1;
                     } 
                } 
                if (counter == 0) {
                    toTop();
                    $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var tips = $("#textoMensaje");
                    tips.text("Error ! " + "No se han seleccionado niveles de alerta a eliminar.");
                } 
            }else{
                toTop();
                $('#mensajes').addClass('alert alert-danger');
                $('#mensajes').removeClass('hide');
                var tips = $("#textoMensaje");
                tips.text("Error ! " + "No se ha añadido niveles de alerta."); 
            }  
        break;
        case 3:
            if (objTable.rows.length > 0) { 
                for (var i = 0; i < objTable.rows.length; i++) { 
                     var chk = objTable.rows[i].cells[0].childNodes[0]; 
                     if (chk.checked) { 
                            objTable.deleteRow(i); 
                            iRow--; 
                            i--; 
                            counter = counter + 1;
                            totR_Guia = totR_Guia - 1;
                     } 
                } 
                if (counter == 0) {
                    toTop();
                    $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var tips = $("#textoMensaje");
                    tips.text("Error ! " + "No se han seleccionado guias a eliminar.");
                } 
            }else{
                toTop();
                $('#mensajes').addClass('alert alert-danger');
                $('#mensajes').removeClass('hide');
                var tips = $("#textoMensaje");
                tips.text("Error ! " + "No se ha añadido guias del indicador."); 
            }  
        break;
        case 4:
            if (objTable.rows.length > 0) { 
                for (var i = 0; i < objTable.rows.length; i++) { 
                     var chk = objTable.rows[i].cells[0].childNodes[0]; 
                     if (chk.checked) { 
                            objTable.deleteRow(i); 
                            iRow--; 
                            i--; 
                            counter = counter + 1;
                            totR_Cambio = totR_Cambio - 1;
                     } 
                } 
                if (counter == 0) {
                    toTop();
                    $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var tips = $("#textoMensaje");
                    tips.text("Error ! " + "No se han seleccionado acciones de cambio y mejora a eliminar.");
                } 
            }else{
                toTop();
                $('#mensajes').addClass('alert alert-danger');
                $('#mensajes').removeClass('hide');
                var tips = $("#textoMensaje");
                tips.text("Error ! " + "No se ha añadido acciones de cambio y mejora."); 
            }  
        break;
    }
    
    
    
}
 
$(document).ready(function () {

 
    $('#nivel_alerta_id').selectpicker();
    $('#nivel_alerta_id_2').selectpicker();
    $('#tipo_nivel_alerta_id').selectpicker();
    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var valorFormula = '';
    var formula = $("#lblFormula").text();
    var tipoIndicadorFin = <?php echo json_encode($tipoIndicadorFin); ?>;

    $("input[name=tipo_indicador_id]").click(function () {
        var tipoIndicadorSelect = parseInt($('input[name=tipo_indicador_id]:checked').val());
        var radios = document.getElementsByName('indicador_proyeccion_financiera');
        for (var i = 0; i < radios.length; i++) {
            radios[i].checked = false;
        }

        if (tipoIndicadorFin === tipoIndicadorSelect) {
        $('#lblProyFinanciera').removeClass('hide');
        $('#divProyFinanciera').removeClass('hide');
        } else {
        $('#lblProyFinanciera').addClass('hide');
        $('#divProyFinanciera').addClass('hide');
        }
    });

    $("#variable_indicador_id").change(function () {
        var varIndicador = $("#variable_indicador_id option:selected").val();
        $("#lblArbol").text("");
        toTop();
        $.ajax({
        url: '{{ url('/buscarLstVariableIndicador') }}',
                type: 'post',
                "_token": "{{ csrf_token() }}",
                async: true,
                data: {'tipoVarIndicador': varIndicador},
                beforeSend: function () {
                $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                },
                error: function (jqXHR, textStatus, errorThrown) {
                var gnrMensaje = $("#gnrMensaje");
                if (jqXHR.status == '401') {
                gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                $('#gnrError').modal('show');
                }
                },
                success: function (data) {
                if (data == '') {
                $('#mensajes').removeClass('alert alert-danger hide');
                $('#mensajes').addClass('alert alert-warning');
                var tips = $("#textoMensaje");
                tips.text("Advertencia !  No se han encontrado datos de la Variable Seleccionada. ");
                $('#arbolVarIndicador').treeview({
                data: null,
                });
                } else
                {
                var valor1 = <?php echo json_encode($varIndicadorFinanciera); ?>;
                var valor2 = <?php echo json_encode($varIndicadorServicios); ?>;
                var arbol;
                if (parseInt(varIndicador) == valor1) {
                $("#lblArbol").text("Catálogo de Cuentas Oficiales");
                arbol = dataArbolDinamico(data, "cuenta_nombre", "cuenta_id", "cuenta_hijo");
                } else {
                if (parseInt(varIndicador) == valor2) {
                $("#lblArbol").text("Variables de Servicio");
                arbol = dataArbolDinamicoSinHijo(data, "variable_servicio_criterio", "variable_servicio_id");
                } else {
                $("#lblArbol").text("Variables Generales");
                arbol = dataArbolDinamicoSinHijo(data, "variable_general_nombre_programatico", "variable_general_id");
                }
                }

                $('#arbolVarIndicador').treeview({
                levels: 1,
                        color: "#555",
                        showBorder: false,
                        data: arbol,
                        multiSelect: $('#chk-select-multi').is(':checked'),
                        onNodeSelected: function (event, node) {
                        valorFormula = node.text;
                        },
                        onNodeUnselected: function (event, node) {
                        valorFormula = '';
                        }
                });
                }
                },
                complete: function () {
                setTimeout($.unblockUI, 1000);
                },
        });
    });

    $('#btn_agregar').click(function (event) {
        var operador_matematico = $("#operador_matematico").val();
        var signo_agrupacion = $("#signo_agrupacion").val();
        var valorConstante = $("#valorConstante").val();
        if (valorFormula != '') {
            $("#lblFormula").text(formula + '  ' + valorFormula);
        } else {
        if (operador_matematico != '') {
            $("#lblFormula").text(formula + '  ' + operador_matematico);
        } else
                if (signo_agrupacion != '') {
                    $("#lblFormula").text(formula + '  ' + signo_agrupacion);
        } else
                if (valorConstante != '') {
                    $("#lblFormula").text(formula + '  ' + valorConstante);
                }
        }
        var operador_matematico = $("#operador_matematico").val("");
        var signo_agrupacion = $("#signo_agrupacion").val("");
        var valorConstante = $("#valorConstante").val("");
        valorFormula = '';
        formula = $("#lblFormula").text();
    });

    $('#btn_quitar').click(function (event) {
        var str = $("#lblFormula").text();
        var res = str.split("  ");
        res.pop();
        var valor = res.toString()
                var res2 = valor.replace(/,/gi, "  ");
        var operador_matematico = $("#operador_matematico").val("");
        var signo_agrupacion = $("#signo_agrupacion").val("");
        var valorConstante = $("#valorConstante").val("");
        valorFormula = '';
        $("#lblFormula").text(res2);
    });

    $('#btn_limpiar').click(function (event) {
        toTop();
        $('#mensajes').removeClass('alert alert-danger alert alert-warning');
        $('#mensajes').addClass('hide');
        $('#btn_guardar').text('Almacenar');
        $('#variable_indicador_id').val("");
        $('#lblProyFinanciera').addClass('hide');
        $('#divProyFinanciera').addClass('hide');
        $("#tipo_indicador_id").val("");
        $("#indicador_nombre").val("");
        $("#indicador_objetivo").val("");
        $("#indicador_alcance").val("");
        $("#lblFormula").text("");
        $("#lblFormula_2").text("");
        $("#indicador_formula").val("");
        $('#nivel_alerta_id').selectpicker('destroy');
        $('#nivel_alerta_id').html('');
        $('#nivel_alerta_id_2').selectpicker('destroy');
        $('#nivel_alerta_id_2').html('');
        
        $("#indicador_id").val(""); 
        $("#lstRangosAceptacion").val("");
        $("#lstNivelesAlerta").val("");
        $("#lstNivelesAlertaAct").val(""); 
        $("#criterioId").val("");
        $("#lstCambioMejora").val("");

        $("#categoria_id").val(""); 
        $("#valor_minimo").val("");
        $("#valor_maximo").val("");
        
        $("#rango_aceptacion_id_1").val("");
        $("#tipo_nivel_alerta_id").selectpicker('val',''); 
        $("#valorDesde").val("");
        $("#valorHasta").val("");
        
        $("#rango_aceptacion_id").val(""); 
        $("#nivel_alerta_id").selectpicker('val',''); 
        $("#nivel_alerta_descripcion").val("");
        $("#nivel_alerta_interpretacion").val("");  
        
        $("#rango_aceptacion_id_2").val(""); 
        $("#nivel_alerta_id_2").selectpicker('val',''); 
        $("#componentes_formula").val("");
        $("#criterio_cambio_criterio").val("");  
        valorFormula = '';
        formula = '';
        document.formIndicador.action = " {{url('/guardarIndicador')}} ";
        var radios = document.getElementsByName('indicador_proyeccion_financiera');
        for (var i = 0; i < radios.length; i++) {
            radios[i].checked = false;
        }
        $("#variable_indicador_id").val("");
        $("#operador_matematico").val("");
        $("#signo_agrupacion").val("");
        $("#valorConstante").val("");
        $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
        $('#arbolVarIndicador').treeview({
        data: null,
        });
        setTimeout($.unblockUI, 1000);
    });

    $('#btn_guardar').click(function (event) {
        toTop();
        var bValid = true;
        var tipo_indicador_id = $('input[name=tipo_indicador_id]:checked').val();
        var indicador_nombre = $("#indicador_nombre").val();
        var indicador_objetivo = $("#indicador_objetivo").val();
        var indicador_alcance = $("#indicador_alcance").val();
        var indicador_formula = $("#lblFormula").text();
        bValid = bValid && validarCampoLleno(tipo_indicador_id, 'Tipo Indicador');
        if (tipoIndicadorFin === parseInt(tipo_indicador_id)) {
            var indicador_proyeccion_financiera = $('input[name=indicador_proyeccion_financiera]:checked').val();
            bValid = bValid && validarCampoLleno(indicador_proyeccion_financiera, 'Proyección Financiera');
        }
        bValid = bValid && validarCampoLleno(indicador_nombre, 'Nombre');
        bValid = bValid && validarCampoLleno(indicador_objetivo, 'Objetivo');
        bValid = bValid && validarCampoLleno(indicador_alcance, 'Alcance');
        bValid = bValid && validarCampoLleno(indicador_formula, 'Fórmula');

        $("#indicador_formula").val(indicador_formula);
        if (bValid) {
            $('#mensajes').removeClass('alert alert-danger');
            $('#mensajes').addClass('hide');
            $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
            document.formIndicador.submit();
            setTimeout($.unblockUI, 3000);
        }
    });

    $("#btn_editar").click(function (event) {
        toTop();
        $('#mensajes').removeClass('alert alert-danger');
        $('#mensajes').addClass('hide');
        var bValid = true;
        var indicador_nombre = $("#indicador_nombre").val();
        bValid = bValid && validarCampoLleno(indicador_nombre, 'Nombre Indicador');
        if (bValid){
            $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
            $('#variable_indicador_id').val("");
            $('#lblProyFinanciera').addClass('hide');
            $('#divProyFinanciera').addClass('hide');
            $("#indicador_nombre").val("");
            $("#indicador_objetivo").val("");
            $("#indicador_alcance").val("");
            $("#lblFormula").text("");
            $("#lblFormula_2").text("");
            $("#rango_aceptacion").html("");
            $("#rango_aceptacion_1").html("");
            $("#rango_aceptacion_2").html("");
            $("#nivel_alerta_1").html("");
            $("#nivel_alerta_2").html("");
            $("#indicador_formula").val("");
            $("#tblRangos tr").remove();
            $("#tblNivel tr").remove();
            $("#tblGuia tr").remove();
            valorFormula = '';
            formula = '';
            var radios = document.getElementsByName('indicador_proyeccion_financiera');
            for (var i = 0; i < radios.length; i++) {
                radios[i].disabled = false;
                radios[i].checked = false;
            }
            $("#variable_indicador_id").val("");
            $("#operador_matematico").val("");
            $("#signo_agrupacion").val("");
            $("#valorConstante").val("");
            $.ajax({
            url: '{{ url('/buscarIndicadorxNombre') }}',
                    type: 'post',
                    "_token": "{{ csrf_token() }}",
                    async: true,
                    data: {'indicador_nombre':indicador_nombre},
                    error : function(jqXHR, textStatus, errorThrown) {
                    var gnrMensaje = $("#gnrMensaje");
                    if (jqXHR.status == '401'){
                    gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                    $('#gnrError').modal('show');
                    }
                    },
                    success:  function(data) {
                    if (data == ''){
                        $('#mensajes').removeClass('alert alert-warning');
                        $('#mensajes').addClass('alert alert-danger');
                        $('#mensajes').removeClass('hide');
                        var tips = $("#textoMensaje");
                        tips.text("Error ! " + "No se han encontrado resultados.");
                    } else{
                        $('#btn_guardar').text('Actualizar');
                        document.formIndicador.action = " {{url('/actualizarIndicador')}} ";
                        var radios = document.getElementsByName('tipo_indicador_id');
                        for (var i = 0; i < radios.length; i++){
                            if (radios[i].value == data[0].tipo_indicador_id){
                                radios[i].checked = true;
                            }
                        }

                        if (tipoIndicadorFin === parseInt(data[0].tipo_indicador_id)) {
                            $('#lblProyFinanciera').removeClass('hide');
                            $('#divProyFinanciera').removeClass('hide');
                            var radios = document.getElementsByName('indicador_proyeccion_financiera');
                            for (var i = 0; i < radios.length; i++){
                                if (radios[i].value == data[0].indicador_proyeccion_financiera){
                                    radios[i].checked = true;
                                }
                            }
                        } else{
                            $('#lblProyFinanciera').addClass('hide');
                            $('#divProyFinanciera').addClass('hide');
                        }
                        $("#indicador_id").val(data[0].indicador_id);
                        $("#indicador_nombre").val(data[0].indicador_nombre);
                        $("#indicador_objetivo").val(data[0].indicador_objetivo);
                        $("#indicador_alcance").val(data[0].indicador_alcance);
                        $("#lblFormula").text(data[0].indicador_formula);
                        $("#lblFormula_2").text(data[0].indicador_formula);
                        $("#indicador_formula").val(data[0].indicador_formula);
                        $('#tabInfoEsp').removeClass('hide');
                        $('#tabInfoPro').removeClass('hide');
                        valorFormula = '';
                        formula = data[0].indicador_formula;
                        document.getElementById("rango_aceptacion_id").options.length=0;
                        document.getElementById("rango_aceptacion_id").options[0]=new Option("Selecciona...", "");
                        
                        document.getElementById("rango_aceptacion_id_1").options.length=0;
                        document.getElementById("rango_aceptacion_id_1").options[0]=new Option("Selecciona...", "");
                        
                        document.getElementById("rango_aceptacion_id_2").options.length=0;
                        document.getElementById("rango_aceptacion_id_2").options[0]=new Option("Selecciona...", "");
                        
                        document.getElementById("componentes_formula").options.length=0;
                        document.getElementById("componentes_formula").options[0]=new Option("Selecciona...", "");
                        
                        var tblNivel = document.getElementById('tblNivel');
                        var objRow1 = tblNivel.insertRow(tblNivel.rows.length);
                        var objCell1 = objRow1.insertCell(0);
                        var objCell2 = objRow1.insertCell(1);
                        var objCell3 = objRow1.insertCell(1);
                        var objCell4 = objRow1.insertCell(1);
                        var objInputLabel0Box = document.createElement("label");
                        var objInputLabel1Box = document.createElement("label");
                        var objInputLabel2Box = document.createElement("label");
                        var objInputLabel3Box = document.createElement("label");
                        objCell1.appendChild(objInputLabel0Box);
                        objCell2.appendChild(objInputLabel1Box);
                        objCell3.appendChild(objInputLabel2Box);
                        objCell4.appendChild(objInputLabel3Box);
                        objInputLabel3Box.innerHTML ='Categoría';
                        objInputLabel2Box.innerHTML ='Nivel';
                        objInputLabel1Box.innerHTML ='Desde - Hasta';
                        
                        for(var f = 0; f < data[0].rango_aceptacion.length; f++){
                            var objTable = document.getElementById('tblRangos');
                            var objRow = objTable.insertRow(objTable.rows.length);
                            var objCell1 = objRow.insertCell(0);
                            var objCell2 = objRow.insertCell(1);
                            var objCell3 = objRow.insertCell(1);
                            var objInputLabel0Box = document.createElement("label");
                            var objInputLabel1Box = document.createElement("label");
                            var objInputLabel2Box = document.createElement("label");
                            objCell1.appendChild(objInputLabel0Box);
                            objCell2.appendChild(objInputLabel1Box);
                            objCell3.appendChild(objInputLabel2Box);
                            objInputLabel2Box.innerHTML = data[0].rango_aceptacion[f].rango_aceptacion_minimo + '  -  ' + data[0].rango_aceptacion[f].rango_aceptacion_maximo;
                            objInputLabel1Box.innerHTML = data[0].rango_aceptacion[f].categoria.categoria_nombre;
                            objInputLabel0Box.innerHTML = '';
                            document.getElementById("rango_aceptacion_id").options[document.getElementById("rango_aceptacion_id").options.length]=new Option(data[0].rango_aceptacion[f].categoria.categoria_nombre +" ~ "+data[0].rango_aceptacion[f].rango_aceptacion_minimo +" - "+data[0].rango_aceptacion[f].rango_aceptacion_maximo, data[0].rango_aceptacion[f].rango_aceptacion_id);
                            document.getElementById("rango_aceptacion_id_1").options[document.getElementById("rango_aceptacion_id_1").options.length]=new Option(data[0].rango_aceptacion[f].categoria.categoria_nombre +" ~ "+data[0].rango_aceptacion[f].rango_aceptacion_minimo +" - "+data[0].rango_aceptacion[f].rango_aceptacion_maximo, data[0].rango_aceptacion[f].rango_aceptacion_id);
                            document.getElementById("rango_aceptacion_id_2").options[document.getElementById("rango_aceptacion_id_2").options.length]=new Option(data[0].rango_aceptacion[f].categoria.categoria_nombre +" ~ "+data[0].rango_aceptacion[f].rango_aceptacion_minimo +" - "+data[0].rango_aceptacion[f].rango_aceptacion_maximo, data[0].rango_aceptacion[f].rango_aceptacion_id);
                          
                            for(var g = 0; g < data[0].rango_aceptacion[f].nivel_alerta.length; g++){
                                var tblNivel = document.getElementById('tblNivel');
                                
                                var objRow1 = tblNivel.insertRow(tblNivel.rows.length);
                                
                                var objCell11 = objRow1.insertCell(0);
                                var objCell12 = objRow1.insertCell(1);
                                var objCell13 = objRow1.insertCell(1);
                                var objCell14 = objRow1.insertCell(1);
                                
                                var objInputLabel10Box = document.createElement("label");
                                var objInputLabel11Box = document.createElement("label");
                                var objInputLabel12Box = document.createElement("label");
                                var objInputLabel13Box = document.createElement("label");
                                
                                objCell11.appendChild(objInputLabel10Box);
                                objCell12.appendChild(objInputLabel11Box);
                                objCell13.appendChild(objInputLabel12Box);
                                objCell14.appendChild(objInputLabel13Box);
                                
                                objInputLabel13Box.innerHTML = data[0].rango_aceptacion[f].categoria.categoria_nombre;
                                objInputLabel12Box.innerHTML = "<span style='background-color:"+data[0].rango_aceptacion[f].nivel_alerta[g].tipo_nivel_alerta.tipo_nivel_alerta_color+";color:"+data[0].rango_aceptacion[f].nivel_alerta[g].tipo_nivel_alerta.tipo_nivel_alerta_color+"' >...</span> &nbsp;&nbsp;"+data[0].rango_aceptacion[f].nivel_alerta[g].tipo_nivel_alerta.tipo_nivel_alerta_nombre;
                                objInputLabel11Box.innerHTML = data[0].rango_aceptacion[f].nivel_alerta[g].nivel_alerta_desde + ' - ' + data[0].rango_aceptacion[f].nivel_alerta[g].nivel_alerta_hasta;
                                objInputLabel10Box.innerHTML = '';
                                
                            }
                        }
                        
                        var str = (data[0].indicador_formula).trim();
                        var res = str.split('  ');
                        
                        for(var f = 0; f < res.length; f++){
                            document.getElementById("componentes_formula").options[document.getElementById("componentes_formula").options.length]=new Option(res[f],res[f]);
                        }
                    }
                },
            });
            setTimeout($.unblockUI, 1000);
        }
    });
       
    
    // INFORMACION ESPECIFICA INDICADOR
    
    $('#btn_guardar_rangos').click(function (event) {
        toTop();
        var bValid = true;
        var indicador_id = $("#indicador_id").val();
        
        bValid = bValid && validarCampoLleno(indicador_id, 'Indicador');
        var tblRangos = document.getElementById("tblRangos");
        if(tblRangos.rows.length <= 0){
            $('#mensajes').addClass('alert alert-danger');
            $('#mensajes').removeClass('hide');
            var tips = $("#textoMensaje");
            tips.text("Error ! " + "No se han añadido rangos de aceptación.");
        }else{
            if(totR_Rangos > 0){
                var rangosNuevos = 0;
                for (var i = 0; i < tblRangos.rows.length; i++)
                {
                    var cellsOfRow = tblRangos.rows[i].getElementsByTagName('td');
                    if(cellsOfRow[0].textContent != ''){
                        var registros= [];
                        rangosNuevos = rangosNuevos +","+ cellsOfRow[0].textContent +" - "+ cellsOfRow[1].innerText; 
                    }
                }    
                $("#lstRangosAceptacion").val(rangosNuevos);
                if (bValid) {
                    $('#mensajes').removeClass('alert alert-danger');
                    $('#mensajes').addClass('hide');
                    $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                    document.formIndicador.action= " {{url('/guardarRangosAceptacion')}} ";
                    document.formIndicador.submit();
                    setTimeout($.unblockUI, 3000);
                }
            }else{
                $('#mensajes').addClass('alert alert-danger');
                $('#mensajes').removeClass('hide');
                var tips = $("#textoMensaje");
                tips.text("Error ! " + "No se han añadido nuevos registros de rangos de aceptación.");
            }
            
        }
        
    });

    $('#btn_guardar_nivelAlerta').click(function (event) {
        toTop();
        var bValid = true;
        var tblNivel = document.getElementById("tblNivel");
        if(tblNivel.rows.length <= 1){
            $('#mensajes').addClass('alert alert-danger');
            $('#mensajes').removeClass('hide');
            var tips = $("#textoMensaje");
            tips.text("Error ! " + "No se han añadido niveles de alerta.");
        }else{
            if(totR_Nivel > 0){
                var nivelesNuevos = 0;
                for (var i = 1; i < tblNivel.rows.length; i++)
                {
                    var cellsOfRow = tblNivel.rows[i].getElementsByTagName('td');
                    if(cellsOfRow[0].textContent != ''){
                        var registros= [];
                        nivelesNuevos = nivelesNuevos +","+ cellsOfRow[0].textContent +" - "+ cellsOfRow[3].innerText; 
                    }
                }
                $("#lstNivelesAlerta").val(nivelesNuevos);
                if (bValid) {
                    $('#mensajes').removeClass('alert alert-danger');
                    $('#mensajes').addClass('hide');
                    $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                    document.formIndicador.action= " {{url('/guardarNivelesAlerta')}} ";
                    document.formIndicador.submit();
                    setTimeout($.unblockUI, 3000);
                }
               
            }else{
                $('#mensajes').addClass('alert alert-danger');
                $('#mensajes').removeClass('hide');
                var tips = $("#textoMensaje");
                tips.text("Error ! " + "No se han añadido nuevos registros de niveles de alerta.");
            }
        }
        
      
    });


    // INFORMACION PROSPECTIVA INDICADOR
    
    $("#rango_aceptacion_id").change(function(){
        var rango_aceptacion_id = $("#rango_aceptacion_id option:selected").val();
        $.ajax({
        url: '{{ url('/buscarLstNivelAlertaxRango') }}',
                type: 'post',
                "_token": "{{ csrf_token() }}",
                async: true,
                data: {'rango_aceptacion_id': rango_aceptacion_id},
                beforeSend: function () {
                    $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toTop();
                    var gnrMensaje = $("#gnrMensaje");
                    if (jqXHR.status == '401') {
                        gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                        $('#gnrError').modal('show');
                    }
                },
                success: function (data) {
                    if (data == '') {
                        toTop();
                        $('#mensajes').removeClass('alert alert-danger hide');
                        $('#mensajes').addClass('alert alert-warning');
                        var tips = $("#textoMensaje");
                        tips.text("Advertencia !  No se han encontrado niveles de Alerta. ");
                        $('#nivel_alerta_id').selectpicker('destroy');
                        $('#nivel_alerta_id').html('');
                    } else{
                        $('#nivel_alerta_id').selectpicker('destroy');
                        $('#nivel_alerta_id').html('');
                        for(var i=0; i < data.length ; i++){
                             $('#nivel_alerta_id')
                                .append($("<option></option>")
                                .attr("value",data[i].nivel_alerta_id)
                                .attr("data-content","<span class=label label-success style='background-color:"+data[i].tipo_nivel_alerta.tipo_nivel_alerta_color+";color: "+data[i].tipo_nivel_alerta.tipo_nivel_alerta_color+"' >...</span> &nbsp;&nbsp;"+data[i].tipo_nivel_alerta.tipo_nivel_alerta_nombre)
                                .text(data[i].tipo_nivel_alerta.tipo_nivel_alerta_color + "-" +data[i].tipo_nivel_alerta.tipo_nivel_alerta_nombre+ "-"+data[i].nivel_alerta_id.nivel_alerta_descripcion+"-"+data[i].nivel_alerta_id.nivel_alerta_interpretacion+"-"+data[i].nivel_alerta_id.nivel_alerta_criterio));
                        } 
                        $('#nivel_alerta_id').selectpicker('refresh');
                    }
                },
                complete: function () {
                setTimeout($.unblockUI, 1000);
                },
        });
    });
    
    $("#rango_aceptacion_id_2").change(function(){
        var rango_aceptacion_id = $("#rango_aceptacion_id_2 option:selected").val();
        $('#nivel_alerta_id_2').html('');
        $.ajax({
        url: '{{ url('/buscarLstNivelAlertaxRango') }}',
                type: 'post',
                "_token": "{{ csrf_token() }}",
                async: true,
                data: {'rango_aceptacion_id': rango_aceptacion_id},
                beforeSend: function () {
                    $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toTop();
                    var gnrMensaje = $("#gnrMensaje");
                    if (jqXHR.status == '401') {
                        gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                        $('#gnrError').modal('show');
                    }
                },
                success: function (data) {
                    if (data == '') {
                        toTop();
                        $('#mensajes').removeClass('alert alert-danger hide');
                        $('#mensajes').addClass('alert alert-warning');
                        var tips = $("#textoMensaje");
                        tips.text("Advertencia !  No se han encontrado niveles de Alerta. ");
                        $('#nivel_alerta_id_2').selectpicker('destroy');
                        $('#nivel_alerta_id_2').html('');
                    } else{
                        $('#nivel_alerta_id').selectpicker('destroy');
                        $('#nivel_alerta_id').html('');
                        for(var i=0; i < data.length ; i++){
                            $('#nivel_alerta_id_2')
                                .append($("<option></option>")
                                .attr("value",data[i].nivel_alerta_id)
                                .attr("data-content","<span class=label label-success style='background-color:"+data[i].tipo_nivel_alerta.tipo_nivel_alerta_color+";color: "+data[i].tipo_nivel_alerta.tipo_nivel_alerta_color+"' >...</span> &nbsp;&nbsp;"+data[i].tipo_nivel_alerta.tipo_nivel_alerta_nombre)
                                .text(data[i].tipo_nivel_alerta.tipo_nivel_alerta_color + "-" +data[i].tipo_nivel_alerta.tipo_nivel_alerta_nombre));
                        } 
                        $('#nivel_alerta_id_2').selectpicker('refresh');
                    }
                },
                complete: function () {
                setTimeout($.unblockUI, 1000);
                },
        });
    });
    
    $('#btn_actualizar_nivel_alerta').click(function (event) {
        toTop();
        var bValid = true;
        var tblGuia = document.getElementById("tblGuia");
        if(tblGuia.rows.length <= 0){
            $('#mensajes').addClass('alert alert-danger');
            $('#mensajes').removeClass('hide');
            var tips = $("#textoMensaje");
            tips.text("Error ! " + "No se ha(n) añadido guía(s) del indicador.");
        }else{
            if(totR_Guia > 0){
                var guiaNuevos = 0;
                for (var i = 0; i < tblGuia.rows.length; i++)
                {
                    var cellsOfRow = tblGuia.rows[i].getElementsByTagName('td');
                    if(cellsOfRow[0].textContent != ''){
                        var registros= [];
                        guiaNuevos = guiaNuevos +","+ cellsOfRow[0].textContent +" - "+ cellsOfRow[1].innerText+" - "+ cellsOfRow[4].innerText+" - "+ cellsOfRow[5].innerText; 
                    }
                }
                $("#lstNivelesAlertaAct").val(guiaNuevos);
                if (bValid) {
                    $('#mensajes').removeClass('alert alert-danger');
                    $('#mensajes').addClass('hide');
                    $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                     document.formIndicador.action= " {{url('/actualizarNivelesAlerta')}} ";
                        document.formIndicador.submit();
                    setTimeout($.unblockUI, 3000);
                }
               
            }else{
                $('#mensajes').addClass('alert alert-danger');
                $('#mensajes').removeClass('hide');
                var tips = $("#textoMensaje");
                tips.text("Error ! " + "No se han añadido nuevos registros de guias del indicador.");
            }
        }
        
       
    });

    $("#nivel_alerta_id").change(function(){
        var nivel_alerta_id = $("#nivel_alerta_id option:selected").val();
        var nivel_alerta_text=$("#nivel_alerta_id option:selected").text();
        
        var str1 = nivel_alerta_text;
        var res1 = str1.split("-");
        var valor1 = res1[2];
        var valor2 = res1[3];
        var valor3 = res1[4];
        
        if(valor1 === undefined && valor2 === undefined && valor3 === undefined){
            $("#nivel_alerta_descripcion").val(valor1);
            $("#nivel_alerta_interpretacion").val(valor2);
        }

    });
    
    $('#btn_guardar_cambio').click(function (event) {
        toTop();
        var bValid = true;
        var tblCambio = document.getElementById("tblCambio");
        if(tblCambio.rows.length <= 0){
            $('#mensajes').addClass('alert alert-danger');
            $('#mensajes').removeClass('hide');
            var tips = $("#textoMensaje");
            tips.text("Error ! " + "No se ha(n) añadido cambio(s) y mejora(s).");
        }else{
            if(totR_Cambio > 0){
                var cambioNuevos = 0;
                for (var i = 0; i < tblCambio.rows.length; i++)
                {
                    var cellsOfRow = tblCambio.rows[i].getElementsByTagName('td');
                    if(cellsOfRow[0].textContent != ''){
                        var registros= [];
                        cambioNuevos = cambioNuevos +","+ cellsOfRow[0].textContent +" - "+ cellsOfRow[1].innerText+" - "+ cellsOfRow[4].innerText+" - "+ cellsOfRow[5].innerText; 
                    }
                }
                $("#lstCambioMejora").val(cambioNuevos);
                if (bValid) {
                    $('#mensajes').removeClass('alert alert-danger');
                    $('#mensajes').addClass('hide');
                    $.blockUI({message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'});
                    document.formIndicador.action= " {{url('/guardarCambioMejora')}} ";
                    document.formIndicador.submit();
                    setTimeout($.unblockUI, 3000);
                }
               
            }else{
                $('#mensajes').addClass('alert alert-danger');
                $('#mensajes').removeClass('hide');
                var tips = $("#textoMensaje");
                tips.text("Error ! " + "No se han añadido nuevos cambios y mejoras.");
            }
        }
        
       
    });

    
});
</script>
@endsection