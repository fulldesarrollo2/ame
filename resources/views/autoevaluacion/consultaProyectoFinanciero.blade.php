@extends('layouts\app')

@section('content')
<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2><span class="text-navy"><i class="fa fa-chevron-circle-right"></i> Consultar Proyecto Financiero</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                <div class="col-lg-12">
                                    @if (session()->has('mensajeexito')) 
                                    <div class="alert alert-success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <ul>
                                            <li>{{ session()->pull('mensajeexito') }}</li>			            
                                        </ul>
                                    </div>
                                    @endif

                                    @if (session()->has('mensajeerror')) 
                                    <div class="alert alert-danger">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <ul>
                                            <li>{{ session()->pull('mensajeerror') }}</li>			            
                                        </ul>
                                    </div>
                                    @endif


                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif


                                    <div id="mensajes" name="mensajes" class="hide">
                                        <label id="textoMensaje" name="textoMensaje"></label>
                                    </div>

                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <form class="form-horizontal" method="POST" id="formValiInconInformacion" enctype="multipart/form-data">
                                                <input type="hidden" name="division_politica_id" class="division_politica_id" id="division_politica_id">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
												<div class="form-group">

													<label class="col-lg-2 control-label">Entidad</label>
													<div class="col-lg-4 radio" id="tipo_entidad">
														<label> <input type="radio" checked="checked" name="estado_id"
															class="tipo-Entidad" value="1" />GADM
														</label> <label> <input type="radio" name="estado_id"
															class="tipo-Entidad" value="2" />Empresa De Servicio Publico
														</label>
													</div>
												</div>
												<div class="form-group">
												<div class="col-lg-6">
                                                    <label class="col-sm-2 control-label">Seleccione</label>
                                                    <div class="col-sm-10">
                                                        <div class="panel panel-default">
                                                            <div class="panel-body" >
                                                                <div id="arbolDivisionPolitica" class="step-content" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-6">
                                                	<div id="entidades">
                                                	
                                                	</div>
                                                	
                                                	<div id="container-periodo">
                                                	
                                                	</div>
                                                </div>
                                                </div>
                                               <input type="hidden" name="id" class="id_entidad">
                                               <input type="hidden" name="periodo" class="periodoselect">
                                               	<div id="tabla-proyeccion">
                                               	
                                               	</div>
                                                        
											</form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

<script type="text/javascript">
    
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
var lstDivPoljs = <?php echo json_encode($objDivisionPolitica); ?>;
var arbolDivisionPolitica = dataArbolDinamico(lstDivPoljs,"division_politica_nombre","division_politica_id","division_politica","true","division_politica_codigo","division_politica_nombre");
$('#arbolDivisionPolitica').treeview({
    levels: 1,
    color: "#555",
    showBorder: false,
    data: arbolDivisionPolitica,
    multiSelect: $('#chk-select-multi').is(':checked'),
    onNodeSelected: function (event, node) {
        $('#division_politica_nom').val(node.text);
        $('#division_politica_id').val(node.tags);
        consultaEntidad();
    },
    onNodeUnselected: function (event, node) {
        $('#nomCtaSeleccionada').val("");
    }
});

function imprimir()
{
    window.print( );
    window.close( );

} 

function consultaEntidad(){
	var division = $('#division_politica_id').val();	
	var tipo=$('#tipo_entidad input[type="radio"]:checked').val();
	if(division!='' && tipo!=''){
		 $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
	$.ajax({
        url: '{{ url('/consultarEntidadDivisionPolitica') }}',
        type: 'post',
        "_token": "{{ csrf_token() }}",
        async: true,
        data: {'id': division,
             'tipo':tipo},
        error : function(jqXHR, textStatus, errorThrown) {
                    var gnrMensaje =$("#gnrMensaje");
                    if(jqXHR.status == '401'){
                        gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar."); 
                        $('#gnrError').modal('show');
                    }     
               },
        success: function(data){
            var tperiodos = '<option value="0">Seleccione una entidad</option>';

        	for(var i in data){
                                    
            tperiodos=tperiodos +'<option value='+data[i].entidad.entidad_id+'>'+data[i].entidad.entidad_nombre;                          
            tperiodos = tperiodos +'</option>' ;
                 
            }
        	var combo='<label class="col-sm-2 control-label">Entidades</label>'+
            '<div class="col-sm-10"><div class="panel panel-default"><div class="panel-body">'+                                                 
            '<select onchange="mostrarfecha();" multiple size="3" class="form-control" id="combo-entidades"> '  + 
        	tperiodos  +
		    '</select></div></div></div>';
        	document.getElementById("entidades").innerHTML =combo;
            
            setTimeout($.unblockUI, 1000);            
        }     
        }); 
    }
	
}

function verperiodos(data){
	var tperiodos = '<option value="0">Seleccione un periodo</option>';

	for(var i in data){
                            
    tperiodos=tperiodos +'<option value='+data[i].periodo_anio+'>'+data[i].periodo_anio;                          
    tperiodos = tperiodos +'</option>' ;
         
    }
	return tperiodos; 
}

function mostrarfecha(){
	var identidad=document.getElementById("combo-entidades").value;

            if(identidad!=""){
            	$.ajax({
                    url: '{{ url('/consultaPeriodo') }}',
                        type: 'post',
                        async: 'true',
                        beforeSend: function(data){   
                            $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                        },
                        success: function(data) {
            
                        variables= verperiodos(data);
                       	var combo='<label class="col-sm-2 control-label">Periodos</label>'+
                       	    '<div class="col-sm-10"><div class="panel panel-default"><div class="panel-body">'+  
                           	'<select onchange="mostrarproyectos();" multiple size="2" class="form-control" id="periodos"> '  + 
                		    variables  +
                		    '</select></div></div></div>';

                        document.getElementById("container-periodo").innerHTML = combo;
                        setTimeout($.unblockUI, 1000); 
                		$("#periodo").show();
                        }
            	});
             } 
            else{
            	document.getElementById("container-periodo").innerHTML = "";
				$("#periodo").hide();
            }

}
function botones(){
	var botones='';
	botones=botones+'<div class="col-xs-12 text-center">'+
    '<button  id="btn_imprimir" style="margin-right: 1%;" class="btn btn-primary" type="button" onclick="imprimir();" >'+
    '<i class="fa fa-print"></i> Imprimir</button><button onclick="exportexcel();" style="margin-right: 1%;" type="button" class="btn btn-primary" id="btn_exportarExcel" >'+
   	'<i class="fa fa-file-excel-o"></i> Exportar a Excel</button><button type="button" onclick="exportpdf();" class="btn btn-primary" id="btn_exportarPdf">'+
  	'<i class="fa fa-file-pdf-o"></i> Exportar a pdf </button></div>';

	return botones;
}

function mostrarproyectos(){
var entidad=document.getElementById("combo-entidades").value;var periodo=document.getElementById("periodos").value;

	if(entidad!="" && periodo!="" && entidad>0 && periodo>0){
		 $(".id_entidad").val(entidad);$(".periodoselect").val(periodo);
    	$.ajax({
            url: '{{ url('/consultarProyectoFinanciero') }}',
                type: 'post',
                async: 'true',
                "_token": "{{ csrf_token() }}",
                async: true,
                data: {'id': entidad,
                     'periodo':periodo},
                beforeSend: function(data){   
                    $.blockUI({ message: '<h4><img src="http://www.ontariodirectors.ca/CODE-TLF/images/icons/progress/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                },
                success: function(data) {
                    console.log(data);
                if(data.length>0){
    			index=0;index2=0;index3=0;mayor=0;conteo=0;
    			for(var i in data){
    				for(var j in data[i].componenteproyecto){
	    				for(var k in data[i].componenteproyecto[j].actividadproyecto){
	    				conteo=data[i].componenteproyecto[j].actividadproyecto[k].ejecucion.length;

		    				if(conteo>mayor){
		    					mayor=conteo;index=i;index2=j;index3=k;
		    				}
	    				}
    			}
    			}
				console.log('El numero mayor es :'+mayor);
    			
    			var variables=proyecto(data,mayor);
    			var btnexport=botones();
    			var tabla='<table id="proyecto-consulta" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">' +
    		    ' <thead>' +
    		    '<tr>'+
    		    '<th>Proyecto</th>' +
    		    '<th>Componentes</th>' +
    		    '<th>Actividades </th>' +
    		    '<th>Fuente Finan </th>' ;
    		   	for(var i in data[index].componenteproyecto[index2].actividadproyecto[index3].ejecucion){
					tabla=tabla+'<th>'+data[index].componenteproyecto[index2].actividadproyecto[index3].ejecucion[i].periodo.periodo_anio+'</th>';
            	}
    		    tabla=tabla+'</tr>'+
    		    ' </thead>' +
    		    '    <tbody> '  + 
    		    variables+
    		    '    </tbody> ' +
    		    '</table>'+
    		    btnexport;
    		    document.getElementById("tabla-proyeccion").innerHTML = tabla;

    		       /* $(document).ready(function() {
    		            $('#proyecto-consulta').DataTable( {
    		                dom: 'Bfrtip',
    		                buttons: [
    		    		                
    		                          'print',
    		                          'excelHtml5',
    		                          'pdfHtml5',
    		                      ]
    		            } );
    		        } );*/

    		      //begin export buttons
    		      $(document).ready(function() {
    		        codeListTable = $("#proyecto-consulta").DataTable();
    		       /* new $.fn.dataTable.Buttons( codeListTable, {

    		            buttons: [
    		                {
    		                extend:    'excel',
    		                text:      '<img src="../imagenes/icoexport/Excel.png" /> Excel',
    		                titleAttr: 'Excel',
    		                className: 'btn btn-sucess',
    		                
    		                },
    		                {
    		                extend:    'pdf',
    		                text:      '<img src="../imagenes/icoexport/pdf.png" /> PDF',
    		                titleAttr: 'PDF',
    		                className: 'btn btn-sucess',
    		                
    		                },               
    		                {
    		                extend:    'print',
    		                text:      '<img src="../imagenes/icoexport/print.png" /> Print',
    		                titleAttr: 'Print',
    		                className: 'btn btn-sucess',
    		                
    		                },  
    		            ]
    		        } );
    		        codeListTable.buttons().container().appendTo('#botones_export');*/
    		      } );
                }else{document.getElementById("tabla-proyeccion").innerHTML = "";}    		 
               /* variables= verperiodos(data);
               	var combo='<label class="col-sm-2 control-label">Periodos</label>'+
               	    '<div class="col-sm-10"><div class="panel panel-default"><div class="panel-body">'+  
                   	'<select onchange="aplicacionespecifica();" multiple size="2" class="form-control" id="periodos"> '  + 
        		    variables  +
        		    '</select></div></div></div>';

                document.getElementById("container-periodo").innerHTML = combo;*/
                setTimeout($.unblockUI, 1000); 
                }
               
    	});
     } 
    
}

function proyecto(data,mayor){
	var tregistros = '';
	for(var i in data){
		for(var j in data[i].componenteproyecto){
			for(var k in data[i].componenteproyecto[j].actividadproyecto){
				tregistros =tregistros + '<tr>';
				tregistros =tregistros +'  <td>'+ data[i].proyecto_financiero_nombre  +' </td>' ;
				tregistros =tregistros +'  <td>'+ data[i].componenteproyecto[j].componente_proyecto_descripcion  +' </td>' ;
				tregistros =tregistros +'  <td>'+ data[i].componenteproyecto[j].actividadproyecto[k].actividad_proyecto_descripcion +' </td>' ;
				tregistros =tregistros +'  <td>'+ data[i].componenteproyecto[j].actividadproyecto[k].actividad_proyecto_fuente_financiamiento +' </td>' ;
				for(var l in data[i].componenteproyecto[j].actividadproyecto[k].ejecucion){
					tregistros =tregistros + '  <td>'+ data[i].componenteproyecto[j].actividadproyecto[k].ejecucion[l].ejecucion_valor+' </td>' ;
				}
				if(mayor>data[i].componenteproyecto[j].actividadproyecto[k].ejecucion.length){
					for(m=0;m<(mayor-data[i].componenteproyecto[j].actividadproyecto[k].ejecucion.length);m++){
						tregistros =tregistros +'  <td> </td>' ;	
					}
				}
				tregistros = tregistros +'</tr>' ;
			}
	}
		
	}
	console.log(tregistros);
	return tregistros; 
}
  function exportpdf(){
                    $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                    setTimeout($.unblockUI, 1000);                      
                    $('#formValiInconInformacion').attr('action', '{{ url('/imprimirPdfProyectoFinanciero') }}');
                    $("#formValiInconInformacion").submit();    
  }
    
  function exportexcel(){
             $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
             setTimeout($.unblockUI, 1000);
             $('#formValiInconInformacion').attr('action', '{{ url('/imprimirExcelProyectoFinanciero') }}');
             $("#formValiInconInformacion").submit();
  }
    
</script>

@endsection