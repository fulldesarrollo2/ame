@extends('layouts.app')
@section('content')

<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2><span class="text-navy"><i class="fa fa-chevron-circle-right"></i> Analisis Pre-Configurado</span></h2>
                    </div>
                </div>
            </div>
        </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox float-e-margins">
          <div class="ibox-content text-left p-md">
            <div class="wrapper wrapper-content animated fadeInRight">
              <div class="row">
                  <div class="col-lg-12">
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                   @if(Session::has('message'))
                    <div class="alert alert-success" >
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <p> {{Session::get('message')}} </p>
                    </div>
                    @endif
                    @if (notify()->ready())
                    <div class="alert alert-{{ notify()->type() }}">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <label>{{ notify()->message() }}</label>
                    </div>
                    @endif
                      <div id="mensajes" name="mensajes" class="hide">
                          <label id="textoMensaje" name="textoMensaje"></label>
                      </div>
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                             <form class="form-horizontal" id="formImprimir" method="POST" name="formValiInconInformacion" action="{{url('/imprimirExcelAnalisis')}}" role="form">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  <input type="hidden" id="cuenta_id_editar" name="cuenta_id_editar"/>

                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-2">Tipo</label>
                                        <div class="col-sm-10">
                                            @foreach($lstTipoEntidad as $lstTipoEntidad)
                                            <div class="radio"><label> <input type="radio" class="check_entidad" value="{{ $lstTipoEntidad->tipo_entidad_id}}" id="tipo_entidad_id" required name="tipo_entidad_id">{{ $lstTipoEntidad->nombre}}</label></div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-12">División Política</label>
                                        <div class="col-sm-12">
                                            <div class="panel panel-default">
                                                <div class="panel-body" id="divCuentas">
                                                    <div id="arbolDivisionPolitica" class="step-content" >
                                                    </div>
                                                </div>
                                            </div>
                                           <input type="hidden" name="division_politica_id" class="division_politica_id" id="division_politica_id">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="col-sm-4 control-label">Entidades</label>
                                        <div class="col-sm-8">
                                            <select class="form-control m-b" name="entidad_id" id="entidad_id" >
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                  <div class="col-sm-12">
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                             <label class="col-sm-12">Fuente de información</label>
                                             <div class="col-sm-12">
                                                 <select class="form-control m-b col-md-6" name="tipo_fuente_id" id="tipo_fuente_id" >
                                                     <option value="">Seleccionar...</option>
                                                    @foreach($lstTipoFuente as $lstTipoFuente)
                                                    <option value="{{ $lstTipoFuente->tipo_fuente_id}}"  name="">{{ $lstTipoFuente->tipo_fuente_descripcion_alias}}</option>
                                                   @endforeach
                                                 </select>
                                             </div>
                                         </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                             <label class="col-sm-12">Selección de Periodo Desde</label>
                                             <div class="col-sm-12">
                                                 <select class="form-control m-b col-md-6" name="periodo_desde_id" id="periodo_desde_id" >
                                                     <option value="">Seleccionar...</option>
                                                    @foreach($lstPeriodo as $lstPeriodo)
                                                    <option value="{{ $lstPeriodo->periodo_id}}"  name="">{{ $lstPeriodo->periodo_anio}}</option>
                                                   @endforeach
                                                 </select>
                                             </div>
                                         </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                             <label class="col-sm-12">Selección de Periodo Hasta</label>
                                             <div class="col-sm-12">
                                                 <select class="form-control m-b col-md-6" name="periodo_hasta_id" id="periodo_hasta_id" >
                                                     <option value="">Seleccionar...</option>
                                                     @foreach($lstPeriodoHasta as $lstPeriodo)
                                                     <option value="{{ $lstPeriodo->periodo_id}}"  name="" id="">{{ $lstPeriodo->periodo_anio}}</option>
                                                   @endforeach
                                                 </select>
                                             </div>
                                         </div>
                                      </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                             <label class="col-sm-12">Análisis Pre-Configurado</label>
                                             <div class="col-sm-12">
                                                <select class="form-control m-b col-md-6" name="analisis_pre_config" id="analisis_pre_config" multiple>
                                                    <option value="">Seleccionar...</option>
                                                    @foreach($lstAnalisisPreConf as $lstAnalisisPreConf)
                                                    <option value="{{ $lstAnalisisPreConf->analisis_pre_configurado_id}}"  name="analisis_pre_configurado_id">{{ $lstAnalisisPreConf->analisis_pre_configurado_nombre}}</option>
                                                   @endforeach
                                                </select>
                                            </div>
                                        </div>
                                     </div>
                                    <div class="form-group text-center">
                                      <div class="col-sm-12 text-right">
                                          <button class="btn btn-white" type="button" id="btn_limpiar">Cancelar</button>
                                          <button class="btn btn-primary" onclick="cargarListaCuentas()" type="button" id="btn_guardar">Procesar</button>
                                        </div>
                                    </div>
                                  
                                    <div class="form-group">
                                        <label class="col-sm-12">Cuentas</label>
                                        <div class="col-sm-12">
                                            <div class="panel panel-default">
                                                <div class="panel-body" id="divCuentas">
                                                    <div id="arbolCuentas" class="step-content" >
                                                    </div>
                                                </div>
                                            </div>
                                           <input type="hidden" name="division_politica_id" class="division_politica_id" id="division_politica_id">
                                        </div>
                                    </div>
                                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                                  <script src="https://code.highcharts.com/highcharts.js"></script>
                                    <script src="https://code.highcharts.com/modules/exporting.js"></script>
                                    <script src="http://highcharts.github.io/export-csv/export-csv.js"></script>

                                    <script src="//a----.github.io/highcharts-export-clientside/bower_components/highcharts-export-clientside/highcharts-export-clientside.js"></script>
                               
                                <div id="divGraficos"></div>
                                
                                    <input type="hidden" id="dataExcel" name="dataExcel"/>
                                <div class="modal-footer col-sm-12">
                                    <a  id="btn_imprimir" class="btn btn-primary" type="submit">
                                        <i class="fa fa-print"></i> Imprimir
                                    </a>
                                    <button type="button" class="btn btn-primary" id="btn_exportarExcel" data-type="application/vnd.ms-excel" disabled >
                                       <i class="fa fa-file-excel-o"></i> Exportar a Excel

                                     </button>
                                    <button type="button" class="btn btn-primary" id="btn_exportarPdf" disabled>
                                      <i class="fa fa-file-pdf-o"></i> Exportar a pdf
                                     </button>  
                                 </div>
                                
                          </form>
                        </div>
                     </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')

<script>
    
    var lstGraficos = [];
    var chart;
    var lstDivGraficos = [];
    
    /**
 * Create a global getSVG method that takes an array of charts as an argument*/
 
Highcharts.getSVG = function(charts) {
    var svgArr = [],
        top = 0,
        width = 0;

    $.each(charts, function(i, chart) {
        var svg = chart.getSVG();
        svg = svg.replace('<svg', '<g class="col-md-6" transform="translate(0,' + top + ')" ');
        svg = svg.replace('</svg>', '</g>');

        top += chart.chartHeight;
        width = Math.max(width, 500);

        svgArr.push(svg); 
    });

    return '<svg height="'+ top +'" width="' + width + '" version="1.1" xmlns="http://www.w3.org/2000/svg">' + svgArr.join('') + '</svg>';
};

/**
 * Create a global exportCharts method that takes an array of charts as an argument,
 * and exporting options as the second argument*/
 
Highcharts.exportCharts = function(charts, options) {
    var form
        svg = Highcharts.getSVG(charts);

    // merge the options
    options = Highcharts.merge(Highcharts.getOptions().exporting, options);

    // create the form
    form = Highcharts.createElement('form', {
        method: 'post',
        action: options.url
    }, {
        display: 'none'
    }, document.body);

    // add the values
    Highcharts.each(['filename', 'type', 'width', 'svg'], function(name) {
        Highcharts.createElement('input', {
            type: 'hidden',
            name: name,
            value: {
                filename: options.filename || 'chart',
                type: options.type,
                width: options.width,
                svg: svg
            }[name]
        }, null, form);
    });
   
    form.submit();

    form.parentNode.removeChild(form);
};
   
   function generarGrafico(data, periodos, container){

     container = eval(container);
    $("#divGraficos").append("<div style='height: 480px;width: 830px;'><canvas id='"+container+"' height='374'  width='740'></canvas><div id='"+container+"legend' class='chart-legend'></div></div>");  
   // $('"#'+container+'"').remove();    
    var dataset = [];
        data.forEach(function(obj){
            var randomColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
            var nuevo = {
                label: obj.label,
                data: obj.data,
                fillColor: randomColor,
                strokeColor: randomColor,
                pointColor: "rgba(26,179,148,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(26,179,148,1)",
                backgroundColor: randomColor
            }
            dataset.push(nuevo);
        });
    
        var lineData = {
                labels: periodos,
                datasets: dataset
                 
             };

         var lineOptions = {
             scaleShowGridLines: true,
             scaleGridLineColor: "rgba(0,0,0,.05)",
             scaleGridLineWidth: 1,
             bezierCurve: true,
             bezierCurveTension: 0.4,
             pointDot: true,
             pointDotRadius: 4,
             pointDotStrokeWidth: 1,
             pointHitDetectionRadius: 20,
             datasetStroke: true,
             datasetStrokeWidth: 2,
             datasetFill: true,
             responsive: true,
             legend: {
                display: true,
                position:'right',
                labels: {
                    display: true
                  }
            }
         };
         var ctx = document.getElementById(container).getContext("2d");
         var myNewChart = new Chart(ctx).Line(lineData, lineOptions);  
         
         document.getElementById(container+'legend').innerHTML = myNewChart.generateLegend();
         
         
    }
       /*   container = eval(container);
    $("#divGraficos").append("<canvas  class='col-md-6' style='height: 400px;width:450px;' id='"+container+"'></canvas>");  
        
    var barData = {
            labels: periodos,
            datasets: data
            };

             var lineOptions = {
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                bezierCurve: true,
                bezierCurveTension: 0.4,
                pointDot: true,
                pointDotRadius: 4,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                responsive: true,
            };


            var ctx = document.getElementById(container).getContext("2d");
    var myNewChart = new Chart(ctx).Line(barData, lineOptions);
      }*/
        
        
        
        /*console.log(container);
     chart = new Highcharts.Chart({
                chart: {
                    type: 'line',
                    renderTo: container
                },
        title: {
            text: data[0].analisis_pre_configurado,
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: periodos,
            title: {
                text: 'Periodos'
            }
        },
        yAxis: {
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
             valuePrefix: '$'
        },
        legend: {
            layout: 'vertical',
        
        },
        
        
        series:  data
    });
    lstDivGraficos.push(container);
    lstGraficos.push(chart);
     
       var dataCsv=[];  
      lstGraficos.forEach(function(obj, index) {
        dataCsv.push(obj.getCSV());
      });
  
    console.log(dataCsv);*/
    
    //}
      
  

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


   function imprimir()
    {
        console.log(dataExcel)
         $.ajax({
             url: '{{ url('/imprimirExcelAnalisis') }}',
             type: 'post',
             "_token": "{{ csrf_token() }}",
             async: true,
             data: {'dataExcel':dataExcel},
             beforeSend: function() {
                 $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
             },
             error: function (jqXHR, textStatus, errorThrown) {
                 var gnrMensaje = $("#gnrMensaje");
                 if (jqXHR.status == '401') {
                     gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                     $('#gnrError').modal('show');
                 }
                 toTop();
             },
             success:  function(data) {
                 console.log(data);
                  setTimeout($.unblockUI, 1000);  
             }
        });
        /*window.print( );
        window.close( );*/
    }
    
    /*************************************************************************************************************************************************************/
    
       $("#btn_imprimir").click(function(){
            window.print();
            //$("#formImprimir").submit();
       });
       
    $("#btn_exportarPdf").click(function(){
        bValid = true; 
        var entidad_id=$('#entidad_id').val();
        var tipo_fuente_id=$('#tipo_fuente_id').val();
        bValid = bValid && validarCampoLleno(entidad_id, " entidad");
        bValid = bValid && validarCampoLleno(tipo_fuente_id, " Fuente de Información"); 
        
        if(bValid){
            $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
            setTimeout($.unblockUI, 1000);  
            var options= {
                type:"application/pdf",
                filename: 'Análisis PreConfigurado'
            }
            Highcharts.exportCharts(lstGraficos,options); 
           
        }
    });
    
    /*************************************************************************************************************************************************************/
    
    $("#btn_exportarExcel").click(function(){
        var entidad_id=$('#entidad_id').val();
        var tipo_fuente_id=$('#tipo_fuente_id').val();
        var periodo_id=$('#periodo_id').val();
        
        
        console.log(lstGraficos);
        lstGraficos.forEach(function(obj){
           obj.exportChartLocal({ type: 'application/vnd.ms-excel' });
        });
           
        
        setTimeout($.unblockUI, 1000);
        bValid = true; 
        
       /* bValid = bValid && validarCampoLleno(entidad_id, " entidad");
        bValid = bValid && validarCampoLleno(periodo_id, " periodo");
        bValid = bValid && validarCampoLleno(tipo_fuente_id, " Fuente de Información"); */
       
        
           
       
    });
        
var lstDivPoljs = <?php echo json_encode($objDivisionPolitica); ?>;
console.log(lstDivPoljs);
var arbolDivisionPolitica = dataArbolDinamico(lstDivPoljs,"division_politica_nombre","division_politica_id","division_politica","true","division_politica_codigo","division_politica_nombre");
    $('#arbolDivisionPolitica').treeview({
        levels: 1,
        color: "#555",
        showBorder: false,
        data: arbolDivisionPolitica,
        multiSelect: $('#chk-select-multi').is(':checked'),
        onNodeSelected: function (event, node) {
            $('#checkable-output').prepend('<p>' + node.text + ' was checked</p>');
                 if(node.nodes && node.nodes.length>0){
                     $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var textoMensaje =$("#textoMensaje");
                    textoMensaje.text("Error! Solo se puede seleccionar cantones. "); 
                 }else{
                      $('#mensajes').addClass('hide');
                      $('#division_politica_nom').val(node.text);
                      $('#division_politica_id').val(node.tags);
                      cargarEntidades();
                 }
        },
        onNodeUnselected: function (event, node) {
            $('#nomCtaSeleccionada').val("");
        }
    });

    $("input[name=tipo_entidad_id]").click(function () {
        cargarEntidades();
    })


   function cargarEntidades(){
            var division_politica_id  =  $('#division_politica_id').val(),
            tipoEntidad = $('input[name=tipo_entidad_id]:checked').val();
            var bValid = true;
            $('#entidad_id').empty();
            $('#mensajes').removeClass('alert alert-danger');
            $('#mensajes').addClass('hide');
            var textoMensaje =$("#textoMensaje");
            textoMensaje.text("");  
            bValid = bValid && validarCampoLleno(tipoEntidad, 'Tipo Entidad');
            bValid = bValid && validarCampoLleno(division_politica_id, 'División Politíca');

            if(bValid){
                $.ajax({
                url: '{{ url('/consultaEntRelacionaDivPoTipo') }}',
                type: 'post',
                "_token": "{{ csrf_token() }}",
                async: true,
                data: {'division_politica_id':division_politica_id,'tipoEntidad':tipoEntidad},
                beforeSend: function() {
                    $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var gnrMensaje = $("#gnrMensaje");
                    if (jqXHR.status == '401') {
                        gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                        $('#gnrError').modal('show');
                    }
                    toTop();
                },
                success:  function(data) {
                    if (data == ''){
                       toTop();
                       $('#mensajes').addClass('alert alert-danger');
                       $('#mensajes').removeClass('hide');
                       var textoMensaje =$("#textoMensaje");
                       textoMensaje.text("No se han encontrados datos. ");  
                    }else{
                        $('#entidad_id').append('<option>Seleccione...</option>');
                        for(var i in data){
                            $('#entidad_id').append('<option id='+data[i].entidad_id+' value='+data[i].entidad_id+'>'+ data[i].entidad_nombre+' </option>');
                        }
                    }
                },
                complete: function() {
                    setTimeout($.unblockUI, 1000);
                },
            });

            }
            else{
                toTop();
            }
        }
  

   //Metodo para cargar la lista de las cuentas con los valores
    function cargarListaCuentas(){
        var textoMensaje =$("#textoMensaje");
        $('#mensajes').removeClass();
        textoMensaje.text("");  
        var entidad_id=$('#entidad_id').val();
        var tipo_fuente_id= 1;
        //$('#tipo_fuente_id').val();
        var periodo_desde_id=$('#periodo_desde_id').val(); 
        var periodo_hasta_id=$('#periodo_hasta_id').val(); 
        var analisis_pre_config_id=$('#analisis_pre_config').val(); 
         
        bValid = true; 
        bValid = bValid && validarCampoLleno(entidad_id, " Entidad");
        bValid = bValid && validarCampoLleno(tipo_fuente_id, " Fuente de Información");
        bValid = bValid && validarCampoLleno(periodo_desde_id, "Periodo Desde");
        bValid = bValid && validarCampoLleno(periodo_hasta_id, "Periodo Hasta");
        bValid = bValid && validarCampoLleno(analisis_pre_config_id, "Análisis Preconfigurado");
        
        if(bValid){ 
            $.ajax({
                url: '{{ url('/consultaValCuentaEntidadXPeriodo') }}',
                type: 'post',
                data: {
                    'entidad_id': entidad_id,
                    'tipo_fuente_id': tipo_fuente_id,
                    'periodo_desde_id': periodo_desde_id,
                    'periodo_hasta_id':periodo_hasta_id,
                    'analisis_pre_config_id':analisis_pre_config_id },
                beforeSend: function() {
                     $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento por favor </h4>' });
                },

                error : function(jqXHR, textStatus, errorThrown) {
                   var gnrMensaje =$("#gnrMensaje");
                   if(jqXHR.status == '401'){
                       gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar."); 
                       $('#gnrError').modal('show');
                   }     
               },
               success: function(data) {
                  setTimeout($.unblockUI, 1000);
                  if (data.length==0){
                       $('#mensajes').addClass('alert alert-danger');
                       $('#mensajes').removeClass('hide');
                       var textoMensaje =$("#textoMensaje");
                       textoMensaje.text("No se han encontrados datos. "); 
                       $('#arbolCuentas').empty();
                   }
                   else{  
                    var cuentas = [];
                    var periodos = [];
                    var grafico = "";
                    /*$("#btn_exportarPdf").prop( "disabled", false );
                    $("#btn_exportarExcel").prop( "disabled", false );
                    $("#btn_imprimir").prop( "disabled", false );*/
                 
                   data.forEach(function(obj, index) {
                       var periodo = new Object();
                       var periodoExiste = false;
                       periodos.forEach(function(_periodo) {
                           if(_periodo==obj['periodo_anio']) {
                               periodoExiste = true;
                           }
                       });
                       if(!periodoExiste)
                           periodos.push(obj['periodo_anio']);                                
                   }); //devuelve los periodos
                    
                    var analisis = [];
                    data.forEach(function(obj, index) {
                        var analisisExiste = false;
                        analisis.forEach(function(_analisis) {
                           if(_analisis.id==obj['analisis_pre_configurado_id']) { // SI LA CUENTA EXISTE
                               analisisExiste = true; 
                               var cuentaExiste = false; 
                               _analisis.cuentas.forEach(function(_cuenta) {
                                    if(_cuenta.name==obj['cuenta_nombre']) { // SI LA CUENTA EXISTE
                                        cuentaExiste = true;
                                        periodos.forEach(function(_periodo, indexPeriodo) {
                                            if(obj['periodo_anio']==_periodo)
                                                _cuenta.data[indexPeriodo] = obj['val_cuenta_entidad_valor'];
                                        });
                                    }
                                });
                                if(!cuentaExiste) { //EN CASO DE QUE LA CUENTA NO EXISTA
                                    var cuenta = new Object();
                                    cuenta.label = obj['cuenta_nombre'];
                                    cuenta.data = [];
                                    periodos.forEach(function(_periodo) {
                                        if(obj['periodo_anio']==_periodo)
                                            cuenta.data.push(obj['val_cuenta_entidad_valor']);
                                        else 
                                            cuenta.data.push('0');
                                    });
                                    cuenta.analisis_pre_configurado = obj['analisis_pre_configurado_nombre']
                                    _analisis.cuentas.push(cuenta);
                                }
                           }
                        });
                        if(!analisisExiste) {
                            var nuevo = new Object();
                            nuevo.id = obj['analisis_pre_configurado_id'];
                            nuevo.nombre = obj['analisis_pre_configurado_nombre'];
                            nuevo.cuentas = [];
                            var cuenta = new Object();
                            cuenta.label = obj['cuenta_nombre'];
                            cuenta.data = [];
                            periodos.forEach(function(_periodo) {
                                if(obj['periodo_anio']==_periodo)
                                    cuenta.data.push(obj['val_cuenta_entidad_valor']);
                                else 
                                    cuenta.data.push('0');
                            });
                            cuenta.analisis_pre_configurado = obj['analisis_pre_configurado_nombre']
                            nuevo.cuentas.push(cuenta);
                            analisis.push(nuevo);
                           
                            
                        }                        
                    });
                     var analisisJson = JSON.stringify(analisis);
                      $("#dataExcel").val(analisisJson);
                     
                    analisis.forEach(function(obj) {
                        container = '"grafico'+obj['id']+'"';
                        generarGrafico(obj.cuentas, periodos, container);
                    });
                   
                   console.log('PASO POR AQUI....');
                   console.log(analisis);
                   //generarGrafico(cuentas, periodos, container);

                   
                   //var arbolCuentas = armarArbolCuentasAP(data,data,"cuenta","cuenta_codigo","cuenta_nombre");
                   var arbolCuentas = armarArbol(data);

                       console.log(arbolCuentas);
                       $('#arbolCuentas').treeview({
                          levels: 1,
                          showTags: true,
                          color: "#ffffff",
                          data: arbolCuentas,
                          multiSelect: $('#chk-select-multi').is(':checked'),
                       });  
                   }          
               },
               complete: function() {

               }       
           });
        }
    }

  
    
    
    
    
 
 
</script>
@endsection