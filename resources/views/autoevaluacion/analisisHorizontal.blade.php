@extends('layouts.app')

@section('content')
<div id="page-wrapper" class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <h2><span class="text-navy"><i class="fa fa-chevron-circle-right"></i> Ánalisis Horizontal - Tendencia</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content text-left p-md">
                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                <div class="col-lg-12">
                                    @if(count($errors) > 0)
                                            <div class="alert alert-danger">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    @foreach($errors->all() as $error)
                                                    <label>{{ $error }}</label> 
                                                    </br>
                                                    @endforeach
                                                    
                                            </div>
                                    @endif
                                    
                                    @if (notify()->ready())
                                    <div class="alert alert-{{ notify()->type() }}">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <label>{{ notify()->message() }}</label>
                                    </div>
                                    @endif
                                    

                                    <div id="mensajes" name="mensajes" class="hide">

                                        <label id="textoMensaje" name="textoMensaje"></label>
                                    </div>

                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <form class="form-horizontal" method="POST" name="formAnalisisHor" enctype="multipart/form-data"  action="{{url('/guardarInfoEspIndicador')}}" role="form">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="division_politica_id" class="division_politica_id" id="division_politica_id">
                                                
                                                <div class="form-group">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                                <label class="col-sm-1 control-label">Tipo</label>
                                                                <div class="col-sm-10">
                                                                        @foreach($lstTipoEntidad as $objTipoEntidad)
                                                                        <div class="radio"><label> <input type="radio" class="check_entidad" value="{{ $objTipoEntidad->tipo_entidad_id}}" id="tipo_entidad_id" required name="tipo_entidad_id">{{ $objTipoEntidad->nombre}}</label></div>
                                                                        @endforeach
                                                                </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-12">División Política</label>
                                                            <div class="col-sm-12">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-body" id="divCuentas">
                                                                        <div id="arbolDivisionPolitica" class="step-content" >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                               <input type="hidden" name="division_politica_id" class="division_politica_id" id="division_politica_id">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Entidades</label>
                                                            <div class="col-sm-8">
                                                                <select class="form-control m-b" name="entidad_id" id="entidad_id" >
                                                                    <option value="">Seleccionar...</option>
                                                                </select>
                                                            </div>
                                                        
                                                            <label class="col-sm-4">Fuente de Información</label>
                                                            <div class="col-sm-8">
                                                                <select class="form-control m-b" name="tipo_fuente_id" id="tipo_fuente_id" >
                                                                    <option value="">Seleccionar...</option>
                                                                    @foreach($lstTipoFuente as $objTipoFuente)
                                                                        <option value="{{ $objTipoFuente->tipo_fuente_id}}"  name="tipo_fuente_id">{{ $objTipoFuente->tipo_fuente_descripcion_alias}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                       
                                                            <label class="col-sm-4">Período Desde</label>
                                                            <div class="col-sm-8">
                                                                <select class="form-control m-b" name="periodo_id_desde" id="periodo_id_desde" >
                                                                    <option value="">Seleccionar...</option>
                                                                </select>
                                                            </div>
                                                       
                                                            <label class="col-sm-4">Período Hasta</label>
                                                            <div class="col-sm-8">
                                                                <select class="form-control m-b" name="periodo_id_hasta" id="periodo_id_hasta" >
                                                                    <option value="">Seleccionar...</option>
                                                                </select>
                                                            </div>
                                                            
                                                            <label class="col-sm-4">Tipo Gráfico</label>
                                                            <div class="col-sm-8">
                                                                <select class="form-control m-b" name="tipo_grafico_id" id="tipo_grafico_id" >
                                                                    <option value="">Seleccionar...</option>
                                                                    @foreach($lstTipoGrafico as $objTipoGrafico)
                                                                        <option value="{{ $objTipoGrafico->tipo_grafico_id}}"  name="tipo_fuente_id">{{ $objTipoGrafico->tipo_grafico_nombre}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="col-sm-12 text-right">
                                                                <button id="btn_limpiar" class="btn btn-default" type="button">Limpiar</button>
                                                                <button id="btn_buscar" class="btn btn-primary" type="button">Buscar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  id="divGeneral" class="col-sm-12">
                                                        <div id="divGraficos" class="row">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <table id="tblCuentas" class="table table-striped table-bordered" style="text-align: center;" cellspacing="0" width="100%">
                                                            <thead>
                                                                <td>ÁNALISIS HORIZONTAL</td>
                                                            </thead>
                                                            
                                                        </table>
                                                    </div>
                                                    
                                                    
                                               </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
var tipoFuenteBdE = <?php echo json_encode($fuenteBdE); ?>;


$(document).ready(function (){
    
    $('#tblCuentas').dataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
    });
    
    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var lstDivPoljs = <?php echo json_encode($lstDivisionPolitica); ?>;
    var tipoGraficoBarra = <?php echo json_encode($tipoGraficoBarra); ?>;
    var arbolDivisionPolitica = dataArbolDinamico(lstDivPoljs,"division_politica_nombre","division_politica_id","division_politica","true","division_politica_codigo","division_politica_nombre");

    $("#arbolDivisionPolitica").treeview({
            levels: 1,
            color: "#555",
            showBorder: false,
            data: arbolDivisionPolitica,
            multiSelect: $('#chk-select-multi').is(':checked'),
            onNodeSelected: function (event, node) {
                $('#checkable-output').prepend('<p>' + node.text + ' was checked</p>');
                if(node.nodes && node.nodes.length>0){
                    $('#mensajes').addClass('alert alert-danger');
                   $('#mensajes').removeClass('hide');
                   var textoMensaje =$("#textoMensaje");
                   textoMensaje.text("Error! Solo se puede seleccionar cantones. "); 
                }else{
                     $('#mensajes').addClass('hide');
                     $('#division_politica_nom').val(node.text);
                     $('#division_politica_id').val(node.tags);
                     cargarEntidades();
                }
            },
            onNodeUnselected: function (event, node) {
                $('#nomCtaSeleccionada').val("");
            }
        });

    $("input[name=tipo_entidad_id]").click(function () {
        cargarEntidades();
    });

    $("#tipo_fuente_id").change(function(){
      periodos(2);       
    });

    $("#entidad_id").change(function(){
      periodos(2);       
    });

    var lgGrafico = [];
    var totCeldas = 0;
    var totCeldasGraf = 0;

    $('#btn_buscar').click(function (event) {
        lgGrafico = [];
        totCeldas = 0;
        totCeldasGraf = 0;
        var entidad_id = $("#entidad_id option:selected").val();
        var tipo_fuente_id = $("#tipo_fuente_id option:selected").val();
        var periodo_id_desde = $("#periodo_id_desde option:selected").val();
        var periodo_id_desde_text = $("#periodo_id_desde option:selected").text();
        var periodo_id_hasta = $("#periodo_id_hasta option:selected").val();
        var periodo_id_hasta_text = $("#periodo_id_hasta option:selected").text();
        var tipo_grafico_id = $("#tipo_grafico_id option:selected").val();
        var bValid = true;
        tipo_fuente_id = parseInt(tipo_fuente_id);
        var totAnios= 0;
        var totMeses= 0;
        
        var table = $('#tblCuentas').DataTable();
        table.clear();
        $('#tblCuentas').dataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "destroy": true
        });
        $('#tblCuentas thead').html('<tr><td>ÁNALISIS HORIZONTAL</td></tr>');
        
        $('#tblCuentas').dataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "destroy": true
        });
        
        $('#mensajes').removeClass('alert alert-danger');
        $('#mensajes').addClass('hide');
        var textoMensaje =$("#textoMensaje");
        textoMensaje.text("");  
        var thead= "<tr><td rowspan='2'>CUENTAS</td>";
        var cabecera;
        var thead2= "<tr><td>Recaudado/Devengado</td>";
        var tamanio;
        var inicioPeriodo;
        var finPeriodo;
                       
        if(tipo_fuente_id === tipoFuenteBdE){
           if(parseInt(periodo_id_hasta_text) <= parseInt(periodo_id_desde_text)){
                bValid = false;
                toTop();
                $('#mensajes').addClass('alert alert-danger');
                $('#mensajes').removeClass('hide');
                var textoMensaje =$("#textoMensaje");
                textoMensaje.text("El período hasta no puede ser menor ni igual al periodo desde.");
           }else{
                totAnios = parseInt(periodo_id_hasta_text) - parseInt(periodo_id_desde_text);
                var anios = periodo_id_desde_text;
                var anios2 = periodo_id_desde_text;
                tamanio =  totAnios;
                inicioPeriodo = anios;
                finPeriodo = periodo_id_hasta_text;
                totCeldas = totAnios + 1;
                totCeldasGraf = totCeldas - 1;
                for(var j=0;j<totAnios;j++){
                    thead = thead + "<td rowspan='2'>"+anios+"</td>";
                    anios= parseInt(anios)+1;
                }
                thead = thead + "<td colspan='1'>"+periodo_id_hasta_text+"</td>";
                thead = thead + "<td colspan='"+(totAnios)+"'>CRECIMIENTO HORIZONTAL</td><td rowspan='2'>% PROMEDIO HISTÓRICO</td></tr>";

                for(var j=0;j<(totAnios);j++){
                    anios2= parseInt(anios2)+1;
                    thead2 = thead2 + "<td>"+anios2+"</td>";
                    lgGrafico.push(anios2);
                }
                thead2 = thead2 +"</tr>";
                cabecera= thead +thead2;
                bValid = true;
           }
        }
        else{
            var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
            
            var str1 = periodo_id_desde_text;
            var res1 = str1.split(" - ");
            var mes1 = res1[0];
            var anio1 = res1[1];
            
            var str2 = periodo_id_hasta_text;
            var res2 = str2.split(" - ");
            var mes2 = res2[0];
            var anio2 = res2[1];
            
            var index1 = meses.indexOf(mes1);
            var index2 = meses.indexOf(mes2);
            
            index1= parseInt(index1)+1;
            index2= parseInt(index2)+1;

            if(parseInt(anio1) === parseInt(anio2)){
                if(index1 >= index2){
                    bValid = false;
                    toTop();
                    $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var textoMensaje =$("#textoMensaje");
                    textoMensaje.text("El período hasta no puede ser menor ni igual al periodo Desde.");
                }else{
                    totAnios = anio1 - anio2;
                    totMeses = index2 - index1;
                    var meseIn= parseInt(index1);
                    var meseIn2= parseInt(index1);
                    var meseFn= parseInt(index2);
                    
                    inicioPeriodo = meseIn;
                    finPeriodo = meseFn;
                    totCeldas = totMeses;
                    totCeldasGraf = totMeses-1;
                    
                    for(var j=0;j<totMeses;j++){
                        var nombreMes= meses[meseIn - 1];
                        thead = thead + "<td rowspan='2'>"+nombreMes+"</td>";
                        meseIn= parseInt(meseIn)+1;
                    }
                    var nombreMesFn= meses[meseFn - 1];
                    thead = thead + "<td colspan='1'>"+nombreMesFn+"</td>";
                    thead = thead + "<td colspan='"+(totMeses)+"'>CRECIMIENTO HORIZONTAL</td><td rowspan='2'>% PROMEDIO HISTÓRICO</td></tr>";

                    for(var j=0;j<(totMeses);j++){
                        meseIn2= parseInt(meseIn2)+1;
                        var nombreMes= meses[meseIn2 - 1];
                        thead2 = thead2 + "<td>"+nombreMes+"</td>";
                    }
                    thead2 = thead2 +"</tr>";
                    cabecera= thead +thead2;
                    bValid = true;
                }
            }else{
                
                    bValid = false;
                    toTop();
                    $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var textoMensaje =$("#textoMensaje");
                    textoMensaje.text("El año del período hasta es mayor al año del periodo Desde.");
            }
        }
            
        bValid = bValid && validarCampoLleno(entidad_id, 'Entidad');
        bValid = bValid && validarCampoLleno(tipo_fuente_id, 'Fuente de Información');
        bValid = bValid && validarCampoLleno(periodo_id_desde, 'Período Desde');
        bValid = bValid && validarCampoLleno(periodo_id_hasta, 'Período Hasta');
        bValid = bValid && validarCampoLleno(tipo_grafico_id, 'Tipo Gráfico');

        if(bValid)
        {
            $.ajax({
                url: '/buscarLstValCtaEntidadxFiltro',
                type: 'post',
                "_token": "{{ csrf_token() }}",
                async: true,
                data: {'entidad_id':entidad_id,'tipo_fuente_id':tipo_fuente_id,'periodo_id_desde':periodo_id_desde,'periodo_id_hasta':periodo_id_hasta,'tamanio':tamanio,'inicioPeriodo':inicioPeriodo,'finPeriodo':finPeriodo},
                beforeSend: function() {
                    $.blockUI({ message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>' });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var gnrMensaje = $("#gnrMensaje");
                    toTop();
                    if (jqXHR.status == '401') {
                        gnrMensaje.text(textStatus+" / "+errorThrown+" ,Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                        $('#gnrError').modal('show');
                    }else{
                        $('#mensajes').addClass('alert alert-danger');
                        $('#mensajes').removeClass('hide');
                        var textoMensaje =$("#textoMensaje");
                        textoMensaje.text("ERROR: "+jqXHR.status+" - "+textStatus +" - "+errorThrown);
                    }
                },
                success:  function(data) {
                    if (data == ''){
                       toTop();
                       $('#mensajes').addClass('alert alert-danger');
                       $('#mensajes').removeClass('hide');
                       var textoMensaje =$("#textoMensaje");
                       textoMensaje.text("No se han encontrados datos. ");  
                    }else{
                        $('#tblCuentas thead').html(cabecera);
                        $('#tblCuentas').dataTable({
                            data: data,
                            "scrollX": true,
                            "paging":   false,
                            "ordering": false,
                            "info":     false,
                            "destroy": true
                        });
                    }
                },
                complete: function() {
                    setTimeout($.unblockUI, 1000);
                },
            });
        }else{
           toTop();
        }

    });
    
 
    $('#tblCuentas tbody').on( 'click', 'tr', function () {
       var tipo_grafico_id = $("#tipo_grafico_id option:selected").val();
       if(tipoGraficoBarra === parseInt(tipo_grafico_id)){
        $("#divLinea").remove();
        var table = $('#tblCuentas').DataTable();
        var indice= table.row(this).index();

        if ($(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            $("#div"+indice).remove();
        }
        else {
            $(this).addClass('selected');
            var array=[];
            var celdaInicial = 1;
            var tot=celdaInicial + totCeldas;
            var totFin=tot+totCeldasGraf;
            var $i=0;
            var nomCuenta;
            $(this).children("td").each(function() 
            {
                if($i === 0){
                    nomCuenta=$(this).text();
                }
                if($i >= tot && $i<totFin){
                   var col= $(this).text();
                   array.push(col); 
                }
                $i++;
            });
            var div = "<div id='div"+indice+"' class='col-lg-6'><div class='ibox float-e-margins'><div class='ibox-title'>";
                div = div + "<h5>"+nomCuenta+"</h5><div ibox-tools></div></div><div class='ibox-content'>";
                div = div + "<div><canvas id='can"+indice+"' height='140'></canvas></div></div></div></div>";
            $("#divGraficos").append(div);
            console.log(indice);

            

            var barOptions = {
                scaleBeginAtZero: false,
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                barShowStroke: true,
                barStrokeWidth: 2,
                barValueSpacing: 3,
                barDatasetSpacing: 1,
                responsive: true,
            }


            var ctx = document.getElementById('can'+indice).getContext("2d");
			var data = {
				labels: lgGrafico,
				datasets: [
					{
						label: nomCuenta,
						backgroundColor: [
							'rgba(255, 99, 132, 0.2)',
							'rgba(54, 162, 235, 0.2)',
							'rgba(255, 206, 86, 0.2)',
							'rgba(75, 192, 192, 0.2)',
							'rgba(153, 102, 255, 0.2)',
							'rgba(255, 159, 64, 0.2)'
						],
						borderColor: [
							'rgba(255,99,132,1)',
							'rgba(54, 162, 235, 1)',
							'rgba(255, 206, 86, 1)',
							'rgba(75, 192, 192, 1)',
							'rgba(153, 102, 255, 1)',
							'rgba(255, 159, 64, 1)'
						],
						borderWidth: 1,
						data: array
					}
				]
			};
			
            var myBarChart = new Chart(ctx, {
				type: 'bar',
				data: data,
				options: barOptions
			});
			}
       }
       else{
            $("#divLinea").remove();
            $("#divGraficos").remove();
            $("#divGeneral").append('<div id="divGraficos" class="row"></div>');
            var lstCuentas= [];
            
            if ($(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                $(this).addClass('selected');
            }
            
            $("#tblCuentas tbody tr").each(function () 
            {
                var randomColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
                
                if ($(this).hasClass('selected') ) {
                    var array=[];
                    var celdaInicial = 1;
                    var tot=celdaInicial + totCeldas;
                    var totFin=tot+totCeldasGraf;
                    var $i=0;
                    var nomCuenta;
                    $(this).children("td").each(function() 
                    {
                        if($i === 0){
                            nomCuenta=$(this).text();
                        }
                        if($i >= tot && $i<totFin){
                           var col= $(this).text();
                           array.push(col); 
                        }
                        $i++;
                    });
                    var randomColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
                    var nuevo = {
						
						label: nomCuenta,
						fill: false,
						fillColor: randomColor,
						lineTension: 0.1,
						strokeColor: randomColor,
                        pointColor: randomColor,
						backgroundColor: randomColor,
						borderColor: randomColor,
						borderCapStyle: 'butt',
						borderDash: [],
						borderDashOffset: 0.0,
						borderJoinStyle: 'miter',
						pointBorderColor: randomColor,
						pointBackgroundColor: randomColor,
						pointBorderWidth: 1,
						pointHoverRadius: 5,
						pointHighlightStroke: randomColor,
						pointHoverBackgroundColor: randomColor,
						pointHoverBorderColor: randomColor,
						pointHoverBorderWidth: 2,
						pointRadius: 1,
						pointHitRadius: 10,
						data: array,
						spanGaps: false,
                    }
                    lstCuentas.push(nuevo);
                    
                }
            });
       
            if(lstCuentas.length > 0){
                var div = "<div id='divLinea' class='col-lg-12'><div class='ibox float-e-margins'><div class='ibox-title'>";
                    div = div + "<h5>Ánalisis de Tendencia</h5><div ibox-tools></div></div><div class='ibox-content'>";
                    div = div + "<div><canvas id='canLinea' height='100'></canvas></div></div></div><div id='js-legend' class='chart-legend'></div></div>";
                $("#divGraficos").append(div);

                var lineOptions = {
                 scaleShowGridLines: true,
                 scaleGridLineColor: "rgba(0,0,0,.05)",
                 scaleGridLineWidth: 1,
                 bezierCurve: true,
                 bezierCurveTension: 0.4,
                 pointDot: true,
                 pointDotRadius: 4,
                 pointDotStrokeWidth: 1,
                 pointHitDetectionRadius: 20,
                 datasetStroke: true,
                 datasetStrokeWidth: 2,
                 datasetFill: true,
                 responsive: true,
                 legend: {
                 display: true,
                 position:'right',
                 labels: {
                     display: true
                   }
             }
             };
                var ctx = document.getElementById('canLinea').getContext("2d");
				var myChart = new Chart(ctx, {
					type: 'line',
					data: {
						labels: lgGrafico,
						datasets: lstCuentas
					},
					options: lineOptions
				});
                
            }
        }
    });

    $('#btn_limpiar').click(function (event) {
            toTop();
            $("#divGraficos").remove();
            $("#divGeneral").append('<div id="divGraficos" class="row"></div>');
            $('#mensajes').removeClass('alert alert-danger alert alert-warning');
            $('#mensajes').addClass('hide');
            $('#btn_guardar').text('Almacenar');
            $('#variable_indicador_id').val("");
            $('#lblProyFinanciera').addClass('hide');
            $('#divProyFinanciera').addClass('hide');
            $("#tipo_indicador_id").val("");
            $("#indicador_nombre").val("");
            $("#indicador_objetivo").val("");
            $("#indicador_alcance").val("");
            $("#lblFormula").text("");
            $("#lblFormula_2").text("");
            $("#indicador_formula").val("");
            $('#nivel_alerta_id').selectpicker('destroy');
            $('#nivel_alerta_id').html('');
            $('#nivel_alerta_id_2').selectpicker('destroy');
            $('#nivel_alerta_id_2').html('');

            $("#indicador_id").val(""); 
            $("#lstRangosAceptacion").val("");
            $("#lstNivelesAlerta").val("");
            $("#lstNivelesAlertaAct").val(""); 
            $("#criterioId").val("");
            $("#lstCambioMejora").val("");

            $("#categoria_id").val(""); 
            $("#valor_minimo").val("");
            $("#valor_maximo").val("");

            $("#rango_aceptacion_id_1").val("");
            $("#tipo_nivel_alerta_id").selectpicker('val',''); 
            $("#valorDesde").val("");
            $("#valorHasta").val("");

            $("#rango_aceptacion_id").val(""); 
            $("#nivel_alerta_id").selectpicker('val',''); 
            $("#nivel_alerta_descripcion").val("");
            $("#nivel_alerta_interpretacion").val("");  

            $("#rango_aceptacion_id_2").val(""); 
            $("#nivel_alerta_id_2").selectpicker('val',''); 
            $("#componentes_formula").val("");
            $("#criterio_cambio_criterio").val("");  
            valorFormula = '';
            formula = '';
            lgGrafico = [];
            totCeldas = 0;
            totCeldasGraf = 0;
          
            
            setTimeout($.unblockUI, 1000);
        });

});
</script>
@endsection
