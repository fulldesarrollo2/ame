/**
 * 
 * @param {type} mesNumero
 * @returns {String}
 */
function obtenerMes(mesNumero) {
    var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    return meses[mesNumero - 1];
}

/**
 * 
 * @returns {undefined}
 */
function cargarEntidades() {
    var division_politica_id = $('#division_politica_id').val(),
        tipoEntidad = $('input[name=tipo_entidad_id]:checked').val();
    var bValid = true;
    $('#entidad_id').empty();
    $('#mensajes').removeClass('alert alert-danger');
    $('#mensajes').addClass('hide');
    var textoMensaje = $("#textoMensaje");
    textoMensaje.text("");
    bValid = bValid && validarCampoLleno(tipoEntidad, 'Tipo Entidad');
    bValid = bValid && validarCampoLleno(division_politica_id, 'División Politíca');
    if (bValid) {
        $.ajax({
            url: '/consultaEntRelacionaDivPoTipo',
            type: 'post',
            "_token": "{{ csrf_token() }}",
            async: true,
            data: {
                'division_politica_id': division_politica_id,
                'tipoEntidad': tipoEntidad
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var gnrMensaje = $("#gnrMensaje");
                if (jqXHR.status == '401') {
                    gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                    $('#gnrError').modal('show');
                }
                toTop();
            },
            success: function (data) {
                if (data == '') {
                    toTop();
                    $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var textoMensaje = $("#textoMensaje");
                    textoMensaje.text("No se han encontrados datos. ");
                } else {
                    $('#entidad_id').append('<option>Seleccione...</option>');
                    for (var i in data) {
                        $('#entidad_id').append('<option id=' + data[i].entidad_id + ' value=' + data[i].entidad_id + '>' + data[i].entidad_nombre + ' </option>');
                    }
                }
            },
            complete: function () {
                setTimeout($.unblockUI, 1000);
            }
        });
    } else {
        toTop();
    }
}

/**
 * 
 * @param {type} opcion
 * @returns {undefined}
 */
function periodos(opcion) {
    var entidad_id = $('#entidad_id').val();
    var tipofuente = $('#tipo_fuente_id').val();
    var bValid = true;
    var tipoFuenteBdE = 2;
    $('#mensajes').removeClass('alert alert-danger');
    $('#mensajes').addClass('hide');
    var textoMensaje = $("#textoMensaje");
    textoMensaje.text("");
    $('#periodo_id').empty();
    $('#periodo_id_desde').empty();
    $('#periodo_id_hasta').empty();
    $('#periodo_id').append($("<option></option>").text("Seleccionar.."));
    $('#periodo_id_desde').append($("<option></option>").text("Seleccionar.."));
    $('#periodo_id_hasta').append($("<option></option>").text("Seleccionar.."));
    bValid = bValid && validarCampoLleno(entidad_id, 'Entidad');
    bValid = bValid && validarCampoLleno(tipofuente, 'Fuente de Información');
    if (bValid) {
        $.ajax({
            url: '/consultarLstPeriodoDinamico',
            type: 'post',
            async: 'true',
                    data: {
                        'tipofuente': tipofuente
                    },
            beforeSend: function () {
                $.blockUI({
                    message: '<h4><img src="../img/gif/progress.gif" /></br> PROCESANDO... Espere un momento porfavor </h4>'
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var gnrMensaje = $("#gnrMensaje");
                if (jqXHR.status == '401') {
                    gnrMensaje.text("Su sesión ha caducado, porfavor de click en Salir y vuelva a Ingresar.");
                    $('#gnrError').modal('show');
                }
                toTop();
            },
            success: function (data) {
                if (data == '') {
                    toTop();
                    $('#mensajes').addClass('alert alert-danger');
                    $('#mensajes').removeClass('hide');
                    var textoMensaje = $("#textoMensaje");
                    textoMensaje.text("No se han encontrados datos. ");
                } else {
                    switch (opcion) {
                        case 1:
                            if (tipofuente == tipoFuenteBdE) {
                                for (var i = 0; i < data.length; i++) {
                                    $('#periodo_id').append($("<option></option>").attr("value", data[i].periodo_id).text(data[i].periodo_anio));
                                }
                            } else {
                                for (var i = 0; i < data.length; i++) {
                                    var mes = obtenerMes(data[i].periodo_mes);
                                    $('#periodo_id').append($("<option></option>").attr("value", data[i].periodo_id).text(mes + " - " + data[i].periodo_anio));
                                }
                            }
                            break;
                        case 2:
                            if (tipofuente == tipoFuenteBdE) {
                                for (var i = 0; i < data.length; i++) {
                                    $('#periodo_id_desde').append($("<option></option>").attr("value", data[i].periodo_id).text(data[i].periodo_anio));
                                    $('#periodo_id_hasta').append($("<option></option>").attr("value", data[i].periodo_id).text(data[i].periodo_anio));
                                }
                            } else {
                                for (var i = 0; i < data.length; i++) {
                                    var mes = obtenerMes(data[i].periodo_mes);
                                    $('#periodo_id_desde').append($("<option></option>").attr("value", data[i].periodo_id).text(mes + " - " + data[i].periodo_anio));
                                    $('#periodo_id_hasta').append($("<option></option>").attr("value", data[i].periodo_id).text(mes + " - " + data[i].periodo_anio));
                                }
                            }
                            break;
                        case 3:
                            for (var i = 0; i < data.length; i++) {
                                $('#periodo_id_desde').append($("<option></option>").attr("value", data[i].periodo_id).text(data[i].periodo_anio));
                                $('#periodo_id_hasta').append($("<option></option>").attr("value", data[i].periodo_id).text(data[i].periodo_anio));
                            }
                            break;
                    }
                }
            },
            complete: function () {
                setTimeout($.unblockUI, 1000);
            }
        });
    } else {
        toTop();
    }
}


