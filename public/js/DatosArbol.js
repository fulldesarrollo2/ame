/**
 * Funcion que realiza el armado de datos para arbol de bootstrap
 * lstArbol Variable Json en el cual se ingresara el arbol
 * nombreVarTexto nombre de la Variable en la estructura json y se mostrara en el arbol
 * nombreVarId id de la Variable en la estructura json y se mostrara en el arbol
 * nombreVarLstHijos nombre de la lista de hijos en la estructura json y se mostrara en el arbol
 */
function dataArbolDinamico(lstArbol, nombreVarTexto, nombreVarId, nombreVarLstHijos) {
    var lst = [];
    for (var i = 0; i < lstArbol.length; i++) {
        var raiz = lstArbol[i];
        var arbol = {text: "", href: "", tags: 0};
        var hijo = 'raiz.' + nombreVarLstHijos;
        var texto = 'raiz.' + nombreVarTexto;
        var id = 'raiz.' + nombreVarId;
        //Longitud los hijos del arbol
        var tamanioHijos = eval(hijo) ? eval(hijo).length : 0;
        arbol.text = eval(texto);
        arbol.href = eval(id);
        arbol.tags = [eval(id)];
        //Si el arbol tiene raices, se le agregan los hijos
        if (tamanioHijos > 0) {
            arbol.nodes = dataArbolDinamico(eval(hijo), nombreVarTexto, nombreVarId, nombreVarLstHijos);
        }
        lst.push(arbol);
    }
    return lst;
}


/**
 * Funcion que realiza el armado de datos para arbol de bootstrap
 * lstArbol Variable Json en el cual se ingresara el arbol
 * nombreVarTexto nombre de la Variable en la estructura json y se mostrara en el arbol
 * nombreVarId id de la Variable en la estructura json y se mostrara en el arbol
 * nombreVarLstHijos nombre de la lista de hijos en la estructura json y se mostrara en el arbol
 */
function dataArbolDinamico(lstArbol, nombreVarTexto, nombreVarId, nombreVarLstHijos, concatenar, nombreConcatenar1, nombreConcatenar2) {
    var lst = [];
    for (var i = 0; i < lstArbol.length; i++) {
        var raiz = lstArbol[i];
        var arbol = {text: "", href: "", tags: 0};
        var hijo = 'raiz.' + nombreVarLstHijos;
        var texto;
        if (concatenar) {
            texto = "raiz." + nombreConcatenar1 + " + ' ' + raiz." + nombreConcatenar2;
        } else
            texto = 'raiz.' + nombreVarTexto;

        var id = 'raiz.' + nombreVarId;
        //Longitud los hijos del arbol
        var tamanioHijos = eval(hijo) ? eval(hijo).length : 0;
        arbol.text = eval(texto);
        arbol.href = eval(id);
        arbol.tags = [eval(id)];
        //Si el arbol tiene raices, se le agregan los hijos
        if (tamanioHijos > 0) {
            arbol.nodes = dataArbolDinamico(eval(hijo), nombreVarTexto, nombreVarId, nombreVarLstHijos, concatenar, nombreConcatenar1, nombreConcatenar2);
        }
        lst.push(arbol);
    }
    return lst;
}

/**
 * Funcion que realiza el armado de datos para arbol de bootstrap
 * lstArbol Variable Json en el cual se ingresara el arbol
 * nombreVarTexto nombre de la Variable en la estructura json y se mostrara en el arbol
 * nombreVarId id de la Variable en la estructura json y se mostrara en el arbol
 * nombreVarLstHijos nombre de la lista de hijos en la estructura json y se mostrara en el arbol
 */
function dataArbolDinamicoSinHijo(lstArbol, nombreVarTexto, nombreVarId) {
    var lst = [];
    for (var i = 0; i < lstArbol.length; i++) {
        var raiz = lstArbol[i];
        var arbol = {text: "", href: "", tags: 0};
        var texto = 'raiz.' + nombreVarTexto;
        var id = 'raiz.' + nombreVarId;
        //Longitud los hijos del arbol
        arbol.text = eval(texto);
        arbol.href = eval(id);
        arbol.tags = [eval(id)];
        //Si el arbol tiene raices, se le agregan los hijos
        lst.push(arbol);
    }
    return lst;
}

/*
 * Función que le da formato Moneda a un número
 */
function formatoMoneda(numero)
{
    if (!isNaN(numero)) {
        numero = numero.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    }
    return numero;
}

/*
 * Función que realiza el armado de datos para arbol de bootstrap con Valor Tag
 * recibe una lista solo con los elementos que son padres del arbol
 * recibe otra lista que tiene los valores que se mostraran en Valor Tag
 */
function armarArbolCuentas(lstArbolArmar, lstOriginal, nombreVarLstHijos, nombreConcatenar1, nombreConcatenar2) {
    var lst = [];
    for (var i = 0; i < lstArbolArmar.length; i++) {
        var raiz = lstArbolArmar[i];

        var arbol = {text: "", href: "", tags: 0};
        var texto = "raiz." + nombreConcatenar1 + " + ' ' + raiz." + nombreConcatenar2;
        arbol.text = eval(texto);

        //Obtener valor
        for (var j = 0; j < lstOriginal.length; j++) {
            if (raiz.cuenta_id == lstOriginal[j].cuenta_id) {
                var valorCuenta = lstOriginal[j].valor;
                valorCuenta = formatoMoneda(valorCuenta);
                arbol.tags = ['$ ' + valorCuenta];
            }
        }

        //Obtener hijos
        var hijo = 'raiz.' + nombreVarLstHijos;
        var tamanioHijos = eval(hijo) ? eval(hijo).length : 0;
        //Si el arbol tiene raices, se le agregan los hijos
        if (tamanioHijos > 0) {
            arbol.nodes = armarArbolCuentas(eval(hijo), lstOriginal, nombreVarLstHijos, nombreConcatenar1, nombreConcatenar2);
        }

        //Agregar al árbol
        lst.push(arbol);
    }
    return lst;
}

/*
 * Función que recibe una lista y
 * genera otra lista solo con los elementos que son padres 
 * para luego realizar el armado de datos para arbol de bootstrap con Valor Tag
 * Devuelve las mismas variables que recibe con una variable adicional 
 * que corresponde a la lista de padres que genero
 */
function dataArbolDinamicoValorTag(lstCuentas, nombreVarLstHijos, nombreConcatenar1, nombreConcatenar2) {
    var lst = [];
    var lstPadres = [];

    //obtener lista de nodos padre
    for (var i = 0; i < lstCuentas.length; i++) {
        var raiz = lstCuentas[i];

        if (raiz.cuenta_cuenta_id == null) {
            lstPadres.push(raiz);
        }
    }

    //obtener el árbol dinámico
    lst = armarArbolCuentas(lstPadres, lstCuentas, nombreVarLstHijos, nombreConcatenar1, nombreConcatenar2);
    return lst;
}
