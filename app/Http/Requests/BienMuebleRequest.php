<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BienMuebleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descripcion'       =>  'required',
            'valorAdquisicion'  =>  'required|numeric',
            'vidaUtil'          =>  'required|numeric',
            'fechaInversion'    =>  'required|date',
            'tipoInversion'     =>  'required',
            'estado'            =>  'required',
            'cantidad'          =>  'required|numeric'
        ];
    }
    public function messages(){
        return[
            
        ];
    }
}
