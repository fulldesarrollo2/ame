<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class adminServMuninRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'proyectos'     =>'required',
            'subsistema'    =>'required',
            'periodo'       =>'required',
            'unidad_servicio'=>'required',

            //'tipos'         =>'required',
            'subTipo'       =>'required',
            /*'caudalSalidaDia'=>'required|numeric|max:1000000',

            'caudalSalidaAnio'=>'required|numeric|max:15000',
            'TiempoExistenciaAnio'=>'required|digits_between:1,2',
            'vidaUtil'      =>'required|digits_between:1,2',
            'ubicacionUTM'  =>'required',
            'numUnidades'   =>'required|digits_between:1,7',
            'capacidad'     =>'required',
            'caudalEntrada' =>'required',
            'caudalSalidaLS'=>'required',
            'perdidas'      =>'required',

            'estadoFuncionamiento'=>'required'*/



        ];
    }
}
