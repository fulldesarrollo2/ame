<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class variableServAdminRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cargo' =>'required',
            'remuneracion'          =>'required|numeric',
            'participacion'         =>'required|numeric|between:0,100',
            'tipoCosto'             =>'required',
            'cantidadPersonas'      =>'required|numeric'
        ];
    }
}
