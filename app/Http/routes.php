<?php
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::auth();
Route::get('/', function () {
    return view('auth.login');
});

Route::get('/noAutorizado', function () {
    return view('auth.notAutorize');
});

Route::group(['middleware' => ['verifyToken', 'auth']], function () {
    Route::get('/welcome', function () {
        return view('welcome');
    });
    Route::get('/', function () {
    return view('welcome');
});
});

Route::group(['middleware' => ['verifyToken', 'auth', 'ameTecnico']], function () {
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});

Route::group(['middleware' => ['verifyToken', 'auth']], function () {

    //HOME
    Route::resource('home', 'HomeController');

//////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                  //
//                                                                                                  //
    //  inventario y costeo
    Route::resource('adminInforServMunicipales','Administracion\AdminInforServMunicipalController');       
    Route::post('buscarSubsistemaXproyecto','Administracion\AdminInforServMunicipalController@buscarSubsistemaXproyecto');
    Route::post('buscarVariableCosteoXunidadServicio','Administracion\AdminInforServMunicipalController@buscarVariableCosteoXunidadServicio');
    Route::post('buscarSubTipoXUnidadServicio','Administracion\AdminInforServMunicipalController@buscarSubTipoXUnidadServicio');
     //ADMINISTRACION DE INFORMACION DE SERVICION BASICOS MUNINCIPALES
    Route::post('infServBasicosMuninc','Administracion\AdminInforServMunicipalController@guardarInfoServBasicosMuninc');
    //ESTRUCTURA ADMINISTRATIVA
    
    /******************    Administrativa    ********************/
    Route::resource('variableServAdministrativa','Autoevaluacion\VariableServicioAdministrativaController');
    /******************    Inversion    **********************************/
    Route::resource('serviciosBasicosInversiones', 'Autoevaluacion\servicioBasicoInversionesController');
        Route::post('guardarInversiones','Autoevaluacion\servicioBasicoInversionesController@guardarInversionInfraestructura');
        Route::post('guardarInversionesBM','Autoevaluacion\servicioBasicoInversionesController@guardarInversionBienMueble');
        
    
//                                                                                                  //
//                                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////
    //PERFIL INGRESO
    Route::resource('perfil', 'Administracion\PerfilController');
    Route::post('validarPerfil', 'Administracion\PerfilController@validarPerfil');
    Route::post('buscarPerfil', 'Administracion\PerfilController@buscarPerfil');
    Route::post('guardarPerfil', 'Administracion\PerfilController@guardarPerfil');
    Route::post('actualizarPerfil', 'Administracion\PerfilController@actualizarPerfil');

    //ENTIDAD
    Route::resource('entidad', 'Administracion\EntidadController');
    Route::post('guardarEntidad', 'Administracion\EntidadController@guardarEntidad');
    Route::post('consultaEntRelacionaDivPo', 'Administracion\EntidadController@consultaEntRelacionaDivPo');
    Route::post('editarEntidad', 'Administracion\EntidadController@editarEntidad');

    //DIVISION POLITICA
    Route::resource('divisionPolitica', 'Administracion\DivisionPoliticaController');
    Route::post('validarDivisionPolitica', 'Administracion\DivisionPoliticaController@validarDivisionPolitica');
    Route::post('guardaDivisionPolitica', 'Administracion\DivisionPoliticaController@guardaDivisionPolitica');
    Route::post('recuperarDivisionPolitica', 'Administracion\DivisionPoliticaController@buscarObjDivisionPolitica');
    Route::post('actualizaDivisionPolitica', 'Administracion\DivisionPoliticaController@actualizaDivisionPolitica');

    //CATALOGO DE CUENTAS FINANCIERAS OFICIALES
    Route::resource('catalogoCtasFinOficiales', 'Administracion\CtasFinOficialesController');
    Route::post('buscarLstCuentaxTipoxFuente', 'Administracion\CtasFinOficialesController@buscarLstCuentaxTipoxFuente');
    Route::post('buscarCuentaSeleccionada', 'Administracion\CtasFinOficialesController@buscarCuentaSeleccionada');
    Route::post('guardarCuenta', 'Administracion\CtasFinOficialesController@guardarCuenta');
    Route::post('actualizarCuenta', 'Administracion\CtasFinOficialesController@actualizarCuenta');

    //CATALOGO DE VARIABLES GENERALES
    Route::resource('catalogoVarGenerales', 'Administracion\ValorVariableGeneralController');
    Route::post('buscarLstVariableGeneral', 'Administracion\ValorVariableGeneralController@consultaVariableGeneral');
    Route::post('buscarLstVariableEspecifica', 'Administracion\ValorVariableGeneralController@consultarVariableEspecifica');
    Route::post('consultaPeriodo', 'Administracion\ValorVariableGeneralController@consultaPeriodos');
    Route::post('consultaEspecifica', 'Administracion\ValorVariableGeneralController@consultaInfoVariableEspecifica');
    Route::post('consultaVariableGeneral', 'Administracion\ValorVariableGeneralController@consultaInfoVariableGeneral');
    Route::post('editarVariableGeneral', 'Administracion\ValorVariableGeneralController@editarVariableGeneral');
    Route::post('editarValorVariableGeneral', 'Administracion\ValorVariableGeneralController@editaValorVariableGeneral');
    Route::post('divisionPoliticaCanton', 'Administracion\DivisionPoliticaController@consultaDivisionPoliticaxId');

    //USUARIO
    Route::resource('usuario', 'Administracion\UsuarioController');
    Route::post('consultaEntRelacionaDivPoTipo', 'Administracion\UsuarioController@consultaEntRelacionaDivPoTipo');
    Route::post('guardarUsuario', 'Administracion\UsuarioController@guardarUsuario');
    Route::post('buscarUsuario', 'Administracion\UsuarioController@buscarUsuario');
    Route::post('actualizarUsuario', 'Administracion\UsuarioController@actualizarUsuario');

    //PERIODO
    Route::resource('periodo', 'Administracion\PeriodoController');
    Route::post('cierrePeriodo', 'Administracion\PeriodoController@cierrePeriodo');
    Route::post('cambioPeriodoFiscal', 'Administracion\PeriodoController@cambioPeriodoFiscal');
    Route::post('consultarLstPeriodoDinamico', 'Administracion\PeriodoController@consultarLstPeriodoDinamico');

    //CATALOGO CUENTAS FINANCIERAS PERSONALIZADAS
    Route::resource('catalogoCtasFinPersonalizadas', 'Administracion\CtasFinPersonalizadasController');
    Route::post('buscarCuentaxTipo', 'Administracion\CtasFinPersonalizadasController@buscarCuentaxTipo');
    Route::post('guardarCtaPersonalizada', 'Administracion\CtasFinPersonalizadasController@guardarCtaPersonalizada');
    Route::post('actualizarCtaPersonalizada', 'Administracion\CtasFinPersonalizadasController@actualizarCtaPersonalizada');

    //ADMINISTRACIÓN DE CATÁLOGO DE CATEGORÍAS
    Route::resource('catalogoCategoria', 'Administracion\CatalogoCategoriaController');
    Route::post('consultaCantonesCategorias', 'Administracion\CatalogoCategoriaController@consultaCantonesCategorias');
    Route::post('guardarCatalogoCategoria', 'Administracion\CatalogoCategoriaController@guardarCatalogoCategoria');
    Route::post('actualizarCategoria', 'Administracion\CatalogoCategoriaController@actualizarCategoria');
    Route::post('buscarDatosCategoria', 'Administracion\CatalogoCategoriaController@buscarDatosCategoria');

    //ADMINISTRACION DE LA INFORMACION FINANCIERA
    Route::resource('infoFinanciera', 'Autoevaluacion\InfoFinancieraController@index');
    Route::post('buscarTipoArchivo', 'Autoevaluacion\InfoFinancieraController@buscarTipoArchivo');
    Route::post('buscarTipCuentDeTipArchivo', 'Autoevaluacion\InfoFinancieraController@buscarTipCuentDeTipArchivo');
    Route::post('buscarCedulasGastos', 'Autoevaluacion\InfoFinancieraController@buscarCedulasGastos');
    Route::post('buscarCedulasIngresos', 'Autoevaluacion\InfoFinancieraController@buscarCedulasIngresos');
    Route::post('buscarPeriodoPorFuente', 'Autoevaluacion\InfoFinancieraController@buscarPeriodoPorFuente');
    Route::post('guardarInfoFinan', 'Autoevaluacion\InfoFinancieraController@guardarInfoFinan');
    Route::post('buscarParamArchCarga', 'Autoevaluacion\InfoFinancieraController@buscarParamArchCarga');
    Route::post('imprimirPdf', 'Autoevaluacion\InfoFinancieraController@imprimirPdf');
    Route::post('imprimirExcel', 'Autoevaluacion\InfoFinancieraController@imprimirExcel');

    //VALIDACIÓN DE INCONSISTENCIA DE INFORMACIÓN
    Route::resource('valiIncosInformacion', 'Autoevaluacion\ValiIncosInformacionController');
    Route::post('consultaEntidadPorDivTipo', 'Autoevaluacion\ValiIncosInformacionController@consultaEntidadPorDivTipo');
    Route::post('consultaValCuentaEntidad', 'Autoevaluacion\ValiIncosInformacionController@consultaValCuentaEntidad');
    Route::post('imprimirPdfValiIncosIn', 'Autoevaluacion\ValiIncosInformacionController@imprimirPdfValiIncosIn');
    Route::post('imprimirExcelValiIncosIn', 'Autoevaluacion\ValiIncosInformacionController@imprimirExcelValiIncosIn');

    //INICIO INFORMACION DE INDICADORES
    
    //BASICA
    Route::resource('indicador', 'Autoevaluacion\IndicadorController');
    Route::post('buscarLstVariableIndicador', 'Autoevaluacion\IndicadorController@buscarLstVariableIndicador');
    Route::post('buscarIndicadorxNombre', 'Autoevaluacion\IndicadorController@buscarIndicadorxNombre');
    Route::post('guardarIndicador', 'Autoevaluacion\IndicadorController@guardarIndicador');
    Route::post('actualizarIndicador', 'Autoevaluacion\IndicadorController@actualizarIndicador');

    //ESPECIFICA
    Route::post('guardarRangosAceptacion', 'Autoevaluacion\IndicadorController@guardarRangosAceptacion');
    Route::post('guardarNivelesAlerta', 'Autoevaluacion\IndicadorController@guardarNivelesAlerta');

    //PROSPECTIVA
    Route::post('buscarLstNivelAlertaxRango', 'Autoevaluacion\IndicadorController@buscarLstNivelAlertaxRango');
    Route::post('actualizarNivelesAlerta', 'Autoevaluacion\IndicadorController@actualizarNivelesAlerta');
    Route::post('guardarCambioMejora', 'Autoevaluacion\IndicadorController@guardarCambioMejora');

    //FIN INFORMACION DE INDICADORES
    
    //ANALISIS FINANCIERO PRECONFIGURADO
    Route::resource('analisisfinPreConfig', 'Autoevaluacion\AnalisisPreConfiguradoController');
    Route::post('guardaAnalisisPreConfig', 'Autoevaluacion\AnalisisPreConfiguradoController@guardarAnalisisPreConfig');
    Route::post('consultarAnalisisPreConfig', 'Autoevaluacion\AnalisisPreConfiguradoController@buscarAnalisisPreConfig');
    Route::post('actualizarAnalisisPreConfig', 'Autoevaluacion\AnalisisPreConfiguradoController@actualizarAnalisisPreConfig');

    //PROYECTO FINANCIERO PLANIFICACION PLURIANUAL DE INVERSION
    Route::resource('proyectoFinancieroConsulta', 'Autoevaluacion\ProyectoFinancieroController');
    Route::post('consultarEntidadDivisionPolitica', 'Autoevaluacion\ProyectoFinancieroController@consultaEntRelacionaDivPoPorDivisionPolitica');
    Route::post('consultarProyectoFinanciero', 'Autoevaluacion\ProyectoFinancieroController@consultarProyectoFinanciero');
    Route::post('imprimirExcelProyectoFinanciero', 'Autoevaluacion\ProyectoFinancieroController@imprimirExcelProyectoFinanciero');
    Route::post('imprimirPdfProyectoFinanciero', 'Autoevaluacion\ProyectoFinancieroController@imprimirPdfProyectoFinanciero');

    //ADMINISTRACIÓN DE PROYECTOS FINANCIEROS
    Route::resource('admProyecFinanciero', 'Autoevaluacion\AdmProyecFinancieroController');
    Route::post('cargarEstados', 'Autoevaluacion\AdmProyecFinancieroController@cargarEstados');
    Route::post('buscarPeriodo', 'Autoevaluacion\AdmProyecFinancieroController@buscarPeriodo');
    Route::post('guardarComponente', 'Autoevaluacion\AdmProyecFinancieroController@guardarComponente');
    Route::post('guardarActividad', 'Autoevaluacion\AdmProyecFinancieroController@guardarActividad');
    Route::post('eliminarActividad', 'Autoevaluacion\AdmProyecFinancieroController@eliminarActividad');
    Route::post('editarProyecto', 'Autoevaluacion\AdmProyecFinancieroController@editarProyecto');
    Route::post('eliminarComponente', 'Autoevaluacion\AdmProyecFinancieroController@eliminarComponente');
    Route::post('agregarEjecucion', 'Autoevaluacion\AdmProyecFinancieroController@agregarEjecucion');
    Route::post('guardarProyecto', 'Autoevaluacion\AdmProyecFinancieroController@guardarProyecto');

    //ANALISIS HORIZONTAL
    Route::resource('analisisHorizontal', 'Autoevaluacion\AnalisisHorizontalController');
    Route::post('buscarLstValCtaEntidadxFiltro', 'Autoevaluacion\AnalisisHorizontalController@buscarLstValCtaEntidadxFiltro');

    //ANALISIS VERTICAL
    Route::resource('analisisVertical', 'Autoevaluacion\AnalisisVerticalController');
    Route::post('consultaEntRelacionaDivPoTipo', 'Autoevaluacion\AnalisisVerticalController@consultarlstEntidad');

    //VAL CUENTA ENTIDAD
    Route::post('analisisVerConsulValCuentaEntidad', 'Autoevaluacion\AnalisisVerticalController@consultaValCuentaEntidadXPeriodo');
    Route::post('consultaValCuentaEntidad', 'Autoevaluacion\AnalisisVerticalController@consultaValCuentaEntidad');
    Route::post('buscarLstValCtaEntidadxFiltro', 'Autoevaluacion\AnalisisVerticalController@buscarLstValCtaEntidadxFiltro');

    //ANALISIS PRECONFIGURADO
    Route::resource('analisisPreConfig', 'Autoevaluacion\AnalisisPreConfigController');
    Route::post('consultaValCuentaEntidadXPeriodo', 'Autoevaluacion\AnalisisPreConfigController@consultaValCuentaEntidadXPeriodo');
    Route::post('consultaValoresCuentas', 'Autoevaluacion\AnalisisPreConfigController@consultaValoresCuentas');
    Route::post('imprimirExcelAnalisis', 'Autoevaluacion\AnalisisPreConfigController@imprimirExcelAnalisis');

    //EJECUCION DE INDICADORES
    Route::resource('ejeIndicador', 'Autoevaluacion\EjeIndicadorController');
    Route::post('consultaValoresCuentas', 'Autoevaluacion\EjeIndicadorController@consultaValoresCuentas');
    Route::post('ejecutarIndicadores', 'Autoevaluacion\EjeIndicadorController@ejecutarIndicadores');
    Route::post('guardarIndicadorTemporal', 'Autoevaluacion\EjeIndicadorController@guardarIndicadorTemporal');
    Route::post('imprimirPdfEjeIndicador', 'Autoevaluacion\EjeIndicadorController@imprimirPdfEjeIndicador');
    Route::post('imprimirExcelEjeIndicador', 'Autoevaluacion\EjeIndicadorController@imprimirExcelEjeIndicador');

   
});
