<?php

namespace App\Http\Controllers\administracion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entidades\DivisionPolitica;
use App\Entidades\Auditoria;
use App\Entidades\Estado;
use App\Entidades\Categoria;
use App\Entidades\ParametroGeneral;
use Validator;
use Auth;
use Illuminate\Support\Facades\Session;

class DivisionPoliticaController extends Controller {

    /**
     * AUTOR: JTRIVIÑO
     * DESCRIPCION: REALIZA CARGAR PANTALLA: DIVISION POLITICA
     * @return type
     */
    public function index() {
        try {
            $objDivisionPolitica = DivisionPolitica::consultarDivisionPolitica();
            $objParametroGeneral = ParametroGeneral::buscarParametroGeneral('USUARIO ENTIDAD');
            $permiso = false;
            Session::forget('message');
            Session::forget('messageError');
            if ($objParametroGeneral != null) {
                if (Auth::user()->perfilIngreso->perfil_ingreso_id == $objParametroGeneral->parametro_general_descripcion) {
                    $permiso = false;
                    Session::flash('message', 'Usuario con acceso para visualizar información.');
                } else {
                    $permiso = true;
                }
            } else {
                $permiso = false;
                Session::flash('messageError', 'No se ha configurado Parámetro General: Usuario Entidad');
            }
            return view('administracion.divisionpolitica', compact('objDivisionPolitica', 'permiso'));
        } catch (\Exception $ex) {
            Session::flash('messageError', $ex->getMessage());
            return redirect()->back();
        }
    }

    /**
     * AUTOR: JTRIVIÑO
     * DESCRIPCION: MÉTODO REALIZA ACCIÓN VALIDAR: DIVISION
     * @param Request $request
     * @return type
     */
    public function validarDivisionPolitica(Request $request) {
        try {
            $lstError = '';
            $id = '';
            $array = '';
            $objDivisionPolitica = DivisionPolitica::find($request->division_politica_id);
            $messages = [
                'division_politica_codigo.required' => 'Campo código es requerido.',
                'division_politica_nombre.required' => 'Campo nombre es requerido.',
                'division_politica_codigo.unique' => 'Campo código ya se encuentra en uso.',
                'division_politica_nombre.unique' => 'Campo nombre ya se encuentra en uso.',
                'division_politica_capital.required' => 'Campo capital es requerido.',
                'division_politica_id.required' => 'Campo división política es requerido.',
            ];
            //actualizar
            if ($request->accion == 'A') {
                $validator = Validator::make($request->all(), [
                            'division_politica_id' => 'required|numeric',
                            'division_politica_codigo' => 'required|unique:division_politica,division_politica_codigo,' . $request->division_politica_id . ',division_politica_id',
                            'division_politica_nombre' => 'required|min:3|max:254|unique:division_politica,division_politica_nombre,' . $request->division_politica_id . ',division_politica_id',
                            'division_politica_capital' => 'required|max:1',
                                ], $messages);
                if ($validator->fails()) {
                    $lstError = $validator->errors();
                } else {
                    if ($objDivisionPolitica != null) {
                        if ($objDivisionPolitica->division_politica_tipo == DivisionPolitica::$canton && $request->division_politica_capital == 'S') {
                            $count = DivisionPolitica::with('division_politica_division_politica_id', $$objDivisionPolitica->division_politica_division_politica_id)->where('division_politica_capital', 'S')->count();
                            if ($count > 0) {
                                $array = array("La provincia seleccionada ya tiene registrado una capital.");
                                $lstError = $array;
                            }
                        }
                    }
                }
            } else {
                //guardar
                $validator = Validator::make($request->all(), [
                            'division_politica_id' => 'required|numeric',
                            'division_politica_codigo' => 'required|unique:division_politica',
                            'division_politica_nombre' => 'required|min:3|max:254|unique:division_politica',
                            'division_politica_capital' => 'required|max:1',
                                ], $messages);
                if ($validator->fails()) {
                    $lstError = $validator->errors();
                } else {
                    if ($objDivisionPolitica != null) {
                        if ($objDivisionPolitica->division_politica_tipo == DivisionPolitica::$canton) {
                            $array = array("No se puede ingresar una división politica dentro de un cantón");
                            $lstError = $array;
                        }
                        
                        if ($objDivisionPolitica->division_politica_tipo == DivisionPolitica::$provincia && $request->division_politica_capital == 'S') {
                            $count = DivisionPolitica::with('division_politica_division_politica_id', $request->division_politica_id)->where('division_politica_capital', 'S')->count();
                            if ($count > 0) {
                                $array = array("La provincia seleccionada ya tiene registrado una capital.");
                                $lstError = $array;
                            }
                        }
                    }
                }
            }




            return $lstError;
        } catch (\Exception $ex) {
            Session::flash('messageError', $ex->getMessage());
            return redirect()->back();
        }
    }

    /**
     * AUTOR: JTRIVIÑO
     * DESCRIPCION: REALIZA ACCIÓN GUARDAR: DIVISION POLITICA
     * @param Request $request
     * @return type
     */
    public function guardaDivisionPolitica(Request $request) {
        $interfaz = url()->previous();
        try {
            $objDivisionPolitica = DivisionPolitica::find($request->division_politica_id);
            if ($objDivisionPolitica != null) {
                $divisionpolitica = new DivisionPolitica();
                if ($objDivisionPolitica->division_politica_tipo == DivisionPolitica::$provincia) {
                    $divisionpolitica->division_politica_tipo = DivisionPolitica::$canton;
                    $divisionpolitica->division_politica_codigo = $request->division_politica_codigo;
                    $divisionpolitica->estado_id = Estado::$estadoActivo;
                    $divisionpolitica->division_politica_division_politica_id = $request->division_politica_id;
                    $divisionpolitica->categoria_id = Categoria::consultaCategoriaxDefecto()->categoria_id;
                    $divisionpolitica->division_politica_nombre = strtoupper($request->division_politica_nombre);
                    $divisionpolitica->division_politica_capital = $request->division_politica_capital;
                    $divisionpolitica->save();
                    Auditoria::ingresoAuditoria(Auditoria::$guardar, $interfaz);
                    Session::flash('message', 'DIVISIÓN POLÍTICA: Provincia ha sido registrado con éxito.');
                    return redirect()->back();
                }
                if ($objDivisionPolitica->division_politica_tipo == DivisionPolitica::$region) {
                    $divisionpolitica->division_politica_tipo = DivisionPolitica::$provincia;
                    $divisionpolitica->division_politica_codigo = $request->division_politica_codigo;
                    $divisionpolitica->estado_id = Estado::$estadoActivo;
                    $divisionpolitica->division_politica_division_politica_id = $request->division_politica_id;
                    $divisionpolitica->categoria_id = Categoria::consultaCategoriaxDefecto()->categoria_id;
                    $divisionpolitica->division_politica_nombre = strtoupper($request->division_politica_nombre);
                    $divisionpolitica->division_politica_capital = $request->division_politica_capital;
                    $divisionpolitica->save();
                    Auditoria::ingresoAuditoria(Auditoria::$guardar, $interfaz);
                    Session::flash('message', 'DIVISIÓN POLÍTICA: Región ha sido registrado con éxito.');
                    return redirect()->back();
                }
            } else {
                Session::flash('messageError', 'No se ha encontrado Divisón Política.');
                return redirect()->back();
            }
        } catch (\Exception $ex) {
            Session::flash('messageError', $ex->getMessage());
            return redirect()->back();
        }
    }

    /**
     * AUTOR: JTRIVIÑO
     * DESCRIPCION: MÉTODO REALIZA ACCIÓN BUSCAR: DIVISION POLITICA
     * @param Request $request
     * @return type
     */
    public function buscarObjDivisionPolitica(Request $request) {
        try {
            $objDivisionPolitica = DivisionPolitica::find($request->id);
            return $objDivisionPolitica;
        } catch (\Exception $ex) {
            Session::flash('messageError', $ex->getMessage());
            return redirect()->back();
        }
    }

    /**
     * AUTOR: JTRIVIÑO
     * DESCRIPCION: REALIZA ACCIÓN ACTUALIZAR: DIVISION POLITICA
     * @param Request $request
     * @return type
     */
    public function actualizaDivisionPolitica(Request $request) {
        $interfaz = url()->previous();
        try {
            $objActualizaDivision = DivisionPolitica::find($request->division_politica_id);
            $objActualizaDivision->division_politica_codigo = $request->division_politica_codigo;
            if ($request->estado_id == Estado::$estadoActivo) {
                $objActualizaDivision->estado_id = Estado::$estadoActivo;
            } else {
                $objActualizaDivision->estado_id = Estado::$estadoInactivo;
            }
            $objActualizaDivision->division_politica_nombre = strtoupper($request->division_politica_nombre);
            $objActualizaDivision->division_politica_capital = $request->division_politica_capital;
            $objActualizaDivision->save();
            Auditoria::ingresoAuditoria(Auditoria::$actualizar, $interfaz);
            Session::flash('message', 'DIVISIÓN POLÍTICA: ha sido actualizado con éxito.');
            return redirect()->back();
        } catch (\Exception $ex) {
            Session::flash('messageError', $ex->getMessage());
            return redirect()->back();
        }
    }

    /**
     * AUTOR: JTRIVIÑO
     * DESCRIPCION: BUSCA POR ID: DIVISION POLITICA
     * @param Request $request
     * @return type
     */
    public function buscarDivisionPolitica(Request $request) {
        try {
            $objConsultaDivision = DivisionPolitica::buscarDivisionPolitica($request);
            return $objConsultaDivision;
        } catch (\Exception $ex) {
            Session::flash('messageError', $ex->getMessage());
            return redirect()->back();
        }
    }

}
