<?php

namespace App\Http\Controllers\Administracion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entidades\TipoCuenta;
use App\Entidades\TipoFuente;
use App\Entidades\Cuenta;
use App\Entidades\Auditoria;
use Validator;
use Auth;

class CtasFinPersonalizadasController extends Controller {

    /**
     * 
     * @return type
     */
    public function index() {
        
         $lstCuenta = [TipoCuenta::$contable, TipoCuenta::$presupuestaria];
        $lstTipoCuenta = TipoCuenta::buscarLstTipoCuenta($lstCuenta);
        $lstCodigo = [TipoFuente::$fuenteMF,  TipoFuente::$fuenteBsE];
        $lstTipoFuente = TipoFuente::buscarLstTipoFuentexId($lstCodigo);
        return view('administracion.ctasFinPersonalizadas', compact('lstTipoCuenta','lstTipoFuente'));
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function buscarCuentaxTipo(Request $request) {
        $tipoCuenta = $request->tipoCuenta;
        $tipoFuente = TipoFuente::$fuenteMF;
        $lstCuenta = Cuenta::buscarLstCuentaxTipoxFuente($tipoCuenta, $tipoFuente);

        return $lstCuenta;
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function buscarCuentaSeleccionada(Request $request) {
        $cuenta_id = $request->cuenta_id;
        $objCuenta = Cuenta::find($cuenta_id);
        return $objCuenta;
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function guardarCtaPersonalizada(Request $request) {
        try {
            $messages = [
                'cuenta.required' => 'Campo :attribute es requerido.',
                'cuenta.max' => 'Campo :attribute debe tener un tamaño de :max.',
                'cuenta.unique' => 'Campo :attribute ya se encuentra registrado.',
            ];
            $validator = Validator::make($request->all(), [
                        'cuenta_cuenta_id' => 'max:11',
                        'cuenta_codigo' => 'required|unique:cuenta|max:255',
                        'cuenta_nombre' => 'required|unique:cuenta|max:255',
                        'tipo_cuenta_id' => 'required|max:11'
                            ], $messages);

            if ($validator->fails()) {
                return  redirect('catalogoCtasFinPersonalizadas')
                                ->withErrors($validator)
                                ->withInput();
            } else {

                $objCuenta = new Cuenta($request->all());
                $objCuenta->entidad_id = Auth::user()->entidad_id;
                $objCuenta->tipo_fuente_id =TipoFuente::$fuenteMF;

                $resultNivel = Cuenta::where('cuenta_cuenta_id', $request->cuenta_id_editar)->count();
                $nivel = $resultNivel + 1;
                $objCuenta->cuenta_nivel = $nivel;
                $objCuenta->cuenta_cuenta_id = $request->cuenta_id_editar;
                
                
                $objCuenta->save();

                Auditoria::ingresoAuditoria(Auditoria::$guardar, url()->previous());

                notify()->flash('CUENTA PERSONALIZADA: ' . $objCuenta->cuenta_codigo . ' ' . $objCuenta->cuenta_nombre . ' ha sido registrado con éxito.', 'success');
               
                return redirect()->back();
            }
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
            return redirect()->back();
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function actualizarCtaPersonalizada(Request $request) {
        try {
            $messages = [
                'cuenta.required' => 'Campo :attribute es requerido.',
                'cuenta.max' => 'Campo :attribute debe tener un tamaño de :max.',
                'cuenta.unique' => 'Campo :attribute ya se encuentra registrado.',
            ];

            $validator = Validator::make($request->all(), [
                        'cuenta_codigo' => 'required|max:255|unique:cuenta,cuenta_codigo,' . $request->cuenta_id_editar . ',cuenta_id',
                        'cuenta_nombre' => 'required|max:255|unique:cuenta,cuenta_nombre,' . $request->cuenta_id_editar . ',cuenta_id',
                            ], $messages);

            if ($validator->fails()) {
                return redirect('catalogoCtasFinPersonalizadas')
                                ->withErrors($validator)
                                ->withInput();
            } else {
                $objCuenta = Cuenta::find($request->cuenta_id_editar);
                $objCuenta->cuenta_codigo = $request->cuenta_codigo;
                $objCuenta->cuenta_nombre = $request->cuenta_nombre;
                $objCuenta->save();

                Auditoria::ingresoAuditoria(Auditoria::$actualizar, url()->previous());

                notify()->flash('CUENTA PERSONALIZADA: ' . $objCuenta->cuenta_codigo . ' ' . $objCuenta->cuenta_nombre . ' ha sido actualizado con éxito.', 'success');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

}
