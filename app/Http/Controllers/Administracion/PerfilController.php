<?php

namespace App\Http\Controllers\Administracion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entidades\Interfaz;
use App\Entidades\PerfilIngreso;
use App\Entidades\Estado;
use App\Entidades\Auditoria;
use App\Entidades\InterfazRelacionaPerfil;
use App\Entidades\ParametroGeneral;
use Validator;
use Illuminate\Support\Facades\Session;

class PerfilController extends Controller {

    /**
     * AUTOR: GCASTILLO
     * DESCRIPCION: MÉTODO REALIZA CARGAR PANTALLA: PERFIL
     * @return type
     */
    public function index() {
        try {
            $interfaz = Interfaz::buscarInterfaz();
            $lstPerfilIngreso = PerfilIngreso::All();
            $i = 0;
            foreach ($interfaz as $interfaz) {
                $lstInterfaz[$i] = array('id' => $interfaz->interfaz_id, 'text' => $interfaz->interfaz_descripcion);
                $i++;
            }
            return view('administracion.perfil', compact('lstInterfaz', 'lstPerfilIngreso'));
        } catch (\Exception $ex) {
            Session::flash('messageError', $ex->getMessage());
            return redirect()->back();
        }
    }

    /**
     * AUTOR: GCASTILLO
     * DESCRIPCION: MÉTODO REALIZA ACCIÓN VALIDAR: PERFIL
     * @param Request $request
     * @return type
     */
    public function validarPerfil(Request $request) {
        try {
            $lstError = '';
            $messages = [
                'perfil_ingreso.required' => 'Campo :attribute es requerido.',
                'perfil_ingreso.max' => 'Campo :attribute debe tener un tamaño de :max.',
                'perfil_ingreso.unique' => 'Campo :attribute ya se encuentra registrado.',
            ];
            //actualizar
            if ($request->perfil_ingreso_id != '') {
                $validator = Validator::make($request->all(), [
                            'perfil_ingreso_nombre' => 'max:100 |required||unique:perfil_ingreso,perfil_ingreso_nombre,' . $request->perfil_ingreso_id . ',perfil_ingreso_id',
                            'perfil_ingreso_descripcion' => 'max:100|required|',
                            'perfil_ingreso_interfaz' => '|required|',
                                ], $messages);
            } else {
                //guardar
                $validator = Validator::make($request->all(), [
                            'perfil_ingreso_nombre' => 'max:100|required|unique:perfil_ingreso',
                            'perfil_ingreso_descripcion' => 'max:100|required|',
                            'perfil_ingreso_interfaz' => '|required|',
                                ], $messages);
            }
            if ($validator->fails()) {
                $lstError = $validator->errors();
            }
            return $lstError;
        } catch (\Exception $ex) {
            Session::flash('messageError', $ex->getMessage());
            return redirect()->back();
        }
    }

    /**
     * AUTOR: GCASTILLO
     * DESCRIPCION: MÉTODO REALIZA ACCIÓN GUARDAR: PERFIL
     * @param Request $request
     * @return type
     */
    public function guardarPerfil(Request $request) {
        $interfaz = url()->previous();
        try {
            /* Guarda el Perfil Ingreso */
            $lstInterfaz = array();
            $objPerfilIngreso = new PerfilIngreso();
            $objPerfilIngreso->perfil_ingreso_nombre = strtoupper($request->perfil_ingreso_nombre);
            $objPerfilIngreso->perfil_ingreso_descripcion = $request->perfil_ingreso_descripcion;
            $objPerfilIngreso->estado_id = Estado::$estadoActivo;
            $string = $request->perfil_ingreso_interfaz;
            $lstInterfaz = explode(',', $string);
            $objPerfilIngreso->save();
            /* Recorre la lista de interfaces seleccionadas */
            foreach ($lstInterfaz as $lstInterfaz) {
                $objInterfazRelacionaPerfil = new InterfazRelacionaPerfil();
                $objInterfazRelacionaPerfil->interfaz_id = $lstInterfaz;
                $objInterfazRelacionaPerfil->perfil_ingreso_id = $objPerfilIngreso->perfil_ingreso_id;
                $objInterfazRelacionaPerfil->save();
            }
            Auditoria::ingresoAuditoria(Auditoria::$guardar, $interfaz);
            $objParametroGeneral = new ParametroGeneral();
            $objParametroGeneral->parametro_general_nombre = $objPerfilIngreso->perfil_ingreso_nombre;
            $objParametroGeneral->estado_id = Estado::$estadoActivo;
            $objParametroGeneral->save();
            Session::flash('message', 'PERFIL: ha sido registrado con éxito.');
            return redirect()->back();
        } catch (\Exception $ex) {
            Session::flash('messageError', $ex->getMessage());
            return redirect()->back();
        }
    }

    /**
     * AUTOR: GCASTILLO
     * DESCRIPCION: MÉTODO REALIZA ACCIÓN BUSCAR: PERFIL
     * @param Request $request
     * @return type
     */
    public function buscarPerfil(Request $request) {
        try {
            $perfil_ingreso_nombre = strtoupper($request->perfil_ingreso_nombre);
            $objPerfilIngreso = PerfilIngreso::buscarPerfilIngreso($perfil_ingreso_nombre);
            $result = "";
            if ($objPerfilIngreso) {
                $perfil_ingreso_id = $objPerfilIngreso->perfil_ingreso_id;
                $perfil_ingreso_descripcion = $objPerfilIngreso->perfil_ingreso_descripcion;
                $interfazRelacionaPerfil = InterfazRelacionaPerfil::buscarInterfazRelacionaPerfil($perfil_ingreso_id);
                $i = 0;
                if ($interfazRelacionaPerfil->count() > 0) {
                    foreach ($interfazRelacionaPerfil as $interfaz) {
                        $lstInterfazRelacionaPerfil[$i] = array('id' => $interfaz->interfaz->interfaz_id, 'text' => $interfaz->interfaz->interfaz_descripcion);
                        $i++;
                    }
                    $result = array('perfil_ingreso_descripcion' => $perfil_ingreso_descripcion, 'perfil_ingreso_id' => $perfil_ingreso_id, 'lstInterfazRelacionaPerfil' => $lstInterfazRelacionaPerfil);
                } else
                    $result = array('perfil_ingreso_descripcion' => $perfil_ingreso_descripcion, 'perfil_ingreso_id' => $perfil_ingreso_id);
            }
            return $result;
        } catch (\Exception $ex) {
            Session::flash('messageError', $ex->getMessage());
            return redirect()->back();
        }
    }

    /**
     * AUTOR: GCASTILLO
     * DESCRIPCION: MÉTODO REALIZA ACCIÓN ACTUALIZAR: PERFIL
     * @param Request $request
     * @return type
     */
    public function actualizarPerfil(Request $request) {
        $interfaz = url()->previous();
        try {
            /* Actualizar el Perfil Ingreso */
            $lstInterfaz = array();
            $perfil_ingreso_nombre = strtoupper($request->perfil_ingreso_nombre);
            $perfil_ingreso_id = $request->perfil_ingreso_id;
            $objPerfilIngreso = PerfilIngreso::find($perfil_ingreso_id);
            /* Busca parametro general del perfil */
            $objParametroGeneral = ParametroGeneral::buscarParametroGeneral($objPerfilIngreso->perfil_ingreso_nombre);
            $objPerfilIngreso->perfil_ingreso_nombre = $perfil_ingreso_nombre;
            $objPerfilIngreso->perfil_ingreso_descripcion = $request->perfil_ingreso_descripcion;
            $objPerfilIngreso->estado_id = Estado::$estadoActivo;
            $string = $request->perfil_ingreso_interfaz;
            $lstInterfaz = explode(',', $string);
            $objPerfilIngreso->save();
            /* Elimina las interfaces antes asignadas */
            $elimarInterfazRelacionPerfil = InterfazRelacionaPerfil::where('perfil_ingreso_id', $objPerfilIngreso->perfil_ingreso_id)->delete();
            /* Recorre la lista de interfaces seleccionadas */
            foreach ($lstInterfaz as $lstInterfaz) {
                $objInterfazRelacionaPerfil = new InterfazRelacionaPerfil();
                $objInterfazRelacionaPerfil->interfaz_id = $lstInterfaz;
                $objInterfazRelacionaPerfil->perfil_ingreso_id = $objPerfilIngreso->perfil_ingreso_id;
                $objInterfazRelacionaPerfil->save();
            }
            Auditoria::ingresoAuditoria(Auditoria::$actualizar, $interfaz);
            /* Actualizar Parametros */
            if ($objParametroGeneral) {
                $objParametroGeneral->parametro_general_nombre = $objPerfilIngreso->perfil_ingreso_nombre;
                $objParametroGeneral->estado_id = Estado::$estadoActivo;
                $objParametroGeneral->save();
            }
            Session::flash('message', 'PERFIL: ha sido actualizado con éxito.');
            return redirect()->back();
        } catch (\Exception $ex) {
            Session::flash('messageError', $ex->getMessage());
            return redirect()->back();
        }
    }

}
