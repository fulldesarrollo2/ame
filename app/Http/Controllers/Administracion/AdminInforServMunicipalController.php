<?php

namespace App\Http\Controllers\Administracion;
use App\Entidades\DatoCosteo;
use App\Entidades\SubSistema;
use App\Entidades\UnidadServicio;
use App\Entidades\VariableCosteo;
use App\Entidades\VariableSeguridad;
use App\Entidades\catalogoDatoCosteo;
use App\Entidades\proyectoServicio;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
class AdminInforServMunicipalController extends Controller
{
    //
    public function index()
    {
        $lstProyectoServicio=proyectoServicio::buscarProyecServActivo();
        $variableSeguidad=VariableSeguridad::$periodoActual;
        $subirArchivo=false;
        $objVariableSeguridad=VariableSeguridad::ConsultaVariable($variableSeguidad);
        $lstUnidadServicio=unidadServicio::whereNull('unidad_servicio_codigo')->get();


        return view('administracion.adminInforServiciosMunicipales', compact('lstProyectoServicio','objVariableSeguridad','lstUnidadServicio'));        
    }

    public function guardarInfoServBasicosMuninc(Request $request)
    {
      //  dd($request->all());
        $variableCosteoId = VariableCosteo::buscarVariableCosteoXunidadServicio($request->subTipo);
        
        /*===============================================================================================
        =            Se busca si en la tabla variable_costeo existe un registro con ese dato
                     De lo contrario se crea
        ===============================================================================================*/
        
        if($variableCosteoId->count() > 0 ){
            $variableCosteoId = $variableCosteoId->first()->unidad_servicio_id;
         }else{
            $variableCosteoId = new VariableCosteo();
            $variableCosteoId->unidad_servicio_id = $request->subTipo;
            //NOTA: COMO NO SE ESPECIFICA DE DONDE SALE EL TIPO SE ASUMIO 1
            $variableCosteoId->variable_costeo_tipo = '1';
            $variableCosteoId->save();
            
            $variableCosteoId = $request->subTipo;
            // SE INSERTAN LOS DATOS EN LA TABLA catalogo_dato_costeo

            $catalogo = new catalogoDatoCosteo();
            $catalogo->unidad_servicio_id = $request->subTipo;
            $catalogo->catalogo_dato_costeo_nombre = 'UBICACION';
            $catalogo->catalogo_dato_costeo_tipo_dato = 'SISTEMA UTM';
            $catalogo->catalogo_dato_costeo_logitud_dato = 8;
            $catalogo->save();

            $catalogo = new catalogoDatoCosteo();
            $catalogo->unidad_servicio_id = $request->subTipo;
            $catalogo->catalogo_dato_costeo_nombre = '# UNIDADES';
            $catalogo->catalogo_dato_costeo_tipo_dato = 'NUMERICO';
            $catalogo->catalogo_dato_costeo_logitud_dato = 4;
            $catalogo->save();

            $catalogo = new catalogoDatoCosteo();
            $catalogo->unidad_servicio_id = $request->subTipo;
            $catalogo->catalogo_dato_costeo_nombre = 'CAPACIDAD';
            $catalogo->catalogo_dato_costeo_tipo_dato = 'NUMERICO';
            $catalogo->catalogo_dato_costeo_logitud_dato = 4;
            $catalogo->save();

            $catalogo = new catalogoDatoCosteo();
            $catalogo->unidad_servicio_id = $request->subTipo;
            $catalogo->catalogo_dato_costeo_nombre = 'CAUDAL ENTRADA L/S';
            $catalogo->catalogo_dato_costeo_tipo_dato = 'NUMERICO';
            $catalogo->catalogo_dato_costeo_logitud_dato = 4;
            $catalogo->save();

            $catalogo = new catalogoDatoCosteo();
            $catalogo->unidad_servicio_id = $request->subTipo;
            $catalogo->catalogo_dato_costeo_nombre = 'CAUDAL SALIDA L/S';
            $catalogo->catalogo_dato_costeo_tipo_dato = 'NUMERICO';
            $catalogo->catalogo_dato_costeo_logitud_dato = 4;
            $catalogo->save();

            $catalogo = new catalogoDatoCosteo();
            $catalogo->unidad_servicio_id = $request->subTipo;
            $catalogo->catalogo_dato_costeo_nombre = 'PERDIDAS %';
            $catalogo->catalogo_dato_costeo_tipo_dato = 'DECIMAL';
            $catalogo->catalogo_dato_costeo_logitud_dato = 5;
            $catalogo->save();

            $catalogo = new catalogoDatoCosteo();
            $catalogo->unidad_servicio_id = $request->subTipo;
            $catalogo->catalogo_dato_costeo_nombre = 'CAUDAL SALIDA(L/DIA)';
            $catalogo->catalogo_dato_costeo_tipo_dato = 'NUMERICO';
            $catalogo->catalogo_dato_costeo_logitud_dato = 4;
            $catalogo->save();

            $catalogo = new catalogoDatoCosteo();
            $catalogo->unidad_servicio_id = $request->subTipo;
            $catalogo->catalogo_dato_costeo_nombre = 'CAUDAL SALIDA(M3/AÑO)';
            $catalogo->catalogo_dato_costeo_tipo_dato = 'NUMERICO';
            $catalogo->catalogo_dato_costeo_logitud_dato = 4;
            $catalogo->save();

            $catalogo = new catalogoDatoCosteo();
            $catalogo->unidad_servicio_id = $request->subTipo;
            $catalogo->catalogo_dato_costeo_nombre = 'TIEMPO DE EXISTENCIA';
            $catalogo->catalogo_dato_costeo_tipo_dato = 'NUMERICO';
            $catalogo->catalogo_dato_costeo_logitud_dato = 4;
            $catalogo->save();

            $catalogo = new catalogoDatoCosteo();
            $catalogo->unidad_servicio_id = $request->subTipo;
            $catalogo->catalogo_dato_costeo_nombre = 'VIDA UTIL';
            $catalogo->catalogo_dato_costeo_tipo_dato = 'NUMERICO';
            $catalogo->catalogo_dato_costeo_logitud_dato = 4;
            $catalogo->save();

            $catalogo = new catalogoDatoCosteo();
            $catalogo->unidad_servicio_id = $request->subTipo;
            $catalogo->catalogo_dato_costeo_nombre = 'ESTADO DE FUNCIONAMIENTO';
            $catalogo->catalogo_dato_costeo_tipo_dato = 'VARCHAR';
            $catalogo->catalogo_dato_costeo_logitud_dato = 15;
            $catalogo->save();

         }

        /*=====  End of Se busca si en la tabla variable_costeo existe un registro con ese dato  ======*/
         $datoCosteo = new DatoCosteo();
         $datoCosteo->periodo_id = $request->seguridadPeriodo;
         $datoCosteo->catalogo_dato_costeo_id = $request->subTipo;
         $datoCosteo->sub_sistema_id = $request->subsistema;
         $variableCosteoId = VariableCosteo::where('unidad_servicio_id','=',$request->subTipo)->get();
         $variableCosteoId = $variableCosteoId->first()->variable_costeo_id;
         $datoCosteo->variable_costeo_id = $variableCosteoId;
         
         $datoCosteo->save();

         Session::flash('message', 'El registro ha sido Almacenado');
            return redirect()->back();

        
    }
    
    public function buscarSubsistemaXproyecto(Request $request){

        if($request->proyecto_servicio_id!=null || $request->proyecto_servicio_id!=''){ 
            $lstSubsistema=SubSistema::buscarSubsistemaXproyecto($request->proyecto_servicio_id);
            return $lstSubsistema;    
        }
    }
    
    public function buscarVariableCosteoXunidadServicio(Request $request){
            $lstVariableCosteo=unidadServicio::buscarTipoXUnidadServicio($request->unidad_servicio_id);
            return $lstVariableCosteo;
    }    
    public function buscarSubTipoXUnidadServicio(Request $request){

        $lstSubTipo = unidadServicio::buscarSubTipo($request->tipoServ_id);
        return $lstSubTipo;
    }
    
    
}
