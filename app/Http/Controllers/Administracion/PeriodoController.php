<?php

namespace App\Http\Controllers\Administracion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Entidades\Periodo;
use Carbon\Carbon;
use App\Entidades\Estado;
use App\Entidades\VariableSeguridad;
use Validator;
use Illuminate\Support\Facades\Storage;
use App\Entidades\ArchivoCarga;
use Auth;

use App\Entidades\Entidad;
use App\Entidades\PerfilIngreso;
use Illuminate\Support\Facades\File;
use App\Entidades\ParametroGeneral;
use App\Entidades\Auditoria;
use App\Entidades\ValCuentaEntidad;
class PeriodoController extends Controller
{
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            
         $fecha=Carbon::now();
         $mes=0;
         $objPeriodo= Periodo::consultaPeriodoAbierto($mes);
         $objEntidad= Entidad::find(Auth::user()->entidad_id);
         $variableSeguidad=VariableSeguridad::$periodoActual;
         $subirArchivo=false;
         $objVariableSeguridad=VariableSeguridad::ConsultaVariable($variableSeguidad);
         
         
         if(Auth::user()->perfil_ingreso_id==PerfilIngreso::$entidad){
            if($objPeriodo->periodo_anio!=$objVariableSeguridad->variable_seguridad_valor)
            {
               Session::flash('message', 'El valor de la variable de seguridad no es igual al perido actual, Proceda a cerrar el periodo actual.'); 
               $subirArchivo=false; 
               return view('administracion.periodo',compact('objPeriodo','subirArchivo'));
            }
            if($objEntidad->entidad_periodo_vigente!=$objVariableSeguridad->variable_seguridad_valor)
            {
               Session::flash('message', 'Para actualizar entidad con el nuevo periodo vigente  suba el  archivo de la información financiera del cierre del ejercicio .');
               $subirArchivo=true;
               return view('administracion.periodo',compact('objPeriodo','subirArchivo'));
            }
            if($objEntidad->entidad_periodo_vigente==$objVariableSeguridad->variable_seguridad_valor){
               Session::flash('message', ' Entidad Actualizada con el periodo vigente .');
               $subirArchivo=false; 
               return view('administracion.periodo',compact('objPeriodo','subirArchivo'));
            }
         }
          if(Auth::user()->perfil_ingreso_id==PerfilIngreso::$ameTecnico){
              return view('administracion.periodo',compact('objPeriodo','subirArchivo'));}
              
               }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                   return redirect()->back();
           } 
          
    }
          
    
    
    /*
     * Descripcion: Metodo de cierre de periodo
     */
    
    public function cierrePeriodo(Request $request){
        try {
            
                $fecha=Carbon::now();
                $lstobjPeriod= Periodo::consulPeriodoAbierto();//find($request->periodo_id);
                $periodoAnt;
                $ultimoPeriodo;
                
                $variableSeguidad=VariableSeguridad::$periodoActual;
                $objVariableSeguridad=VariableSeguridad::ConsultaVariable($variableSeguidad);
                if($objVariableSeguridad==null){
                     Session::flash('message',  ' No se ha parametrizado la variable de seguridad del periodo actual' );
                     return redirect()->back();
                }
                
                if(!empty($lstobjPeriod)){
                    foreach ($lstobjPeriod as $select){
                        $select->estado_id=Estado::$estadoCerrado;
                        $select->save();
                        $ultimoPeriodo=$select->periodo_anio;
                        $periodoAnt=$select->periodo_anio;

                    }

                    $ultimoPeriodo=$ultimoPeriodo+1;
                    for($i=0;$i<13;$i++){

                        $objPeriodo= new Periodo();
                        $objPeriodo->periodo_anio=$ultimoPeriodo;
                        $objPeriodo->periodo_mes=$i;
                        $objPeriodo->estado_id=Estado::$estadoActivo;
                        $objPeriodo->save();
                    }

                    $variableSeguidad=VariableSeguridad::$periodoActual;
                    $objVariableSeguridad=VariableSeguridad::ConsultaVariable($variableSeguidad);
                    $objVariableSeguridad->variable_seguridad_valor=$ultimoPeriodo; 
                    $objVariableSeguridad->save();

                    $interfaz=url()->previous();
                    $paramGeneral='cierrePeriodo';
                    $variable=  ParametroGeneral::buscarParametroGeneral($paramGeneral);
                    Auditoria::ingresoAuditoria($variable->parametro_general_id,$interfaz); 
                    Session::flash('message',  ' El Periodo '.$periodoAnt.' cerrado con exito.');
                    return redirect()->back();
                }else{
                     Session::flash('message',  ' No existen Periodos activos para cerrar' );
                     return redirect()->back();
                }
                
                
                         
                    
          }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                   return redirect()->back();
           } 
            
     }
    
    public  function cambioPeriodoFiscal(Request $request) 
    {
        try {
            
             $messages = [
                'archivo_carga.required' => 'Campo :attribute es requerido.',
                'archivo_carga.max' => 'Campo :attribute debe tener un tamaño de :max.',
                'archivo_carga.unique' => 'Campo :attribute ya se encuentra registrado.',
            ];
            $validator = Validator::make($request->all(),[
                'archivo_carga_archivo_byte' => 'required'
            ], $messages);

            if ($validator->fails()) {
                return redirect('periodo')
                                ->withErrors($validator)
                                ->withInput();
            }else{
            
                
                    // Consulto la variable de seguridad    
                    $variableSeguidad=VariableSeguridad::$periodoActual;
                    $objVariableSeguridad=VariableSeguridad::ConsultaVariable($variableSeguidad);
                     if($objVariableSeguridad==null){
                       Session::flash('message',  ' No se ha parametrizado la variable de seguridad del periodo actual' );
                       return redirect()->back();
                     }
                    // Actualizo la actual variable de seguridad en la tabla entidad
                    $objEntidad= Entidad::find(Auth::user()->entidad_id);
                    $lstPeriodo=  Periodo::consultarPeriodoXanio($objEntidad->entidad_periodo_vigente);
                    // consulta el archivo_carga de para obtener el id y borrar los valores de val cuenta entidad
                     if(!empty($lstPeriodo)){
                        foreach ($lstPeriodo  as $select){
                            $objArchioCarga=ArchivoCarga::consultaArchivoCargaPorPeriodo($select->periodo_id);
                            if($objArchioCarga!=null)
                            {  
                              ValCuentaEntidad::removerValoresValuentaEntidad($objArchioCarga->periodo_id,$objEntidad->entidad_id);
                            }
                        }
                    }
                   
                    $objEntidad->entidad_periodo_vigente=$objVariableSeguridad->variable_seguridad_valor;
                    $objEntidad->save();

                    // subo el archivo
                    $pathToFile="";
                    $containfile=false; 
                    if($request->hasFile('archivo_carga_archivo_byte') ){
                        $containfile=true; 
                        $file = $request->file('archivo_carga_archivo_byte');
                        $nombre=$file->getClientOriginalName();
                        $tipo=$file->getClientOriginalExtension();
                        //$tipo=$file->getMimeType();
                        $r=Storage::disk('local')->put($nombre,  \File::get($file));
                        $pathToFile= storage_path('app') ."/". $nombre;
                   }


                    File::delete(storage_path('app') ."/". $nombre);  
                    $objArchivoCarga= new ArchivoCarga();
                    $objArchivoCarga->periodo_id=$request->periodo_id;
                    $objArchivoCarga->usuario_id=$usuario=Auth::user()->usuario_id;
                    $objArchivoCarga->archivo_carga_tipo=$tipo;
                    $objArchivoCarga->archivo_carga_fecha= Carbon::now();
                    $objArchivoCarga->archivo_carga_nombre=$nombre; 
                    $byteArray =file_get_contents($file);
                    //$byteArray = unpack("N*",file_get_contents($file));
                    //$cadena_equipo = implode(";", $byteArray); 
                    $objArchivoCarga->archivo_carga_archivo_byte=$byteArray;
                    $objArchivoCarga->save();
                    
                    
                     
                    
                    $interfaz=url()->previous();
                    $paramGeneral='cambioPeriodoFiscal';
                    $variable=  ParametroGeneral::buscarParametroGeneral($paramGeneral);
                    Auditoria::ingresoAuditoria($variable->parametro_general_id,$interfaz); 
                    Session::flash('message', ' La información fianciera ha sido cerrada.');
                    return redirect()->back();
                }
        
         }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                   return redirect()->back();
           } 
         
    } 
     
    /**
     * 
     */
    public function cambioPeriodo(){
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /**
     * Permite consultar los periodos mesuales o anuales
     * @param Request $request
     * @return type
     */
    public function consultarLstPeriodoDinamico(Request $request){
        $periodo=Periodo::consultarLstPeriodoxTipoFuente($request->tipofuente);
        return $periodo;
    }
}
