<?php

namespace App\Http\Controllers\Administracion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entidades\DivisionPolitica;
use App\Entidades\TipoEntidad;
use App\Entidades\Entidad;
use App\Entidades\Usuario;
use App\Entidades\Auditoria;
use App\Entidades\PerfilIngreso;
use App\Entidades\EntRelacionaDivPo;
use Validator;
use Illuminate\Support\Facades\Session;

class UsuarioController extends Controller
{
    /**
     * AUTOR: GCASTILLO
     * DESCRIPCION: REALIZA CARGAR PANTALLA: USUARIO
     * @return type
     */
    public function index()
    {
        $objDivisionPolitica = DivisionPolitica::consultarDivisionPolitica();
        $objPerfil = PerfilIngreso::buscarPerfilIngresoActivos();
        $objTipoEntidad = TipoEntidad::All();
        $objEntidad = Entidad::All();
        return view('administracion.usuario',compact('objTipoEntidad','objEntidad','objDivisionPolitica','objPerfil'));
        
    }
        /*
     * Metodo: consultaEntRelacionaDivPoTipo
     * NameCreate : GCASTILLO
     * Create: 08/septiembre/2016 
     * Detalle: Metodo parea consultar entidad por division politica y por tipo
    */  

     public function consultaEntRelacionaDivPoTipo(Request $request){

            $tipoEntidad = $request->tipoEntidad;
            $divisionPoliticaId = $request->division_politica_id;
            $lstEntRelacionDivPoTipo= EntRelacionaDivPo::consultaEntidadTipo($divisionPoliticaId,$tipoEntidad);
            return $lstEntRelacionDivPoTipo;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function guardarUsuario(Request $request)
    {
        $interfaz = url()->previous();
        try {
            $messages = [
                'usuario.required' => 'Campo :attribute es requerido.',
                'usuario.max' => 'Campo :attribute debe tener un tamaño de :max.',
                'usuario.unique' => 'Campo :attribute ya se encuentra registrado.',
            ];

            $validator = Validator::make($request->all(), [
                        'usuario_nick' => 'max:100 |required|unique:usuario',
                        'usuario_nombres' => 'max:100  |required|',
                        'usuario_apellidos' => '|required|',
                        'usuario_identificacion' => '|required|',
                        'usuario_telefono' => '|required|numeric',
                        'usuario_email' => '|required|email',
                        'entidad_id' => '|required|numeric',
                        'perfil_id' => '|required|',
                        //'estado_id' => 'max:2 |required|',
                            ], $messages);

        if ($validator->fails()) {
            return redirect('usuario')
                                ->withErrors($validator)
                                ->withInput();
        } else {
                /* Verifica si el perfil ingresado es un perfil administrador*/
            if($request->perfil_id == PerfilIngreso::$administrador)
                {
                    /*Valida si existe un perfil administrador en la entidad seleccionada*/
                    $validarPerfilUsuario = Usuario::where('perfil_ingreso_id', PerfilIngreso::$administrador)
                    ->where('entidad_id', '=', $request->entidad_id)->get();

                    if ($validarPerfilUsuario->count() > 0) {
                        Session::flash('message', 'Error! ya se ha ingresado administrador para la entidad seleccionada.');
                    } else {

                         $objUsuario = new Usuario();
                         $objUsuario->entidad_id = $request->entidad_id; 
                         $objUsuario->perfil_ingreso_id =  $request->perfil_id; 
                         $objUsuario->usuario_nick =  strtoupper($request->usuario_nick); 
                         $objUsuario->usuario_nombres =   $request->usuario_nombres; 
                         $objUsuario->usuario_apellidos =  $request->usuario_apellidos; 
                         $objUsuario->usuario_identificacion  =  $request->usuario_identificacion; 
                         $objUsuario->usuario_telefono = $request->usuario_telefono; 
                         $objUsuario->usuario_email = $request->usuario_email;
                         $objUsuario->usuario_password = hash::make('1234');
                         $estadoId = (isset($request->estado_id)?2:1);
                         $objUsuario->estado_id = $estadoId;
                         $objUsuario->save();

                        Auditoria::ingresoAuditoria(Auditoria::$guardar, $interfaz);

                        Session::flash('message', 'Ha sido registrado con éxito.');
                    }
                }  else{

                         $objUsuario = new Usuario();
                         $objUsuario->entidad_id = $request->entidad_id; 
                         $objUsuario->perfil_ingreso_id =  $request->perfil_id; 
                         $objUsuario->usuario_nick =  strtoupper($request->usuario_nick); 
                         $objUsuario->usuario_nombres =   $request->usuario_nombres; 
                         $objUsuario->usuario_apellidos =  $request->usuario_apellidos; 
                         $objUsuario->usuario_identificacion  =  $request->usuario_identificacion; 
                         $objUsuario->usuario_telefono = $request->usuario_telefono; 
                         $objUsuario->usuario_email = $request->usuario_email;
                         $objUsuario->usuario_password = hash::make('1234');
                         $estadoId = (isset($request->estado_id)?2:1);
                         $objUsuario->estado_id = $estadoId;
                         $objUsuario->save();

                        Auditoria::ingresoAuditoria(Auditoria::$guardar, $interfaz);

                        Session::flash('message', 'Ha sido registrado con éxito.');
                }    
            return redirect()->back();  
        }
            
    } catch (\Exception $e) {
            Session::flash('message', $e->getMessage());
            return redirect()->back();
        }
   
    }

    public function buscarUsuario(Request $request){
        $usuario_nick = strtoupper($request->usuario_nick);
        $objUsuario = Usuario::buscarUsuario($usuario_nick);
        return $objUsuario;
    }

    public function actualizarUsuario(Request $request){
        $interfaz = url()->previous();
        try {
            $messages = [
                'usuario.required' => 'Campo :attribute es requerido.',
                'usuario.max' => 'Campo :attribute debe tener un tamaño de :max.',
                'usuario.unique' => 'Campo :attribute ya se encuentra registrado.',
            ];

            $validator = Validator::make($request->all(), [
                        'usuario_nick' => 'max:100 |required|',
                        'usuario_nombres' => 'max:100  |required|',
                        'usuario_apellidos' => '|required|',
                        'usuario_identificacion' => '|required|',
                        'usuario_telefono' => '|required|',
                        'usuario_email' => '|required|',
                        'entidad_id' => '|required|',
                        'perfil_id' => '|required|',
                            ], $messages);

            if ($validator->fails()) {
                return redirect('usuario')
                                ->withErrors($validator)
                                ->withInput();
            } else {

                $usuario_nick = strtoupper($request->usuario_nick); 
                $usuario_id = $request->usuario_id;

                $validarUsuarioNick= Usuario::where('usuario_nick', $usuario_nick)->
                                where('usuario_id', '!=', $usuario_id)->get();


                if ($validarUsuarioNick->count() > 0) {
                    Session::flash('message', 'El nombre de usuarioya se encuentra en uso.');
                } else {
                    /*Actualizar el Perfil Ingreso */
                     $objUsuario = Usuario::find($request->usuario_id);
                     $objUsuario->entidad_id = $request->entidad_id; 
                     $objUsuario->perfil_ingreso_id =  $request->perfil_id; 
                     $objUsuario->usuario_nick =  strtoupper($request->usuario_nick); 
                     $objUsuario->usuario_nombres =   $request->usuario_nombres; 
                     $objUsuario->usuario_apellidos =  $request->usuario_apellidos; 
                     $objUsuario->usuario_identificacion  =  $request->usuario_identificacion; 
                     $objUsuario->usuario_telefono = $request->usuario_telefono; 
                     $objUsuario->usuario_email = $request->usuario_email;
                     $estadoId = (isset($request->estado_id)?2:1);
                     $objUsuario->estado_id = $estadoId;
                     $objUsuario->save();

                    Auditoria::ingresoAuditoria(Auditoria::$actualizar, $interfaz);

                    Session::flash('message', 'Ha sido actualizado con éxito.');
                }      
                    return redirect()->back(); 
            }
            
    } catch (\Exception $e) {
            Session::flash('message', $e->getMessage());
            return redirect()->back();
        }

    }
}