<?php

namespace App\Http\Controllers\Administracion;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entidades\ValorVariableGeneral;
use App\Entidades\DivisionPolitica;
use App\Entidades\Periodo;
use App\Entidades\VariableGeneral;
use App\Entidades\Auditoria;

class ValorVariableGeneralController extends Controller
{
	public function index()
    {

		$objDivisionPolitica = DivisionPolitica::buscarlstdivisionpoliticActiva();
		
		return view('administracion.confVarGenerales',compact('objDivisionPolitica'));
  
    }
    
    public function editarVariableGeneral(Request $request){

    	$validator=ValorVariableGeneral::validaVariableGeneral($request);
    	if ($validator->fails()) {
        	return redirect('catalogoVarGenerales')
        	->withErrors($validator)
        	->withInput();
        }
        else{
        	try{
        		$variableGeneral=VariableGeneral::find($request->variable_general_id);
        		$variableGeneral->variable_general_nombre_programatico=$request->variable_general_nombre_programatico;
        		$variableGeneral->variable_general_descripcion=$request->variable_general_descripcion;
        		$variableGeneral->variable_general_fuente=$request->variable_general_fuente;
        		$variableGeneral->variable_general_unidad=$request->variable_general_unidad;
        		$variableGeneral->estado_id=$request->estado_id;
        		$variableGeneral->save();
        		$interfaz=url()->previous();
        		Auditoria::ingresoAuditoria(Auditoria::$actualizar, $interfaz);
        		$mensaje='Actualización Exitosa';
        		session()->put('mensajeexito', $mensaje);
        		return redirect('catalogoVarGenerales');
        	}
	        catch (Exception $e){
	    		session()->put('mensajeerror', $e->getMessage());
	    		return redirect('catalogoVarGenerales');
	    	}
        }
    }
    
    public function editaValorVariableGeneral(Request $request){

    	$validator=ValorVariableGeneral::validaValorVariableGeneral($request);
   		 if ($validator->fails()) {
        	return redirect('catalogoVarGenerales')
        	->withErrors($validator)
        	->withInput();
        }
        else{
        	try{
        		$valorVariableGeneral= ValorVariableGeneral::find($request->valor_variable_general_id);
        		$valorVariableGeneral->valor_variable_general_valor=$request->valor_variable_general_valor;
        		$valorVariableGeneral->save();
        		$interfaz=url()->previous();
        		Auditoria::ingresoAuditoria(Auditoria::$actualizar, $interfaz);
        		$mensaje='Actualización Exitosa';
        		session()->put('mensajeexito', $mensaje);
        		return redirect('catalogoVarGenerales');
        	}
        	catch (Exception $e){
        		session()->put('mensajeerror', $e->getMessage());
        		return redirect('catalogoVarGenerales');
        	}
        }

    }
    
    public function consultarVariableEspecifica(Request $request){
    	
    	$objValorVariableGeneral=ValorVariableGeneral::consultaValVarRelacionadoxDivPolitica($request->id, $request->periodo);
    	return $objValorVariableGeneral;
    }
    
    public function consultaInfoVariableGeneral(Request $request){
    	$objVariableGeneral=VariableGeneral::find($request->id);
    	return $objVariableGeneral;
    }
    
    public function consultaInfoVariableEspecifica(Request $request){
    	$objVariableGeneral=ValorVariableGeneral::consultaInfoVariableEspecifica($request->id);
    	return $objVariableGeneral;
    }
    
    public function consultaVariableGeneral(){
    	$objVariablegeneral=ValorVariableGeneral::consultarVariableGeneral();
    	return $objVariablegeneral;
    }
    
    public function consultaPeriodos(){
    	$LstConsulPeriodos=Periodo::consultarLstPeriodo();
    	return $LstConsulPeriodos;
    }
    
}
