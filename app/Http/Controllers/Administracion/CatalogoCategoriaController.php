<?php

namespace App\Http\Controllers\Administracion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entidades\DivisionPolitica;
use App\Entidades\Categoria;
use App\Entidades\Auditoria;
use Validator;


class CatalogoCategoriaController extends Controller
{
     /**
     * 
     * @return type
     */
    public function index() {
        $objDivisionPolitica = DivisionPolitica::consultarDivisionPolitica();
        $objCategoria = Categoria::all();
        return view('administracion.catalogoCategoria', compact('objDivisionPolitica', 'objCategoria'));
    }
    

    public function consultaCantonesCategorias(Request $request){
        $lstDivPoxEntidad=DivisionPolitica::consultaDivPoliticaxEntidad($request->categoria_id);
        return $lstDivPoxEntidad;
        
    }

    public function  guardarCatalogoCategoria(Request $request){

    	 try {
            $messages = [
                'division_politica.required' => 'Campo :attribute es requerido.',
                'division_politica.max' => 'Campo :attribute debe tener un tamaño de :max.',
                'division_politica.unique' => 'Campo :attribute ya se encuentra registrado.',
            ];



            $validator = Validator::make($request->all(), [
                        'categoria_id' => 'max:11|required|',
                        'division_politica_id' => 'required|'
                            ], $messages);

            if ($validator->fails()) {
                return redirect('catalogoCategoria')
                                ->withErrors($validator)
                                ->withInput();
            } else {

                $categoria_id = $request->categoria_id;
                $string = $request->division_politica_id;
                $lstCategoria = explode(',', $string);

                $string2 = $request->lsta_remove_categoria;

                if($string2 != ''){
                    $lstCategoriaRemove = explode(',', $string2);

                }else{
                     $lstCategoriaRemove = null;
                 }
               
                 /* Recorre la lista de interfaces deseleccionadas */
                  if (!is_null($lstCategoriaRemove)) {
                    foreach ($lstCategoriaRemove as $lstCategoriaRemove) {
                        $objDivisionPoliticaRemove = DivisionPolitica::find($lstCategoriaRemove);
                        $objDivisionPoliticaRemove->categoria_id = '1' ;
                        $objDivisionPoliticaRemove->save();
                       // return $objDivisionPoliticaRemove;
                    }

                }
                /* Recorre la lista de categorias seleccionadas */
                  if (count($lstCategoria)>0){
                    //dd($lstCategoria);
                    foreach ($lstCategoria as $lstCategoria) {
                        $objDivisionPolitica = DivisionPolitica::find($lstCategoria);
                        $objDivisionPolitica->categoria_id = $categoria_id ;
                        $objDivisionPolitica->save();
                    }
                }

               



                Auditoria::ingresoAuditoria(Auditoria::$guardar, url()->previous());

                notify()->flash('Se ha registrado el catálogo categoría con éxito.', 'success');
               
                return redirect()->back();
            }
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
            return redirect()->back();
        }
    	
	}
    public function buscarDatosCategoria (Request $request){
        $objCategoria = Categoria::find($request->categoria_id);
        return $objCategoria;
    }

    public function  actualizarCategoria (Request $request){

         try {
            $messages = [
                'categoria.required' => 'Campo :attribute es requerido.',
                'categoria.max' => 'Campo :attribute debe tener un tamaño de :max.',
                'categoria.unique' => 'Campo :attribute ya se encuentra registrado.',
            ];



            $validator = Validator::make($request->all(), [
                        'idCategoria' => 'max:11|required|',
                        'categoria_nombre' => 'required|',
                        'categoria_minimo' => 'required|numeric',
                        'categoria_maximo' => 'required|numeric'
                            ], $messages);

            if ($validator->fails()) {
                return redirect('catalogoCategoria')
                                ->withErrors($validator)
                                ->withInput();
            } else {

                $objCategoria = Categoria::find($request->idCategoria);
                $objCategoria->categoria_nombre = $request->categoria_nombre;
                $objCategoria->categoria_minimo = $request->categoria_minimo;
                $objCategoria->categoria_maximo = $request->categoria_maximo;
                $objCategoria->save();

                Auditoria::ingresoAuditoria(Auditoria::$actualizar, url()->previous());

                notify()->flash('Se ha actualizado datos de la categoría con éxito.', 'success');
               
                return redirect()->back();
            }
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
            return redirect()->back();
        }



      
    }
}
