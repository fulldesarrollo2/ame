<?php

namespace App\Http\Controllers\administracion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entidades\Entidad;
use App\Entidades\DivisionPolitica;
use App\Entidades\EntRelacionaDivPo;
use Illuminate\Support\Facades\Session;
use App\Entidades\Estado;
use App\Entidades\Auditoria;
use Validator;
use App\Entidades\Periodo;
class EntidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lstDivision = DivisionPolitica::buscarlstdivisionpoliticActiva();
        $lstEntidad=Entidad::consultaEntidad(); 
        $cantidad=  DivisionPolitica::buscarCountdivisionpoliticActiva();
        
        
       return view('administracion.entidad',compact('lstEntidad','lstDivision','cantidad'));
    }
    
    public function consultaEntRelacionaDivPo(Request $request){
        try {
            
            $lstEntRelacionDivPo= EntRelacionaDivPo::consultaEntRelacionaDivPoPorEntidad($request->entidad_id);
            return $lstEntRelacionDivPo;
            
        }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                   return redirect()->back();
           } 
    }
    
    /*
     * Metodo: editarEntidad
     * NameCreate : JRendon
     * Create: 06/septiembre/2016 
     * Detalle: Metodo parea editar entidad
    */
    public function editarEntidad(Request $request){
        try {
             $messages = [
                'entidad.required' => 'Campo :attribute es requerido.',
                'entidad.max' => 'Campo :attribute debe tener un tamaño de :max.',
                'entidad.unique' => 'Campo :attribute ya se encuentra registrado.',
            ];
            $validator = Validator::make($request->all(),[
                'entidad_ruc' => 'required|max:255|unique:entidad,entidad_ruc,' . $request->entidad_id . ',entidad_id',
                'entidad_nombre' =>'required|max:255|unique:entidad,entidad_nombre,' . $request->entidad_id . ',entidad_id', 
                'entidad_codigo_ente' => 'required|max:254',
                'entidad_direccion' => 'required|max:254',
                'entidad_telefono' => 'required|max:20',
                'entidad_autoridad' => 'required|max:254',
                'tipo_entidad_id' => 'required|max:11'
            ], $messages);

            if ($validator->fails()) {
                return redirect('entidad')
                                ->withErrors($validator)
                                ->withInput();
            }else{
                    $string1 = $request->entidad_listNuev;
                    $string2 = $request->entidad_listAnt;
                    if($string1!=null){
                    $lstdiNuev = explode(',', $string1);}
                    if($string2!=null){
                    $lstdivant = explode(',', $string2);}

                   // edita Entidad   
                    $objEntidad=  Entidad::find($request->entidad_id);
                    $objEntidad->entidad_ruc=$request->entidad_ruc;
                    $objEntidad->entidad_nombre=strtoupper($request->entidad_nombre);
                    $objEntidad->entidad_codigo_ente=$request->entidad_codigo_ente;
                    $objEntidad->entidad_direccion=strtoupper($request->entidad_direccion);
                    $objEntidad->entidad_autoridad=strtoupper($request->entidad_autoridad);
                    $objEntidad->entidad_telefono=$request->entidad_telefono;
                    $objEntidad->tipo_entidad_id=$request->tipo_entidad_id;
                    $objEntidad->entidad_periodo_vigente=  Periodo::consultaUltimoPeriodo();
                    $objEntidad->save();
                    
                  // edita 
                    
                    if(!empty($lstdivant)){
                        foreach($lstdivant as $select){  

                          $objEntRelacionDivPo=EntRelacionaDivPo::find($select);
                          $objEntRelacionDivPo->estado_id= Estado::$estadoInactivo;
                          $objEntRelacionDivPo->save();
                        }
                     }
                  
                  if(!empty($lstdiNuev)){
                    foreach($lstdiNuev as $selec){
                     $objEntRelacionDivP= new EntRelacionaDivPo();
                     $objEntRelacionDivP->entidad_id=$request->entidad_id;
                     $objEntRelacionDivP->division_politica_id=$selec;
                     $objEntRelacionDivP->estado_id= Estado::$estadoActivo;
                     $objEntRelacionDivP->save();
                   }
                  }

                   $interfaz=url()->previous();
                   $variable=2;
                   Auditoria::ingresoAuditoria($variable,$interfaz); 
                   Session::flash('message', 'Entidad: ' . $objEntidad->entidad_nombre . ', ha sido actualizado con éxito.'); 
                   return redirect()->back();
            }
         }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                  // return redirect()->back();
           } 
        
    }

    /*
     * Metodo: guardarEntidad
     * NameCreate : JRendon
     * Create: 06/septiembre/2016 
     * Detalle: Metodo parea guardar entidad
    */
    public function guardarEntidad(Request $request){
            
        try {
            
            $messages = [
                'entidad.required' => 'Campo :attribute es requerido.',
                'entidad.max' => 'Campo :attribute debe tener un tamaño de :max.',
                'entidad.unique' => 'Campo :attribute ya se encuentra registrado.',
            ];
            $validator = Validator::make($request->all(),[
                'entidad_ruc' => 'max:13|unique:entidad',
                'entidad_nombre' => 'required|max:254|unique:entidad',
                'entidad_codigo_ente' => 'required|max:254',
                'entidad_direccion' => 'required|max:254',
                'entidad_telefono' => 'required|max:20',
                'entidad_autoridad' => 'required|max:254',
                'tipo_entidad_id' => 'required|max:11'
            ], $messages);

            if ($validator->fails()) {
                return redirect('entidad')
                                ->withErrors($validator)
                                ->withInput();
            }else{
                    $string = $request->entidad_listNuev;
                    $lstDivision = explode(',', $string);
                    //$lstDivision=$request->lsySelect;
                    
                    $objEntidad=new Entidad();
                    $objEntidad->entidad_ruc=$request->entidad_ruc;
                    $objEntidad->entidad_nombre=strtoupper($request->entidad_nombre);
                    $objEntidad->entidad_codigo_ente=$request->entidad_codigo_ente;
                    $objEntidad->entidad_direccion=strtoupper($request->entidad_direccion);
                    $objEntidad->entidad_autoridad=strtoupper($request->entidad_autoridad);
                    $objEntidad->entidad_telefono=$request->entidad_telefono;
                    $objEntidad->tipo_entidad_id=$request->tipo_entidad_id;
                    $objEntidad->entidad_periodo_vigente=Periodo::consultaUltimoPeriodo();                   
                    $objEntidad->save();


                    foreach($lstDivision as $select){
                       $objEntRelacionDivPo= new EntRelacionaDivPo();
                       $objEntRelacionDivPo->entidad_id=$objEntidad->entidad_id;
                       $objEntRelacionDivPo->division_politica_id=$select;
                       $objEntRelacionDivPo->estado_id= Estado::$estadoActivo;
                       $objEntRelacionDivPo->save();
                     }
                      $interfaz=url()->previous();
                      $variable=1;
                      Auditoria::ingresoAuditoria($variable,$interfaz);
                      Session::flash('message', 'Entidad: ' . $objEntidad->entidad_nombre . ', ha sido Guardado con éxito.');
                      return redirect()->back();
            }
        }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                  // return redirect()->back();
        }    

    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
