<?php

namespace App\Http\Controllers\Autoevaluacion;

use Illuminate\Http\Request;
use App\Entidades\VariableSeguridad;
use App\Entidades\ComponenteProyecto;
use App\Entidades\Auditoria;
use App\Http\Controllers\Controller;
use App\Entidades\Estado;
use App\Entidades\TipoCuenta;
use App\Entidades\ProyectoFinanciero;
use App\Entidades\Cuenta;
use App\Entidades\Periodo;
use App\Entidades\Ejecucion;
use App\Entidades\ActividadProyecto;

class AdmProyecFinancieroController extends Controller {

    /**
     * 
     * @return type
     */
    public function index() {
        try {
            $objComponentes = ComponenteProyecto::all();
            $lstCuentasPresupuestarias = Cuenta::buscarLstCuentaxTipo(TipoCuenta::$presupuestaria);
            $objPeriodoActual = VariableSeguridad::ConsultaVariable(VariableSeguridad::$periodoActual);
            $lstaProyectoFinanciero = ProyectoFinanciero::all();
            return view('autoEvaluacion.admProyecFinanciero', compact('objPeriodoActual', 'objComponentes', 'lstCuentasPresupuestarias', 'lstaProyectoFinanciero'));
        } catch (Exception $ex) {
            
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function guardarProyecto(Request $request) {
        try {
            $interfaz = url()->previous();
            if ($request->proyecto_id == '') {
                $existNombreProyecto = ProyectoFinanciero::where('proyecto_financiero_nombre', $request->proyecto_financiero_nombre)->get();
                if ($existNombreProyecto->count() > 0) {
                    $lstComponentes = [];
                    $mensaje = 'El nombre del proyecto ya se encuentra en uso';
                    return array('componente' => $lstComponentes, 'mensaje' => $mensaje);
                } else {
                    $objProyectoFinanciero = new ProyectoFinanciero();
                    $objProyectoFinanciero->entidad_id = 1;
                    $objProyectoFinanciero->proyecto_financiero_nombre = $request->proyecto_financiero_nombre;
                    $objProyectoFinanciero->proyecto_financiero_descripcion = $request->proyecto_financiero_descripcion;
                    $objProyectoFinanciero->proyecto_financiero_tipo = $request->proyecto_financiero_tipo;
                    $objProyectoFinanciero->proyecto_financiero_inicio = $request->fcha_inicio;
                    $objProyectoFinanciero->proyecto_financiero_fin = $request->fcha_fin;
                    $objProyectoFinanciero->estado_id = $request->estado_id;
                    $objProyectoFinanciero->save();
                    $lstaProyectoFinanciero = ProyectoFinanciero::all();
                    return $lstaProyectoFinanciero;
                }
            } else {
                $existNombreProyecto = ProyectoFinanciero::where('proyecto_financiero_nombre', $request->proyecto_financiero_nombre)->where('proyecto_financiero_id', '!=', $request->proyecto_id)->get();
                if ($existNombreProyecto->count() > 0) {
                    $lstComponentes = [];
                    $mensaje = 'El nombre del proyecto ya se encuentra en uso';
                    return array('componente' => $lstComponentes, 'mensaje' => $mensaje);
                } else {
                    $objProyectoFinanciero = ProyectoFinanciero::find($request->proyecto_id);
                    $objProyectoFinanciero->entidad_id = 1;
                    $objProyectoFinanciero->proyecto_financiero_nombre = $request->proyecto_financiero_nombre;
                    $objProyectoFinanciero->proyecto_financiero_descripcion = $request->proyecto_financiero_descripcion;
                    $objProyectoFinanciero->proyecto_financiero_tipo = $request->proyecto_financiero_tipo;
                    $objProyectoFinanciero->proyecto_financiero_inicio = $request->fcha_inicio;
                    $objProyectoFinanciero->proyecto_financiero_fin = $request->fcha_fin;
                    $objProyectoFinanciero->estado_id = $request->estado_id;
                    $objProyectoFinanciero->save();
                    Auditoria::ingresoAuditoria(Auditoria::$guardar, $interfaz);
                    $lstaProyectoFinanciero = ProyectoFinanciero::all();
                    return $lstaProyectoFinanciero;
                }
            }
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

    public function guardarComponente(Request $request) {
        try {
            $interfaz = url()->previous();
            //Se guarda el proyecto

            if ($request->proyecto_id == '') {

                $existNombreProyecto = ProyectoFinanciero::where('proyecto_financiero_nombre', $request->proyecto_financiero_nombre)->get();

                if ($existNombreProyecto->count() > 0) {
                    $lstComponentes = [];
                    $mensaje = 'El nombre del proyecto ya se encuentra en uso';
                    return array('componente' => $lstComponentes, 'mensaje' => $mensaje);
                } else {

                    $objProyectoFinanciero = new ProyectoFinanciero();
                    $objProyectoFinanciero->entidad_id = 1;

                    $objProyectoFinanciero->proyecto_financiero_nombre = $request->proyecto_financiero_nombre;
                    $objProyectoFinanciero->proyecto_financiero_descripcion = $request->proyecto_financiero_descripcion;
                    $objProyectoFinanciero->proyecto_financiero_tipo = $request->proyecto_financiero_tipo;
                    $objProyectoFinanciero->proyecto_financiero_inicio = $request->fcha_inicio;
                    $objProyectoFinanciero->proyecto_financiero_fin = $request->fcha_fin;
                    $objProyectoFinanciero->estado_id = $request->estado_id;
                    $objProyectoFinanciero->save();
                    Auditoria::ingresoAuditoria(Auditoria::guardar, $interfaz);

                    $mensaje = 'El proyecto ha sido creado con éxito';
                    //Se guarda los componentes
                    $lstaComponentes = $request->lstaComponentesGuardar;
                    //$lstaComponentes = explode(',', $string);
                    foreach ($lstaComponentes as $key => $descripcion_componente) {
                        $objComponenteProyecto = new ComponenteProyecto();
                        $objComponenteProyecto->proyecto_financiero_id = $objProyectoFinanciero->proyecto_financiero_id;
                        $objComponenteProyecto->componente_proyecto_descripcion = $descripcion_componente;
                        $objComponenteProyecto->save();
                    }

                    $lstComponentes = ComponenteProyecto::buscarComponentexProyecto($objProyectoFinanciero->proyecto_financiero_id);
                    return array('componente' => $lstComponentes, 'mensaje' => $mensaje);
                }
            } else {

                $existNombreProyecto = ProyectoFinanciero::where('proyecto_financiero_nombre', $request->proyecto_financiero_nombre)->where('proyecto_financiero_id', '!=', $request->proyecto_id)->get();

                if ($existNombreProyecto->count() > 0) {
                    $lstComponentes = [];
                    $mensaje = 'El nombre del proyecto ya se encuentra en uso';
                    return array('componente' => $lstComponentes, 'mensaje' => $mensaje);
                } else {

                    $objProyectoFinanciero = ProyectoFinanciero::find($request->proyecto_id);
                    $objProyectoFinanciero->entidad_id = 1;
                    $objProyectoFinanciero->proyecto_financiero_nombre = $request->proyecto_financiero_nombre;
                    $objProyectoFinanciero->proyecto_financiero_descripcion = $request->proyecto_financiero_descripcion;
                    $objProyectoFinanciero->proyecto_financiero_tipo = $request->proyecto_financiero_tipo;
                    $objProyectoFinanciero->proyecto_financiero_inicio = $request->fcha_inicio;
                    $objProyectoFinanciero->proyecto_financiero_fin = $request->fcha_fin;
                    $objProyectoFinanciero->estado_id = $request->estado_id;
                    $objProyectoFinanciero->save();

                    Auditoria::ingresoAuditoria(Auditoria::$actualizar, $interfaz);

                    $mensaje = 'Se ha actualizado el proyecto con éxito';
                    //Se guarda los componentes
                    $lstaComponentes = $request->lstaComponentesGuardar;
                    //$lstaComponentes = explode(',', $string);
                    foreach ($lstaComponentes as $key => $descripcion_componente) {
                        $objComponenteProyecto = new ComponenteProyecto();
                        $objComponenteProyecto->proyecto_financiero_id = $objProyectoFinanciero->proyecto_financiero_id;
                        $objComponenteProyecto->componente_proyecto_descripcion = $descripcion_componente;
                        $objComponenteProyecto->save();
                    }

                    $lstComponentes = ComponenteProyecto::buscarComponentexProyecto($objProyectoFinanciero->proyecto_financiero_id);
                    return array('componente' => $lstComponentes, 'mensaje' => $mensaje);
                }
            }
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
    /**
     * 
     * @param Request $request
     * @return type
     */
    public function editarProyecto(Request $request) {
        try {
            $objProyectoFinanciero = ProyectoFinanciero::find($request->proyecto_id);
            //Busca los componentes que tiene el proyecto
            $objComponenteProyecto = ComponenteProyecto::buscarComponentexProyecto($request->proyecto_id);

            //Busca las actividades de cada componente
            $i = 0;
            if ($objComponenteProyecto->count() > 0) {
                foreach ($objComponenteProyecto as $componenteProyecto) {
                    $objActividadProyecto[$i] = ActividadProyecto::buscarActividadxComponente($componenteProyecto->componente_proyecto_id);
                    $i++;
                }
            }

            $lstCuentasPresupuestarias = Cuenta::buscarLstCuentaxTipo(TipoCuenta::$presupuestaria);

            return array('proyecto' => $objProyectoFinanciero, 'componentes' => $objComponenteProyecto, 'actividad' => $objActividadProyecto, 'lstCuentasPresupuestarias' => $lstCuentasPresupuestarias);
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

    public function guardarActividad(Request $request) {
        try {
            $interfaz = url()->previous();
            $objActividadProyecto = new ActividadProyecto ();
            $objActividadProyecto->cuenta_id = $request->cuenta_id;
            $objActividadProyecto->componente_proyecto_id = $request->componente_id;
            $objActividadProyecto->actividad_proyecto_descripcion = $request->descripcion_actividad;
            $objActividadProyecto->actividad_proyecto_fuente_financiamiento = $request->fuente_financiamiento;
            $objActividadProyecto->save();
            Auditoria::ingresoAuditoria(Auditoria::$guardar, $interfaz);
            $objCuentaActividad = Cuenta::find($request->cuenta_id);

            return array("objActividadProyecto" => $objActividadProyecto, 'objCuentaActividad' => $objCuentaActividad);
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

    public function eliminarActividad(Request $request) {
        try {
            $interfaz = url()->previous();
            $objActividadProyecto = ActividadProyecto::find($request->actividad_id);
            $objEjecucionEliminar = Ejecucion::where('actividad_proyecto_id', '=', $request->actividad_id)->delete();
            $objActividadProyecto->delete();
            return 'ok';
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

    public function eliminarComponente(Request $request) {
        try {
            $interfaz = url()->previous();
            $objComponente = ComponenteProyecto::find($request->componente_id);
            //Busca las actividades de cada componente
            foreach ($objComponente as $objComponente) {
                /* Elimina las actividades */
                $eliminarActividadProyecto = ActividadProyecto::where('componente_proyecto_id', $objComponente->componente_proyecto_id)->delete();
            }
            $objComponente->delete();

            $objActividadProyecto = ActividadProyecto::buscarActividadComponente();
            return $objActividadProyecto;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

    public function buscarPeriodo(Request $request) {
        try {
            $objPeriodoActual = VariableSeguridad::ConsultaVariable(VariableSeguridad::$periodoActual);
            $periodoActual = $objPeriodoActual->variable_seguridad_valor;
            $time = strtotime($request->fcha_inicio);
            $fcha_anio = date('Y', $time);
            /* Estados según el perido */
            if ($fcha_anio == $periodoActual) {
                //Periodo Actual
                $lstCodigo = [Estado::$estadoFormulacion, Estado::$estadoAprobado, Estado::$estadoEnEjecucion, Estado::$estadoCerrado];
                $objEstado = Estado::buscarLstEstadoxId($lstCodigo);
            } if ($fcha_anio < $periodoActual) {

                $lstCodigo = [Estado::$estadoAprobado, Estado::$estadoEnEjecucion, Estado::$estadoCerrado];
                $objEstado = Estado::buscarLstEstadoxId($lstCodigo);
            }if ($fcha_anio > $periodoActual) {
                $lstCodigo = [Estado::$estadoFormulacion];
                $objEstado = Estado::buscarLstEstadoxId($lstCodigo);
            }
            //Buscar periodos
            $anio_inicio = strtotime($request->fcha_inicio);
            $anio_inicio = date('Y', $anio_inicio);

            $anio_fin = strtotime($request->fcha_fin);
            $anio_fin = date('Y', $anio_fin);

            $objPeriodos = Periodo::buscarPeriodoxFecha($anio_inicio, $anio_fin);
            $objEjecucion = Ejecucion::buscarEjecucionxPeriodo($anio_inicio, $anio_fin);

            if ($request->proyecto_id == '') {
                $objActividadProyecto = '';
            } else {
                //Busca el proyecto
                $objProyectoFinanciero = ProyectoFinanciero::find($request->proyecto_id);
                //Busca los componentes que tiene el proyecto
                $objComponenteProyecto = ComponenteProyecto::buscarComponentexProyecto($request->proyecto_id);

                //Busca las actividades de cada componente
                $i = 0;
                if ($objComponenteProyecto->count() > 0) {
                    foreach ($objComponenteProyecto as $componenteProyecto) {
                        $objActividadProyecto[$i] = ActividadProyecto::buscarActividadxComponente($componenteProyecto->componente_proyecto_id);
                        $i++;
                    }
                }
            }
            return array('countPeriodos' => $objPeriodos, 'objEstado' => $objEstado, 'actividad' => $objActividadProyecto, 'ejecucion' => $objEjecucion);
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

    public function agregarEjecucion(Request $request) {
        try {
            $interfaz = url()->previous();
            $lstEjecucion = explode(';', $request->lstEjecucion);
            $mensaje = "";
            unset($lstEjecucion[0]);

            $i = 0;
            foreach ($lstEjecucion as $ejecucion) {
                $lstEjecucion = explode(',', $ejecucion);

                if ($lstEjecucion[2] == "undefined") {
                    $objEjecucion = new Ejecucion ();
                    $objEjecucion->periodo_id = $lstEjecucion[0];
                    $objEjecucion->actividad_proyecto_id = $lstEjecucion[1];
                    $objEjecucion->ejecucion_valor = $lstEjecucion[3];
                    $objEjecucion->save();
                    Auditoria::ingresoAuditoria(Auditoria::$guardar, $interfaz);
                    $mensaje = 'Tabla de información registrada';
                } else {
                    $objEjecucion = Ejecucion::find($lstEjecucion[2]);
                    $objEjecucion->periodo_id = $lstEjecucion[0];
                    $objEjecucion->actividad_proyecto_id = $lstEjecucion[1];
                    $objEjecucion->ejecucion_valor = $lstEjecucion[3];
                    $objEjecucion->save();
                    Auditoria::ingresoAuditoria(Auditoria::$actualizar, $interfaz);
                    $mensaje = 'Tabla de información modificada';
                }
            }
            return $mensaje;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

}
