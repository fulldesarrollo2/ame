<?php

namespace App\Http\Controllers\Autoevaluacion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Entidades\DivisionPolitica;
use App\Entidades\TipoEntidad;
use App\Entidades\Entidad;
use App\Entidades\Cuenta;
use App\Entidades\Periodo;
use App\Entidades\Auditoria;
use App\Entidades\PerfilIngreso;
use App\Entidades\TipoFuente;
use Maatwebsite\Excel\Facades\Excel;
use App\Entidades\ArchivoCarga;
use App\Entidades\AnalisisPreConfigurado;
use App\Entidades\ValCuentaEntidad;
use Validator;
use Illuminate\Support\Facades\Session;

class AnalisisPreConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        
        //ValCuentaEntidad::consultaCtaVal(TipoEntidad::$gadm);
        //dd($lstaCta);
        $objDivisionPolitica = DivisionPolitica::consultarDivisionPolitica();
        $objPerfil = PerfilIngreso::buscarPerfilIngresoActivos();
        $lstaCodigoEntidad = [TipoEntidad::$gadm,  TipoEntidad::$empServBasMunicipales];
        $lstTipoEntidad = TipoEntidad::buscarLstTipoEntidadxId($lstaCodigoEntidad);
        $lstCodigoFuente = [TipoFuente::$fuenteBdE , TipoFuente::$fuenteEntidad]; 
        $lstTipoFuente  = TipoFuente::buscarLstTipoFuentexId($lstCodigoFuente);
        $lstPeriodo = Periodo::consultarLstPeriodoxTipoFuente(TipoFuente::$fuenteBdE);
        $lstPeriodoHasta = $lstPeriodo;
        $objEntidad = Entidad::All();
        $lstAnalisisPreConf = AnalisisPreConfigurado::All();
        return view('autoevaluacion.analisisPreConfig',compact('lstPeriodo','lstPeriodoHasta','lstTipoFuente','lstTipoEntidad','objEntidad','objDivisionPolitica','objPerfil','lstAnalisisPreConf'));
        
    }
        /*
     * Metodo: consultaValCuentaEntidadXPeriodo
     * NameCreate : GCASTILLO
     * Create: 08/septiembre/2016 
     * Detalle: Metodo parea consultar entidad por division politica y por tipo
    */  
    
  
    public function consultaValCuentaEntidadXPeriodo(Request $request){
        //consultaValCuentaEntidadXPeriodo($entidad_id (1),$tipo_fuente_id(1),$periodo_id_desde(1),$periodo_id_hasta(9),
        //$analisis_Preconfigurado(lsta[4,5])) {
        $analisis_id = $request->analisis_pre_config_id;
        
        foreach($analisis_id as $analisis){
            $analisis = str_replace('"','',$analisis);
        }
     	
        $objValCuentaEntidad = ValCuentaEntidad::consultaValCuentaEntidadXPeriodo($request->entidad_id,$request->tipo_fuente_id,$request->periodo_desde_id,$request->periodo_hasta_id,$analisis_id);
        return $objValCuentaEntidad;
    }
    
    public  function imprimirExcelAnalisis(Request $request){
       
        $arrayExcel=[];
        $dataExcel = $request->dataExcel;
        $dataExcel =json_decode($dataExcel); 
        
         Excel::create('Laravel Excel', function($excel)use($dataExcel)  {
                 $lstArray = $dataExcel;
               //dd($lstArray);  
                  $excel->sheet('$selectArray->nombre', function($sheet)use($lstArray)  {
                      
               foreach($lstArray as $selectArray){
                    $x=0;    
                    
                    foreach($selectArray->cuentas as $selectCuentas){
                         $lstExcel=array();
                         $lstExcel= array_add($lstExcel,'analisisCuenta',$selectCuentas->name);
                        $name=$selectCuentas->name;
                         $i=0;
                       // foreach($selectCuentas->data as $selectValor){ 
                       for ($i=0; $i <count($selectCuentas->data ); $i++)
                        {
                            $lstExcel= array_add($lstExcel,$name,$selectCuentas->data[$i]);
                           
                        }
                         $objErrorFormato[$x]=$lstExcel;
                  $x++;
                   $sheet->fromArray($objErrorFormato);
                     }
                    
                  //  $lstExcel=array_add($lstExcel,'analisisCuentas',$lstArray[$i]['cuentas']);
                  
                 
               }
                 
                });
                   
                
               
            
 
               // $sheet->fromArray($arrayExcel);
 
          
        })->export('csv');
        
        
       /*  Excel::create('Análisis Preconfigurado', function($excel) {
              $lstArray = ValCuentaEntidad::consultaValCuentaEntidadXPeriodo(1,1,1,95,4);
            //  $lstArray= $request->dataExcel;
              return ($lstArray);
               //$lstCuentasRegistradas=Session::get('lstCuentaRegistradas');
               //$lstCuentasInconsist=Session::get('lstCuentasInconsist');
             
        if(!is_null($lstArray)){       
            $excel->sheet('Errores de formato', function($sheet)use($lstArray) {
                $lstExcel=array();
                $objErrorFormato=array();
                $x=0;
                 $sheet->cells('A1:F1', function($cells) {
                     $cells->setFont(array(
                      'family'     => 'Calibri',
                      'size'       => '12',
                      'bold'       =>  true
                        ));
                });
                
                for ($i=0; $i <count($lstArray); $i++)
                {
                    $lstExcel=array();
                    $lstExcel=array_add($lstExcel,'idColumna',number_format($lstArray[$i][0]));
                    $lstExcel=array_add($lstExcel,'numColum',$lstArray[$i]['numColum']);
                    $lstExcel=array_add($lstExcel,'posColum',$lstArray[$i]['posColum']);
                    $lstExcel=array_add($lstExcel,'tipoDato',$lstArray[$i]['tipoDato']);
                    $lstExcel=array_add($lstExcel,'logDato',$lstArray[$i]['logDato']);
                    $lstExcel=array_add($lstExcel,'requerido',$lstArray[$i]['requerido']);

                    $objErrorFormato[$x]=$lstExcel;
                  $x++;
                }
               $sheet->fromArray($objErrorFormato);
                   
               
            });
         }  
        })->export('xls');*/
 
        
    }
    
    public function consultaValoresCuentas(Request $request){
        try {
                //lista de las cuentas para armar el árbol
                $lstCuenta= Cuenta::buscarLstCuentaxEntidadxFuente($request->entidad_id,$request->tipo_fuente_id); 
                $lstCuenta= Cuenta::buscarLstCuentaxEntidadxFuente($request->entidad_id,2); 
                
                $objArchivoCarga= ArchivoCarga::consultaArchivoCargaPorPeriodo($request->periodo_id);
                
                if(!empty($lstCuenta) && !empty($objArchivoCarga)){  
                    foreach ($lstCuenta as $select){
                        $valor = ValCuentaEntidad::consultaValCuentaEntidad($select->entidad_id,$select->cuenta_id,$objArchivoCarga->archivo_carga_id);
                        $valor = $valor!=null?$valor->val_cuenta_entidad_valor:0;
                        $select = array_add($select,'valor',$valor);
                        
                    }
                    return $lstCuenta;
                }

        }catch(\Exception $e){
                Session::flash('message',$e->getMessage());
                   
        }
    }
}