<?php

namespace App\Http\Controllers\Autoevaluacion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Entidades\Cuenta;
use App\Entidades\AnalisisPreConfigurado;
use App\Entidades\Estado;
use App\Entidades\ctaRelacionadaApconfig;
use App\Entidades\Auditoria;

class AnalisisPreConfiguradoController extends Controller {
	
	public function index() {
		//dd(Cuenta::buscarCuenta(3)); dd(ctaRelacionadaApconfig::consultaCuentaRelacion());
		$objAnalisisPreConfigurado=AnalisisPreConfigurado::consultaLstAnalisisPreConfigurado();
		$objCuenta=Cuenta::buscarLstCuenta();
		
		return view('autoevaluacion.analisisFinanPreConfig',compact('objCuenta','objAnalisisPreConfigurado'));
		
	}
	/*
	* Metodo: guardarAnalisisPreConfig
	* NameCreate : Jtrivino
	* Create: 14/septiembre/2016
	* Detalle: Metodo para ingreso de nuevo analisis financiero
	*/
	public function guardarAnalisisPreConfig(Request $request){
		//Interfaz pantalla actual de trabajo
		$interfaz=url()->previous();
		$validator=AnalisisPreConfigurado::validarguardarAnalisisPreConfig($request);
	if ($validator->fails()) {
        	return redirect('analisisPreConfig')
        	->withErrors($validator)
        	->withInput();
        }
    else{
    	  try {
    	  	
				$objAnalisisPreConfig=new AnalisisPreConfigurado();
				
				$objAnalisisPreConfig->analisis_pre_configurado_nombre=$request->analisis_pre_configurado_nombre;
				$objAnalisisPreConfig->analisis_pre_configurado_criterio=$request->analisis_preconfigurado_criterio;
				$objAnalisisPreConfig->estado_id=Estado::$estadoActivo;	
				$objAnalisisPreConfig->save();
    	  } catch (\Exception $e) {
    	  	session()->put('mensajeerror', $e->getMessage());
			return redirect('analisisPreConfig');
        }
        
        try {
        	$cuentasarray=$request->cuentasComparativas;
        	$ultimoAnalisis=AnalisisPreConfigurado::consultarUltimoAnalisisPreConfig();
        	
        	for ($i=0;$i<count($cuentasarray);$i++){
        		$objCtaRelacionaApConfig=new ctaRelacionadaApconfig();
        		$objCtaRelacionaApConfig->cuenta_id=$cuentasarray[$i];
        		$objCtaRelacionaApConfig->analisis_pre_configurado_id=$ultimoAnalisis->analisis_pre_configurado_id;
        		$objCtaRelacionaApConfig->save();
        	}
        	//dd($objCtaRelacionaApConfig);
        	Auditoria::ingresoAuditoria(Auditoria::$guardar, $interfaz);
        } catch (\Exception $e) {
        		session()->put('mensajeerror', $e->getMessage());
				return redirect('analisisPreConfig');
        }
        $mensaje='Ingreso Exitosa';
        session()->put('mensajeexito', $mensaje);
        return redirect('analisisPreConfig');
        
    }
		//dd($request->all());
		
	}
	
	/*
	 * Metodo: guardarAnalisisPreConfig
	 * NameCreate : Jtrivino
	 * Create: 14/septiembre/2016
	 * Detalle: Metodo para buscar analisis pre configurado
	 */
	public function buscarAnalisisPreConfig(Request $request){
		$objAnalisisPreConfig=ctaRelacionadaApconfig::consultaCuentaRelacion($request);
		return $objAnalisisPreConfig;
	}
	
	/*
	 * Metodo: actualizarAnalisisPreConfig
	 * NameCreate : Jtrivino
	 * Create: 14/septiembre/2016
	 * Detalle: Metodo para actualizar analisis financiero
	 */
	public function actualizarAnalisisPreConfig(Request $request){
		
		//Interfaz pantalla actual de trabajo
		$interfaz=url()->previous();
		$validator=AnalisisPreConfigurado::validarActualizarAnalisisPreConfig($request);
		if ($validator->fails()) {
			return redirect('analisisPreConfig')
			->withErrors($validator)
			->withInput();
		}
		else{
			try {
		
				$objAnalisisPreConfig=AnalisisPreConfigurado::buscarAnalisisPreConfigurado($request);
				
				$objAnalisisPreConfig->analisis_pre_configurado_nombre=$request->analisis_pre_configurado_nombre;
				$objAnalisisPreConfig->analisis_pre_configurado_criterio=$request->analisis_preconfigurado_criterio;
				$objAnalisisPreConfig->estado_id=$request->estado_id;
				$objAnalisisPreConfig->save();
			} catch (\Exception $e) {
				
				session()->put('mensajeerror', $e->getMessage());
				return redirect('analisisPreConfig');
			}
		
			try {
				$cuentasarray=$request->cuentasComparativas;
				
				$objCtaRelacionaApConfigBorar=ctaRelacionadaApconfig::buscarCtaRelacionaApconfig($request);
				//$objCtaRelacionaApConfigBorar->delete();
				foreach ($objCtaRelacionaApConfigBorar as $select)
				{
					$select->delete();
				}
				
				for ($i=0;$i<count($cuentasarray);$i++){
					$objCtaRelacionaApConfig=new ctaRelacionadaApconfig();
					$objCtaRelacionaApConfig->cuenta_id=$cuentasarray[$i];
					$objCtaRelacionaApConfig->analisis_pre_configurado_id=$request->analisis_pre_configurado_id;
					$objCtaRelacionaApConfig->save();
				}
				//dd($objCtaRelacionaApConfig);
				Auditoria::ingresoAuditoria(Auditoria::$actualizar, $interfaz);
			} catch (\Exception $e) {
				session()->put('mensajeerror', $e->getMessage());
				return redirect('analisisPreConfig');
			}
			$mensaje='Actualización Exitosa';
			session()->put('mensajeexito', $mensaje);
			return redirect('analisisPreConfig');
		
		}
	}
	
}
