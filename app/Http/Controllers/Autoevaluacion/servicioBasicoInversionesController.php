<?php

namespace App\Http\Controllers\Autoevaluacion;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\inversionesRequest as iRequest;
use App\Http\Requests\BienMuebleRequest as BMRequest;
use App\Http\Controllers\Controller;
use App\Entidades\Estado;
use App\Entidades\Infraestructura;
use App\Entidades\VariableSeguridad;
use App\Entidades\BienMueble;
use Illuminate\Support\Facades\Session;
use Auth;

class servicioBasicoInversionesController extends Controller
{
    public function index(){
    	$listEstados = Estado::all();
    	return view('autoevaluacion.serviciosBasicosInversiones', compact('listEstados'));
    }

    public function guardarInversionInfraestructura(iRequest $request)
    {
    	$variableSeguidad=VariableSeguridad::$periodoActual;
		$objVariableSeguridad=VariableSeguridad::ConsultaVariable($variableSeguidad);
    	
    	$objInversion=new Infraestructura();
    	$objInversion->entidad_id = Auth::user()->entidad_id;
    	$objInversion->periodo_id=$objVariableSeguridad->variable_seguridad_id;
    	$objInversion->Infraestructura_descripcion=$request->descripcion;
    	$objInversion->Infraestructura_fecha_construccion=$request->fechaInversion;
    	$objInversion->Infraestructura_vida_util=$request->vidaUtil;
    	$objInversion->Infraestructura_valor_actual=$request->valorAdquisicion;
    	$objInversion->estado_id=1;
    	$objInversion->save();
    	Session::flash('message', 'El registro ha sido Almacenado');
            return redirect()->back();

    }

    public function guardarInversionBienMueble(BMRequest $request)
    {

        $variableSeguidad=VariableSeguridad::$periodoActual;
        $objVariableSeguridad=VariableSeguridad::ConsultaVariable($variableSeguidad);
     
        $objInversionBienMueble=new BienMueble();
        $objInversionBienMueble->entidad_id = Auth::user()->entidad_id;
        $objInversionBienMueble->periodo_id=$objVariableSeguridad->variable_seguridad_id;
        $objInversionBienMueble->bien_mueble_descripcion=$request->descripcion;
        $objInversionBienMueble->bien_mueble_fecha_compra=$request->fechaInversion;
        $objInversionBienMueble->bien_mueble_vida_util=$request->vidaUtil;
        $objInversionBienMueble->bien_mueble_valor_compra=$request->valorAdquisicion;
        $objInversionBienMueble->estado_id=1;
        $objInversionBienMueble->bien_mueble_cantidad=$request->cantidad;
        $objInversionBienMueble->save();
        Session::flash('message', 'El registro ha sido Almacenado');
            return redirect()->back();

    }
}
