<?php

namespace App\Http\Controllers\Autoevaluacion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entidades\TipoFuente;
use App\Entidades\Periodo;
use App\Entidades\TipoArchivo;
use App\Entidades\TipoCuenta;
use App\Entidades\CedulaTipoCuenta;
use Illuminate\Support\Facades\File;
use App\Entidades\ArchivoCarga;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Entidades\TipoArchivoFuente;
use Illuminate\Support\Facades\Session;
use App\Entidades\ParametroArchivoCarga;
use App\Entidades\ConfiguracionColumna;
use Validator;
use DateTime;
use App\Http\Controllers\PdfController;
use Illuminate\Support\Facades\Config;
use Maatwebsite\Excel\Facades\Excel;
use App\Entidades\Cuenta;
use App\Entidades\ValCuentaEntidad;
class InfoFinancieraController extends Controller
{
    /**
     * 
     * @return type
     */
    
    
    public function __construct() 
    {
        // Fetch the Site Settings object
       
      
    }
    
    public function index() {
        $lstCodigo = [TipoFuente::$fuenteMF,  TipoFuente::$fuenteBsE , TipoFuente::$fuenteEntidad];
        $lstTipoFuente = TipoFuente::buscarLstTipoFuentexId($lstCodigo);
        $lstPeriodo = Periodo::all();
        $lstErrores=array();
        $lstCuentaNoReferencia=array();
        $lstCuentaRegistradas=array();
        $error=false;
        // Limpiar variables de sessión
        Session::forget('variable');
        Session::forget('lstCuentasInconsist');
        Session::forget('lstCuentaRegistradas');
        return view('autoevaluacion.infoFinanciera', compact('lstTipoFuente','lstPeriodo','lstErrores','lstCuentaNoReferencia','error','lstCuentaRegistradas'));
    }
    
    public function buscarTipoArchivo(Request $request){
        try {
             if($request->tipo_fuente_id!=null || $request->tipo_fuente_id!=''){          
                $lstArchivoCarga= TipoArchivoFuente::consultarArchivoCarga($request->tipo_fuente_id);
                return $lstArchivoCarga; 
             }
            
         }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                   return redirect()->back();
           } 
        
    }
    
    public function buscarTipCuentDeTipArchivo(Request $request){
        try {
             
            if($request->tipo_archivo_id!=null || $request->tipo_archivo_id!=''){                    
                $objTipoArchivo=  TipoArchivo::find($request->tipo_archivo_id);
                $lstCodigo = [$objTipoArchivo->tipo_cuenta_id];
                $lstTipoCuenta= TipoCuenta::buscarLstTipoCuenta($lstCodigo);
                return $lstTipoCuenta; 

            }
            }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                   return redirect()->back();
           } 
    }
    
    public function buscarCedulasGastos(Request $request){
        try {
          
            $lstcedulaGasto= CedulaTipoCuenta::consultaCedulasDeGastos();
            return $lstcedulaGasto;
        }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                   return redirect()->back();
           } 
        
    }
    
    public function buscarCedulasIngresos(Request $request){
        try {
            
            $lstcedulaIngreso=CedulaTipoCuenta::consultaCedulasDeIngresos();
            return $lstcedulaIngreso;
        }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                   return redirect()->back();
           } 
        
    }
    
    public function buscarPeriodoPorFuente(Request $request){
        try {
            
            if(TipoFuente::$fuenteEntidad==$request->tipo_fuente_id)
            {
               $lstPeriodo=Periodo::consulPeriodoAbierto(); 
                
            }else{
                
               $lstPeriodo=Periodo::consultarLstPeriodo();  
            }
           
            return $lstPeriodo;
         }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                   return redirect()->back();
           }  
        
    }
    public function imprimirError($lstErrores,$lstCuentaNoReferencia,$error,$lstCuentaRegistradas){
        
        $lstCodigo = [TipoFuente::$fuenteMF,  TipoFuente::$fuenteBsE , TipoFuente::$fuenteEntidad];
        $lstTipoFuente = TipoFuente::buscarLstTipoFuentexId($lstCodigo);
        $lstPeriodo = Periodo::all();
        if(!empty($lstErrores) || $lstErrores!=null)
         Session::put('variable', $lstErrores);
        if(!empty($lstCuentaNoReferencia) || $lstCuentaNoReferencia!=null)
         Session::put('lstCuentasInconsist', $lstCuentaNoReferencia);
        if(!empty($lstCuentaRegistradas) && $lstCuentaRegistradas!=null)
            Session::put('lstCuentaRegistradas', $lstCuentaRegistradas);
             
     
        
        return view('autoevaluacion.infoFinanciera', compact('lstTipoFuente','lstPeriodo','lstErrores','lstCuentaNoReferencia','error','lstCuentaRegistradas'));
    }
  
    
   

    public function guardarInfoFinan(Request $request){
      
      
        try {
            
             $messages = [
                'archivo_carga.required' => 'Campo :attribute es requerido.',
                'archivo_carga.max' => 'Campo :attribute debe tener un tamaño de :max.',
                'archivo_carga.unique' => 'Campo :attribute ya se encuentra registrado.',
            ];
            $validator = Validator::make($request->all(),[
                'archivo_carga_archivo_byte' => 'required'
            ], $messages);

            if ($validator->fails()) {
                return redirect('infoFinanciera')
                                ->withErrors($validator)
                                ->withInput();
            }else{
              
                    //  $lstCuentas=  Cuenta::buscarLstCuentaxTipoxFuente($request->tipo_cuenta_id, $request->tipo_fuente_id);
                   // $lstValCuenta=  ValCuentaEntidad::all();
                  // Lee los nombres de los campos
                 //$products = csvToArray('products.csv');
                $objTipoArchivo=TipoArchivo::find($request->tipo_archivo_id); 
                $objParamArchivCarga= ParametroArchivoCarga::consultaParametroArchivo($objTipoArchivo->tipo_archivo_parametro);
                if($objParamArchivCarga!=null){
                    $lstconfigColumna= ConfiguracionColumna::consultLstConfigArchivo($objParamArchivCarga->parametro_archivo_carga_id);
                    if(!$lstconfigColumna->isEmpty()){
                        $lstconfigColum=$lstconfigColumna->toArray();
                        $delimiter="~";// le envio el delimitardor que se pondr en cada fila del csv
                        $array_product = array();
                        $file=$request->file('archivo_carga_archivo_byte');  
                        $array_product=$this->get2DArrayFromCsv($file,$delimiter);
                        if($array_product!=null){
                            $i=1;
                            $x=0;
                            $r=1;
                            $y=0;
                            $lstErrores=array();
                            $lstCuentaNoReferencia=array();
                            $lstCuentaRegistradas=array();
                            for ($c=0; $c < count($array_product); $c++) {
                               if($c==1)
                                $valFila=array(1);
                                $valFila=@explode(';', $array_product[$c][$i]);
                                $cantColumnas=count($valFila);
                                    if(intval($c)>0){
                                        //Recorro las columnas de la fila $valFila
                                        for($d=0; $d <count($valFila); $d++){
                                            for($e=0; $e <count($lstconfigColumna); $e++){
                                                $lstErrorCsv= array();
                                                if($lstconfigColumna[$e]['configuracion_columna_posicion']==$d){
                                                     $valid=false;
                                                    if($lstconfigColumna[$e]['configuracion_columna_requerido']){
                                                        if($valFila[$d]!=""){
                                                            if($lstconfigColumna[$e]['configuracion_columna_tipo_dato']=="number"){
                                                                if(!(integer)$valFila[$d]){ // valida si es integer devuelve 0 si no es
                                                                    $valid=true; 
                                                                    $lstErrorCsv = array('tipoDato'=>"number" );
                                                                }
                                                            } 
                                                            if(strlen($valFila[$d])>$lstconfigColumna[$e]['configuracion_columna_longitud_dato']){
                                                                   $lstErrorCsv=array_add($lstErrorCsv,"logDato", $lstconfigColumna[$e]['configuracion_columna_longitud_dato']);
                                                                   $valid=true;
                                                               }
                                                            if(count($valFila)!=$objParamArchivCarga->parametro_archivo_carga_num_columnas_fijas){
                                                                   $lstErrorCsv=array_add($lstErrorCsv,"numColum", $objParamArchivCarga->parametro_archivo_carga_num_columnas_fijas); 
                                                                   $valid=true;
                                                               }   
                                                            if($lstconfigColumna[$e]['configuracion_columna_tipo_dato']=="fecha"){
                                                               $fechValida=$this->validateDate($valFila[$d],'Y-m-d');
                                                               if($fechValida){
                                                                   $lstErrorCsv=array_add($lstErrorCsv,"tipoDato", 'Fecha formato(yyyy/mm/dd) : '.date("Y/m/d").' '); 
                                                                   $valid=true;
                                                                }
                                                            }
                                                        }else{
                                                               $valid=true;
                                                               $lstErrorCsv=array_add($lstErrorCsv,"requerido" ,'Si'); 
                                                        }  
                                                        if($valid){
                                                            $lstErrorCsv=array_add($lstErrorCsv, 'idColumna' ,$d); 
                                                            $lstErrorCsv=array_add($lstErrorCsv, "numColum", ''); 
                                                            $lstErrorCsv=array_add($lstErrorCsv,  'posColum',$c); 
                                                            $lstErrorCsv=array_add($lstErrorCsv, "logDato", ''); 
                                                            $lstErrorCsv=array_add($lstErrorCsv, "tipoDato", ''); 
                                                            $lstErrorCsv=array_add($lstErrorCsv, "requerido", ''); 
                                                            $lstErrores[$x]=$lstErrorCsv;
                                                             $x++;
                                                        } 
                                                    } 
                                                    if($lstconfigColumna[$e]['configuracion_columna_posicion']==0) {
                                                       if($valFila[$d]!=null){ 
                                                        $cuentaExis=Cuenta::consultaCuentaExistente($valFila[$d],$request->tipo_cuenta_id, $request->tipo_fuente_id); 
                                                        if($cuentaExis==0){
                                                            $objError=array('cuenta'=> $valFila[$d]);
                                                            $lstCuentaNoReferencia[$r]=$objError;
                                                            $r++;
                                                        }
                                                       }
                                                      $lstCuentas=array();
                                                      $lstCuentas=array_add($lstCuentas, 'codigo' , $valFila[0]);
                                                    }
                                                    if($lstconfigColumna[$e]['configuracion_columna_posicion']==1){$lstCuentas=array_add($lstCuentas, 'cuenta' , $valFila[$d]);}
                                                    if($lstconfigColumna[$e]['configuracion_columna_posicion']==2){
                                                        $lstCuentas=array_add($lstCuentas, 'totales' ,$valFila[$d]);
                                                        $lstCuentaRegistradas[$y]=$lstCuentas;
                                                        $y++;
                                                        
                                                    }
                                                    
                                                }
                                                
                                            }
                                                     
                                        }             
                                    }     
                            }
                             
                        }else{
                             Session::flash('message', 'El archivo subido no contiene datos .');
                            return redirect()->back();
                        }
                        
                    }else{
                           Session::flash('message', ' No se ha parametrizado la configuracion columna del archivo cargado.');
                           return redirect()->back();
                    }   
                }else{
                         Session::flash('message', ' No se registrado el parametro del archivo carga de '.$objTipoArchivo->tipo_archivo_nombre.'');
                        return redirect()->back();
                }
                if(!empty($lstErrores)  || !empty($lstCuentaNoReferencia)){
                         Session::forget('lstCuentaRegistradas');
                         $lstCuentaRegistradas=null;
                         //dd($lstCuentaNoReferencia);
                        return  $this->imprimirError($lstErrores,$lstCuentaNoReferencia,TRUE,$lstCuentaRegistradas);
                }else{
                        
                         //Session::put('variable', $lstErrores);
                         //Session::put('lstCuentasInconsist', $lstCuentaNoReferencia);
                         Session::forget('variable');
                         Session::forget('lstCuentasInconsist');
                        // subo el archivo
                        $pathToFile="";
                        $containfile=false;
                        if($request->hasFile('archivo_carga_archivo_byte') ){
                            $containfile=true; 
                            $file = $request->file('archivo_carga_archivo_byte');
                            $nombre=$file->getClientOriginalName();
                            $tipo=$file->getClientOriginalExtension();
                            //$tipo=$file->getMimeType();
                            $r=Storage::disk('local')->put($nombre,  \File::get($file));
                            $pathToFile= storage_path('app') ."/". $nombre;
                       }
                        $objArchivoCarga= new ArchivoCarga();
                        $objArchivoCarga->periodo_id=$request->periodo_id;
                        $objArchivoCarga->usuario_id=$usuario=Auth::user()->usuario_id;
                        $objArchivoCarga->archivo_carga_tipo=$tipo;
                        $objArchivoCarga->archivo_carga_fecha= Carbon::now();
                        $objArchivoCarga->archivo_carga_nombre=$nombre; 
                        $byteArray =file_get_contents($file);
                        //$byteArray = unpack("N*",file_get_contents($file));
                        //$cadena_equipo = implode(";", $byteArray); 
                        $objArchivoCarga->archivo_carga_archivo_byte=$byteArray;
                        $objArchivoCarga->save();
                        File::delete(storage_path('app') ."/". $nombre);    
                        
                     return  $this->imprimirError($lstErrores,$lstCuentaNoReferencia,false,$lstCuentaRegistradas);      
                }
                  
            }
            
         }catch(\Exception $e){
            Session::flash('message',$e->getMessage());
            return redirect()->back();
        }        
    }
    
    public function buscarParamArchCarga(Request $request){
        try {
            $objTipoArchivo=TipoArchivo::find($request->tipo_archivo_id);
            $countParamArchCarg=ParametroArchivoCarga::consultaParametroCarga($objTipoArchivo->tipo_archivo_parametro);
            $valArray=array();
            $boo=1;
            if($countParamArchCarg!=0 ){
                if($countParamArchCarg==1){
                    $objParamArchCarg=ParametroArchivoCarga::consultaParametroArchivo($objTipoArchivo->tipo_archivo_parametro);
                        $lstConfigColum= ConfiguracionColumna::consultLstConfigArchivo($objParamArchCarg->parametro_archivo_carga_id);
                        if(count($lstConfigColum)>0){
                            $nombreColum='';
                            $ejempColum='';
                            foreach ($lstConfigColum as $select){
                                $nombreColum=$nombreColum.'|'.$select->configuracion_columna_nombre_columna;
                                $ejempColum=$ejempColum.'|'.$select->configuracion_columna_ejemplo;
                            }
                            $mensaje='El archivo Excel debe tener '.count($lstConfigColum).' columnas ordenadas por: '.$nombreColum.'|'.' como muestra el Ejemplo : '.$ejempColum;
                            return array('mensaje'=> $mensaje, 'bar'=>$boo=1); 
                        }else{
                            $mensaje='El tipo de archivo '.$objTipoArchivo->tipo_archivo_nombre.' no se ha parametrizado en la configuración columna comuniquese con sistemas'; 
                            return array('mensaje'=> $mensaje, 'bar'=>$boo=0);
                        }   

                }else{
                    $mensaje='La parametrización del tipo de archivo '.$objTipoArchivo->tipo_archivo_nombre.' esta repetido comuniquese con sistemas comuniquese con sistemas';
                    return array('mensaje'=> $mensaje, 'bar'=>$boo=0);
                }
            }else{
                $mensaje='no se ha parametrizado el tipo de archivo '.$objTipoArchivo->tipo_archivo_nombre.' comuniquese con sistemas';
                return array('mensaje'=> $mensaje, 'bar'=>$boo=0);
            }
         } catch (\Exception $e) {
             Session::flash('message',$e->getMessage());
             return redirect()->back();
        }
    }


    /**
     * 
     * @param type $lstArray
     * @param type $lstCuentasInconsist
     * @param type $vistUrl
     * @param type $lstCuentasRegistradas
     * @return type
     * @author JRendon
     * @abstract metrodo imprimir pdf
     */

    public  function crearPdf($lstArray,$lstCuentasInconsist,$vistUrl,$lstCuentasRegistradas){
            $date = date('Y-m-d');
            $view =  \View::make($vistUrl, compact('lstArray','lstCuentasInconsist','date','lstCuentasRegistradas'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            return $pdf->download('reporte.pdf');

    }
    
    public function imprimirPdf(Request $request){
         $lstArray=Session::get('variable');
         $lstCuentasInconsist=Session::get('lstCuentasInconsist');
         $lstCuentasRegistradas=Session::get('lstCuentaRegistradas');
         
         $vistUrl="pdf.informeValidacionCarga";
         return $this->crearPdf($lstArray,$lstCuentasInconsist,$vistUrl,$lstCuentasRegistradas);
       
      
    }
    /**
     *  @abstract imprimir excel
     */
    public  function imprimirExcel(){
        
        //dd(Session::get('variable'));
        
         Excel::create('Administración de informaciòn financiera', function($excel) {
               $lstArray=Session::get('variable');
               $lstCuentasRegistradas=Session::get('lstCuentaRegistradas');
               $lstCuentasInconsist=Session::get('lstCuentasInconsist');
             
        if(!is_null($lstArray)){       
            $excel->sheet('Errores de formato', function($sheet)use($lstArray) {
                $lstExcel=array();
                $objErrorFormato=array();
                $x=0;
                 $sheet->cells('A1:F1', function($cells) {
                     $cells->setFont(array(
                      'family'     => 'Calibri',
                      'size'       => '12',
                      'bold'       =>  true
                        ));
                });
                
                for ($i=0; $i <count($lstArray); $i++)
                {
                    $lstExcel=array();
                    $lstExcel=array_add($lstExcel,'idColumna',number_format($lstArray[$i]['idColumna']));
                    $lstExcel=array_add($lstExcel,'numColum',$lstArray[$i]['numColum']);
                    $lstExcel=array_add($lstExcel,'posColum',$lstArray[$i]['posColum']);
                    $lstExcel=array_add($lstExcel,'tipoDato',$lstArray[$i]['tipoDato']);
                    $lstExcel=array_add($lstExcel,'logDato',$lstArray[$i]['logDato']);
                    $lstExcel=array_add($lstExcel,'requerido',$lstArray[$i]['requerido']);

                    $objErrorFormato[$x]=$lstExcel;
                  $x++;
                }
               $sheet->fromArray($objErrorFormato);
                   
               
            });
         }  
          if(!is_null($lstCuentasInconsist)){
            // Our second sheet
              
            $excel->sheet('Errores de contenido', function($sheet)use($lstCuentasInconsist) {
                 $sheet->cells('A1', function($cells) {
                     $cells->setFont(array(
                      'family'     => 'Calibri',
                      'size'       => '12',
                      'bold'       =>  true
                        ));
                });
                $sheet->fromArray($lstCuentasInconsist);
            });
        }    
         if(!is_null($lstCuentasRegistradas)){  
            // Our second sheet
              $excel->sheet('Totales Registrados', function($sheet)use($lstCuentasRegistradas) {
               $sheet->cells('A1:D1', function($cells) {
                     $cells->setFont(array(
                      'family'     => 'Calibri',
                      'size'       => '12',
                      'bold'       =>  true
                        ));
                });    
              $sheet->fromArray($lstCuentasRegistradas);
            });
          }   
        })->export('xls');
 
        
    }
    public static function validateDate($date, $format)
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
    /**
     * 
     * @param type $file
     * @param type $delimiter
     * @return type
     * @abstract Lee el excel
     */
    public static function get2DArrayFromCsv($file,$delimiter) {
        $data2DArray=array();
        if (($handle = fopen($file, "r")) !== FALSE) { 
            $i = 0; 
            while (($lineArray = fgetcsv($handle, 4000, $delimiter)) !== FALSE) { 
                for ($j=0; $j<count($lineArray); $j++) { 
                    $data2DArray[$i][$j+1] = $lineArray[$j]; 
                    
                } 
                $i++; 
            } 
            fclose($handle); 
        } 
        return $data2DArray;
        
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
