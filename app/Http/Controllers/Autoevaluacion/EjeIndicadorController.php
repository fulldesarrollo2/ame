<?php

namespace App\Http\Controllers\Autoevaluacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Illuminate\Support\Facades\Session;
use App\Entidades\TipoEntidad;
use App\Entidades\DivisionPolitica;
use App\Entidades\ValCuentaEntidad;
use App\Entidades\Cuenta;
use App\Entidades\TipoFuente;
use App\Entidades\ArchivoCarga;
use App\Entidades\Indicador;
use App\Entidades\IndicadorTemporal;
use App\Entidades\VariableIndicador;
use App\Entidades\OperadoresMatematicos;
use App\Entidades\SignosAgrupacion;
use App\Entidades\Auditoria;
use Maatwebsite\Excel\Facades\Excel;

class EjeIndicadorController extends Controller
{
    /* 
     * @return type
     */
    
    public function index() {
        $fuenteBdE = TipoFuente::$fuenteBdE;
        $lstCodigoEntidad = [TipoEntidad::$gadm,  TipoEntidad::$empServBasMunicipales];
        $lstTipoEntidad = TipoEntidad::buscarLstTipoEntidadxId($lstCodigoEntidad);
        $lstCodigoFuente = [TipoFuente::$fuenteBdE , TipoFuente::$fuenteEntidad];
        $lstTipoFuente = TipoFuente::buscarLstTipoFuentexId($lstCodigoFuente);
        $objDivisionPolitica = DivisionPolitica::consultarDivisionPolitica();
        $lstIndicador = Indicador::buscarLstIndicador();
        $lstVariableIndicador = VariableIndicador::buscarLstVariableIndicador();
        $lstOperadoresMatematicos = OperadoresMatematicos::buscarLstOperadoresMatematicos();
        $lstSignosAgrupacion = SignosAgrupacion::buscarLstSignosAgrupacion();
        $varIndicadorFinanciera = VariableIndicador::$varIndicadorFinanciera;
        $varIndicadorServicios = VariableIndicador::$varIndicadorServicios;
        $varIndicadorGeneral = VariableIndicador::$varIndicadorGeneral;
        
        
        return view('autoevaluacion.ejeIndicador', compact('lstTipoEntidad','objDivisionPolitica','lstTipoFuente','fuenteBdE','lstIndicador','lstVariableIndicador','lstOperadoresMatematicos','lstSignosAgrupacion','varIndicadorFinanciera', 'varIndicadorServicios','varIndicadorGeneral'));
    }
    
    
    
    /*
     * @param Request $request
     * @author RGARCIA
     * @abstract implementado para generar el árbol de cuentas con los valores
     */

    public function consultaValoresCuentas(Request $request){
        try {
            
                //lista de las cuentas para armar el árbol
                $lstCuenta= Cuenta::buscarLstCuentaxEntidadxFuente($request->entidad_id,$request->tipo_fuente_id); 
                //$lstCuenta= Cuenta::buscarLstCuentaxEntidadxFuente($request->entidad_id,2); 
                
                $objArchivoCarga= ArchivoCarga::consultaArchivoCargaPorPeriodo($request->periodo_id);
                
                if(!empty($lstCuenta) && !empty($objArchivoCarga)){  
                    foreach ($lstCuenta as $select){
                        $valor = ValCuentaEntidad::consultaValCuentaEntidad($select->entidad_id,$select->cuenta_id,$objArchivoCarga->archivo_carga_id);
                        $valor = $valor!=null?$valor->val_cuenta_entidad_valor:0;
                        
                        $select = array_add($select,'valor',$valor);
                    }
                    return $lstCuenta;
                }

        }catch(\Exception $e){
                Session::flash('message',$e->getMessage());
                   
        }
    }
    
    /*
    * Permite ejecutar la fórmula de un indicador
    * @author RGARCIA
    */
    
    public function ejecutarFormulaIndicador($formula,$entidad_id,$tipo_fuente_id,$periodo_id,$operadores,$signosAgrupacion){
        $esOperando = true;
        $esVariable = false;

        $variableNoEsEncontrada = false;
        $variableNoEncontrada = "";

        $formula = ltrim($formula);
        $elementosFormula = preg_split("/\s{2}/", $formula);
        
        $error = false;

        //Por cada elemento de la fórmula verificar si es variable para buscar su valor
        foreach($elementosFormula as &$elemento){
            if (in_array($elemento, $operadores)){
                $esOperando=false;
                $esVariable=false;
            }
            else if(in_array($elemento, $signosAgrupacion)){
                $esOperando=false;
                $esVariable=false;
            }
            else{
                $esOperando=true;
                if (is_numeric ($elemento)) {
                     $esVariable=false;
                }
                else{
                     $esVariable=true;
                }
                if($esOperando && $esVariable){
                    //busca en Cuentas cuando es Variable Financiera

                    $valorVariable=$this->buscarValorVariableFinancieraIndicador($elemento,$entidad_id,$tipo_fuente_id,$periodo_id);

                    if($valorVariable == 0){
                        $variableNoEsEncontrada = true;
                        $variableNoEncontrada = $elemento;
                    }
                    else {
                        $elemento=$valorVariable;
                    }

                    //busca en xxxx cuando es Variable Servicios Básicos

                    //busca en xxxx cuando es Variable General
                }
            }
        }
        unset($elemento);

        //Si no encuentra el valor de la variable no es posible ejecutar la fórmula
        if($variableNoEsEncontrada){
            $valorFormula = 'Variable no encontrada: '. $variableNoEncontrada; 
            $error = true;
        }
        else{
            $formula= implode($elementosFormula);  

            if($this->validaFormula($formula, $operadores, $signosAgrupacion)){
                eval("\$valorFormula = $formula;");  
            }
            else{
                $valorFormula = "Error. Verifique la fórmula";   
                $error = true;
            }   
        }
        //return $valorFormula;
        return array($valorFormula, $error);
    }
    
    /*
    * Permite actualizar la lista deIndicadores Ejecutados
    * @param Request $request
    * @return type
    * @author RGARCIA
    */
    public function ejecutarIndicadores(Request $request){
        try {
            $lstIndicadoresEjecutados = [];
            $lstIdIndicadores = $request->indicadores_id;
            $entidad_id = $request->entidad_id;
            $tipo_fuente_id = $request->tipo_fuente_id;
            $periodo_id = $request->periodo_id;
            
            $operadores   = array('+', '-', '*', '/', '^');
            $signosAgrupacion = array('(', ')', '[', ']', '{', '}');
            
            if(!empty($lstIdIndicadores)){  
                $lstIndicadoresEjecutados=Indicador::buscarIndicadores($lstIdIndicadores); 
                if(!empty($lstIndicadoresEjecutados)){  
                    
                    //Por cada indicador de la lista, ejecutar la fórmula
                    foreach ($lstIndicadoresEjecutados as $select){
                        $formula = $select->indicador_formula;
                        list($valorFormula, $error)=$this->ejecutarFormulaIndicador($formula,$entidad_id,$tipo_fuente_id,$periodo_id,$operadores,$signosAgrupacion);
                        //$valorFormula=$this->ejecutarFormulaIndicador($formula,$entidad_id,$tipo_fuente_id,$periodo_id,$operadores,$signosAgrupacion);
                        $select = array_add($select,'valorFormula',$valorFormula);
                    }
                }
            }
            return $lstIndicadoresEjecutados;
        } catch (\Exception $ex) {
            Session::flash('message',$e->getMessage());
        }   
    }
    
    
    /*
     * Busca el valor que corresponde a la variable financiera del indicador
     * @author RGARCIA
     */
    public function buscarValorVariableFinancieraIndicador($variable,$entidad_id,$tipo_fuente_id,$periodo_id){
        try {
                $objCuenta=Cuenta::buscarCuentaxNombrexEntidadxFuente($variable,$entidad_id,$tipo_fuente_id); 
                $objArchivoCarga= ArchivoCarga::consultaArchivoCargaPorPeriodo($periodo_id);
                
                if( $objCuenta!=null && $objArchivoCarga!=null){ 
                    
                    $valor = ValCuentaEntidad::consultaValCuentaEntidad($entidad_id,$objCuenta->cuenta_id,$objArchivoCarga->archivo_carga_id);
                    
                    if($valor!=null){     
                        return $valor->val_cuenta_entidad_valor;
                    }
                    else{     
                        return 0;
                    }
                }
                else{     
                    return 0;
                }

        }catch(\Exception $e){
                Session::flash('message',$e->getMessage());
                   
        }
    }
    
    /*
     * Busca el valor que corresponde a la variable general del indicador
     * @author RGARCIA
     */
    public function buscarValorVariableGeneralIndicador(){
        
    }
    
    /*
     * Busca el valor que corresponde a la variable servicios del indicador
     * @author RGARCIA
     */
    public function buscarValorVariableServiciosIndicador(){
        
    }
    
    /**
     * Verifica que la fórmula se pueda ejecutar
     * @author RGARCIA
     */
    public function validaFormula($formula, $operadores, $signosAgrupacion){
        
        $formula = preg_replace('/\s+/', '', $formula);
 
        $formula = str_split($formula);
        $error = false;
        $numParentesis = 0;
        $numCorchetes = 0;
        $numLlaves = 0;
        
        if(empty($formula)) {
            $error = true;
        }
        
        for($i=0; $i<count($formula); $i++){
 
                // Si no es número debe ser operador o signo de agrupación
                if(     !is_numeric($formula[$i]) 
                        && !in_array($formula[$i], $operadores) 
                        && !in_array($formula[$i], $signosAgrupacion)             
                ){
                        $error = true;
                        break;
                }
               
                // Verifica cantidad de signos de agrupación
                if($formula[$i]=='(') {  
                    $numParentesis++ ;
                }
                if($formula[$i]==')') { 
                    $numParentesis--;
                }
                if($formula[$i]=='[') { 
                    $numCorchetes++;
                }
                if($formula[$i]==']') {
                    $numCorchetes--;
                }
                if($formula[$i]=='{') {
                    $numLlaves++;
                }    
                if($formula[$i]=='}') {
                    $numLlaves--;
                }
               
               
                if($formula[$i]===')'){                                         
                    if(     isset($formula[$i+1])
                            &&      !in_array($formula[$i+1], $operadores) 
                            &&      $formula[$i+1]!=')'
                            &&      $formula[$i+1]!=']'
                            &&      $formula[$i+1]!='}'
                    ){
                            $error = true;
                            break;
                    }
                    continue;
                }
 
                //Si es número, el siguiente debe ser un operador, cierre de signo de agrupación u otro número
                if(is_numeric($formula[$i])                                     
                ){
                    if(     isset($formula[$i+1]) 
                            &&      !in_array($formula[$i+1], $operadores) 
                            &&      $formula[$i+1]!=')'
                            &&      !is_numeric($formula[$i+1])
                    ){
                            $error = true;
                            break;
                    }
                    continue;
                } 
               
                //No deben ser dos operadores seguidos ni un operador debe ser el último elemento de la fórmula
                if(     in_array($formula[$i], $operadores) || $formula[$i]=='(' ){
                        if(     (
                                isset($formula[$i+1])   
                                    && (in_array($formula[$i+1], $operadores) || $formula[$i+1]==')')
                                )
                                || ($i+1 == count($formula))
                        ){
                                $error = true;
                                break;
                        }
                        continue;
                }       
        }
       
        //La misma cantidad de signos de agrupación que se abren deben cerrarse
        if($numParentesis!=0 || $numCorchetes !=0 || $numLlaves !=0){
                $error = true;
        }
       
        return !$error;
    }
    
    /**
     * Verifica que no se repita el nombre del indicador
     * Guarda los datos del Indicador Temporal   
     * @param Request $request
     * @return type
     */
    public function guardarIndicadorTemporal(Request $request){
        try {
            $messages = [
                'indicador_temporal.required' => 'Campo :attribute es requerido.',
                'indicador_temporal.max' => 'Campo :attribute debe tener un tamaño de :max.',
            ];

            $validator = Validator::make($request->all(), [
                        'indicador_temporal_nombre' => 'required|max:254',
                        'indicador_temporal_descripcion' => 'required|max:254',
                        'indicador_temporal_formula' => 'required|max:254',
                            ], $messages);

            if ($validator->fails()) {
                //Obtenemos los mensajes de error de la validation
                $messages = $validator->messages();
                //Redireccionamos a nuestro formulario de atras con los errores de la validación 
                return response()->json(array('errors' => $messages));
                
                
            } else {
                $operadores   = array('+', '-', '*', '/', '^');
                $signosAgrupacion = array('(', ')', '[', ']', '{', '}');
                
                $formula = $request->indicador_temporal_formula;
                $entidad_id = $request->entidad_id;
                $periodo_id = $request->periodo_id;
                $tipo_fuente_id = $request->tipo_fuente_id;
                
                //Obtener el valor de la fórmula
                list($valorFormula, $error)=$this->ejecutarFormulaIndicador($formula,$entidad_id,$tipo_fuente_id,$periodo_id,$operadores,$signosAgrupacion);
                
                if($error){
                    $mensajes=['ERROR EN LA FORMULA DEL INDICADOR TEMPORAL: ' . $valorFormula . ' .'];
                    $mensajesError=[$mensajes];

                    return response()->json(array('errors' => $mensajesError));
                }
                else{
                    $usuario=Auth::user();
                    $objIndicadorTemporal = new IndicadorTemporal();
                    $objIndicadorTemporal->indicador_temporal_nombre=strtoupper($request->indicador_temporal_nombre);
                    $objIndicadorTemporal->indicador_temporal_descripcion=strtoupper($request->indicador_temporal_descripcion);
                    $objIndicadorTemporal->indicador_temporal_formula=$formula;
                    $objIndicadorTemporal->indicador_temporal_valor=$valorFormula;
                    $objIndicadorTemporal->usuario_id=$usuario->usuario_id;
                    $objIndicadorTemporal->periodo_id=$periodo_id;
                    $objIndicadorTemporal->entidad_id=$entidad_id;

                    $objIndicadorTemporal->save();

                    Auditoria::ingresoAuditoria(Auditoria::$guardar, url()->previous());

                    $mensajeExito='INDICADOR TEMPORAL: ' . $objIndicadorTemporal->indicador_temporal_nombre . ' ha sido registrado con éxito.';

                    return response()->json(array('success' => $mensajeExito,'indicadorTemporal'=> $objIndicadorTemporal->indicador_temporal_nombre));
                }
                
                
            }

        } catch (\Exception $ex) {
            Session::flash('message',$e->getMessage());
        }
        
    }
    
    /**
     * @param Request $request
     * @author RGARCIA
     * @abstract implementado para generar el pdf
     */
    public function imprimirPdfEjeIndicador(Request $request){
        try {
                $vistUrl="pdf.ejeIndicadorPdf";
                $lstValores=array();
                return $this->crearPdf($vistUrl,$lstValores);
                
        
        }catch(\Exception $e){
                   Session::flash('message',$e->getMessage());       
        }    
    }
   
    public  function crearPdf($vistUrl,$lstValores){
        try {
             
                $date = date('Y-m-d');
                $view =  \View::make($vistUrl, compact('lstValores'))->render();
                $pdf = \App::make('dompdf.wrapper');
                $pdf->loadHTML($view);
                return $pdf->download('Reporte de Ejecución de Indicadores.pdf');
                
        }catch(\Exception $e){
                   Session::flash('message',$e->getMessage());
                   return redirect()->back();
        }                
    }
    
    public  function imprimirExcelEjeIndicador(Request $request){
 
        Excel::create('Reporte de Ejecución de Indicadores', function($excel) {
            $excel->sheet('Ejecución de indicadores', function($sheet){
                $sheet->fromArray();
            });
            
        })->export('xls');      
         
    }
    
    
 

}
