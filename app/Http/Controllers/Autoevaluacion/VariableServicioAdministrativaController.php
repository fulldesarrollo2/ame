<?php

namespace App\Http\Controllers\Autoevaluacion;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entidades\Cargo;
use App\Entidades\InformacionAdministrativa;
use App\Entidades\VariableSeguridad;
use Illuminate\Support\Facades\Session;
use Auth;

class VariableServicioAdministrativaController extends Controller
{
    public function index(){
    	$listCargo = Cargo::all();
    	return view('autoevaluacion.variableServAdministrativa', compact('listCargo')) ;
    }
    public function store(Request $request)
    {

        $variableSeguidad=VariableSeguridad::$periodoActual;
        $objVariableSeguridad=VariableSeguridad::ConsultaVariable($variableSeguidad);
        $objAdministrativo=new InformacionAdministrativa();
        $objAdministrativo->entidad_id=Auth::user()->entidad_id;
        $objAdministrativo->periodo_id=$objVariableSeguridad->variable_seguridad_id;
        $objAdministrativo->cargo_id=$request->cargo;
        $objAdministrativo->informacion_administrativa_remuneracion=$request->remuneracion;
        $objAdministrativo->informacion_administrativa_participacion=$request->participacion;
        $objAdministrativo->informacion_administrativa_tipo_costo=$request->tipoCosto;
        $objAdministrativo->informacion_administrativa_cantidad_personas=$request->cantidadPersonas;
        $objAdministrativo->save();
        Session::flash('message', 'El registro ha sido Almacenado');
            return redirect()->back();
    }
}
