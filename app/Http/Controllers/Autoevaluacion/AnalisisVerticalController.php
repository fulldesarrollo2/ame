<?php

namespace App\Http\Controllers\Autoevaluacion;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Entidades\DivisionPolitica;
use App\Http\Controllers\Controller;
use App\Entidades\ValCuentaEntidad;
use App\Entidades\TipoFuente;
use App\Entidades\Entidad;
use App\Entidades\TipoEntidad;
use App\Entidades\EntRelacionaDivPo;
use App\Entidades\Periodo;

class AnalisisVerticalController extends Controller {

    public function index() {
        $fuenteBdE = TipoFuente::$fuenteBdE;
        $lstCodigofuente = [TipoFuente::$fuenteBdE, TipoFuente::$fuenteEntidad];

        $lstTipoFuente = TipoFuente::buscarLstTipoFuentexId($lstCodigofuente);
        $lstaCodigo = [TipoEntidad::$gadm, TipoEntidad::$empServBasMunicipales];
        $lstTipoEntidad = TipoEntidad::buscarLstTipoEntidadxId($lstaCodigo);
        $lstDivisionPolitica = DivisionPolitica::consultarDivisionPolitica();
        return view('autoevaluacion.analisisVertical', compact('lstDivisionPolitica', 'lstTipoEntidad', 'lstTipoFuente', 'fuenteBdE'));
    }

    public function consultarlstEntidad(Request $request) {

        $lstEntidad = EntRelacionaDivPo::consultaEntidadTipo($request->division_politica_id, $request->tipoEntidad);
        return $lstEntidad;
    }

    public function consultarValCuentaEntidadxEntidad() {
        $cuentas = ValCuentaEntidad::consultarValCuentaEntidadxEntidad();
        dd($cuentas);
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function buscarLstValCtaEntidadxFiltro(Request $request) {
        try {
            $entidad_id =  $request->entidad_id;
            $tipo_fuente_id =   $request->tipo_fuente_id;
            $periodo_id_desde = $request->periodo_id_desde;
            $periodo_id_hasta = $request->periodo_id_hasta;
            $tamanio = $request->tamanio;
            $inicioPeriodo =$request->inicioPeriodo;
            $finPeriodo = $request->finPeriodo;
            
            $lstValCuentaEntidad = ValCuentaEntidad::consultaValCuentaEntidadXPeriodo($entidad_id, $tipo_fuente_id, $periodo_id_desde, $periodo_id_hasta, null);
            
            $lstCuentaTbl = [];
            
            foreach ($lstValCuentaEntidad as $cuenta) {
                $codCuenta =    $cuenta->cuenta_codigo;
                $nombreCuenta = $cuenta->cuenta_nombre;
                $valorCuenta= $codCuenta .' '.$nombreCuenta;
                $lstValCuenta = $cuenta->valCuentaEntidad;
                
                
                $cuentaTbl = array($valorCuenta);
                
                foreach ($lstValCuenta as $valcuenta) {
                    
                    if($tipo_fuente_id == TipoFuente::$fuenteBdE){
                        $periodo = $valcuenta->archivoCarga->periodo->periodo_anio;
                    }else{
                        $periodo = $valcuenta->archivoCarga->periodo->periodo_mes;
                    }
                    
                    if($periodo >= $inicioPeriodo && $periodo <= $finPeriodo){
                        $valCuentaValor = $valcuenta->val_cuenta_entidad_valor;
                        array_push($cuentaTbl, $valCuentaValor); 
                    }
                }
                
                $arrayAnios= [];
                for ($i=1;$i<=$tamanio;$i++) {
                    $p=$i;
                    $q=$i+1;
                    $valorAnioActual = $cuentaTbl[$q];
                    $valorAnioAnterior = $cuentaTbl[$p];
                    $totValorAnio; 
                 
                    if($valorAnioAnterior > 0){
                       $totValorAnio = (($valorAnioActual / $valorAnioAnterior)-1)*100;
                    }else
                       $totValorAnio = 100;
                    
                    $totValorAnio = round($totValorAnio);     
                    array_push($arrayAnios,$totValorAnio);
                    array_push($cuentaTbl,$totValorAnio);
                }
               
                	
                $tmValor= count($arrayAnios);
                $totalValores = 0;
                for ($j=0;$j<$tmValor;$j++) {
                     $totalValores = $totalValores + $arrayAnios[$j];
                }
                
                $porcentaje = ($totalValores/ $tamanio);            
                $porcentaje = round($porcentaje); 
                array_push($cuentaTbl,$porcentaje); 
                array_push($lstCuentaTbl , $cuentaTbl);
            }
            
            
            return $lstCuentaTbl;
        } catch (Exception $e) {
                notify()->flash($e->getMessage(), 'danger');  
        }
    }

    public function consultarLstPeriodoDinamico(Request $request) {
        $periodo = Periodo::consultarLstPeriodoDinamico($request->tipofuente);
        return $periodo;
    }

    public function consultaValCuentaEntidadXPeriodo(Request $request) {

        $valCuentaEntidad = ValCuentaEntidad::consultaValCuentaEntidadXPeriodo($request->entidad, $request->tipo_fuente, $request->valdesde, $request->valhasta, null);
        return $valCuentaEntidad;
    }

}
