<?php

namespace App\Http\Controllers\Autoevaluacion;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Entidades\ProyectoFinanciero;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Entidades\EntRelacionaDivPo;
use App\Entidades\DivisionPolitica;
use Maatwebsite\Excel\Facades\Excel;

class ProyectoFinancieroController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{		
		$objDivisionPolitica = DivisionPolitica::consultarDivisionPolitica();
		return view('autoevaluacion.consultaProyectoFinanciero',compact('objDivisionPolitica'));	    
	}
	
	public function consultaEntRelacionaDivPoPorDivisionPolitica(Request $request){
		$objDivisionPolitica = EntRelacionaDivPo::consultaEntRelacionaDivPoPorDivisionPolitica($request->id,$request->tipo);
		return $objDivisionPolitica;
	}
	
	public function consultarProyectoFinanciero(Request $request){
		$objProyectoFinanciero = ProyectoFinanciero::consultarProyectoFinanciero($request);
		
		return $objProyectoFinanciero;

	}
	
	public function imprimirExcelProyectoFinanciero(request $request){
		$objProyectoFinanciero = ProyectoFinanciero::consultarProyectoFinanciero($request);
		
		
		$index=0;$index2=0;$index3=0;$mayor=0;$conteo=0;

		for($i=0;$i<count($objProyectoFinanciero);$i++){
			for($j=0;$j<count($objProyectoFinanciero[$i]->componenteproyecto);$j++){
				for($k=0;$k<count($objProyectoFinanciero[$i]->componenteproyecto[$j]->actividadproyecto);$k++){
					$conteo=count($objProyectoFinanciero[$i]->componenteproyecto[$j]->actividadproyecto[$k]->ejecucion);
		
					if($conteo>$mayor){
						$mayor=$conteo;$index=$i;$index2=$j;$index3=$k;
					}
				}
			}
		}
		
		
		$cabecera=array("Proyecto","Componentes","Actividades","Fuente Financiamiento");
		for($i=0;$i<count($objProyectoFinanciero[$index]->componenteproyecto[$index2]->actividadproyecto[$index3]->ejecucion);$i++){
			array_push($cabecera,"".$objProyectoFinanciero[$index]->componenteproyecto[$index2]->actividadproyecto[$index3]->ejecucion[$i]->periodo->periodo_anio);
		}
		
		Excel::create('Reporte de Proyecto Financiero', function($excel)use($objProyectoFinanciero,$mayor,$cabecera) {
			if(!is_null($objProyectoFinanciero)){
				$excel->sheet('Proyecto Financiero', function($sheet)use($objProyectoFinanciero,$mayor,$cabecera) {
					$sheet->row(1,$cabecera);
					$nfila=2;
					for($i=0;$i<count($objProyectoFinanciero);$i++){
						for($j=0;$j<count($objProyectoFinanciero[$i]->componenteproyecto);$j++){
							for($k=0;$k<count($objProyectoFinanciero[$i]->componenteproyecto[$j]->actividadproyecto);$k++){
								
								$contenido=array();
							
								array_push($contenido,"".$objProyectoFinanciero[$i]->proyecto_financiero_nombre);
								array_push($contenido,"".$objProyectoFinanciero[$i]->componenteproyecto[$j]->componente_proyecto_descripcion);
								array_push($contenido,"".$objProyectoFinanciero[$i]->componenteproyecto[$j]->actividadproyecto[$k]->actividad_proyecto_descripcion);
								array_push($contenido,"".$objProyectoFinanciero[$i]->componenteproyecto[$j]->actividadproyecto[$k]->actividad_proyecto_fuente_financiamiento);
								for($l=0;$l<count($objProyectoFinanciero[$i]->componenteproyecto[$j]->actividadproyecto[$k]->ejecucion);$l++){
									array_push($contenido,"".$objProyectoFinanciero[$i]->componenteproyecto[$j]->actividadproyecto[$k]->ejecucion[$l]->ejecucion_valor);
								}
								if($mayor>count($objProyectoFinanciero[$i]->componenteproyecto[$j]->actividadproyecto[$k]->ejecucion)){
									for($m=0;$m<($mayor- count($objProyectoFinanciero[$i]->componenteproyecto[$j]->actividadproyecto[$k]->ejecucion));$m++){
										array_push($contenido,"");
									}
								}
								
								$sheet->row($nfila,$contenido);
								$nfila++;
								
							}
						
						}
					}
					
					
					
				});
			}
		})->export('xlsx');
		
	}
	
	public  function crearPdf($vistUrl,$objProyectoFinanciero,$index,$index2,$index3,$mayor){
		try {
			 
			$date = date('Y-m-d');
			$view =  \View::make($vistUrl, compact('objProyectoFinanciero','index','index2','index3','mayor'))->render();
			$pdf = \App::make('dompdf.wrapper');
			$pdf->loadHTML($view);
			return $pdf->download('Reporte de Inconsistencias de la Información.pdf');
	
		}catch(\Exception $e){
			Session::flash('message',$e->getMessage());
			return redirect()->back();
		}
	
	
	}
	
	public function imprimirPdfProyectoFinanciero(Request $request){
		
		
		
		try {
	
			$objProyectoFinanciero = ProyectoFinanciero::consultarProyectoFinanciero($request);
			
			
			$index=0;$index2=0;$index3=0;$mayor=0;$conteo=0;
			
			for($i=0;$i<count($objProyectoFinanciero);$i++){
				for($j=0;$j<count($objProyectoFinanciero[$i]->componenteproyecto);$j++){
					for($k=0;$k<count($objProyectoFinanciero[$i]->componenteproyecto[$j]->actividadproyecto);$k++){
						$conteo=count($objProyectoFinanciero[$i]->componenteproyecto[$j]->actividadproyecto[$k]->ejecucion);
			
						if($conteo>$mayor){
							$mayor=$conteo;$index=$i;$index2=$j;$index3=$k;
						}
					}
				}
			}
			
			//return view('pdf.consultaProyectoFinancieroPdf',compact('objProyectoFinanciero','index','index2','index3','mayor'));
			
			if(!empty($objProyectoFinanciero)){

				$vistUrl="pdf.consultaProyectoFinancieroPdf";
				return $this->crearPdf($vistUrl,$objProyectoFinanciero,$index,$index2,$index3,$mayor);
				
			}else{
				$vistUrl="pdf.consultaProyectoFinancieroPdf";
				$objProyectoFinanciero=array();
				return $this->crearPdf($vistUrl,$objProyectoFinanciero,$objProyectoFinanciero,$index,$index2,$index3,$mayor);
			}
	
	
		}catch(\Exception $e){
			Session::flash('message',$e->getMessage());
			 
		}
	
	}
	
}
