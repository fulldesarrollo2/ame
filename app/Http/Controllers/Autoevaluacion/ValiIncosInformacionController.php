<?php

namespace App\Http\Controllers\Autoevaluacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Entidades\TipoEntidad;
use App\Entidades\Entidad;
use App\Entidades\TipoCuenta;
use App\Entidades\Periodo;
use App\Entidades\DivisionPolitica;
use App\Entidades\EntRelacionaDivPo;
use App\Entidades\ValCuentaEntidad;
use App\Entidades\Cuenta;
use App\Entidades\TipoFuente;
use App\Entidades\ArchivoCarga;
use Maatwebsite\Excel\Facades\Excel;
class ValiIncosInformacionController extends Controller
{
    /**
     * 
     * @return type
     */
    protected $periodoId;


    public function index() {
        
        $lstCodigo = [TipoEntidad::$gadm,  TipoEntidad::$empServBasMunicipales];
        $lstTipoEntidad = TipoEntidad::buscarLstTipoEntidadxId($lstCodigo);
        $lstPeriodo = Periodo::consultarLstPeriodo();
        $lstCuenta = [TipoCuenta::$contable, TipoCuenta::$presupuestaria];
        $lstTipoCuenta = TipoCuenta::buscarLstTipoCuenta($lstCuenta);
        $objDivisionPolitica = DivisionPolitica::consultarDivisionPolitica();
        
        return view('autoevaluacion.valiIncosInformacion', compact('lstTipoEntidad','objDivisionPolitica','lstPeriodo','lstTipoCuenta'));
    }
    
      /*
     * Metodo: consultaEntRelacionaDivPoTipo
     * NameCreate : Jrendon
     * Create: 28/septiembre/2016 
     * Detalle: Metodo parea consultar entidad por division politica y por tipo
    */  

     public function consultaEntidadPorDivTipo(Request $request){
         try {
                $tipoEntidad = $request->tipoEntidad;
                $divisionPoliticaId = $request->division_politica_id;
                $lstEntRelacionDivPoTipo= EntRelacionaDivPo::consultaEntidadTipo($divisionPoliticaId,$tipoEntidad);
                return $lstEntRelacionDivPoTipo;
            }catch(\Exception $e){
                Session::flash('message',$e->getMessage());
                   
        }
    }
    
    /**
     * 
     * @param Request $request
     * @return type
     * @author JRENDON
     * @since 03/10/2016
     * @abstract implementado para armar arbol de cuentas con sus respectivos valores y cuentas inconsistentes
     * 
     */
    
    public function consultaValCuentaEntidad(Request $request){
        try {
            
                if($request->tipo_cuenta_id==TipoCuenta::$contable){
                    $lstFuente = [TipoFuente::$fuenteEntidad ,  TipoFuente::$fuenteMF];
                    $lstValCuentaEntidad= Cuenta::buscarLstCuentaxEntidadxTipo($request->entidad_id,$request->tipo_cuenta_id,$lstFuente); 
                }    
                if($request->tipo_cuenta_id==TipoCuenta::$presupuestaria)    
                {
                     $lstFuente = [TipoFuente::$fuenteBsE];
                     $lstValCuentaEntidad= Cuenta::buscarLstCuentaxEntidadxTipo($request->entidad_id,$request->tipo_cuenta_id,$lstFuente); 
                } 
                
                $objArchivoCarga= ArchivoCarga::consultaArchivoCargaPorPeriodo($request->periodo_id);
                
                if(!empty($lstValCuentaEntidad) && !empty($objArchivoCarga)){
                    
                    foreach ($lstValCuentaEntidad as $select){
                        $this->arbolDinamico($select,$select->cuentaHijo,$objArchivoCarga->archivo_carga_id);
                    }
                    
                    return $lstValCuentaEntidad;
                }
                    
                

        }catch(\Exception $e){
                Session::flash('message',$e->getMessage());
                   
        }
    }
    
   /**
    * 
    * @param type $padre
    * @param type $lstHijos
    * @param type $archivoCargaId
    * @abstract metodo implementado para armar el arbol dinamico
    */
    public static function arbolDinamico($padre , $lstHijos, $archivoCargaId){
        try {
            
                $valorPadre = ValCuentaEntidad::consultaValCuentaEntidad($padre->entidad_id,$padre->cuenta_id,$archivoCargaId);
                $valorPadre = $valorPadre!=null?$valorPadre->val_cuenta_entidad_valor:0;
                $padre=array_add($padre,'valor',$valorPadre);

                $valorTotal = 0;
                     foreach ($lstHijos as $hijo)
                     {
                        $valorHijo=ValCuentaEntidad::consultaValCuentaEntidad($hijo->entidad_id,$hijo->cuenta_id,$archivoCargaId);
                        $valorHijo = $valorHijo!=null?$valorHijo->val_cuenta_entidad_valor:0;
                        $hijo=array_add($hijo,'valor',$valorHijo);
                        $valorTotal = $valorTotal + $valorHijo;
                        if(count($hijo->cuentaHijo)>0){

                            ValiIncosInformacionController::arbolDinamico($hijo, $hijo->cuentaHijo, $archivoCargaId);
                        }

                     }

                if($valorPadre!=$valorTotal){
                   $padre=array_add($padre,'inconsistente',true);
                } else {
                   $padre=array_add($padre,'inconsistente',false);
                }
                
            }catch(\Exception $e){
                   Session::flash('message',$e->getMessage());
                   
           }
   }
 
   public function imprimirPdfValiIncosIn(Request $request){
        try {
            
                $objArchivoCarga= ArchivoCarga::consultaArchivoCargaPorPeriodo($request->periodo_id);
                if(!empty($objArchivoCarga)){
                    if($request->tipo_cuenta_id==TipoCuenta::$contable){
                      $lstFuente = [TipoFuente::$fuenteEntidad ,  TipoFuente::$fuenteMF];
                      $lstValCuentaEntidad= Cuenta::buscarLstCuentaxEntidadxTipoEloquent($request->entidad_id,$request->tipo_cuenta_id,$lstFuente,$objArchivoCarga->archivo_carga_id); 

                    }    
                    if($request->tipo_cuenta_id==TipoCuenta::$presupuestaria)    
                    {
                     $lstFuente = [TipoFuente::$fuenteBsE];
                     $lstValCuentaEntidad= Cuenta::buscarLstCuentaxEntidadxTipoEloquent($request->entidad_id,$request->tipo_cuenta_id,$lstFuente,$objArchivoCarga->archivo_carga_id); 
                    } 
                }
                if(!empty($lstValCuentaEntidad)){
                        foreach ($lstValCuentaEntidad as $select)  {
                              $this->datosInconsistentes($select,$lstValCuentaEntidad); 
                        }
                        $vistUrl="pdf.valiIncosInformacionPdf";
                        return $this->crearPdf($vistUrl,$lstValCuentaEntidad);
                        //return $this->imprimirPdf($lstValCuentaEntidad);
                }else{
                        $vistUrl="pdf.valiIncosInformacionPdf";
                        $lstValCuentaEntidad=array();
                        return $this->crearPdf($vistUrl,$lstValCuentaEntidad);
                }  
                
        
        }catch(\Exception $e){
                   Session::flash('message',$e->getMessage());
                   
        }
        
   }
   
     public  function crearPdf($vistUrl,$lstValCuentaEntidad){
         try {
             
                $date = date('Y-m-d');
                $view =  \View::make($vistUrl, compact('lstValCuentaEntidad'))->render();
                $pdf = \App::make('dompdf.wrapper');
                $pdf->loadHTML($view);
                return $pdf->download('Reporte de Inconsistencias de la Información.pdf');
                
        }catch(\Exception $e){
                   Session::flash('message',$e->getMessage());
                   return redirect()->back();
        }     
            

    }
    public function datosInconsistentes($select,$lstValCuentaEntidad){
        
        try {
            
        
            $valorTotal =0;
            $valorPadre =$select!=null?$select->val_cuenta_entidad_valor:0;
            foreach ($lstValCuentaEntidad as $selectVal){
                if($selectVal->cuenta_cuenta_id==$select->cuenta_id){
                    $valorHijo = $selectVal!=null?$selectVal->val_cuenta_entidad_valor:0;
                    $valorTotal = $valorTotal + $valorHijo;
                }
            }
            if($valorTotal!=0){
                if($valorPadre!=$valorTotal){
                       $select->inconsistente=true;
                } else {
                       $select->inconsistente=false;
                }
            }else{
                       $select->inconsistente=false;
                }
         }catch(\Exception $e){
                   Session::flash('message',$e->getMessage());
        }    
            
    }

    
    public  function imprimirExcelValiIncosIn(Request $request){
       
            
         

            $objArchivoCarga= ArchivoCarga::consultaArchivoCargaPorPeriodo($request->periodo_id);
            if(!empty($objArchivoCarga)){
                if($request->tipo_cuenta_id==TipoCuenta::$contable){
                    $lstFuente = [TipoFuente::$fuenteEntidad ,  TipoFuente::$fuenteMF];
                    $lstValCuentaEntidad= Cuenta::buscarLstCuentaxEntidadxTipoEloquent($request->entidad_id,$request->tipo_cuenta_id,$lstFuente,$objArchivoCarga->archivo_carga_id); 

                 }    
                if($request->tipo_cuenta_id==TipoCuenta::$presupuestaria)    
                {
                     $lstFuente = [TipoFuente::$fuenteBsE];
                     $lstValCuentaEntidad= Cuenta::buscarLstCuentaxEntidadxTipoEloquent($request->entidad_id,$request->tipo_cuenta_id,$lstFuente,$objArchivoCarga->archivo_carga_id); 

                } 
            }
            if(!empty($lstValCuentaEntidad)){
                 
                foreach ($lstValCuentaEntidad as $select) 
                {
                    $this->datosInconsistentes($select,$lstValCuentaEntidad); 
                }
            
            Excel::create('Reporte de Inconsistencias de la Información', function($excel)use($lstValCuentaEntidad) {
            if(!is_null($lstValCuentaEntidad)){       
                $excel->sheet('Errores de formato', function($sheet)use($lstValCuentaEntidad) {

                     $sheet->cells('A1:D1', function($cells) {
                         $cells->setFont(array(
                          'family'     => 'Calibri',
                          'size'       => '12',
                          'bold'       =>  true
                            ));
                    });
                     $objErrorFormato=array();
                     $x=2;
                     foreach ($lstValCuentaEntidad as $señect)
                     {


                            $lstExcel=array();
                            $lstExcel=array_add($lstExcel,'Cuenta',$señect->cuenta_codigo);
                            $lstExcel=array_add($lstExcel,'Nombre',$señect->cuenta_nombre);
                            $lstExcel=array_add($lstExcel,'Valor',$señect->val_cuenta_entidad_valor);
                            $objErrorFormato[$x]=$lstExcel;


                                $sheet->cells('A'.$x.':D'.$x.'', function($cells) use($señect){

                                     if($señect->inconsistente==true){
                                       $cells->setFontColor('#e82323');
                                       $cells->setFont(array(
                                        'family'     => 'Calibri',
                                        'size'       => '12',
                                        'bold'       =>  true
                                          ));

                                     }
                                     if($señect->inconsistente==false){

                                          $cells->setFontColor('#2570BB');
                                          $cells->setFont(array(
                                        'family'     => 'Comic Sans MS',
                                        'size'       => '12',
                                        'bold'       =>  true

                                          ));

                                      }
                                });

                        $x++;
                    }

                   $sheet->fromArray($objErrorFormato);
                });
             }  
            })->export('xls');
        }else{
            
             Excel::create('Reporte de Inconsistencias de la Información', function($excel) {
                
                $excel->sheet('Errores de formato', function($sheet){
                
                $sheet->fromArray();
                });
            
            })->export('xls');
            
            
             
        }
           
         
    }
}
