<?php

namespace App\Http\Controllers\Autoevaluacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Entidades\TipoIndicador;
use App\Entidades\OperadoresMatematicos;
use App\Entidades\SignosAgrupacion;
use App\Entidades\VariableIndicador;
use App\Entidades\VariableServicio;
use App\Entidades\VariableGeneral;
use App\Entidades\RangoAceptacion;
use App\Entidades\TipoNivelAlerta;
use App\Entidades\NivelAlerta;
use App\Entidades\Categoria;
use App\Entidades\TipoFuente;
use App\Entidades\Cuenta;
use App\Entidades\Estado;
use App\Entidades\CriterioCambio;
use App\Entidades\Indicador;
use App\Entidades\Auditoria;

class IndicadorController extends Controller {

    /**
     * Carga los datos principales en la vista del Indicador
     * @return type
     */
    public function index() {
        try {
            $lstTipoIndicador = TipoIndicador::buscarLstTipoIndicador();
            $lstVariableIndicador = VariableIndicador::buscarLstVariableIndicador();
            $lstOperadoresMatematicos = OperadoresMatematicos::buscarLstOperadoresMatematicos();
            $lstSignosAgrupacion = SignosAgrupacion::buscarLstSignosAgrupacion();
            $lstTipoNivelAlerta = TipoNivelAlerta::buscarLstTipoNivelAlerta();
            $lstCategoria = Categoria::buscarLstCategorias();
            $tipoIndicadorFin = TipoIndicador::$financiero;
            $varIndicadorFinanciera = VariableIndicador::$varIndicadorFinanciera;
            $varIndicadorServicios = VariableIndicador::$varIndicadorServicios;
            $varIndicadorGeneral = VariableIndicador::$varIndicadorGeneral;
            return view('autoEvaluacion.indicador', compact('lstTipoIndicador', 'lstVariableIndicador', 'lstOperadoresMatematicos','lstTipoNivelAlerta','lstSignosAgrupacion','lstCategoria', 'tipoIndicadorFin', 'varIndicadorFinanciera', 'varIndicadorServicios', 'varIndicadorGeneral'));
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

    /**
     * Busca segun el tipo de variable indicador, en la tabla que coresponda
     * Financiera -> Cuentas
     * Servicios Basicos -> Variable Servicio
     * General -> Variable General
     * @param Request $request
     * @return type
     */
    public function buscarLstVariableIndicador(Request $request) {
        try {
            $tipoVarIndicador = $request->tipoVarIndicador;
            $lstVariableIndicador;

            if ($tipoVarIndicador == VariableIndicador::$varIndicadorFinanciera)
                $lstVariableIndicador = Cuenta::buscarLstCuentaxTipoxFuente(null, TipoFuente::$tipoFuenteBdE);
            else {
                if ($tipoVarIndicador == VariableIndicador::$varIndicadorServicios)
                    $lstVariableIndicador = VariableServicio::buscarLstVariableServicio();
                else {
                    if ($tipoVarIndicador == VariableIndicador::$varIndicadorGeneral) {

                        $lstVariableIndicador = VariableGeneral::buscarLstVariableGeneralxEstado(Estado::$estadoActivo);
                    }
                }
            }
            return $lstVariableIndicador;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

    /**
     * Busca el Indicador por Nombre
     * Busca el Rangos de Aceptacion
     * @param Request $request
     * @return type
     */
    public function buscarIndicadorxNombre(Request $request) {
        try {
            $indicadorNombre = $request->indicador_nombre;
            $objIndicador = Indicador::buscarIndicadorxNombre($indicadorNombre);
            return $objIndicador;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

    /**
     * Verifica que no se repita el nombre del indicador
     * Guarda los datos del Indicador 
     * @param Request $request
     * @return type
     */
    public function guardarIndicador(Request $request) {

        try {
            $messages = [
                'indicador.required' => 'Campo :attribute es requerido.',
                'indicador.max' => 'Campo :attribute debe tener un tamaño de :max.',
                'indicador.unique' => 'Campo :attribute ya se encuentra registrado.',
            ];

            $validator = Validator::make($request->all(), [
                        'tipo_indicador_id' => 'required|max:11',
                        'indicador_nombre' => 'required|unique:indicador|max:254',
                        'indicador_objetivo' => 'required|max:254',
                        'indicador_alcance' => 'required|max:254',
                        'indicador_formula' => 'required|max:254',
                            ], $messages);

            if ($validator->fails()) {
                return redirect('indicador')
                                ->withErrors($validator)
                                ->withInput();
            } else {
                $objIndicador = new Indicador($request->all());
                $objIndicador->estado_id = Estado::$estadoActivo;
                $objIndicador->save();

                Auditoria::ingresoAuditoria(Auditoria::$guardar, url()->previous());

                notify()->flash('INDICADOR: ' . $objIndicador->indicador_nombre . ' ha sido registrado con éxito.', 'success');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

    /**
     * Verifica que no se repita el nombre del indicador
     * Actualiza los datos del Indicador
     * @param Request $request
     * @return type
     */
    public function actualizarIndicador(Request $request) {
        try {
            $messages = [
                'indicador.required' => 'Campo :attribute es requerido.',
                'indicador.max' => 'Campo :attribute debe tener un tamaño de :max.',
                'indicador.unique' => 'Campo :attribute ya se encuentra registrado.',
            ];

            $validator = Validator::make($request->all(), [
                        'indicador_nombre' => 'required|max:255|unique:indicador,indicador_nombre,' . $request->indicador_id . ',indicador_id',
                        'tipo_indicador_id' => 'required|max:11',
                        'indicador_objetivo' => 'required|max:254',
                        'indicador_alcance' => 'required|max:254',
                        'indicador_formula' => 'required|max:254',
                            ], $messages);

            if ($validator->fails()) {
                return redirect('indicador')
                                ->withErrors($validator)
                                ->withInput();
            } else {
                $objIndicador = Indicador::find($request->indicador_id);
                $objIndicador->indicador_nombre = $request->indicador_nombre;
                $objIndicador->tipo_indicador_id = $request->tipo_indicador_id;
                $objIndicador->indicador_proyeccion_financiera = $request->indicador_proyeccion_financiera;
                $objIndicador->indicador_objetivo = $request->indicador_objetivo;
                $objIndicador->indicador_alcance = $request->indicador_alcance;
                $objIndicador->indicador_formula = $request->indicador_formula;


                $objIndicador->save();

                Auditoria::ingresoAuditoria(Auditoria::$actualizar, url()->previous());

                notify()->flash('INDICADOR: ' . $objIndicador->indicador_nombre . ' ha sido actualizado con éxito.', 'success');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

    /**
     * Verifica que no este ingresado la categoría
     * Guarda los datos de los Rangos de aAceptación
     * @param Request $request
     * @return type
     */
    public function guardarRangosAceptacion(Request $request) {
        try {
                $indicador_id=$request->indicador_id;
                $array = explode(",", $request->lstRangosAceptacion);
                unset($array[0]);
                foreach ($array as $item) {
                    $objRangoAceptacion= new RangoAceptacion();
                    $objRangoAceptacion->indicador_id = $indicador_id;
                    $arrayValor = explode(" - ", $item);
                    $objRangoAceptacion->categoria_id = $arrayValor[0];
                    $objRangoAceptacion->rango_aceptacion_minimo = $arrayValor[1];
                    $objRangoAceptacion->rango_aceptacion_maximo = $arrayValor[2];
                    $objRangoAceptacion->save();
                }                    
                Auditoria::ingresoAuditoria(Auditoria::$guardar, url()->previous());
                notify()->flash('RANGO(S) DE ACEPTACIÓN: ha(n) sido registrado(s) con éxito.', 'success');
                return redirect()->back();
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
    
    /**
     * Verifica que no este ingresado la categoría
     * Guarda los datos de los Rangos de aAceptación
     * @param Request $request
     * @return type
     */
    public function guardarNivelesAlerta(Request $request) {
        try {
                $array = explode(",", $request->lstNivelesAlerta);
                unset($array[0]);
                foreach ($array as $item) {
                    $objNivelAlerta= new NivelAlerta();
                    $arrayValor = explode(" - ", $item); 
                    $objNivelAlerta->rango_aceptacion_id = $arrayValor[0];
                    $objNivelAlerta->tipo_nivel_alerta_id = $arrayValor[1];
                    $objNivelAlerta->nivel_alerta_desde = $arrayValor[2];
                    $objNivelAlerta->nivel_alerta_hasta = $arrayValor[3];
                    
                    $objNivelAlerta->save();
                }           
                Auditoria::ingresoAuditoria(Auditoria::$guardar, url()->previous());
                notify()->flash('NIVEL(S) DE ALERTA: ha(n) sido registrado(s) con éxito.', 'success');
                return redirect()->back();
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
    
    
    /**
     * Busca los Niveles de Alerta x Rango de Aceptacion
     * Busca el Rangos de Aceptacion
     * @param Request $request
     * @return type
     */
    public function buscarLstNivelAlertaxRango(Request $request) {
        try {
            $rango_aceptacion_id = $request->rango_aceptacion_id;
            $lstNivelAlerta = NivelAlerta::buscarLstNivelAlertaxRango($rango_aceptacion_id);
            return $lstNivelAlerta;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

    
    /**
     * Actualiza los niveles de alerta de los rangos de aceptacion
     * @param Request $request
     * @return type
     */
    public function actualizarNivelesAlerta(Request $request){
        try {
            $array = explode(",", $request->lstNivelesAlertaAct);
            unset($array[0]);
            $criterio = 0;
            foreach ($array as $item) {
                $criterio = $criterio + 1;
                $arrayValor = explode("-", $item); 
                $objNivelAlerta = NivelAlerta::find($arrayValor[0]);
                $objNivelAlerta->nivel_alerta_descripcion = $arrayValor[2];
                $objNivelAlerta->nivel_alerta_interpretacion = $arrayValor[3];
                $objNivelAlerta->nivel_alerta_nivel = $criterio;
                $objNivelAlerta->save();
            }
            Auditoria::ingresoAuditoria(Auditoria::$actualizar, url()->previous());

            notify()->flash('GUIA DEL INDICADOR: ha sido actualizado con éxito.', 'success');
            return redirect()->back();
        
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

    
    /**
     * Verifica que no este ingresado la categoría
     * Guarda los datos de los Rangos de aAceptación
     * @param Request $request
     * @return type
     */
    public function guardarCambioMejora(Request $request) {
        try {
                $array = explode(",", $request->lstCambioMejora);
                unset($array[0]);
                foreach ($array as $item) {
                    $objCriterioCambio= new CriterioCambio();
                    $arrayValor = explode("-", $item); 
                    $objCriterioCambio->nivel_alerta_id = $arrayValor[0];
                    $objCriterioCambio->criterio_cambio_componente = $arrayValor[2];
                    $objCriterioCambio->criterio_cambio_criterio = $arrayValor[3];
                    $objCriterioCambio->save();
                }           
                Auditoria::ingresoAuditoria(Auditoria::$guardar, url()->previous());
                notify()->flash('CRITERIO(S) CAMBIO(S) Y MEJORA(S): ha(n) sido registrado(s) con éxito.', 'success');
                return redirect()->back();
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
}
