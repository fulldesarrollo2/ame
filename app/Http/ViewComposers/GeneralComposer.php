<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Entidades\TipoEntidad;

class GeneralComposer {

    /**
     * Bind data to the view.
     * @param  View  $view
     * @return void
     */
    public function compose(View $view) {
        try {
            $lstTipoEntidad = TipoEntidad::all();
            $view->with('lstTipoEntidad', $lstTipoEntidad);
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect()->back();
        }
    }

}
