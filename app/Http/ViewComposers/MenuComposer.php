<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Entidades\Interfaz;
use Auth;
use App\Entidades\Estado;
use Illuminate\Support\Facades\Session;

class MenuComposer {

    /**
     * Bind data to the view.
     * @param  View  $view
     * @return void
     */
    public function compose(View $view) {
        try {
            if (Auth::user() != null) {
                if (Session::has('lstMenuInterfaz')) {
                    $value = Session::get('lstMenuInterfaz', Session::has('lstMenuInterfaz'));
                    $view->with('lstMenuInterfaz', $value);
                } else {
                    $menuPadre = Interfaz::whereExists(function ($query) {
                                $query->select(\DB::raw('1 from interfaz_relaciona_perfil a , interfaz b 
                                                where a.interfaz_id = b.interfaz_id
                                                and interfaz.interfaz_id = b.interfaz_padre
                                                and a.perfil_ingreso_id = ' . Auth::user()->perfil_ingreso_id));
                            })->get();
                    $arrayMenuPadre = $menuPadre->toArray();
                    $menu = array();
                    foreach ($arrayMenuPadre as $valor) {
                        $interfaz_id = array_get($valor, 'interfaz_id');
                        $menuHijo = Interfaz::
                                        join('interfaz_relaciona_perfil', 'interfaz.interfaz_id', '=', 'interfaz_relaciona_perfil.interfaz_id')
                                        ->where('interfaz_relaciona_perfil.perfil_ingreso_id', Auth::user()->perfil_ingreso_id)
                                        ->where('estado_id', Estado::$estadoActivo)
                                        ->where('interfaz_padre', $interfaz_id)
                                        ->orderBy('interfaz_relaciona_perfil.interfaz_id','asc')
                                        ->get();

                        $arrayMenuHijo = $menuHijo->toArray();
                        array_push($valor, $arrayMenuHijo);
                        array_push($menu, $valor);
                    }
                    $lstMenuInterfaz = $menu;
                    Session::put('lstMenuInterfaz', $lstMenuInterfaz);
                    $view->with('lstMenuInterfaz', $lstMenuInterfaz);
                }
            } else {
                return redirect('/login');
            }
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect()->back();
        }
    }

}
