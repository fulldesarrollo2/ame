<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Entidades\PerfilIngreso;

class UsuarioAmeAnalista {

    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if ($this->auth->user()->perfil_ingreso_id == PerfilIngreso::$ameAnalista) {
            return $next($request);
        } else {
            return redirect('/noAutorizado');
        }
    }

}
