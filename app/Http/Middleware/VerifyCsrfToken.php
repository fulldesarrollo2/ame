<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
            //
    ];

    public function handle($request, Closure $next) {
        if (
                $this->isReading($request) ||
                $this->runningUnitTests() ||
                $this->shouldPassThrough($request) ||
                $this->tokensMatch($request)
        ) {
            return $this->addCookieToResponse($request, $next($request));
        }

        return Redirect::back()->withError('Disculpe, su sesión ha finalizado, porfavor de click en Salir y vuelva a ingresar.');
    }

}
