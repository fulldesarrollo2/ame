<?php namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model {
    
    protected $table = 'estado';
    protected $primaryKey = 'estado_id';
    public $timestamps = false;
    protected $fillable = [
        'estado_descripcion'
    ];

    //VARIABLES
    public static $estadoActivo = 1;
    public static $estadoInactivo = 2;
    public static $estadoAbiero = 3;
    public static $estadoCerrado = 4;

    //RELACIONES

    public function interfaz() {
        return $this->hasMany('App\Entidades\Interfaz');
    }

    public function perfilIngreso() {
        return $this->hasMany('App\Entidades\PerfilIngreso');
    }

    public function indicador() {
        return $this->hasMany('App\Entidades\Indicador');
    }

    //METODOS
}
