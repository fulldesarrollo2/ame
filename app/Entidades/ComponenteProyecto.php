<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class ComponenteProyecto extends Model
{
    /*
	 * Table: componente_proyecto
	 * NameCreate : Jtriviño
	 * Create: 15/Septiembre/2016
	 * NameUpdate : Jtriviño
	 * Update: 15/Septiembre/2016
	 */
	
	protected $table = 'componente_proyecto';
	protected $primaryKey = 'componente_proyecto_id';
	public $timestamps = false;
	
	protected $fillable = ['proyecto_financiero_id', 'componente_proyecto_descripcion'];
	
	//Relaciones
	public function actividadproyecto(){
		return $this->hasMany('App\Entidades\ActividadProyecto','componente_proyecto_id','componente_proyecto_id');
	}
	//Metodos
        
        //GCASTILLO
     public static function buscarComponentexProyecto($idProyecto) {
        try {
            $lstCuenta = ComponenteProyecto::where('proyecto_financiero_id',$idProyecto)
            ->get();
            return $lstCuenta;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

    //GCASTILLO
     public static function buscarComponentexId($idComponente) {
        try {
            $objComponente = ComponenteProyecto::where('componente_proyecto_id',$idComponente)
            ->get();
            return $objComponente;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

    
  }

