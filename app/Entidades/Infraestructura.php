<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class Infraestructura extends Model
{
    protected $table = 'infraestructura';
      
    protected $primaryKey = 'infraestructura_id';
    
    public $timestamps = false;
    
    protected $fillable = [
        'entidad_id',
        'periodo_id',
        'infraestructura_descripcion',
        'infraestructura_fecha_construccion',
        'infraestructura_vida_util',
        'infraestructura_valor_actual',
        'estado_id'    
    ];
    
    //RELACIONES
    
    
    
    //METODOS
}
