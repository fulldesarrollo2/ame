<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Entidades\TipoFuente;
class Periodo extends Model
{
   /*
     * Table: periodo
     * NameCreate : JRendon
     * Create: 07/Sept/2016 
     * NameUpdate : 
     * Update: 
     */

    protected $table = 'periodo';
    protected $primaryKey = 'periodo_id';
    public $timestamps = false;
    protected $fillable = [
        'estado_id',
        'periodo_anio',
        'periodo_mes',
        
    ];
    
    // PERIODO
    public function estado() {
        return $this->belongsTo('App\Entidades\Estado');
    }
  
    // CONSULTAS
    
    public static function consultaUltimoPeriodo(){
        $anio= DB::table('periodo')->where('estado_id','=',Estado::$estadoActivo)->max('periodo_anio');
        return $anio;
    }
    
    /*
     * Metodo consultar periodo Abierto
     * NameCreate : JRendon
     * Create: 09/Sept/2016 
     * modificado: 16/09/2016
     */
    public static function consultaPeriodoAbierto($mes){
        try {
            
                 $objPeriodo= DB::table('periodo')->where('estado_id','=',Estado::$estadoActivo)
                                            ->where('periodo_mes','=',$mes)
                                            ->first();
                 return $objPeriodo;
            
          }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                   
           }   
    }
    
    /*
     * Metodo consultar periodo Abierto
     * NameCreate : JRendon
     * Create: 14/Sept/2016 
     */
    public static function consulPeriodoAbierto(){
        try {
                 $periodoAnio= Periodo::where('estado_id','=',Estado::$estadoActivo)->get();
                 return $periodoAnio;
            
          }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                   
           }   
    }
    
    /*
     * Metodo consultar periodo Abierto
     * NameCreate : JTribiño
     * Create: 14/Sept/2016 
     */
    public static function consultarLstPeriodo(){
    	try{
    	$lstPeriodo=Periodo::orderBy('periodo_anio', 'asc')
                ->groupBy('periodo_anio')->get();
    	return $lstPeriodo;
    	}
	    catch (Exception $e) {
	    		Session::flash('mensajeerror',$e->getMessage());
                return redirect()->back();
	    }
    }
    
    /*
     * Metodo consultar periodo Abierto
     * NameCreate : JRendon
     * Create: 14/Sept/2016 
     */
    public static function consultarPeriodoXanio($periodoAnio){
        try {
                 $lstPeriodo= Periodo::where('periodo_anio','=', $periodoAnio)->get();
                 return $lstPeriodo;
            
          }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                   
           }   
    }
    
    /**
     * 
     * @param type $tipoFuente
     * @return type
     */
    public static function consultarLstPeriodoxTipoFuente($tipoFuente){
        try {
            if($tipoFuente==TipoFuente::$fuenteEntidad){
               $lstPeriodo=Periodo::whereNotNull('periodo_mes')
                           ->orderBy('periodo_anio', 'asc')
                           ->orderBy('periodo_mes', 'asc')->get();
            }
            if($tipoFuente==TipoFuente::$fuenteBdE){
               $lstPeriodo=Periodo::whereNull('periodo_mes')->get();
            }
            return $lstPeriodo;
        } catch (Exception $ex) {
            \Session::flash('message',$ex->getMessage());
        }
    }
    
    
    
    
}
