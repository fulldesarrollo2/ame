<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class SignosAgrupacion extends Model
{
   /*
     * Table: signos_agrupacion
     * NameCreate : CBastidas
     * Create: 26/Agosto/2016 
     * NameUpdate : CBastidas
     * Update: 29/Agosto/2016
    */
 
    protected $table = 'signos_agrupacion';
    
    protected $primaryKey = 'signos_agrupacion_id';
    
    public $timestamps = false;
    
    protected $fillable = [
        'signos_agrupacion_nombre',
        'signos_agrupacion_simbolo',
    ];
    
    
    //METODOS
    
    /**
     * Busca Todos los registros de la tabla Signos Agrupacion
     * @return type
     */
    public static function buscarLstSignosAgrupacion(){
        try {
            $lstSignosAgrupacion= SignosAgrupacion::all();
            return $lstSignosAgrupacion;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
}
