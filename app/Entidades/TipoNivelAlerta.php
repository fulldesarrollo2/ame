<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class TipoNivelAlerta extends Model
{
   /*
     * Table: tipo_nivel_alerta
     * NameCreate : CBASTIDAS
     * Create: 20/Octubre/2016 
    */
    
    protected $table = 'tipo_nivel_alerta';
      
    protected $primaryKey = 'tipo_nivel_alerta_id';
    public $timestamps = false;    
    protected $fillable = [
        'tipo_nivel_alerta_nombre',
        'tipo_nivel_alerta_color',
    ];
    
     //RELACIONES
    public function nivelAlerta() {
        return $this->hasOne('App\Entidades\NivelAlerta');
    }
    

    //METODOS
    public static function buscarLstTipoNivelAlerta(){
        try {
            $lstLstTipoNivelAlerta= TipoNivelAlerta::all();
            return $lstLstTipoNivelAlerta;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
   
}
