<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class RangoAceptacion extends Model
{
    
    /*
     * Table: rango_aceptacion
     * NameCreate : KPortugal
     * Create: 16/Septiembre/2016 
    */
    
    protected $table = 'rango_aceptacion';
      
    protected $primaryKey = 'rango_aceptacion_id';
    
    public $timestamps = false;
        
    protected $fillable = [
        'indicador_id',
        'categoria_id',
        'rango_aceptacion_minimo',
        'rango_aceptacion_maximo'
    ];
    
     //RELACIONES
    public function categoria() {
        return $this->belongsTo('App\Entidades\Categoria');
    }
    
    public function indicador() {
        return $this->belongsTo('App\Entidades\Indicador');
    }
    
    public function nivelAlerta() {
        return $this->hasMany('App\Entidades\NivelAlerta');
    }
    
     //METODOS
    
    public static function buscarLstRangoAceptacion(){
        try {
            $lstLstRangoAceptacion= RangoAceptacion::all();
            return $lstLstRangoAceptacion;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
}
