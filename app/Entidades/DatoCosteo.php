<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class DatoCosteo extends Model
{
    /*
     * Table: periodo
     * NameCreate : JRendon
     * Create: 14/Sept/2016 
     * NameUpdate : 
     * Update: 
     */
   
    protected $table = 'dato_costeo';
    protected $primaryKey = 'dato_costeo_id';
    public $timestamps = false;
    protected $fillable = [
        'periodo_id',
        'catalogo_dato_costeo_id',
        'sub_sistema_id',
        'variable_costeo_id',
        'dato_costeo_dato'
    ];
    
    
     //RELACIONES

    public function periodo() {
        return $this->belongsTo('App\Entidades\Periodo');
    }
     /*public function periodo() {
        return $this->belongsTo('App\Entidades\Periodo');
    }
     public function periodo() {
        return $this->belongsTo('App\Entidades\Periodo');
    }
     public function periodo() {
        return $this->belongsTo('App\Entidades\Periodo');
    }*/
    
    //METODOS
    
    /*public static function consultLstConfigArchivo($parmArchCargaId){
        try {
            $ConfigColum=ConfiguracionColumna::where('parametro_archivo_carga_id','=',$parmArchCargaId)->get();
            return $ConfigColum;
         }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                   
        }   
    }*/

}
