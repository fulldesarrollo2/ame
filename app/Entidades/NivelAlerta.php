<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class NivelAlerta extends Model
{
    
    /*
     * Table: nivel_alerta
     * NameCreate : KPortugal
     * Create: 16/Septiembre/2016 
    */
    
    protected $table = 'nivel_alerta';
      
    protected $primaryKey = 'nivel_alerta_id';
    public $timestamps = false;    
    protected $fillable = [
        'rango_aceptacion_id',
        'nivel_alerta_desde',
        'nivel_alerta_hasta',
        'nivel_alerta_nivel',
        'nivel_alerta_descripcion',
        'nivel_alerta_interpretacion'
    ];
    
     //RELACIONES
    public function rangoAceptacion() {
        return $this->belongsTo('App\Entidades\RangoAceptacion');
    }
    
    public function tipoNivelAlerta() {
        return $this->belongsTo('App\Entidades\TipoNivelAlerta');
    }
    
     //METODOS

    
    /**
     * Busca Indicador por Nombre
     * @return type
     */
    public static function buscarLstNivelAlertaxRango($rangoAceptacion){
        try {
            $lstNivelAlerta = NivelAlerta::with('tipoNivelAlerta')->where('rango_aceptacion_id',$rangoAceptacion)->get();
            return $lstNivelAlerta;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
}
