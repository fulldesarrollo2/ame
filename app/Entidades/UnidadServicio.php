<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class UnidadServicio extends Model
{    
    protected $table = 'unidad_servicio';
      
    protected $primaryKey = 'unidad_servicio_id';
    public $timestamps = false;    
    protected $fillable = [
        'unidad_servicio_codigo',
        'unidad_servicio_nombre',
        'unidad_servicio_servicio',
    ];
    
    //RELACIONES    
    public function variableCosteo() {
        return $this->hasMany('App\Entidades\VariableCosteo');
    }
    //METODOS
    public static function buscarTipoXUnidadServicio($uni_serv_id){
        $lstTipoServ=UnidadServicio::select('*')
            ->where('unidad_servicio_codigo','=',$uni_serv_id)
            ->get();
        return$lstTipoServ;
    }
    public static function buscarSubTipo($unidad_serv_id){
        $lstSubTipo=UnidadServicio::select('*')->where('unidad_servicio_codigo','=',$unidad_serv_id)
            ->get();
        return$lstSubTipo;
    }
    
}
