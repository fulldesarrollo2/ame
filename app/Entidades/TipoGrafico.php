<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class TipoGrafico extends Model
{
     /*
     * Table: tipo_fuente
     * NameCreate : CBastidas
     * Create: 2/Sept/2016 
     * NameUpdate : 
     * Update: 
     */

    protected $table = 'tipo_grafico';
    protected $primaryKey = 'tipo_grafico_id';
    public $timestamps = false;
    protected $fillable = [
        'tipo_grafico_nombre',
    ];
    
    public static $tipoGraficoBarra = 1;   
    public static $tipoGraficoLinea = 2;
    
    /**
     * 
     * @param type $lstCodigo
     * @return type
     * @author CBASTIDAS
     */
    public static function buscarLstTipoGraficoxId($lstCodigo){
        try {
            $lstTipoGrafico = TipoGrafico::whereIn('tipo_grafico_id', $lstCodigo)->get();
            return $lstTipoGrafico;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

}
