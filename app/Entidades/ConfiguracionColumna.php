<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class ConfiguracionColumna extends Model
{
     /*
     * Table: periodo
     * NameCreate : JRendon
     * Create: 14/Sept/2016 
     * NameUpdate : 
     * Update: 
     */
   
    protected $table = 'configuracion_columna';
    protected $primaryKey = 'configuracion_columna_id';
    public $timestamps = false;
    protected $fillable = [
        'parametro_archivo_carga_id',
        'configuracion_columna_posicion',
        'configuracion_columna_tipo_dato',
        'configuracion_columna_longitud_dato',
        'configuracion_columna_requerido',
        'configuracion_columna_nombre_columna'
    ];
    
    
     //RELACIONES

    public function parametroArchivoCarga() {
        return $this->belongsTo('App\Entidades\ParametroArchivoCarga');
    }
    
    //METODOS
    
    public static function consultLstConfigArchivo($parmArchCargaId){
        try {
            $ConfigColum=ConfiguracionColumna::where('parametro_archivo_carga_id','=',$parmArchCargaId)->get();
            return $ConfigColum;
         }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                   
        }   
    }

}
