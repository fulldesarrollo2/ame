<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;
use Validator;

class DivisionPolitica extends Model {

    protected $table = 'division_politica';
    protected $primaryKey = 'division_politica_id';
    public $timestamps = false;
    protected $fillable = ['categoria_id',
        'division_politica_division_politica_id',
        'division_politica_codigo',
        'division_politica_nombre',
        'division_politica_tipo',
        'division_politica_capital',
        'estado_id'
    ];
    public static $region = "R";
    public static $provincia = "P";
    public static $canton = "C";

    // RELACIONES 

    public function divisionPolitica() {
        return $this->hasMany('App\Entidades\DivisionPolitica', 'division_politica_division_politica_id', 'division_politica_id');
    }

    public function estado() {
        return $this->belongsTo('App\Entidades\Estado');
    }

    public function division_politica_division_politica_id() {
        return $this->hasOne('App\Entidades\DivisionPolitica', 'division_politica_id', 'division_politica_division_politica_id');
    }

    // METODOS

    /**
     * AUTOR: JTRIVIÑO
     * DESCRIPCION: BUSCA LISTA DE DATOS -> DIVISION POLITICA
     * @param type $request
     * @return type
     */
    public static function consultarDivisionPolitica() {
        try {
            $lstDivisionPolitica = DivisionPolitica::with('divisionPolitica.divisionPolitica')
                    ->where('division_politica_division_politica_id', '=', null)
                    ->get();
            return $lstDivisionPolitica;
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect()->back();
        }
    }

    /**
     * AUTOR: JTRIVIÑO
     * DESCRIPCION: BUSCA LISTA DE DATOS ACTIVOS -> DIVISION POLITICA
     * @param type $request
     * @return type
     */
    public static function buscarlstdivisionpoliticActiva() {
        try {
            $lstdivision = DivisionPolitica::with('divisionPolitica.divisionPolitica')
                    ->where('division_politica_division_politica_id', '=', null)
                    ->where('estado_id', '=', Estado::$estadoActivo)
                    ->get();
            return $lstdivision;
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect()->back();
        }
    }

    /**
     * AUTOR: JRENDON
     * DESCRIPCION: BUSCA TOTAL LISTA DE DATOS ACTIVOS -> DIVISION POLITICA
     * @param type $request
     * @return type
     */
    public static function buscarCountdivisionpoliticActiva() {
        try {
            $countDivisionPolitica = DivisionPolitica::where('estado_id', '=', Estado::$estadoActivo)->count();
            return $countDivisionPolitica;
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect()->back();
        }
    }

    /**
     * AUTOR: JTRIVIÑO
     * DESCRIPCION: BUSCA POR ID -> DIVISION POLITICA
     * @param type $request
     * @return type
     */
    public static function buscarDivisionPolitica($request) {
        try {
            $objConsultaDivision = DivisionPolitica::find($request->id);
            return $objConsultaDivision;
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect()->back();
        }
    }

    /**
     * AUTOR: GCASTILLO
     * DESCRIPCION: BUSCA TOTAL LISTA DE DATOS ACTIVOS -> DIVISION POLITICA POR ENTIDAD
     * @param type $categoriaId
     * @return type
     */
    public static function consultaDivPoliticaxEntidad($categoriaId) {
        try {
            $lstDivPoxEntidad = DivisionPolitica::with('estado')
                            ->where('categoria_id', '=', $categoriaId)
                            ->where('estado_id', '=', Estado::$estadoActivo)->get();
            return $lstDivPoxEntidad;
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect()->back();
        }
    }

}
