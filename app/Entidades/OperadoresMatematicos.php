<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class OperadoresMatematicos extends Model
{
   /*
     * Table: operadores_matematicos
     * NameCreate : CBastidas
     * Create: 26/Agosto/2016 
     * NameUpdate : CBastidas
     * Update: 29/Agosto/2016
    */
 
    protected $table = 'operadores_matematicos';
    
    protected $primaryKey = 'operadores_matematicos_id';
    
    public $timestamps = false;
    
    protected $fillable = [
        'operadores_matematicos_nombre',
        'operadores_matematicos_simbolo',
    ];

    
    //METODOS
    
    /**
     * Busca Todos los registros de la tabla Operadores Matematicos
     * @return type
     */
    public static function buscarLstOperadoresMatematicos(){
        try {
            $lstOperadoresMatematicos= OperadoresMatematicos::all();
            return $lstOperadoresMatematicos;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
}
