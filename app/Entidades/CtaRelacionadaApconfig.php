<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class CtaRelacionadaApconfig extends Model
{
    /*
	 * Table: cta_relaciona_apconfig
	 * NameCreate : Jtriviño
	 * Create: 12/Septiembre/2016
	 * NameUpdate : Jtriviño
	 * Update: 12/Septiembre/2016
	 */
	
    protected $table = 'cta_relaciona_apconfig';
    protected $primaryKey = 'cta_relaciona_apconfig_id';
    public $timestamps = false;
    
    protected $fillable = ['cuenta_id', 'analisis_pre_configurado_id'];
    
    //Relaciones
   
    
    public function cuenta() {
    	return $this->belongsTo('App\Entidades\Cuenta');
    }
    
    public function analisisPreConfigurado(){
    	return $this->belongsTo('App\Entidades\AnalisisPreConfigurado');
    }
    //Metodos
    
    public static function buscarCtaRelacionaApconfig($request){
    	$objCtaRelacionaApconfig=ctaRelacionadaApconfig::where('analisis_pre_configurado_id',$request->analisis_pre_configurado_id)->get();
    	return $objCtaRelacionaApconfig;
    }
    
    public static function consultaCuentaRelacion($request){
    	$lstEntidades= ctaRelacionadaApconfig::with('cuenta','analisisPreConfigurado')
    	->where ('analisis_pre_configurado_id','=',$request->analisis_pre_configurado_id)
    	->get();
    	return $lstEntidades;
    }
    
}
