<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class ParametroArchivoCarga extends Model
{
    /*
     * Table: periodo
     * NameCreate : JRendon
     * Create: 14/Sept/2016 
     * NameUpdate : 
     * Update: 
     */
   
    protected $table = 'parametro_archivo_carga';
    protected $primaryKey = 'parametro_archivo_carga_id';
    public $timestamps = false;
    protected $fillable = [
        'parametro_archivo_carga_tipo',
        'parametro_archivo_carga_num_columnas_fijas'
        
    ];
    
    
    
    
    //METODO
    
    public static function consultaParametroArchivo($archivoParametro){
        try {
            $objParametroArchivo=  ParametroArchivoCarga::where('parametro_archivo_carga_tipo','=',$archivoParametro)->first();
            return $objParametroArchivo;
        }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                   
           } 
            
        
    }
    
    /**
     * Metodo implementado para subir el parametro archivo carga
     */
    public static function consultaParametroCarga($parametro){
         try {
             $users = ParametroArchivoCarga::where('parametro_archivo_carga_tipo','=',$parametro)->count();
             return $users;
        
         } catch (\Exception $e) {
            Session::flash('message',$e->getMessage());
        }   
           
    }
}
