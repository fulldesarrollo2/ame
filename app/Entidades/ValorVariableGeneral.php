<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;
use App\Periodo;
use Validator;

class ValorVariableGeneral extends Model
{
	
	/*
	 * Table: valor_variable_general
	 * NameCreate : Jtriviño
	 * Create: 6/Septiembre/2016
	 * NameUpdate : Jtriviño
	 * Update: 6/Septiembre/2016
	 */
	
    protected $table = 'valor_variable_general';
    protected $primaryKey = 'valor_variable_general_id';
    public $timestamps = false;
    
    protected $fillable = ['division_politica_id', 'periodo_id', 'variable_general_id',
    		'valor_variable_general_valor'
    ];
    
    //Relaciones
    
    public function divisionPolitica()
    {
       // return $this->hasOne('App\Entidades\DivisionPolitica', 'division_politica_id');    
        return $this->hasMany('App\Entidades\DivisionPolitica', 'division_politica_id','division_politica_id');
    }
    public function variablegeneral()
    {
    	return $this->hasOne('App\Entidades\VariableGeneral', 'variable_general_id', 'variable_general_id');
    }
    public function periodo()
    {
    	return $this->hasOne('App\Entidades\Periodo', 'periodo_id', 'periodo_id');
    }
    
    //Metodos
    
    public static function consultaValVarRelacionadoxDivPolitica($id,$periodo){
    	$lstEntRelacionValVarDivPol=ValorVariableGeneral::with('divisionPolitica','variablegeneral','periodo')
    	->where ('division_politica_id','=',$id)
    	->get();
    	$filtrado=null;
    	$i=0;
    	
    	foreach ($lstEntRelacionValVarDivPol as $variable){
	
    		if($variable->variablegeneral->estado_id==Estado::$estadoActivo && $variable->periodo->periodo_anio==$periodo[0]){
    			//if($variable->periodo->periodo_anio==$periodo){
    			$filtrado[$i]=$variable;
    			$i++;
    			//}
    		
    		}
    	}
    	$lstEntRelacionValVarDivPol=collect($filtrado);
    	
    	return $lstEntRelacionValVarDivPol;
    
    }
    
    // METODOS
    
    public static function consultarVariableGeneral(){
    	$objVariableGeneral=VariableGeneral::all();
    	return $objVariableGeneral;
    }
    
    public static function  consultarxCantonValorVariableGeneral($idcanton,$periodo){
    	$objVariableGeneral=ValorVariableGeneral::where('division_politica_id',$idcanton)->
       		where('periodo_id','==',$periodo )->get();
    	dd($objVariableGeneral);
    }
    
    public static function consultaInfoVariableEspecifica($id){
    
    	$lstEntRelacionValVarDivPol=ValorVariableGeneral::with('divisionPolitica','variablegeneral')
    	->where ('valor_variable_general_id','=',$id)
    	->get();
    	return $lstEntRelacionValVarDivPol;
    
    }
    //Validacion de variable general
    public static function validaVariableGeneral($request){
    	 
    
    	$messages = [
    			'variable_general_id.required' => 'Se necesario el codigo de variable',
    			'variable_general_nombre_programatico.required' => 'Por favor escriba un nombre',
    			'variable_general_descripcion.required' => 'Por favor escriba un descripción',
    			'variable_general_fuente.required' => 'Es necesario la fuente',
    			'variable_general_unidad.required' => 'Debe establecer una unidad',
    			'estado_id.required' => 'El estado es obligatorio',
     			 
    	];
    
    
    
    	$validator = Validator::make($request->all(), [
    			'variable_general_id'=> 'required|numeric',
    			'variable_general_nombre_programatico' => 'required|max:253|alpha_spaces',
    			'variable_general_descripcion' => 'required|max:253|alpha_spaces',
    			'variable_general_fuente' => 'required|max:253|alpha_spaces',
    			'variable_general_unidad' => 'required|max:253|alpha_spaces',
    			'estado_id' => 'required|min:1|max:2|numeric',
    			
    
    
    	],$messages);
    	 
    	 
    	//dd($validator);
    	return $validator;
    	 
    
    }
    
    public static function validaValorVariableGeneral($request){
    
    
    	$messages = [
    			'valor_variable_general_valor.required' => 'El campo valor no puede estar vacio',
    			'valor_variable_general_valor.numeric' => 'El campo valor solo acepta numeros',    
    	];
    
    
    
    	$validator = Validator::make($request->all(), [
    			'valor_variable_general_valor'=> 'required|numeric'    
    	],$messages);
    
    
    	//dd($validator);
    	return $validator;
    
    
    }
}
