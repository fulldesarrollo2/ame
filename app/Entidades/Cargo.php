<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    
    protected $table = 'cargo';
      
    protected $primaryKey = 'cargo_id';
    public $timestamps = false;    
    protected $fillable = [
        'cargo_nombre',
        'cargo_tipo',
        'cargo_aplica_gadm',
        'cargo_aplica_empresa',
    ];
    
    //RELACIONES
     

    //METODOS
    
}
