<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class BienMueble extends Model
{
    protected $table = 'bien_mueble';
    protected $primaryKey = 'bien_mueble_id';
    public $timestamps = false;
    protected $fillable = [
        'entidad_id',
        'periodo_id',
        'bien_mueble_descripcion',
        'bien_mueble_fecha_compra',
        'bien_mueble_vida_util',
        'bien_mueble_valor_compra',
        'estado_id',
        'bien_mueble_cantidad',
    ];
    
    //VARIABLES
    
    //METODOS

}
