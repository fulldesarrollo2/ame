<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class ParametroGeneral extends Model {

    protected $table = 'parametro_general';
    protected $primaryKey = 'parametro_general_id';
    public $timestamps = false;
    protected $fillable = [
        'parametro_general_nombre',
        'parametro_general_descripcion',
        'parametro_general_valor'
    ];
    
    //VARIABLES 
    public static $cierrePeriodo = 13;
    public static $cambioPeriodoFiscal = 14;

    //METODOS

    /**
     * AUTOR: GCASTILLO
     * DESCRIPCION: BUSCA POR NOMBRE -> PARAMETRO GENERAL
     * @param type $parametro_general_nombre
     * @return type
     */
    public static function buscarParametroGeneral($parametro_general_nombre) {
        try {
            $objParametroGeneral = ParametroGeneral::where('parametro_general_nombre', '=', $parametro_general_nombre)->first();
            return $objParametroGeneral;
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect()->back();
        }
    }

}
