<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class TipoFuente extends Model {
    /*
     * Table: tipo_fuente
     * NameCreate : CBastidas
     * Create: 2/Sept/2016 
     * NameUpdate : 
     * Update: 
     */

    
    protected $table = 'tipo_fuente';
    protected $primaryKey = 'tipo_fuente_id';
    public $timestamps = false;
    protected $fillable = [
        'tipo_fuente_descripcion',
        'tipo_fuente_sigla',
    ];

    public static $fuenteMF = 1;  
    public static $fuenteBsE = 2; 
    public static $fuenteEntidad = 3; 
    public static $fuenteBdE = 2; 
    
    
    //RELACIONES

    public function cuenta() {
        return $this->belongsTo('App\Entidades\Cuenta');
    }

    //METODOS

    public static function buscarLstTipoFuente(){
        try {
            $lstTipoFuente = TipoFuente::all();
            return $lstTipoFuente;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
     /**
     * 
     * @param type $lstCodigo
     * @return type
     * @author JRendon
     */
    public static function buscarLstTipoFuentexId($lstCodigo){
        try {
            $lstTipoFuente = TipoFuente::whereIn('tipo_fuente_id', $lstCodigo)->get();
            return $lstTipoFuente;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

}
