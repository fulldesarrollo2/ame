<?php namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Request as userip;

class Auditoria extends Model {
    
    protected $table = 'auditoria';
    protected $primaryKey = 'auditoria_id';
    public $timestamps = false;
    protected $fillable = [
        'usuario_id',
        'auditoria_fecha_hora',
        'auditoria_tipo_accion',
        'auditoria_interfaz',
        'auditoria_ip'
    ];
    
    //VARIABLES
    public static $guardar = 1;
    public static $actualizar = 2;
    
    //METODOS

    /**
     * AUTOR: JTRIVIÑO
     * DESCRIPCION: REALIZA LA ACCION GUARDAR -> AUDITORIA
     * @param type $variable,$interfaz
     * @return type
     */
    public static function ingresoAuditoria($variable, $interfaz) {
        $usuario = Auth::user();
        $ip = userip::ip();
        try {
            $auditoria = new Auditoria();
            $auditoria->usuario_id = $usuario->usuario_id;
            $auditoria->parametro_general_id = $variable;
            $auditoria->auditoria_interfaz = $interfaz;
            $auditoria->auditoria_ip = $ip;
            $auditoria->save();
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect()->back();
        }
    }

}
