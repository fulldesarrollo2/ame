<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class Indicador extends Model
{
    /*
     * Table: estado
     * NameCreate : CBastidas
     * Create: 26/Agosto/2016 
     * NameUpdate : CBastidas
     * Update: 29/Agosto/2016
    */
    
    protected $table = 'indicador';
      
    protected $primaryKey = 'indicador_id';
    
    public $timestamps = false;
    
    protected $fillable = [
        'tipo_indicador_id',
        'indicador_proyeccion_financiera',
        'indicador_nombre',
        'indicador_objetivo',
        'indicador_alcance',
        'estado_id',
        'indicador_formula'
    ];
    
    //RELACIONES
    
    public function estado() {
        return $this->hasMany('App\Entidades\Estado');
    }
    
    public function tipoIndicador() {
        return $this->hasMany('App\Entidades\TipoIndicador');
    }
    
    public function rangoAceptacion() {
        return $this->hasMany('App\Entidades\RangoAceptacion');
    }
    
    //METODOS
    
    
    /**
     * Busca Indicador por Nombre
     * @return type
     */
    public static function buscarIndicadorxNombre($indicadorNombre){
        try {
            $objIndicador = Indicador::with('tipoIndicador','estado','rangoAceptacion','rangoAceptacion.categoria','rangoAceptacion.nivelAlerta','rangoAceptacion.nivelAlerta.tipoNivelAlerta')->where('indicador_nombre',$indicadorNombre)->get();
            return $objIndicador;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
    /**
     * Busca todos los indicadores que corresponden a una lista de ids de indicadores
     * @return type
     * @author RGARCIA
     */
    public static function buscarIndicadores($lstIdIndicadores){
        try {
            $objIndicador = Indicador::find($lstIdIndicadores);
            return $objIndicador;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
    /**
     * Devuelve lista de todos los indicadores
     * @author RGARCIA
     */
    public static function buscarLstIndicador(){
        try {
            $lstIndicador= Indicador::all();
            return $lstIndicador;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
}
