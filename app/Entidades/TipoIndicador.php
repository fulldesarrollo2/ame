<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class TipoIndicador extends Model
{
    /*
     * Table: tipo_indicador
     * NameCreate : KPortugal
     * Create: 15/Septiembre/2016 
    */
    
    protected $table = 'tipo_indicador';
      
    protected $primaryKey = 'tipo_indicador_id';
        
    protected $fillable = [
        'tipo_indicador_nombre'
    ];
    
    public static $financiero = 1;
    public static $serBasMunicipales = 2;
     //RELACIONES
    
    public function indicador() {
        return $this->hasMany('App\Entidades\Indicador');
    }
    
     //METODOS

    public static function buscarLstTipoIndicador(){
        try {
            $lstTipoIndicador= TipoIndicador::all();
            return $lstTipoIndicador;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
}
