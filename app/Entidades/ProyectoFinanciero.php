<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class ProyectoFinanciero extends Model
{
	/*
	 * Table: proyeccion_financiera
	 * NameCreate : Jtriviño
	 * Create: 15/Septiembre/2016
	 * NameUpdate : Jtriviño
	 * Update: 15/Septiembre/2016
	 */
	
	protected $table = 'proyecto_financiero';
	protected $primaryKey = 'proyecto_financiero_id';
	public $timestamps = false;
	
	protected $fillable = ['entidad_id', 'proyecto_financiero_nombre','proyecto_financiero_desccripcion
			','proyecto_financiero_tipo','proyecto_financiero_inicio','proyecto_financiero_fin'
			,'estado_id'];
	
	//Relaciones
	public function estado() {
		return $this->belongsTo('App\Entidades\Estado');
	}
	
	public function componenteproyecto(){
		return $this->hasMany('App\Entidades\ComponenteProyecto','proyecto_financiero_id','proyecto_financiero_id');
	}
	
	
	public static function consultarProyectoFinanciero($request){
		try {
			$objProyectoFinanciero=ProyectoFinanciero::with('componenteproyecto.actividadproyecto.ejecucion.periodo')
			->where ( 'entidad_id', '=', $request->id )
			->whereYear('proyecto_financiero_inicio', '=', date($request->periodo))
			->get();
			
			
			return $objProyectoFinanciero;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}
	//Metodos
}
