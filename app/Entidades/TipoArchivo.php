<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class TipoArchivo extends Model
{
    /*
     * Table: periodo
     * NameCreate : JRendon
     * Create: 13/Sept/2016 
     * NameUpdate : 
     * Update: 
     */

    protected $table = 'tipo_archivo';
    protected $primaryKey = 'tipo_archivo_id';
    public $timestamps = false;
    protected $fillable = [
        'tipo_fuente_id',
        'tipo_archivo_nombre',
        'estado_id',
        'tipo_cuenta_id',
        'tipo_archivo_parametro'
        
    ];
    
    // RELACIONES
    public function estado() {
        return $this->belongsTo('App\Entidades\Estado');
    }
    
    
    
    
     // METODOS
    
}
