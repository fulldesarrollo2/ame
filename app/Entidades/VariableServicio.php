<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class VariableServicio extends Model
{
    /*
     * Table: variable_indicador
     * NameCreate : CBastidas
     * Create: 26/Agosto/2016 
     * NameUpdate : CBastidas
     * Update: 29/Agosto/2016
    */
 
    protected $table = 'variable_servicio';
    
    protected $primaryKey = 'variable_servicio_id';
    
    
    //METODOS
    
    /**
     * 
     * @return type
     */
    public static function buscarLstVariableServicio(){
        try {
            $lstVariableServicio = VariableServicio::all();
            return $lstVariableServicio;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
}
