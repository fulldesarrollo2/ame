<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class TipoArchivoFuente extends Model
{
     /*
     * Table: periodo
     * NameCreate : JRendon
     * Create: 13/Sept/2016 
     * NameUpdate : 
     * Update: 
     */

    protected $table = 'tipo_archivo_fuente';
    protected $primaryKey = 'tipo_archivo_fuente_id';
    public $timestamps = false;
    protected $fillable = [
        'tipo_archivo_id',
        'tipo_fuente_id'
        
    ];
    
    // RELACIONES
    public function tipoArchivo() {
        return $this->belongsTo('App\Entidades\TipoArchivo');
    }
    public function tipoFuente() {
        return $this->belongsTo('App\Entidades\TipoFuente');
    }
    
    /**
     * 
     * @param type $tipoFuenteId
     * @return type
     * @author JRendon
     * @since 28/09/2016
     * @abstract consulta el tipo de archivo por el tipo de fuente
     */
    public static function consultarArchivoCarga($tipoFuenteId) {
         $lstArchivoCarga= TipoArchivoFuente::with('tipoArchivo','tipoFuente')->where('tipo_fuente_id','=',$tipoFuenteId)->get();
         return $lstArchivoCarga;
        
    }
    
   
    
}
