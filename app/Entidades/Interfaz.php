<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;
use DB;

class Interfaz extends Model {

    protected $table = 'interfaz';
    protected $primaryKey = 'interfaz_id';
    public $timestamps = false;
    protected $fillable = [
        'interfaz_codigo',
        'interfaz_configuracion',
        'interfaz_descripcion',
        'interfaz_padre',
        'estado_id'
    ];

    //RELACIONES

    public function estado() {
        return $this->belongsTo('App\Entidades\Estado');
    }

    public function interfazRelacionaPerfil() {
        return $this->hasOne('App\Entidades\InterfazRelacionaPerfil');
    }

    public function interfazHijo() {
        return $this->hasMany('App\Entidades\Interfaz', 'interfaz_padre', 'interfaz_id');
    }

    public function interfazPadre() {
        return $this->hasOne('App\Entidades\Interfaz', 'interfaz_id', 'interfaz_padre');
    }

    //METODOS 

    /**
     * AUTOR: GCASTILLO
     * DESCRIPCION: BUSCA LISTA DE DATOS RELACIONADOS -> INTERFAZ
     * @param type $perfil_ingreso_id
     * @return type
     */
    public static function buscarInterfaz() {
        try {
            $lstInterfaz = DB::table('interfaz')->whereNotNull('interfaz_padre')->where('estado_id',Estado::$estadoActivo)->get();
            return $lstInterfaz;
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect()->back();
        }
    }

}
