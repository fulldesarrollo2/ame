<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;
use Validator;

class AnalisisPreConfigurado extends Model
{
     /*
	 * Table: analisis_pre_configurado
	 * NameCreate : Jtriviño
	 * Create: 12/Septiembre/2016
	 * NameUpdate : Jtriviño
	 * Update: 12/Septiembre/2016
	 */
	
    protected $table = 'analisis_pre_configurado';
    protected $primaryKey = 'analisis_pre_configurado_id';
    public $timestamps = false;
    
    protected $fillable = ['analisis_pre_configurado_nombre', 'analisis_pre_configurado_criterio','estado_id'];
    
    //Relaciones
    public function estado() {
    	return $this->belongsTo('App\Entidades\Estado');
    }
    
    public function ctaRelacionadaApConfig(){
    	return $this->belongsTo('App\Entidades\ctaRelacionadaApconfig');
    }
    
    //Metodos
    
    public static function consultarUltimoAnalisisPreConfig(){
    	$objAnalisisPreConfig=AnalisisPreConfigurado::orderBy('analisis_pre_configurado_id', 'desc')->first();
    	return $objAnalisisPreConfig;
    }
    
    public static function buscarAnalisisPreConfigurado($request){
    	$objAnalisisPreConfig=AnalisisPreConfigurado::find($request->analisis_pre_configurado_id);
    	return $objAnalisisPreConfig;
    }
    
    public static function consultaLstAnalisisPreConfigurado(){
    	$objAnalisisPreConfig=AnalisisPreConfigurado::all();
    	return $objAnalisisPreConfig;
    }
    
    public static function validarguardarAnalisisPreConfig($request){
    	$messages = [
    			'analisis_pre_configurado_nombre.required' => 'Se necesita escribir un nombre para analisis Pre configurado',
    			'cuentasComparativas.required' => 'Debe seleccionar si por lo menos 2 cuentas',
    			'analisis_preconfigurado_criterio.required' => 'Debe establecer un criterio de analisis',

    			
    	];
    	$rules = array(
    			'analisis_pre_configurado_nombre'=> 'required|max:253|alpha_spaces',
    			'cuentasComparativas' => 'required|min:2',
    			'analisis_preconfigurado_criterio' => 'required|max:253'
    	);
    
    	$validator = Validator::make($request->all(),$rules,$messages);
    	
    	
    	return $validator;
    }
    
    public static function validarActualizarAnalisisPreConfig($request){
    	$messages = [
    			'analisis_pre_configurado_nombre.required' => 'Se necesita escribir un nombre para analisis Pre configurado',
    			'cuentasComparativas.required' => 'Debe seleccionar si por lo menos 2 cuentas',
    			'analisis_preconfigurado_criterio.required' => 'Debe establecer un criterio de analisis',
    			'estado_id.required' => 'Analisis debe contener un estado',
    			'analisis_pre_configurado_id.required' => 'Debe seleccionar un analisis a editar',
    	];
    	$rules = array(
    			'analisis_pre_configurado_id'=> 'required|numeric',
    			'analisis_pre_configurado_nombre'=> 'required|max:253|alpha_spaces',
    			'cuentasComparativas' => 'required|min:2',
    			'estado_id'=>'required|min:1|max:2|numeric',
    			'analisis_preconfigurado_criterio' => 'required|max:253'
    	);
    
    	$validator = Validator::make($request->all(),$rules,$messages);
    	 
    	 
    	return $validator;
    }
}
