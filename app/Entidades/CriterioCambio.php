<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class CriterioCambio extends Model
{
   /*
	 * Table: criterio_cambio
	 * NameCreate : CBASTIDAS
	 * Create: 15/Octubre/2016
	 * NameUpdate : Jtriviño
	 * Update: 15/Octubre/2016
	 */
	
    protected $table = 'criterio_cambio';
    protected $primaryKey = 'criterio_cambio_id';
    public $timestamps = false;
    
    protected $fillable = ['nivel_alerta_id', 'criterio_cambio_componente', 'criterio_cambio_criterio'];
}
