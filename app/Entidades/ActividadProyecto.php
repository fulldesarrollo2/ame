<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;
use PhpParser\Node\Stmt\TryCatch;
use Request as userip;
use DB;

class ActividadProyecto extends Model
{
   
	
	protected $table = 'actividad_proyecto';
	protected $primaryKey = 'actividad_proyecto_id';
	public $timestamps = false;
	
	protected $fillable = ['cuenta_id', 'componente_proyecto_id'
			,'actividad_proyecto_descripcion','actividad_proyecto_financiamiento'];
	
	//Relaciones
        
         public function componenteProyecto() {
                return $this->belongsTo('App\Entidades\ComponenteProyecto');
         }
    
	public function ejecucion(){
		return $this->hasMany('App\Entidades\Ejecucion','actividad_proyecto_id','actividad_proyecto_id');
	}
	//Metodos
        
        //GCASTILLO
     public static function buscarActividadxComponente($idComponenteProyecto) {
        try {

             $lstaActividad = ActividadProyecto::with('ejecucion')
            ->join('componente_proyecto', 'componente_proyecto.componente_proyecto_id', '=', 'actividad_proyecto.componente_proyecto_id')
            ->join('cuenta', 'cuenta.cuenta_id', '=', 'actividad_proyecto.cuenta_id')
            ->where('actividad_proyecto.componente_proyecto_id', $idComponenteProyecto)
         ->get();
            return $lstaActividad;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
     public static function buscarActividadEjecucion() {
        try {

             $lstaActividad = ActividadProyecto::with('ejecucion')
                                ->get();
            return $lstaActividad;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    public static function buscarActividadComponente() {
        try {

             $lstaActividad = ActividadProyecto::with('componenteProyecto')
                                ->get();
            return $lstaActividad;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
}
