<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class IndicadorTemporal extends Model
{
    /*
     * Table: indicadorTemporal
     * NameCreate : RGarcia
     * Create: 28/Octubre/2016 
    */
    
    protected $table = 'indicador_temporal';
      
    protected $primaryKey = 'indicador_temporal_id';
    
    public $timestamps = false;
    
    protected $fillable = [
        'periodo_id',
        'usuario_id',
        'entidad_id',
        'indicador_temporal_nombre',
        'indicador_temporal_descripcion',
        'indicador_temporal_formula',
        'indicador_temporal_valor',
        'indicador_temporal_fecha'    
    ];
    
    //RELACIONES
    
    public function periodo() {
        return $this->belongsTo('App\Entidades\Periodo');
    }
    
    public function usuario() {
        return $this->belongsTo('App\Entidades\Usuario');
    }
    
    public function entidad() {
        return $this->belongsTo('App\Entidades\Entidad');
    }
    
    //METODOS
    
    
}
