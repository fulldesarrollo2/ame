<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class TipoEntidad extends Model
{
    
    /*
     * Table: tipo_entidad
     * NameCreate : JRendon
     * Create: 01/Septiembre/2016 
     * NameUpdate :Jrendon 
     * Update: 07/09/2016
    */
    
    protected $table = 'tipo_entidad';
      
    protected $primaryKey = 'tipo_entidad_id';
    
    public $timestamps = false;
    
    protected $fillable = [
        'nombre'
    ];
    
    public static $gadm = 1;
    public static $empresa = 2;
    public static $mancomunidad = 3;
    public static $empServBasMunicipales = 4;
     //RELACIONES
    
     //METODOS
     public static function buscarLstTipoEntidadxId($lstCodigo){
        try {
            $lstTipoEntidad = TipoEntidad::whereIn('tipo_entidad_id', $lstCodigo)->get();
            return $lstTipoEntidad;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
}
