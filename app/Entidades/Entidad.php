<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class Entidad extends Model
{
    /*
     * Table: entidad
     * NameCreate : CBastidas
     * Create: 31/Agosto/2016 
     * NameUpdate : CBastidas
     * Update: 29/Agosto/2016
    */
    
    protected $table = 'entidad';
      
    protected $primaryKey = 'entidad_id';
    
    public $timestamps = false;
    
    protected $fillable = [
        'entidad_ruc',
        'entidad_nombre',
        'entidad_codigo_ente',
        'entidad_periodo_vigente',
        'entidad_direccion',
        'entidad_telefono',
        'entidad_autoridad',
        'tipo_entidad_id'  
    ];
    
    
    //RELACIONES
    
    public function usuario() {
        return $this->hasMany('App\Entidades\Usuario');
    }
    
     public function tipoEntidad() {
        return $this->belongsTo('App\Entidades\TipoEntidad');
    }
    
    //METODOS
    /*
     * Metodo: consultaEntidad
     * NameCreate : JRendon
     * Create: 06/septiembre/2016 
     * Descripcion: obtiene Entidad
    */
    public static function consultaEntidad(){
       $lstEntidades= Entidad::with('tipoEntidad')->get();
       return $lstEntidades;
    }
    
    /*
     * Metodo: buscarEntidad
     * NameCreate : GCASTILLO
     * Create: 09/septiembre/2016 
     * Descripcion: busca Entidad*/
   
    
     public static function buscarEntidad($entidad_id){
       $objEntidad= Entidad::where('entidad_id',$entidad_id)
                       ->get();
       return $objEntidad; 
    }
}
