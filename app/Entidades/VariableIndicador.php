<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class VariableIndicador extends Model
{
    /*
     * Table: variable_indicador
     * NameCreate : CBastidas
     * Create: 26/Agosto/2016 
     * NameUpdate : CBastidas
     * Update: 29/Agosto/2016
    */
 
    protected $table = 'variable_indicador';
    
    protected $primaryKey = 'variable_indicador_id';
    
    public $timestamps = false;
    
    protected $fillable = [
        'variable_indicador_nombre',
        'variable_indicador_descripcion',
    ];
    
    public static $varIndicadorFinanciera = 1;   
    public static $varIndicadorServicios = 2;
    public static $varIndicadorGeneral = 3;
    
    
    
    //METODOS
    
    /**
     * Busca Todos los registros de la tabla Variable Indicador
     * @return type
     */
    public static function buscarLstVariableIndicador(){
        try {
            $lstVariableIndicador= VariableIndicador::all();
            return $lstVariableIndicador;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
}
