<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class VariableCosteo extends Model
{
    
    protected $table = 'variable_costeo';
      
    protected $primaryKey = 'variable_costeo_id';
    public $timestamps = false;    
    protected $fillable = [
        'unidad_servicio_id',
        'variable_costeo_tipo',
        'variable_costeo_proyeccion_financiera',
    ];
    
     //RELACIONES
    public function unidadServicio() {
        return $this->belongsTo('App\Entidades\UnidadServicio');
    }    

    //METODOS
    public static function buscarVariableCosteoXunidadServicio($uni_serv_id){
    	$lstVarCosteo=VariableCosteo::select('*')
    		->where('unidad_servicio_id','=',$uni_serv_id)
    		->get();
    	return$lstVarCosteo;
    }

}
