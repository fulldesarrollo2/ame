<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class CedulaTipoCuenta extends Model
{
   /*
     * Table: cedula_tipo_cuenta
     * NameCreate : JRendon
     * Create: 14/Sept/2016 
     * NameUpdate : 
     * Update: 
     */

    protected $table = 'cedula_tipo_cuenta';
    protected $primaryKey = 'cedula_tipo_cuenta_id';
    public $timestamps = false;
    protected $fillable = [
        'cedula_id',
        'tipo_cuenta_id'
    ];
    
    // RELACIONES
    public function cedula() {
        return $this->belongsTo('App\Entidades\Cedula');
    }
    
    public function tipoCuenta() {
        return $this->belongsTo('App\Entidades\TipoCuenta');
    }
    
    //CONSULTA
    /** 
     * @return type
     * @abstract Consulta todas las cedulas de gastos por el tipo de cuenta Gasto
     * @author JRendon
     * @since 27/09/2016
     */
    public static function consultaCedulasDeGastos(){
        $lstCedulaTipoCuenta=  CedulaTipoCuenta::with('cedula')->where('tipo_cuenta_id','=', TipoCuenta::$gasto)->get();
        return $lstCedulaTipoCuenta;
        
    }
    
    /**
     * @return type
     * @abstract Consulta todas las cedulas de Ingreso por el tipo de cuenta ingreso
     * @author JRendon
     * @since 27/09/2016
     */
     public static function consultaCedulasDeIngresos(){
         $lstCedulaTipoCuenta=  CedulaTipoCuenta::with('cedula')->where('tipo_cuenta_id','=', TipoCuenta::$ingreso)->get();
        return $lstCedulaTipoCuenta; 
         
     }
    
}
