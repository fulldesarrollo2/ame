<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;
use DB;

class EntRelacionaDivPo extends Model
{
    /*
     * Table: entidad
     * NameCreate : JRendon
     * Create: 31/Agosto/2016 
     * NameUpdate : JRendon
     * Update: 29/Agosto/2016
    */
    
    protected $table = 'ent_relaciona_div_po';
      
    protected $primaryKey = 'ent_relaciona_div_po_id';
    
    public $timestamps = false;
    
    protected $fillable = [
        'entidad_id',
        'division_politica_id',
        'estado_id',
    ];
    
    
    
    //RELACIONES
    
    public function entidad() {
        return $this->belongsTo('App\Entidades\Entidad');
    }
    
     public function divisionPolitica() {
        return $this->belongsTo('App\Entidades\DivisionPolitica');
    }
    
    public function estado() {
        return $this->belongsTo('App\Entidades\Estado');
    }
    
    
    //METODOS
    
     /*
     * Metodo: consultaEntRelacionaDivPoPorEntidad
     * NameCreate : JTRIVIÑO
     * Create: 09/septiembre/2016 
     * Descripcion: obtiene Entidad por Division Politica por Id Entidad*/
    public static function consultaEntRelacionaDivPoPorEntidad($entidadId){
        $lstEntRelacionDivPo=EntRelacionaDivPo::with('entidad','divisionPolitica','estado')
                ->where('entidad_id','=',$entidadId)
                ->where('estado_id','=', Estado::$estadoActivo)->get();
        return $lstEntRelacionDivPo;
        
    }
    
     /*
     * Metodo: consultaEntidadTipo
     * NameCreate : GCASTILLO
     * Create: 09/septiembre/2016 
     * Descripcion: obtiene Entidad por Division Politica y Tipo de Entidad*/

     public static function consultaEntidadTipo($divisionPoliticaId,$tipoEntidad){
        
        $lstEntRelacionDivPo = DB::table('ent_relaciona_div_po')
                    ->join('entidad', 'ent_relaciona_div_po.entidad_id', '=','entidad.entidad_id') 
                    ->where ('ent_relaciona_div_po.division_politica_id','=',$divisionPoliticaId)
                    ->where('ent_relaciona_div_po.estado_id','=', Estado::$estadoActivo)
                    ->where('entidad.tipo_entidad_id','=',$tipoEntidad)
                    ->get();
    
        return $lstEntRelacionDivPo;
        
    }
    
    /**
 * @author JTrivino
 * @entidad EntRelacionaDivPo
 * @since 14/10/2016
 * @retorna las entidades existente por divisionPolitica
	Agregar a Entidad.php
	public static $gadm=1;public static $empServicioBasicoMun=4;
 */	
    public static function consultaEntRelacionaDivPoPorDivisionPolitica($division,$busqueda){ $lstEntRelacionDivPo=EntRelacionaDivPo::with('entidad','divisionPolitica','estado')
    	->where('division_politica_id','=',$division)
    	->groupBy('entidad_id')
    	->where('estado_id','=', Estado::$estadoActivo)->get();
    	
    	$filtrado=null;
    	$i=0;
    	if($busqueda==1){
    	foreach ($lstEntRelacionDivPo as $variable){
    	
    		if($variable->entidad->tipo_entidad_id==Entidad::$gadm){
    			$filtrado[$i]=$variable;
    			$i++;
    		}
    	}
    	
    	}
    	
    	if($busqueda==2){
    		foreach ($lstEntRelacionDivPo as $variable){
    			 
    			if($variable->entidad->tipo_entidad_id==Entidad::$empServicioBasicoMun){
    				$filtrado[$i]=$variable;
    				$i++;
    			}
    		}
    		 
    	}
    	$lstEntRelacionDivPo=collect($filtrado);
    	
    	return $lstEntRelacionDivPo;
    
    }

}
