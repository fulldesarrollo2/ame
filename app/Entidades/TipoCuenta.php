<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class TipoCuenta extends Model
{
    /*
     * Table: tipo_cuenta
     * NameCreate : CBastidas
     * Create: 2/Sept/2016 
     * NameUpdate : 
     * Update: 
    */
    public static $contable = 1;  
    public static $presupuestaria = 2;  
    public static $gasto = 3;  
    public static $ingreso = 4;  
    
    protected $table = 'tipo_cuenta';
      
    protected $primaryKey = 'tipo_cuenta_id';
    
    public $timestamps = false;
    
    protected $fillable = [
        'tipo_cuenta_descripcion',
    ];
    
    
    //RELACIONES
    
    public function cuenta() {
        return $this->belongsTo('App\Entidades\Cuenta');
    }
    
    //METODOS
    
      /**
     * Modificado: Jrendon
     
           * @param type $tipoCuenta
     
           * @return type
     */
   
 public static function buscarLstTipoCuenta($tipoCuenta){
    
    try {
      
          $lstTipoCuenta = TipoCuenta::whereIn('tipo_cuenta_id', $tipoCuenta)->get();
       
          return $lstTipoCuenta;
            
       
      } catch (\Exception $e) {
     
       notify()->flash($e->getMessage(), 'danger');
      
      }
    
  }
}
