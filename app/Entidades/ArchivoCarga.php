<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class ArchivoCarga extends Model {
    /*
     * Table: archivo_carga
     * NameCreate : JRendon
     * Create: 09/Sept/2016 
     * NameUpdate : 
     * Update: 
     */

    protected $table = 'archivo_carga';
    protected $primaryKey = 'archivo_carga_id';
    public $timestamps = false;
    protected $fillable = [
        'periodo_id',
        'usuario_id',
        'archivo_carga_tipo',
        'archivo_carga_fecha',
        'archivo_carga_nombre',
        'archivo_carga_archivo_byte'
    ];

    public function periodo() {
        return $this->belongsTo('App\Entidades\Periodo');
    }

    public function Usuario() {
        return $this->belongsTo('App\Entidades\Usuario');
    }

    /**
     * 
     * @param type $periodoId
     * @return type
     * @abstract CONSULTA EL ARCHIVO DE CARGA SUBIDO EN UN PERIODO SELECCIONADO
     * @author JRENDON
     * @since 04/10/2016
     */
    public static function consultaArchivoCargaPorPeriodo($periodoId) {
        try {
            $objArchCarga = ArchivoCarga::where('periodo_id', $periodoId)->first();
            return $objArchCarga;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * 
     * @param type $periodoId
     * @return type
     * @abstract CONSULTA EL ARCHIVO DE CARGA SUBIDO EN UN PERIODO SELECCIONADO
     * @author JRENDON
     * @since 04/10/2016
     */
    public static function consultaArchivoCarga($periodoId) {
        try {
            $objArchCarga = ArchivoCarga::where('periodo_id', $periodoId)->first();
            return $objArchCarga;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
