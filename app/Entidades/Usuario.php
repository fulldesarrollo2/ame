<?php

namespace App\Entidades;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class Usuario extends Authenticatable
{
    
    /*
     * Table: usuario
     * NameCreate : CBastidas
     * Create: 26/Agosto/2016 
     * NameUpdate : CBastidas
     * Update: 29/Agosto/2016
    */
    
    protected $table = 'usuario'; 
    
    protected $primaryKey = 'usuario_id';
    
    public $timestamps = false;
    
    protected $fillable = [
        'entidad_id',
        'perfil_ingreso_id',
        'usuario_nick', 
        'usuario_nombres',
        'usuario_apellidos',
        'usuario_identificacion',
        'usuario_telefono',
        'usuario_email', 
        'usuario_password',
    ];

    protected $hidden = [
        'usuario_password', 'remember_token',
    ];
    
    
    //RELACIONES
    
    public function perfilIngreso() {
        return $this->belongsTo('App\Entidades\PerfilIngreso');
    }   
    
    
    public function entidad() {
        return $this->belongsTo('App\Entidades\Entidad');
    }
    
    public function tipoEntidad() {
        return $this->belongsTo('App\Entidades\TipoEntidad');
    }
    // METODOS 
   
    /*
     * Metodo: buscarUsuario
     * NameCreate : GCASTILLO
     * Create: 09/septiembre/2016 
     * Descripcion: busca usuario por su nombre de usuario*/

    public static function  buscarUsuario($usuario_nick){
        $objUsuario = DB::table('usuario')
        ->join('entidad','entidad.entidad_id','=','usuario.entidad_id')
        ->join('tipo_entidad','tipo_entidad.tipo_entidad_id','=','entidad.tipo_entidad_id')
        //->join('perfil_ingreso','perfil_ingreso.perfil_ingreso_id','=','usuario.perfil_ingreso_id')
        ->where('usuario_nick', '=', $usuario_nick)->get();

        return $objUsuario; 
    }
}
