<?php namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class InterfazRelacionaPerfil extends Model {

    protected $table = 'interfaz_relaciona_perfil';
    protected $primaryKey = 'interfaz_relaciona_perfil_id';
    public $timestamps = false;
    protected $fillable = [
        'interfaz_id',
        'perfil_ingreso_id',
        'interfaz'
    ];

    //RELACIONES

    public function interfaz() {
        return $this->belongsTo('App\Entidades\Interfaz');
    }

    public function perfilIngreso() {
        return $this->belongsTo('App\Entidades\PerfilIngreso');
    }

    //METODOS

    /**
     * AUTOR: GCASTILLO
     * DESCRIPCION: BUSCA LISTA DE DATOS RELACIONADOS -> INTERFAZ - PERFIL
     * @param type $perfil_ingreso_id
     * @return type
     */
    public static function buscarInterfazRelacionaPerfil($perfil_ingreso_id) {
        try {
            $lstInterfaz = InterfazRelacionaPerfil::with('interfaz')
                    ->where('perfil_ingreso_id', '=', $perfil_ingreso_id)
                    ->get();
            return $lstInterfaz;
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect()->back();
        }
    }

}
