<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class VariableGeneral extends Model
{
    /*
	 * Table: valor_variable_general
	 * NameCreate : Jtriviño
	 * Create: 6/Septiembre/2016
	 * NameUpdate : Jtriviño
	 * Update: 6/Septiembre/2016
	 */
	
    protected $table = 'variable_general';
    protected $primaryKey = 'variable_general_id';
    public $timestamps = false;
    
    protected $fillable = ['variable_general_nombre_programatico', 'variable_general_descripcion',
    		'estado_id','variable_general_unidad','variable_general_tipo'
    ];
    
    //METODOS
    
    /**
     * BUSCA VARIABLE GENERAL POR ESTADO ENVIADOS POR PARAMETRO
     * @param type $lstEstado
     * @return type
     */
    public static function buscarLstVariableGeneralxEstado($lstEstado){
        try {
            $lstVariableGeneral = VariableGeneral::where('estado_id',$lstEstado)->get();
            return $lstVariableGeneral;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
}
