<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model {

    protected $table = 'categoria';
    protected $primaryKey = 'categoria_id';
    public $timestamps = false;
    protected $fillable = ['categoria_nombre',
        'categoria_minimo',
        'categoria_maximo'
    ];

    //METODOS

    /**
     * AUTOR: JTRIVIÑO
     * DESCRIPCION: BUSCA PRIMER DATO TABLA -> CATEGORIA
     * @param type $perfil_ingreso_id
     * @return type
     */
    public static function consultaCategoriaxDefecto() {
        try {
            $objCategoria = Categoria::first();
            return $objCategoria;
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect()->back();
        }
    }

    /**
     * AUTOR: JTRIVIÑO
     * DESCRIPCION: BUSCA LISTA DE DATOS -> CATEGOTIA
     * @param type $perfil_ingreso_id
     * @return type
     */
    public static function buscarLstCategorias() {
        try {
            $lstCategoria = Categoria::all();
            return $lstCategoria;
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect()->back();
        }
    }

}
