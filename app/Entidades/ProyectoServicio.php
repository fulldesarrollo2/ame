<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class proyectoServicio extends Model {

    protected $table = 'proyecto_servicio';
    protected $primaryKey = 'proyecto_servicio_id';
    public $timestamps = false;
    protected $fillable = [
        'entidad_id',
        'cargo_id',
        'proyecto_servicio_nombre',
        'proyecto_servicio_ubicacion',
        'proyecto_servicio_telefono',
        'proyecto_servicio_nombre_administrador',
        'proyecto_servicio_email_administrador',
        'proyecto_servicio_posee_centro_costo',
        'estado_id',
        'proyecto_tipo_servicio_id'
    ];

    // Relación
    public function proyectoTipoServicio() {
        return $this->hasMany('App\Entidades\ProyectoTipoServicio');
    }

    public function entidad() {
        return $this->belongsTo('App\Entidades\Entidad');
    }

    public function cargo() {
        return $this->belongsTo('App\Entidades\Cargo');
    }

    public function estado() {
        return $this->belongsTo('App\Entidades\Estado');
    }
    public function subSistema(){
        return $this->hasMany('App\Entidades\SubSistema');
    }

    // Metodos
    public static function buscarProyecServActivo(){
        $lstProyectoServicio=proyectoServicio::where('estado_id', '=', Estado::$estadoActivo)->get();
        return $lstProyectoServicio;
        
    }
}
