<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class VariableSeguridad extends Model
{
     /*
     * Table: variable_seguridad
     * NameCreate : JRendon
     * Create: 07/Septiembre/2016 
     * NameUpdate : 
     * Update: 
    */
    
    protected $table = 'variable_seguridad';
      
    protected $primaryKey = 'variable_seguridad_id';
    
    public $timestamps = false;
    
    protected $fillable = [
        'variable_seguridad_nombre',
        'variable_seguridad_descripcion',
        'variable_seguridad_valor',
        'estado_id'
         
    ];
    
    public static $periodoActual = 'PERIODO ACTUAL'; 
    
    //RELACIONES
    public function estado() {
        return $this->belongsTo('App\Entidades\Estado');
    }
    /**
     * * NameCreate : JRendon
     * @param type $variableSeguidad
     * @return type
     * Create: 13/Septiembre/2016 
     */
    public static function ConsultaVariable($variableSeguidad){
        try {
                 $objVariableSeguridad= VariableSeguridad::where('variable_seguridad_nombre', '=', $variableSeguidad)->first();
                 return $objVariableSeguridad;
        }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                  
           }   
        
    }
    
}
