<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class InformacionAdministrativa extends Model
{
     protected $table = 'informacion_administrativa';
    protected $primaryKey = 'informacion_administrativa_id';
    public $timestamps = false;
    protected $fillable = [
        'entidad_id',
        'cargo_id',
        'periodo_id',
        'informacion_administrativa_remuneracion',
        'informacion_administrativa_participacion',
        'informacion_administrativa_tipo_costo',
        'informacion_administrativa_cantidad_personas',
        'estado_id',
    ];

    //RELACIONES

    //METODOS
}
