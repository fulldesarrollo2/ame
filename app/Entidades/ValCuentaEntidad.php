<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;
use App\Entidades\Cedula;
use App\Entidades\Estado;

class ValCuentaEntidad extends Model
{
    /*
     * Table: tipo_cuenta
     * NameCreate : JRendon
     * Create: 22/Sept/2016 
     * NameUpdate : 
     * Update: 
    */
    
    protected $table = 'val_cuenta_entidad';
      
    protected $primaryKey = 'val_cuenta_entidad_id';
    
    public $timestamps = false;
    
    protected $fillable = [
        'entidad_id','cuenta_id','archivo_carga_id','val_cuenta_entidad_valor','val_cuenta_entidad_ejecucion','estado_id'
    ];
    
    public function entidad() {
        return $this->belongsTo('App\Entidades\Entidad');
    }
    
    public function cuenta() {
        return $this->belongsTo('App\Entidades\Cuenta');
    }
    
    public function archivoCarga() {
        return $this->belongsTo('App\Entidades\ArchivoCarga');
    }
    
    public function estado() {
        return $this->belongsTo('App\Entidades\Estado');
    }
    
     /**
     * @author JRendon
     * @since 28/09/2016
     * @consulta los valores de las cuentas, que será los que se encuentren en estado "ACTIVO" y de los archivos mas recientes
     */
    
     public static function consultaValCuentaEntidad($entidad_id,$cuenta_id,$archivo_carga_id) {
         
        try {
            $lstCuenta = ValCuentaEntidad::
     			join('archivo_carga', 'val_cuenta_entidad.archivo_carga_id', '=','archivo_carga.archivo_carga_id')
     			->join('periodo', 'archivo_carga.periodo_id', '=','periodo.periodo_id')
                        ->join('cuenta', 'val_cuenta_entidad.cuenta_id', '=','cuenta.cuenta_id')
     			->join('tipo_fuente', 'cuenta.tipo_fuente_id', '=','tipo_fuente.tipo_fuente_id')
     			->join('cedula_tipo_cuenta', 'cuenta.tipo_cuenta_id', '=','cedula_tipo_cuenta.tipo_cuenta_id')
     			->join('cedula', 'cedula_tipo_cuenta.cedula_id', '=','cedula.cedula_id')
     			->join('cta_relaciona_apconfig', 'cuenta.cuenta_id', '=','cta_relaciona_apconfig.cuenta_id')
     			->join('analisis_pre_configurado', 'cta_relaciona_apconfig.analisis_pre_configurado_id', '=','analisis_pre_configurado.analisis_pre_configurado_id')
     			->whereBetween('archivo_carga.periodo_id', [ 95, 95])
     			->where('val_cuenta_entidad.entidad_id','=', 1)
     			->where('tipo_fuente.tipo_fuente_id','=',1)
     			->where('cta_relaciona_apconfig.analisis_pre_configurado_id','=',4)
     			->whereIn('cedula.cedula_id', [ Cedula::$cedulaDevengado , Cedula::$cedulaRecaudado ])
     			->where('val_cuenta_entidad.estado_id',Estado::$estadoActivo)
     			->get();
            return $lstCuenta;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    
     }

     /**
      * @author Jose Luis Rendon 
      * @since 12/11/2016
      */
     
     public static function removerValoresValuentaEntidad($periodoId ,$entidadId){
         
         try {
             
         DB::table('val_cuenta_entidad')
            ->join('archivo_carga', 'archivo_carga.archivo_carga_id' , '=' , 'val_cuenta_entidad.archivo_carga_id')
            ->where('archivo_carga.periodo_id', '='  , $periodoId)
            ->where('val_cuenta_entidad.entidad_id', '=' , $entidadId)->delete();
         
        
           
         }catch(\Exception $e){
                    Session::flash('message',$e->getMessage());
                  
           } 
     }
     /**
     * @author CBASTIDAS
     * @since 24/09/2016
     * @consulta los valores de las cuentas, segun el perdiodo, entidad , fuente y en estado "ACTIVO".
     */
    public static function consultaValCuentaEntidadXPeriodo($entidad_id,$tipo_fuente_id,$periodo_id_desde,$periodo_id_hasta,$analisis_Preconfigurado) {
        try {
            if($analisis_Preconfigurado==null){
                 $lstValCuentaEntidad =  Cuenta::with('valCuentaEntidad','valCuentaEntidad.archivoCarga','valCuentaEntidad.archivoCarga.periodo')
                                        ->join('val_cuenta_entidad', 'cuenta.cuenta_id', '=','val_cuenta_entidad.cuenta_id')
                                        ->join('archivo_carga', 'val_cuenta_entidad.archivo_carga_id', '=','archivo_carga.archivo_carga_id')
                                        ->join('periodo', 'archivo_carga.periodo_id', '=','periodo.periodo_id')
                                        ->join('tipo_fuente', 'cuenta.tipo_fuente_id', '=','tipo_fuente.tipo_fuente_id')
                                        ->join('cedula_tipo_cuenta', 'cuenta.tipo_cuenta_id', '=','cedula_tipo_cuenta.tipo_cuenta_id')
                                        ->join('cedula', 'cedula_tipo_cuenta.cedula_id', '=','cedula.cedula_id')
                                        ->whereBetween('archivo_carga.periodo_id', [ $periodo_id_desde, $periodo_id_hasta])
                                        ->where('val_cuenta_entidad.entidad_id','=', $entidad_id)
                                        ->where('tipo_fuente.tipo_fuente_id','=',$tipo_fuente_id)
                                        ->whereIn('cedula.cedula_id', [ Cedula::$cedulaDevengado , Cedula::$cedulaRecaudado ])
                                        ->where('val_cuenta_entidad.estado_id',Estado::$estadoActivo)
                                        ->orderBy('cuenta.cuenta_codigo','asc')
                                        ->groupBy('cuenta.cuenta_id')   
                                        ->get();
                return $lstValCuentaEntidad;
            }else{
                $lstValCuentaEntidad =  \DB::table('val_cuenta_entidad')
                                        ->join('archivo_carga', 'val_cuenta_entidad.archivo_carga_id', '=','archivo_carga.archivo_carga_id')
                                        ->join('cuenta', 'val_cuenta_entidad.cuenta_id', '=','cuenta.cuenta_id')
                                        ->join('tipo_fuente', 'cuenta.tipo_fuente_id', '=','tipo_fuente.tipo_fuente_id')
                                        ->join('cedula_tipo_cuenta', 'cuenta.tipo_cuenta_id', '=','cedula_tipo_cuenta.tipo_cuenta_id')
                                        ->join('cedula', 'cedula_tipo_cuenta.cedula_id', '=','cedula.cedula_id')
                                        ->join('cta_relaciona_apconfig', 'cuenta.cuenta_id', '=','cta_relaciona_apconfig.cuenta_id')
                                        ->join('analisis_pre_configurado', 'cta_relaciona_apconfig.analisis_pre_configurado_id', '=','analisis_pre_configurado.analisis_pre_configurado_id')
                                        ->whereBetween('archivo_carga.periodo_id', [ $periodo_id_desde, $periodo_id_hasta])
                                        ->where('val_cuenta_entidad.entidad_id','=', $entidad_id)
                                        ->where('tipo_fuente.tipo_fuente_id','=',$tipo_fuente_id)
                                        ->where('cta_relaciona_apconfig.analisis_pre_configurado_id','=',$analisis_Preconfigurado)
                                        ->whereIn('cedula.cedula_id', [ Cedula::$cedulaDevengado , Cedula::$cedulaRecaudado ])
                                        ->where('estado_id',Estado::$estadoActivo)
                                        ->get();
                return $lstValCuentaEntidad;
            }
     	} catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
     	}
    
     }

}
