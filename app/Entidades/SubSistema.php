<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class SubSistema extends Model
{
        
    protected $table = 'sub_sistema';
      
    protected $primaryKey = 'sub_sistema_id';
    public $timestamps = false;    
    protected $fillable = [
        'proyecto_servicio_id',
        'sub_sistema_nombre',
        'susb_sistema_descripcion',
        'estado_id',
    ];
    
     //RELACIONES
    public function proyectoServicio() {
        return $this->belongsTo('App\Entidades\ProyectoServicio');
    }    

    //METODOS
    public static function buscarSubsistemaXproyecto($id){
    	$lstSubsistema=SubSistema::select('*')->where('proyecto_servicio_id','=',$id)->where('estado_id', '=', Estado::$estadoActivo)->get();
    	return$lstSubsistema;
    }

}
