<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class catalogoDatoCosteo extends Model
{
     protected $table = 'catalogo_dato_costeo';
      
    protected $primaryKey = 'catalogo_dato_costeo_id';
    public $timestamps = false;    
    protected $fillable = [
        'catalogo_dato_costeo_id',
        'unidad_servicio_id',
        'catalogo_dato_costeo_nombre',
        'catalogo_dato_costeo_tipo_dato',
        'catalogo_dato_costeo_logitud_dato'
    ];
    
    //RELACIONES
     /**
      *
      * Las relaciones que posee son de uno a muchos para dato_costeo
      * y de muchos a uno para unidad servicio
      *
      */
     public function datoCosteo()
     {
       return $this->hasMany('App\Entidades\Periodo');
     }
     public function unidadServicio()
     {
       return $this->belongsTo('App\Entidades\UnidadServicio');
     }

    //METODOS
    
}
