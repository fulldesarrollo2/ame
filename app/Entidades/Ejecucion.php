<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class Ejecucion extends Model
{
     /*
	 * Table: actividad_proyecto
	 * NameCreate : Jtriviño
	 * Create: 15/Septiembre/2016
	 * NameUpdate : Jtriviño
	 * Update: 15/Septiembre/2016
	 */
	
	protected $table = 'ejecucion';
	protected $primaryKey = 'ejecucion_id';
	public $timestamps = false;
	
	protected $fillable = ['periodo_id', 'actividad_proyecto_id','ejecucion_valor'];
	
	//Relaciones
	public function periodo(){
		return $this->belongsTo('App\Entidades\Periodo','periodo_id','periodo_id');
	}
        
        public function actividadProyecto() {
                return $this->belongsTo('App\Entidades\ActividadProyecto');
        }
	//Metodos
        
        public static function buscarEjecucionxPeriodo($anio_inicio, $anio_fin){
            try{
             $objEjecucion= \DB::table('ejecucion')
             ->join('periodo','ejecucion.periodo_id','=','periodo.periodo_id')
             ->groupBy('periodo.periodo_anio')
             ->whereBetween('periodo.periodo_anio',[$anio_inicio,$anio_fin])
                                        ->get();
             return $objEjecucion;

                  }catch(\Exception $e){
                    \Session::flash('message',$e->getMessage());
                   
              }   

        }
}
