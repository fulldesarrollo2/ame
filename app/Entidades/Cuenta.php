<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model {
    /*
     * Table: cuenta
     * NameCreate : CBastidas
     * Create: 2/Sept/2016 
     * NameUpdate : 
     * Update: 
     */

    protected $table = 'cuenta';
    protected $primaryKey = 'cuenta_id';
    public $timestamps = false;
    protected $fillable = [
        'cuenta_cuenta_id',
        'entidad_id',
        'cuenta_codigo',
        'cuenta_nombre',
        'cuenta_nivel',
        'tipo_cuenta_id',
        'tipo_fuente_id',
    ];
    
    
  

    //RELACIONES

    public function entidad() {
        return $this->belongsTo('App\Entidades\Entidad');
    }

    public function tipoCuenta() {
        return $this->belongsTo('App\Entidades\TipoCuenta');
    }

    public function tipoFuente() {
        return $this->belongsTo('App\Entidades\TipoFuente');
    }

    public function cuentaPadre() {
        return $this->hasOne('App\Entidades\Cuenta','cuenta_id','cuenta_cuenta_id');
    }

    public function cuentaHijo() {
        return $this->hasMany('App\Entidades\Cuenta','cuenta_cuenta_id','cuenta_id');
    }
    
    public function valCuentaEntidad() {
        return $this->hasMany('App\Entidades\ValCuentaEntidad');
    }
    
    public function getCodigoCuentaAttribute()
    {
       return $this->cuenta_codigo . ' ' . $this->cuenta_nombre;
    }


    //METODOS

    
    /**
     * 
     * @param type $tipoCuenta
     * @param type $tipoFuente
     * @return type
     */
    public static function buscarLstCuentaxTipoxFuente($tipoCuenta,$tipoFuente){
        try {
            $lstCuenta = Cuenta::with('tipoCuenta','tipoFuente','cuentaHijo.cuentaHijo','cuentaHijo.cuentaHijo.cuentaHijo','cuentaHijo.cuentaHijo.cuentaHijo.cuentaHijo','cuentaHijo.cuentaHijo.cuentaHijo.cuentaHijo.cuentaHijo')->
                                 where('tipo_cuenta_id',$tipoCuenta)->
                                 where('tipo_fuente_id',$tipoFuente)->
                                 where('cuenta_cuenta_id',null)->
                                 get();
            return $lstCuenta;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
    
    /**
     * 
     * @param type $cuentaCodigo
     * @return type
     */
    public static function buscarCuentaxCodigo($cuentaCodigo) {
        try {
            $objCuenta = Cuenta::with('tipoCuenta','tipoFuente','cuentaPadre')->
                                 where('cuenta_codigo',$cuentaCodigo)->
                                 get();
            return $objCuenta;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
    /*
	* Metodo: buscarLstCuenta
	* NameCreate : Jtrivino
	* Create: 14/septiembre/2016
	* Detalle: Metodo retorna lista de todas cuentas oficiales
    * @param type
    * @return type
    */
    public static function buscarLstCuenta() {
    	try {
    		$lstCuenta = Cuenta::with('tipoCuenta','tipoFuente','cuentaHijo.cuentaHijo')->
    		where('cuenta_cuenta_id',null)->
    		get();
    		// dd($lstCuenta);
    		return $lstCuenta;
    	} catch (\Exception $e) {
    		notify()->flash($e->getMessage(), 'danger');
    	}
    }
    
    /**
     * @param 
     * @author Jrendon
     */
    
    public static function consultaCuentaExistente($valFila, $tipo_cuenta_id, $tipo_fuente_id){
         try {
            $lstCuenta = Cuenta::where('tipo_cuenta_id',$tipo_cuenta_id)->
                                    where('tipo_fuente_id',$tipo_fuente_id)->
                                    where('cuenta_codigo',$valFila)->count();
            return $lstCuenta;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

     /**
     * 
     * @param type $tipoCuenta
     * @param type $tipoFuente
     * @return type
     * @author JRendon
     */
     public static function buscarLstCuentaxEntidadxTipo($entidadId,$tipo_cuenta,$lstFuente){
        try {
             $lstCuenta = Cuenta::with('tipoCuenta','tipoFuente','cuentaHijo.cuentaHijo')->
                                 where('entidad_id',$entidadId)->
                                 where('tipo_cuenta_id',$tipo_cuenta)->
                                 whereIn('tipo_fuente_id',$lstFuente)->
                                 where('cuenta_cuenta_id',null)->
                                 orderBy('cuenta_codigo','asc')->
                                 get();
            return $lstCuenta;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
    /**
     * 
     * @param type $entidadId
     * @param type $tipo_cuenta
     * @param type $tipoFuente
     * @param type $archivoCargaId
     * @return type
     * @since 04-10-2016
     * @author JRENDON
     * @abstract Metodo implementado para consultar cuentas con sus valores
     * @ los valores de las cuentas a consultar son los ultimos del archivo cargado dependiendo del periodo selecionado ($archivoCargaId)
     * 
     */
     public static function buscarLstCuentaxEntidadxTipoEloquent($entidadId,$tipo_cuenta,$tipoFuente,$archivoCargaId){
        try {
          
                    
                   $lstEntRelacionDivPo = DB::table('cuenta')
                    ->select(['cuenta.cuenta_codigo',
                              'cuenta.cuenta_nombre',
                              'val_cuenta_entidad.val_cuenta_entidad_valor',
                              'cuenta.cuenta_cuenta_id',
                              'cuenta.cuenta_id',
                               'cuenta.cuenta_nivel as inconsistente'])
                    ->leftJoin('val_cuenta_entidad', 'cuenta.cuenta_id', '=','val_cuenta_entidad.cuenta_id') 
                    ->where ('cuenta.entidad_id','=',$entidadId)
                    ->where ('cuenta.tipo_cuenta_id','=', $tipo_cuenta)
                    ->whereIn ('cuenta.tipo_fuente_id', $tipoFuente)  
                    ->whereOr ('val_cuenta_entidad.archivo_carga_id','=', $archivoCargaId) 
                    ->orderBy( 'cuenta.cuenta_codigo', 'asc')
                    ->get();
            
                    return $lstEntRelacionDivPo;
                } catch (\Exception $e) {
                    notify()->flash($e->getMessage(), 'danger');
                }
    }
    
      /*GCASTILLO*/
     public static function buscarLstCuentaxTipo($tipoCuenta) {
       try {
           $lstCuenta = Cuenta::with('tipoCuenta','tipoFuente','cuentaHijo.cuentaHijo')->
                                where('tipo_cuenta_id',$tipoCuenta)->
                                where('cuenta_cuenta_id',null)->
                                get();
           return $lstCuenta;
       } catch (\Exception $e) {
           notify()->flash($e->getMessage(), 'danger');
       }
   }

   /*
     * @param type $entidad_id
     * @param type $tipo_fuente_id
     * @author RGarcia
     */
    public static function buscarLstCuentaxEntidadxFuente($entidad_id,$tipo_fuente_id){
        try {
            $lstCuenta = Cuenta::with('cuenta.cuenta')->
                                 where('entidad_id',$entidad_id)->
                                 where('tipo_fuente_id',$tipo_fuente_id)->
                                 orderBy('cuenta_codigo','asc')->
                                 get();
            return $lstCuenta;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }
    
    /*
     * @param type $entidad_id
     * @param type $tipo_fuente_id
     * @author RGarcia
     */
    public static function buscarCuentaxNombrexEntidadxFuente($cuenta_nombre,$entidad_id,$tipo_fuente_id){
        try {
            $objCuenta = Cuenta::where('cuenta_nombre',$cuenta_nombre)->
                                 where('entidad_id',$entidad_id)->
                                 where('tipo_fuente_id',$tipo_fuente_id)->
                                 first();
            return $objCuenta;
        } catch (\Exception $e) {
            notify()->flash($e->getMessage(), 'danger');
        }
    }

}
