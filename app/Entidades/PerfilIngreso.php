<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class PerfilIngreso extends Model {

    protected $table = 'perfil_ingreso';
    protected $primaryKey = 'perfil_ingreso_id';
    public $timestamps = false;
    protected $fillable = [
        'perfil_ingreso_nombre',
        'perfil_ingreso_descripcion',
        'estado_id',
    ];
    
    //VARIABLES
    public static $ameTecnico = 1;
    public static $ameAnalista = 2;
    public static $entidad = 3;
    public static $administrador = 16;
    public static $ameAdministrador = 17;
    public static $usuario = 9;

    //RELACIONES

    public function estado() {
        return $this->belongsTo('App\Entidades\Estado');
    }

    public function interfazRelacionaPerfil() {
        return $this->hasMany('App\Entidades\InterfazRelacionaPerfil');
    }

    public function usuario() {
        return $this->hasMany('App\Entidades\Usuario');
    }

    //METODOS

    /**
     * AUTOR: GCASTILLO
     * DESCRIPCION: BUSCA POR ID -> PERFIL INGRESO
     * @param type $perfil_ingreso_nombre
     * @return type
     */
    public static function buscarPerfilIngreso($perfil_ingreso_nombre) {
        try {
            $objPerfilIngreso = PerfilIngreso::where('perfil_ingreso_nombre', '=', $perfil_ingreso_nombre)->first();
            return $objPerfilIngreso;
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect()->back();
        }
    }

    /**
     * AUTOR: GCASTILLO
     * DESCRIPCION: BUSCA LISTA DE DATOS ACTIVOS -> PERFIL INGRESO
     * @param 
     * @return type
     */
    public static function buscarPerfilIngresoActivos() {
        try {
            $lstPerfilIngresoActivos = PerfilIngreso::where('estado_id', '=', Estado::$estadoActivo)->get();
            return $lstPerfilIngresoActivos;
        } catch (\Exception $ex) {
            Session::flash('message', $ex->getMessage());
            return redirect()->back();
        }
    }

}
