<?php

namespace App\Entidades;

use Illuminate\Database\Eloquent\Model;

class Cedula extends Model
{
    /*
     * Table: periodo
     * NameCreate : JRendon
     * Create: 14/Sept/2016 
     * NameUpdate : 
     * Update: 
     */
    
    public static $cedulaDevengado = 4;
    public static $cedulaRecaudado = 6;

    protected $table = 'cedula';
    protected $primaryKey = 'cedula_id';
    public $timestamps = false;
    protected $fillable = [
        'cedula_nombre'
    ];
    
     // RELACIONES
   
    
}
