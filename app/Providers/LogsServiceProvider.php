<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request as userip;
use Log;
use Illuminate\Support\Facades\View;


class LogsServiceProvider extends ServiceProvider
{
    
    
    
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \DB::listen(function ($query) {
            $ip = userip::ip();
            Log::info(' |IP: '.$ip.' |USER: '.' |QUERY: '.$query->sql.' |VAL: ',$query->bindings); 
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
